<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry;

/**
 * @deprecated use BoundingArea directly
 */
class GeographicPolygon extends BoundingArea
{
}
