<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - east of 32°E.
 * @internal
 */
class Extent1463
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [32.942251205444, -26.807079315186], [31.957601913024, -26.807079315186], [31.957601913024, -28.933485980689], [32.942251205444, -28.933485980689], [32.942251205444, -26.807079315186],
                ],
            ],
        ];
    }
}
