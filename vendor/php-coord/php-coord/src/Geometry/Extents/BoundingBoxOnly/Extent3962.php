<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 138°E to 144°E.
 * @internal
 */
class Extent3962
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [144, 46.048717159], [138, 46.048717159], [138, 17.637073760392], [144, 17.637073760392], [144, 46.048717159],
                ],
            ],
        ];
    }
}
