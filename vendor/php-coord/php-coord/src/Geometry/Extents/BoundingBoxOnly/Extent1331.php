<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska - St. George Island.
 * @internal
 */
class Extent1331
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-169.3828897044, 56.662312097967], [-169.87275598627, 56.662312097967], [-169.87275598627, 56.491541364307], [-169.3828897044, 56.491541364307], [-169.3828897044, 56.662312097967],
                ],
            ],
        ];
    }
}
