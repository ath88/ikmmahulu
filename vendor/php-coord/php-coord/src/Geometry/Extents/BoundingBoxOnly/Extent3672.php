<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 27ºE to 28ºE.
 * @internal
 */
class Extent3672
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28, 71.162711913802], [27, 71.162711913802], [27, 69.907768249512], [28, 69.907768249512], [28, 71.162711913802],
                ],
            ],
        ];
    }
}
