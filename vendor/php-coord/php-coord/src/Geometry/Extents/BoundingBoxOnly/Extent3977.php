<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - East Malaysia.
 * @internal
 */
class Extent3977
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [119.606977611, 7.6624381230005], [109.315949602, 7.6624381230005], [109.315949602, 0.85277760028873], [119.606977611, 0.85277760028873], [119.606977611, 7.6624381230005],
                ],
            ],
        ];
    }
}
