<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Price.
 * @internal
 */
class Extent4332
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.0420656016, 45.982275683824], [-90.679761412142, 45.982275683824], [-90.679761412142, 45.376802082385], [-90.0420656016, 45.376802082385], [-90.0420656016, 45.982275683824],
                ],
            ],
        ];
    }
}
