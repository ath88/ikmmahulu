<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - CS63 zone K3.
 * @internal
 */
class Extent2777
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [55.2666666667, 51.780754089356], [52.2666666667, 51.780754089356], [52.2666666667, 41.462142969202], [55.2666666667, 41.462142969202], [55.2666666667, 51.780754089356],
                ],
            ],
        ];
    }
}
