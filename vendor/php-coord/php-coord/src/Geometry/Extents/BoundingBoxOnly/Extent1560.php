<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - 126°E to 132°E.
 * @internal
 */
class Extent1560
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [132, -9.1066440526194], [125.999999617, -9.1066440526194], [125.999999617, -37.377825629133], [132, -37.377825629133], [132, -9.1066440526194],
                ],
            ],
        ];
    }
}
