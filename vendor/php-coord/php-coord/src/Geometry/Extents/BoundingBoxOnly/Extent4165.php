<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan excluding eastern main province.
 * @internal
 */
class Extent4165
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140.89850244011, 38.573418694456], [129.30164552051, 38.573418694456], [129.30164552051, 30.9498237462], [140.89850244011, 30.9498237462], [140.89850244011, 38.573418694456],
                ],
            ],
            [
                [
                    [145.86544719357, 45.535358965168], [139.70234680179, 45.535358965168], [139.70234680179, 41.348161211323], [145.86544719357, 41.348161211323], [145.86544719357, 45.535358965168],
                ],
            ],
        ];
    }
}
