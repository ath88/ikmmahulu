<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Bac Giang and Thura Thien-Hue.
 * @internal
 */
class Extent4554
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [108.23222144711, 16.783423237038], [107.016052246, 16.783423237038], [107.016052246, 15.994893074], [108.23222144711, 15.994893074], [108.23222144711, 16.783423237038],
                ],
            ],
            [
                [
                    [107.033676147, 21.626274109], [105.881210327, 21.626274109], [105.881210327, 21.121575259], [107.033676147, 21.121575259], [107.033676147, 21.626274109],
                ],
            ],
        ];
    }
}
