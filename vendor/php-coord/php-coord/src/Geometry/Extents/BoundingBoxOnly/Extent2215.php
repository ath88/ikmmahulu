<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Minnesota - SPCS - S.
 * @internal
 */
class Extent2215
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.217148956284, 45.587166831203], [-96.843087187188, 45.587166831203], [-96.843087187188, 43.498102092378], [-91.217148956284, 43.498102092378], [-91.217148956284, 45.587166831203],
                ],
            ],
        ];
    }
}
