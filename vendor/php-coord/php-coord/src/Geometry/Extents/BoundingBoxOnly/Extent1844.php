<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - 22°E to 24°E.
 * @internal
 */
class Extent1844
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [23.999999198239, -17.525258707687], [21.999998863842, -17.525258707687], [21.999998863842, -18.485160827637], [23.999999198239, -18.485160827637], [23.999999198239, -17.525258707687],
                ],
            ],
        ];
    }
}
