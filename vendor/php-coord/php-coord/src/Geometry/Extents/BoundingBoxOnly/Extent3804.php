<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Lyttleton vcrs.
 * @internal
 */
class Extent3804
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [173.76106010679, -41.609596666084], [168.95615039647, -41.609596666084], [168.95615039647, -44.911485600764], [173.76106010679, -44.911485600764], [173.76106010679, -41.609596666084],
                ],
            ],
        ];
    }
}
