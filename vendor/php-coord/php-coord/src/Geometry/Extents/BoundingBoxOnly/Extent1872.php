<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - Minamitori-shima (Marcus Island) - onshore.
 * @internal
 */
class Extent1872
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [154.04585517893, 24.348381243301], [153.917958627, 24.348381243301], [153.917958627, 24.228845366735], [154.04585517893, 24.228845366735], [154.04585517893, 24.348381243301],
                ],
            ],
        ];
    }
}
