<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Botswana - east of 24°E.
 * @internal
 */
class Extent1617
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.373622894287, -17.782085418701], [24, -17.782085418701], [24, -25.832225799561], [29.373622894287, -25.832225799561], [29.373622894287, -17.782085418701],
                ],
            ],
        ];
    }
}
