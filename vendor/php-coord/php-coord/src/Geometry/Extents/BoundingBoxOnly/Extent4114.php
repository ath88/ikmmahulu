<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 71°10'N to 66°10'N,  103°W to 84°W.
 * @internal
 */
class Extent4114
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84, 71.166666030884], [-103, 71.166666030884], [-103, 66.166666030884], [-84, 66.166666030884], [-84, 71.166666030884],
                ],
            ],
        ];
    }
}
