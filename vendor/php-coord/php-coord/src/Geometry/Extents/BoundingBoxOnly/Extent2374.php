<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - CONUS including EEZ.
 * @internal
 */
class Extent2374
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-65.699722220002, 49.376655578613], [-129.16350562, 49.376655578613], [-129.16350562, 23.81750013802], [-65.699722220002, 23.81750013802], [-65.699722220002, 49.376655578613],
                ],
            ],
        ];
    }
}
