<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 21°E to 23°E.
 * @internal
 */
class Extent3621
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [23.000000436347, -4.1830630089076], [21.000001130581, -4.1830630089076], [21.000001130581, -11.237222671509], [23.000000436347, -11.237222671509], [23.000000436347, -4.1830630089076],
                ],
            ],
        ];
    }
}
