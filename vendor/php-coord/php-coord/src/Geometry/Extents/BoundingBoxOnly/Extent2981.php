<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - offshore.
 * @internal
 */
class Extent2981
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8.4834738656435, 6.3766859462825], [2.6674375020002, 6.3766859462825], [2.6674375020002, 1.9216671180004], [8.4834738656435, 1.9216671180004], [8.4834738656435, 6.3766859462825],
                ],
            ],
        ];
    }
}
