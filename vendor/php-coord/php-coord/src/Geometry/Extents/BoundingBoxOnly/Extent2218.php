<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Missouri - SPCS - C.
 * @internal
 */
class Extent2218
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.417674188182, 40.608266621198], [-93.78630370096, 40.608266621198], [-93.78630370096, 36.489884527562], [-91.417674188182, 36.489884527562], [-91.417674188182, 40.608266621198],
                ],
            ],
        ];
    }
}
