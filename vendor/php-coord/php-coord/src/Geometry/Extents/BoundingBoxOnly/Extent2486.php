<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 35°20'N to 36°N; 137°E to 138°E.
 * @internal
 */
class Extent2486
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [138, 35.999466666667], [137, 35.999466666667], [137, 35.332766666667], [138, 35.332766666667], [138, 35.999466666667],
                ],
            ],
        ];
    }
}
