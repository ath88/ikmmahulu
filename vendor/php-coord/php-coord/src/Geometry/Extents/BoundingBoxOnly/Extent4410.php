<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Meghalaya.
 * @internal
 */
class Extent4410
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [92.804420471192, 26.118740081787], [89.821662902832, 26.118740081787], [89.821662902832, 25.031820297241], [92.804420471192, 25.031820297241], [92.804420471192, 26.118740081787],
                ],
            ],
        ];
    }
}
