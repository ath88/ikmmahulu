<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Pratt.
 * @internal
 */
class Extent4509
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-98.347149, 38.261285], [-99.022627, 38.261285], [-99.022627, 36.997969], [-98.347149, 36.997969], [-98.347149, 38.261285],
                ],
            ],
        ];
    }
}
