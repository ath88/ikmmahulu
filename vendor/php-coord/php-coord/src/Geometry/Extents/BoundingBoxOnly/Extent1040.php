<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bahrain.
 * @internal
 */
class Extent1040
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [51.121801548, 27.166667141], [50.269721547, 27.166667141], [50.269721547, 25.53500014], [51.121801548, 25.53500014], [51.121801548, 27.166667141],
                ],
            ],
        ];
    }
}
