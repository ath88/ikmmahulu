<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Botswana and Zambia - west of 24°E.
 * @internal
 */
class Extent1575
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24, -17.995948791504], [19.996109008789, -17.995948791504], [19.996109008789, -26.875556945801], [24, -26.875556945801], [24, -17.995948791504],
                ],
            ],
            [
                [
                    [24, -13.000490583692], [21.996387481689, -13.000490583692], [21.996387481689, -17.625835418701], [24, -17.625835418701], [24, -13.000490583692],
                ],
            ],
            [
                [
                    [24, -12.469545402641], [23.887010574341, -12.469545402641], [23.887010574341, -12.942570151497], [24, -12.942570151497], [24, -12.469545402641],
                ],
            ],
            [
                [
                    [24, -11.849615058334], [23.98416519165, -11.849615058334], [23.98416519165, -12.225744366967], [24, -12.225744366967], [24, -11.849615058334],
                ],
            ],
            [
                [
                    [24, -11.556194173776], [23.967359542847, -11.556194173776], [23.967359542847, -11.727643525661], [24, -11.727643525661], [24, -11.556194173776],
                ],
            ],
            [
                [
                    [24, -10.869167327881], [23.986206054688, -10.869167327881], [23.986206054688, -11.042744498084], [24, -11.042744498084], [24, -10.869167327881],
                ],
            ],
        ];
    }
}
