<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - mainland northeast.
 * @internal
 */
class Extent2367
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [3.3852024078369, 43.813798904419], [-9.3611106872559, 43.813798904419], [-9.3611106872559, 39.964737993576], [3.3852024078369, 39.964737993576], [3.3852024078369, 43.813798904419],
                ],
            ],
            [
                [
                    [2.0150575893806, 42.495050765739], [1.9573331692904, 42.495050765739], [1.9573331692904, 42.447842027764], [2.0150575893806, 42.447842027764], [2.0150575893806, 42.495050765739],
                ],
            ],
        ];
    }
}
