<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Clark.
 * @internal
 */
class Extent4339
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.314995631212, 45.033788006414], [-90.92323663881, 45.033788006414], [-90.92323663881, 44.421997192658], [-90.314995631212, 44.421997192658], [-90.314995631212, 45.033788006414],
                ],
            ],
        ];
    }
}
