<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Mizoram.
 * @internal
 */
class Extent4411
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [93.444702148437, 24.520849227905], [92.259399414063, 24.520849227905], [92.259399414063, 21.946229934692], [93.444702148437, 21.946229934692], [93.444702148437, 24.520849227905],
                ],
            ],
        ];
    }
}
