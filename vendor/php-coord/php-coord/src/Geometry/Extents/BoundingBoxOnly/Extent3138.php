<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Society Islands - Tahaa.
 * @internal
 */
class Extent3138
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-151.36075988347, -16.508612552984], [-151.62250383176, -16.508612552984], [-151.62250383176, -16.712009144804], [-151.36075988347, -16.712009144804], [-151.36075988347, -16.508612552984],
                ],
            ],
        ];
    }
}
