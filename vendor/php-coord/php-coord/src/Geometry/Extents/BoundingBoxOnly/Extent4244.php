<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Burlington.
 * @internal
 */
class Extent4244
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.783739, 41.598379], [-91.718847, 41.598379], [-91.718847, 40.371946618264], [-90.783739, 40.371946618264], [-90.783739, 41.598379],
                ],
            ],
        ];
    }
}
