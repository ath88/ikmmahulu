<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Missouri - SPCS - E.
 * @internal
 */
class Extent2219
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.105033865365, 40.609784367494], [-91.960605785981, 40.609784367494], [-91.960605785981, 35.989656426585], [-89.105033865365, 35.989656426585], [-89.105033865365, 40.609784367494],
                ],
            ],
        ];
    }
}
