<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores and Madeira.
 * @internal
 */
class Extent3670
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-12.483619511985, 36.457610150001], [-21.21825952, 36.457610150001], [-21.21825952, 29.246080143], [-12.483619511985, 29.246080143], [-12.483619511985, 36.457610150001],
                ],
            ],
            [
                [
                    [-20.855319519, 43.067630156], [-35.579299533, 43.067630156], [-35.579299533, 33.520380147], [-20.855319519, 33.520380147], [-20.855319519, 43.067630156],
                ],
            ],
        ];
    }
}
