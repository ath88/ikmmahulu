<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Taylor.
 * @internal
 */
class Extent4356
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.0420656016, 45.382597240729], [-90.925946658618, 45.382597240729], [-90.925946658618, 45.030941836232], [-90.0420656016, 45.030941836232], [-90.0420656016, 45.382597240729],
                ],
            ],
        ];
    }
}
