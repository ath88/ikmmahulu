<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Bumthang district.
 * @internal
 */
class Extent3734
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.015302200471, 28.080551147461], [90.468908234613, 28.080551147461], [90.468908234613, 27.335953283186], [91.015302200471, 27.335953283186], [91.015302200471, 28.080551147461],
                ],
            ],
        ];
    }
}
