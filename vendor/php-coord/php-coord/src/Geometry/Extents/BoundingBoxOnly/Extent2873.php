<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores C - Faial onshore.
 * @internal
 */
class Extent2873
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-28.549195207295, 38.690415788769], [-28.896113802075, 38.690415788769], [-28.896113802075, 38.460276197315], [-28.549195207295, 38.460276197315], [-28.549195207295, 38.690415788769],
                ],
            ],
        ];
    }
}
