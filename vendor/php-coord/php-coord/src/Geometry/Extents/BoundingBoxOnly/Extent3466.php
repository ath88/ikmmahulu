<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - Ordos - 108°E to 108.5°E and 37.75°N to 38.25°N.
 * @internal
 */
class Extent3466
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [110.00709632, 39], [107, 39], [107, 35], [110.00709632, 35], [110.00709632, 39],
                ],
            ],
        ];
    }
}
