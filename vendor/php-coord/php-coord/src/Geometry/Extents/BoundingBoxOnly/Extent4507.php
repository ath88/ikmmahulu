<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Dodge City.
 * @internal
 */
class Extent4507
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-99.541114, 38.69818], [-100.248657, 38.69818], [-100.248657, 36.999529], [-99.541114, 36.999529], [-99.541114, 38.69818],
                ],
            ],
        ];
    }
}
