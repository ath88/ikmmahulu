<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Connecticut.
 * @internal
 */
class Extent1377
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.788248862195, 42.047428050073], [-73.725237656697, 42.047428050073], [-73.725237656697, 40.98738452844], [-71.788248862195, 40.98738452844], [-71.788248862195, 42.047428050073],
                ],
            ],
        ];
    }
}
