<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Pennsylvania - SPCS - S.
 * @internal
 */
class Extent2246
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.725480466185, 41.174637234517], [-80.526045190189, 41.174637234517], [-80.526045190189, 39.719313603532], [-74.725480466185, 39.719313603532], [-74.725480466185, 41.174637234517],
                ],
            ],
        ];
    }
}
