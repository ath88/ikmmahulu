<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Tucano basin central.
 * @internal
 */
class Extent2309
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-37.998065948486, -9.7999992370605], [-39.132966995239, -9.7999992370605], [-39.132966995239, -10.60000038147], [-37.998065948486, -10.60000038147], [-37.998065948486, -9.7999992370605],
                ],
            ],
        ];
    }
}
