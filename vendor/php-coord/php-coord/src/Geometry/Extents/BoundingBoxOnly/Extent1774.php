<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Russia - 84°E to 90°E onshore.
 * @internal
 */
class Extent1774
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [90, 75.629473442704], [84, 75.629473442704], [84, 49.077493667603], [90, 49.077493667603], [90, 75.629473442704],
                ],
            ],
            [
                [
                    [84.593809127808, 74.093130111694], [84, 74.093130111694], [84, 73.903791427612], [84.593809127808, 73.903791427612], [84.593809127808, 74.093130111694],
                ],
            ],
            [
                [
                    [90, 81.262307385749], [89.584478378296, 81.262307385749], [89.584478378296, 81.044608253724], [90, 81.044608253724], [90, 81.262307385749],
                ],
            ],
            [
                [
                    [89.902551651001, 77.364080429077], [88.41241645813, 77.364080429077], [88.41241645813, 76.95534324646], [89.902551651001, 76.95534324646], [89.902551651001, 77.364080429077],
                ],
            ],
        ];
    }
}
