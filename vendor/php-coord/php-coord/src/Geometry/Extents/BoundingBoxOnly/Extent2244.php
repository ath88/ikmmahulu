<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - SPCS - S.
 * @internal
 */
class Extent2244
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-116.9022541326, 44.558045042677], [-124.59242248535, 44.558045042677], [-124.59242248535, 41.987672177954], [-116.9022541326, 41.987672177954], [-116.9022541326, 44.558045042677],
                ],
            ],
        ];
    }
}
