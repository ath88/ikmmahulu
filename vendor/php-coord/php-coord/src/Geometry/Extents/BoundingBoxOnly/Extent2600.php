<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - Hassi Bir Reikaz.
 * @internal
 */
class Extent2600
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8, 32.416667], [7.166667, 32.416667], [7.166667, 31.75], [8, 31.75], [8, 32.416667],
                ],
            ],
        ];
    }
}
