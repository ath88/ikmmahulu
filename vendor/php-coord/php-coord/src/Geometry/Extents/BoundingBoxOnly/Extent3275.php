<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Malta - onshore.
 * @internal
 */
class Extent3275
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.620000291943, 36.041940141796], [14.279095243336, 36.041940141796], [14.279095243336, 35.749994825245], [14.620000291943, 35.749994825245], [14.620000291943, 36.041940141796],
                ],
            ],
        ];
    }
}
