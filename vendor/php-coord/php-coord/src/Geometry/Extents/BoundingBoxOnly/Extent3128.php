<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Ua Pou.
 * @internal
 */
class Extent3128
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-139.9599783772, -9.2740654622436], [-140.200441481, -9.2740654622436], [-140.200441481, -9.5604366760204], [-139.9599783772, -9.5604366760204], [-139.9599783772, -9.2740654622436],
                ],
            ],
        ];
    }
}
