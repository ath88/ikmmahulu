<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Qatar - onshore.
 * @internal
 */
class Extent1346
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [51.670650462043, 26.199457273873], [50.697021308257, 26.199457273873], [50.697021308257, 24.556041717529], [51.670650462043, 24.556041717529], [51.670650462043, 26.199457273873],
                ],
            ],
        ];
    }
}
