<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/North America - Great Lakes basin and St Lawrence Seaway.
 * @internal
 */
class Extent3468
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-54.755732605124, 52.214063284747], [-93.162978293834, 52.214063284747], [-93.162978293834, 40.999080421049], [-54.755732605124, 40.999080421049], [-54.755732605124, 52.214063284747],
                ],
            ],
        ];
    }
}
