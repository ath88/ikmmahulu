<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Vernon.
 * @internal
 */
class Extent4377
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.311047226895, 43.731496518649], [-91.273263499354, 43.731496518649], [-91.273263499354, 43.422344609337], [-90.311047226895, 43.422344609337], [-90.311047226895, 43.731496518649],
                ],
            ],
        ];
    }
}
