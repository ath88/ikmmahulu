<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 132°E to 138°E.
 * @internal
 */
class Extent3961
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [138, 43.545858477699], [132, 43.545858477699], [132, 17.091340132], [138, 17.091340132], [138, 43.545858477699],
                ],
            ],
        ];
    }
}
