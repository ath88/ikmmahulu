<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - SPCS27 - N.
 * @internal
 */
class Extent2211
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.04794311523, 49.000604629517], [-116.06511653997, 49.000604629517], [-116.06511653997, 47.418079376221], [-104.04794311523, 47.418079376221], [-104.04794311523, 49.000604629517],
                ],
            ],
        ];
    }
}
