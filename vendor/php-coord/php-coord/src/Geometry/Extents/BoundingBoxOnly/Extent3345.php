<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Ljubljana east.
 * @internal
 */
class Extent3345
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.008955565602, 46.315724330769], [14.457444349512, 46.315724330769], [14.457444349512, 45.853627722481], [15.008955565602, 45.853627722481], [15.008955565602, 46.315724330769],
                ],
            ],
        ];
    }
}
