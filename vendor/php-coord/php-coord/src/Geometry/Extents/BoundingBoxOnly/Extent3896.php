<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - equatorial margin.
 * @internal
 */
class Extent3896
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-32.439113673359, 7.0353215455681], [-51.638261795044, 7.0353215455681], [-51.638261795044, -5.7352599731574], [-32.439113673359, -5.7352599731574], [-32.439113673359, 7.0353215455681],
                ],
            ],
        ];
    }
}
