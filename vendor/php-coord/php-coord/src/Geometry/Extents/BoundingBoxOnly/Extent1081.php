<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Djibouti.
 * @internal
 */
class Extent1081
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [44.146206541, 12.713750128001], [41.759857177735, 12.713750128001], [41.759857177735, 10.942220687866], [44.146206541, 10.942220687866], [44.146206541, 12.713750128001],
                ],
            ],
        ];
    }
}
