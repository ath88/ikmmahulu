<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New York - SPCS - W.
 * @internal
 */
class Extent2236
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-77.364960603343, 43.639436721802], [-79.763234965406, 43.639436721802], [-79.763234965406, 41.997333182645], [-77.364960603343, 41.997333182645], [-77.364960603343, 43.639436721802],
                ],
            ],
        ];
    }
}
