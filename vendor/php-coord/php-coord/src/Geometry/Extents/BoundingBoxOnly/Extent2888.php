<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Sicily Strait west of 13°E.
 * @internal
 */
class Extent2888
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.000000318569, 38.440032559143], [10.685916665692, 38.440032559143], [10.685916665692, 35.288961149001], [13.000000318569, 35.288961149001], [13.000000318569, 38.440032559143],
                ],
            ],
        ];
    }
}
