<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Halfway.
 * @internal
 */
class Extent4476
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-116.63332406094, 45.277809515328], [-117.60437155336, 45.277809515328], [-117.60437155336, 44.618531017571], [-116.63332406094, 44.618531017571], [-116.63332406094, 45.277809515328],
                ],
            ],
        ];
    }
}
