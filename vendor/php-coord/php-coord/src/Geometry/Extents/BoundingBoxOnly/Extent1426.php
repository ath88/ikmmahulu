<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Quebec - 75°W to 72°W.
 * @internal
 */
class Extent1426
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, 62.529189652736], [-75, 62.529189652736], [-75, 44.992218017578], [-72, 44.992218017578], [-72, 62.529189652736],
                ],
            ],
        ];
    }
}
