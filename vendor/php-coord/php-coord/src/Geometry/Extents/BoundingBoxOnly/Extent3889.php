<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - Fehmarnbelt outer.
 * @internal
 */
class Extent3889
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.002880988139, 54.829750533846], [10.669940630924, 54.829750533846], [10.669940630924, 54.330247496082], [12.002880988139, 54.330247496082], [12.002880988139, 54.829750533846],
                ],
            ],
        ];
    }
}
