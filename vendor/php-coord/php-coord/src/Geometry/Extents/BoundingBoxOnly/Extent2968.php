<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritania - central coast.
 * @internal
 */
class Extent2968
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-15.595863742529, 19.406043476368], [-16.564453348361, 19.406043476368], [-16.564453348361, 16.816507468014], [-15.595863742529, 16.816507468014], [-15.595863742529, 19.406043476368],
                ],
            ],
        ];
    }
}
