<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Espirito Santo and Mucuri.
 * @internal
 */
class Extent2964
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-35.18865776062, -17.592367172241], [-40.368339538574, -17.592367172241], [-40.368339538574, -22.03946685791], [-35.18865776062, -22.03946685791], [-35.18865776062, -17.592367172241],
                ],
            ],
        ];
    }
}
