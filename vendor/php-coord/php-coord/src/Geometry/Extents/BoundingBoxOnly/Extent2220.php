<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Missouri - SPCS - W.
 * @internal
 */
class Extent2220
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.488423879699, 40.58904799521], [-95.76747958524, 40.58904799521], [-95.76747958524, 36.4894141614], [-93.488423879699, 36.4894141614], [-93.488423879699, 40.58904799521],
                ],
            ],
        ];
    }
}
