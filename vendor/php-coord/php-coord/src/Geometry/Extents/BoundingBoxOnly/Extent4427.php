<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Bulgaria - 24°E to 30°E.
 * @internal
 */
class Extent4427
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, 44.147338867188], [24, 44.147338867188], [24, 41.243049621582], [30, 41.243049621582], [30, 44.147338867188],
                ],
            ],
        ];
    }
}
