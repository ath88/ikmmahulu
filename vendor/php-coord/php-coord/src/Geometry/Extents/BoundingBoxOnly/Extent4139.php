<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - Cucuta city.
 * @internal
 */
class Extent4139
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72.460193018, 7.9600009918214], [-72.56, 7.9600009918214], [-72.56, 7.82], [-72.460193018, 7.82], [-72.460193018, 7.9600009918214],
                ],
            ],
        ];
    }
}
