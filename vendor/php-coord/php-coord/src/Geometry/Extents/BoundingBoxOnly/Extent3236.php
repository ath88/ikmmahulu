<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Cyprus - onshore.
 * @internal
 */
class Extent3236
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [34.646670220759, 35.738311016702], [32.209691079084, 35.738311016702], [32.209691079084, 34.590499404593], [34.646670220759, 34.590499404593], [34.646670220759, 35.738311016702],
                ],
            ],
        ];
    }
}
