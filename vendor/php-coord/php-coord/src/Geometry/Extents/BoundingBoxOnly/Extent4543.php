<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Serbia - central Serbia and Vojvodinja.
 * @internal
 */
class Extent4543
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [23.004997253418, 46.18111038208], [18.81702041626, 46.18111038208], [18.81702041626, 42.232494354248], [23.004997253418, 42.232494354248], [23.004997253418, 46.18111038208],
                ],
            ],
        ];
    }
}
