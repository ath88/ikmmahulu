<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - SPCS27 - C.
 * @internal
 */
class Extent2210
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.04544067383, 48.252716064453], [-116.05523781269, 48.252716064453], [-116.05523781269, 46.176582336426], [-104.04544067383, 46.176582336426], [-104.04544067383, 48.252716064453],
                ],
            ],
        ];
    }
}
