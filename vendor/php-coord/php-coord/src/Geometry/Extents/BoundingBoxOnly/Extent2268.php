<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - SPCS - S.
 * @internal
 */
class Extent2268
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.958243150837, 44.327851040331], [-91.42535505314, 44.327851040331], [-91.42535505314, 42.489152849524], [-86.958243150837, 42.489152849524], [-86.958243150837, 44.327851040331],
                ],
            ],
        ];
    }
}
