<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Denio-Burns.
 * @internal
 */
class Extent4475
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-117.67198945162, 43.530722939148], [-119.40610796859, 43.530722939148], [-119.40610796859, 41.883141876983], [-117.67198945162, 41.883141876983], [-117.67198945162, 43.530722939148],
                ],
            ],
        ];
    }
}
