<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australasia - Australia and PNG - 138°E to 144°E.
 * @internal
 */
class Extent1567
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [144, -2.5398248629636], [138, -2.5398248629636], [138, -46.622554172067], [144, -46.622554172067], [144, -2.5398248629636],
                ],
            ],
        ];
    }
}
