<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Comodoro Rivadavia.
 * @internal
 */
class Extent1265
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.10000038147, -45.199998855591], [-69.5, -45.199998855591], [-69.5, -46.699998855591], [-67.10000038147, -46.699998855591], [-67.10000038147, -45.199998855591],
                ],
            ],
        ];
    }
}
