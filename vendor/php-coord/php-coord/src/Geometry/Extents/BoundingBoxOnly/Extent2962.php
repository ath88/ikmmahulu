<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Santos.
 * @internal
 */
class Extent2962
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-40.201427459717, -22.668334960938], [-48.793334960938, -22.668334960938], [-48.793334960938, -28.401002883911], [-40.201427459717, -28.401002883911], [-40.201427459717, -22.668334960938],
                ],
            ],
        ];
    }
}
