<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovakia.
 * @internal
 */
class Extent1211
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.558052062988, 49.600830078125], [16.844720840454, 49.600830078125], [16.844720840454, 47.737499237061], [22.558052062988, 47.737499237061], [22.558052062988, 49.600830078125],
                ],
            ],
        ];
    }
}
