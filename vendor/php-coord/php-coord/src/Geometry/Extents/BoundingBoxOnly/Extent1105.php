<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Gibraltar.
 * @internal
 */
class Extent1105
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-4.8923745049998, 36.159580000374], [-5.4105411295339, 36.159580000374], [-5.4105411295339, 36.009872149], [-4.8923745049998, 36.009872149], [-4.8923745049998, 36.159580000374],
                ],
            ],
        ];
    }
}
