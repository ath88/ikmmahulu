<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Channel Islands - Jersey, Les Ecrehos and Les Minquiers.
 * @internal
 */
class Extent2988
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-1.8167875019996, 49.436187162], [-2.711401503, 49.436187162], [-2.711401503, 48.778405161], [-1.8167875019996, 48.778405161], [-1.8167875019996, 49.436187162],
                ],
            ],
        ];
    }
}
