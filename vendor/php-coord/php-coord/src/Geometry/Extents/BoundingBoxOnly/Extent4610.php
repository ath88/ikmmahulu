<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Buenos Aires city.
 * @internal
 */
class Extent4610
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-58.29240989685, -34.506992229796], [-58.531465195974, -34.506992229796], [-58.531465195974, -34.705314975913], [-58.29240989685, -34.705314975913], [-58.29240989685, -34.506992229796],
                ],
            ],
        ];
    }
}
