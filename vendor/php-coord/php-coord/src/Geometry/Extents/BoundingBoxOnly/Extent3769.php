<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Taranaki vcrs.
 * @internal
 */
class Extent3769
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [174.94569339937, -38.416760200891], [173.68738746643, -38.416760200891], [173.68738746643, -39.916465660851], [174.94569339937, -39.916465660851], [174.94569339937, -38.416760200891],
                ],
            ],
        ];
    }
}
