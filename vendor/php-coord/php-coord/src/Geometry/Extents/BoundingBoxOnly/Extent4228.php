<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - California - San Francisco.
 * @internal
 */
class Extent4228
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.20829458964, 38.864318225651], [-123.55519483605, 38.864318225651], [-123.55519483605, 36.850990162924], [-121.20829458964, 36.850990162924], [-121.20829458964, 38.864318225651],
                ],
            ],
        ];
    }
}
