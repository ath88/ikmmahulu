<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - offshore North Sea.
 * @internal
 */
class Extent2879
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8.8764966165473, 55.919278225498], [3.3499994686502, 55.919278225498], [3.3499994686502, 53.600511029363], [8.8764966165473, 53.600511029363], [8.8764966165473, 55.919278225498],
                ],
            ],
        ];
    }
}
