<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - 42.5°S to 50.3°S.
 * @internal
 */
class Extent4563
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-65.472450256348, -42.498651188945], [-73.582298278809, -42.498651188945], [-73.582298278809, -50.333334334117], [-65.472450256348, -50.333334334117], [-65.472450256348, -42.498651188945],
                ],
            ],
        ];
    }
}
