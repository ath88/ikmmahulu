<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Porto Santo island onshore.
 * @internal
 */
class Extent3680
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-16.231814329957, 33.14011162332], [-16.431488538844, 33.14011162332], [-16.431488538844, 32.969706828737], [-16.231814329957, 32.969706828737], [-16.231814329957, 33.14011162332],
                ],
            ],
        ];
    }
}
