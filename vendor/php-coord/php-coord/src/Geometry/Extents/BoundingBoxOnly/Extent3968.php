<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Saudi Arabia - onshore Gulf coast.
 * @internal
 */
class Extent3968
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [50.80066446034, 28.567302949386], [47.959377554837, 28.567302949386], [47.959377554837, 24.631513573614], [50.80066446034, 24.631513573614], [50.80066446034, 28.567302949386],
                ],
            ],
        ];
    }
}
