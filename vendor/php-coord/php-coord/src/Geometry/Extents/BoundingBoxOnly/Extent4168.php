<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - onshore - Hokkaido.
 * @internal
 */
class Extent4168
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [145.86544719357, 45.535358965168], [139.70234680179, 45.535358965168], [139.70234680179, 41.348161211323], [145.86544719357, 41.348161211323], [145.86544719357, 45.535358965168],
                ],
            ],
        ];
    }
}
