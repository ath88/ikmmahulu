<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Kalbarri.
 * @internal
 */
class Extent4444
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [114.75, -27.166666666667], [113.9, -27.166666666667], [113.9, -28.5], [114.75, -28.5], [114.75, -27.166666666667],
                ],
            ],
        ];
    }
}
