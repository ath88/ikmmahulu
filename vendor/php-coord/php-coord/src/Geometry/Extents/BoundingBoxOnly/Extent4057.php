<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 84°30'N to 79°30'N,  146°E to 174°W.
 * @internal
 */
class Extent4057
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [186, 84.500001907349], [146.00000190735, 84.500001907349], [146.00000190735, 79.500001907349], [186, 79.500001907349], [186, 84.500001907349],
                ],
            ],
        ];
    }
}
