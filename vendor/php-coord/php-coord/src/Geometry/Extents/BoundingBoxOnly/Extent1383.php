<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana.
 * @internal
 */
class Extent1383
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.784579000003, 41.760688386132], [-88.058499371214, 41.760688386132], [-88.058499371214, 37.771742000004], [-84.784579000003, 37.771742000004], [-84.784579000003, 41.760688386132],
                ],
            ],
        ];
    }
}
