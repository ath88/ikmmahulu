<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas - SPCS - N.
 * @internal
 */
class Extent2253
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-99.9947434206, 36.493912716666], [-103.02963820413, 36.493912716666], [-103.02963820413, 34.307820476259], [-99.9947434206, 34.307820476259], [-99.9947434206, 36.493912716666],
                ],
            ],
        ];
    }
}
