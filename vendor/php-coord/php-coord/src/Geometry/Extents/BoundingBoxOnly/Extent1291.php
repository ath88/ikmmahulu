<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - Caspian Sea.
 * @internal
 */
class Extent1291
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.920528153508, 46.962942861514], [46.953628430943, 46.962942861514], [46.953628430943, 37.350638476674], [53.920528153508, 37.350638476674], [53.920528153508, 46.962942861514],
                ],
            ],
        ];
    }
}
