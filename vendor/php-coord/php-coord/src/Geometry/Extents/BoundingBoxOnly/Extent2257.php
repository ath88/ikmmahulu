<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Utah - SPCS - C.
 * @internal
 */
class Extent2257
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.04615572619, 41.07831696326], [-114.04509020615, 41.07831696326], [-114.04509020615, 38.494650913244], [-109.04615572619, 38.494650913244], [-109.04615572619, 41.07831696326],
                ],
            ],
        ];
    }
}
