<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - CONUS west of 107°W - onshore.
 * @internal
 */
class Extent2950
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-107, 49.01227760315], [-124.78931045532, 49.01227760315], [-124.78931045532, 31.332603454538], [-107, 31.332603454538], [-107, 49.01227760315],
                ],
            ],
            [
                [
                    [-122.95846176147, 49.048540115357], [-123.1698513031, 49.048540115357], [-123.1698513031, 48.923215866089], [-122.95846176147, 48.923215866089], [-122.95846176147, 49.048540115357],
                ],
            ],
            [
                [
                    [-119.45556259155, 34.133363723755], [-120.2860660553, 34.133363723755], [-120.2860660553, 33.856344223023], [-119.45556259155, 33.856344223023], [-119.45556259155, 34.133363723755],
                ],
            ],
            [
                [
                    [-118.23365020752, 33.540117263794], [-118.64950942993, 33.540117263794], [-118.64950942993, 33.260522842407], [-118.23365020752, 33.260522842407], [-118.23365020752, 33.540117263794],
                ],
            ],
            [
                [
                    [-118.31002616882, 33.093870162964], [-118.65407371521, 33.093870162964], [-118.65407371521, 32.767755508423], [-118.31002616882, 32.767755508423], [-118.31002616882, 33.093870162964],
                ],
            ],
        ];
    }
}
