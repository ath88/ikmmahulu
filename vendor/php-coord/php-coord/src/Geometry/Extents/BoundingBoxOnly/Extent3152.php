<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 15°E to 17°E.
 * @internal
 */
class Extent3152
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [17.000045487069, -1.1361227254051], [14.999986263211, -1.1361227254051], [14.999986263211, -7.3006183418448], [17.000045487069, -7.3006183418448], [17.000045487069, -1.1361227254051],
                ],
            ],
        ];
    }
}
