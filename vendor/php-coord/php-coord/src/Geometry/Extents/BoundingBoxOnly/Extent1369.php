<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/France - Alsace.
 * @internal
 */
class Extent1369
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8.2260780334473, 49.066417], [6.847663, 49.066417], [6.847663, 47.42166519165], [8.2260780334473, 47.42166519165], [8.2260780334473, 49.066417],
                ],
            ],
        ];
    }
}
