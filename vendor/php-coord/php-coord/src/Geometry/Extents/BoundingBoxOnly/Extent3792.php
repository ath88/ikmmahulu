<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Gawler mc.
 * @internal
 */
class Extent3792
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [172.25067755629, -43.13370042], [170.68361868, -43.13370042], [170.68361868, -44.240411858008], [172.25067755629, -44.240411858008], [172.25067755629, -43.13370042],
                ],
            ],
        ];
    }
}
