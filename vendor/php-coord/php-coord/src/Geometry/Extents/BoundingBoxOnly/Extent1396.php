<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Nebraska.
 * @internal
 */
class Extent1396
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.30829, 43.003087792437], [-104.05604392175, 43.003087792437], [-104.05604392175, 39.999932], [-95.30829, 39.999932], [-95.30829, 43.003087792437],
                ],
            ],
        ];
    }
}
