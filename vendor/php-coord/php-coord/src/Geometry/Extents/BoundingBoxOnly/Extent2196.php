<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - SPCS - E.
 * @internal
 */
class Extent2196
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.784579000003, 41.760592000006], [-86.581738999997, 41.760592000006], [-86.581738999997, 37.958018000004], [-84.784579000003, 37.958018000004], [-84.784579000003, 41.760592000006],
                ],
            ],
        ];
    }
}
