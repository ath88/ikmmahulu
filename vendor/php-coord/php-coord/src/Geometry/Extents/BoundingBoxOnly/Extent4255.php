<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - La Porte, Pulaski, Starke.
 * @internal
 */
class Extent4255
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.466364000003, 41.760309402289], [-86.932981484278, 41.760309402289], [-86.932981484278, 40.909791999982], [-86.466364000003, 40.909791999982], [-86.466364000003, 41.760309402289],
                ],
            ],
        ];
    }
}
