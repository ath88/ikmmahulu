<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Cuba - onshore south of 21°30'N.
 * @internal
 */
class Extent1488
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.079269431982, 21.5], [-78.684332000632, 21.5], [-78.684332000632, 19.772208673813], [-74.079269431982, 19.772208673813], [-74.079269431982, 21.5],
                ],
            ],
        ];
    }
}
