<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - Stewart Island.
 * @internal
 */
class Extent3338
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [168.33160209656, -46.634431838989], [167.29237210887, -46.634431838989], [167.29237210887, -47.327270507812], [168.33160209656, -47.327270507812], [168.33160209656, -46.634431838989],
                ],
            ],
        ];
    }
}
