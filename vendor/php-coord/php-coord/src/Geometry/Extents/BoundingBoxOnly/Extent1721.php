<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Michigan - SPCS - old central.
 * @internal
 */
class Extent1721
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.604683098483, 46.101911440511], [-87.603395313407, 46.101911440511], [-87.603395313407, 41.759048999968], [-84.604683098483, 41.759048999968], [-84.604683098483, 46.101911440511],
                ],
            ],
        ];
    }
}
