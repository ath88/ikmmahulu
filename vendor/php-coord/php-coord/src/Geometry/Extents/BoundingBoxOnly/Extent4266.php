<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Owen.
 * @internal
 */
class Extent4266
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.630717000002, 39.473344999979], [-87.054767999997, 39.473344999979], [-87.054767999997, 39.165746], [-86.630717000002, 39.165746], [-86.630717000002, 39.473344999979],
                ],
            ],
        ];
    }
}
