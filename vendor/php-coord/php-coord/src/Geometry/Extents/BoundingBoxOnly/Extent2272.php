<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wyoming - SPCS - WC.
 * @internal
 */
class Extent2272
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-107.50038862591, 45.002792925692], [-111.05265045459, 45.002792925692], [-111.05265045459, 40.997599517187], [-107.50038862591, 40.997599517187], [-107.50038862591, 45.002792925692],
                ],
            ],
        ];
    }
}
