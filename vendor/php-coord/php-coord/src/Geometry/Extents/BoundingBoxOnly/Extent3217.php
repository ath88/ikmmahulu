<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bangladesh - onshore.
 * @internal
 */
class Extent3217
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.622152239155, 22.672392204355], [91.348913527782, 22.672392204355], [91.348913527782, 22.295853673039], [91.622152239155, 22.295853673039], [91.622152239155, 22.672392204355],
                ],
            ],
            [
                [
                    [92.669342041016, 26.634122848511], [88.010566711426, 26.634122848511], [88.010566711426, 20.523005946139], [92.669342041016, 20.523005946139], [92.669342041016, 26.634122848511],
                ],
            ],
        ];
    }
}
