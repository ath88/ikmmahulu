<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Tuhirangi mc.
 * @internal
 */
class Extent3778
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [176.32626156, -38.87004699], [174.88647198, -38.87004699], [174.88647198, -39.54647052], [176.32626156, -39.54647052], [176.32626156, -38.87004699],
                ],
            ],
        ];
    }
}
