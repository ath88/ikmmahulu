<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Mendoza and Neuquen 70.5°W to 67.5°W.
 * @internal
 */
class Extent1483
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.5, -31.914892582143], [-70.500001907349, -31.914892582143], [-70.500001907349, -43.405476435138], [-67.5, -43.405476435138], [-67.5, -31.914892582143],
                ],
            ],
        ];
    }
}
