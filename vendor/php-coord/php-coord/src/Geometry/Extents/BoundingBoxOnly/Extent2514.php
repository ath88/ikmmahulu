<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 33°20'N to 34°N; 135°E to 136°E.
 * @internal
 */
class Extent2514
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [136, 33.999366666667], [135.0019964742, 33.999366666667], [135.0019964742, 33.405519516154], [136, 33.405519516154], [136, 33.999366666667],
                ],
            ],
        ];
    }
}
