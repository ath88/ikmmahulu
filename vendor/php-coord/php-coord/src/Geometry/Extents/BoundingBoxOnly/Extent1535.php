<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Nova Scotia - west of 63°W.
 * @internal
 */
class Extent1535
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-63, 46.012504204254], [-66.273868827213, 46.012504204254], [-66.273868827213, 43.415647861102], [-63, 43.415647861102], [-63, 46.012504204254],
                ],
            ],
        ];
    }
}
