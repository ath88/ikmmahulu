<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Caribbean - Puerto Rico and US Virgin Islands.
 * @internal
 */
class Extent2251
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-63.88888856, 21.854251136094], [-68.481888563935, 21.854251136094], [-68.481888563935, 14.928194130078], [-63.88888856, 14.928194130078], [-63.88888856, 21.854251136094],
                ],
            ],
        ];
    }
}
