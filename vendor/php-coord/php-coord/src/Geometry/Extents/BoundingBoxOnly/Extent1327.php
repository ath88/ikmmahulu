<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/France - Corsica onshore.
 * @internal
 */
class Extent1327
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.6293959027848, 43.060430409677], [8.503435748653, 43.060430409677], [8.503435748653, 41.314903831524], [9.6293959027848, 41.314903831524], [9.6293959027848, 43.060430409677],
                ],
            ],
        ];
    }
}
