<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - Cabinda.
 * @internal
 */
class Extent1318
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.092638015747, -4.3889913558959], [10.53152041439, -4.3889913558959], [10.53152041439, -6.0332721709077], [13.092638015747, -6.0332721709077], [13.092638015747, -4.3889913558959],
                ],
            ],
        ];
    }
}
