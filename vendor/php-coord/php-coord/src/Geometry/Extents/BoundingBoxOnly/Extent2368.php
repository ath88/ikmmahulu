<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - mainland southwest.
 * @internal
 */
class Extent2368
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [0.27876472473145, 41.970615705848], [-7.5325050354004, 41.970615705848], [-7.5325050354004, 35.956438064575], [0.27876472473145, 35.956438064575], [0.27876472473145, 41.970615705848],
                ],
            ],
        ];
    }
}
