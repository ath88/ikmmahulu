<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Guinea - onshore.
 * @internal
 */
class Extent3257
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-7.6533703804016, 12.677499771118], [-15.12748464842, 12.677499771118], [-15.12748464842, 7.1939268112183], [-7.6533703804016, 7.1939268112183], [-7.6533703804016, 12.677499771118],
                ],
            ],
        ];
    }
}
