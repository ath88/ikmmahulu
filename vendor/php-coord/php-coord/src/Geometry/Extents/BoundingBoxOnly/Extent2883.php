<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Adriatic - South Ancona / North Gargano.
 * @internal
 */
class Extent2883
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.13387965513, 44.035596128508], [13.611771449224, 44.035596128508], [13.611771449224, 41.954054180452], [16.13387965513, 41.954054180452], [16.13387965513, 44.035596128508],
                ],
            ],
        ];
    }
}
