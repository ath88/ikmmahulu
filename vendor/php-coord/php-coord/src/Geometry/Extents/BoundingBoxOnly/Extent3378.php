<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - Pahang.
 * @internal
 */
class Extent3378
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [103.66010523048, 4.7763700485229], [101.331199646, 4.7763700485229], [101.331199646, 2.45831990242], [103.66010523048, 2.45831990242], [103.66010523048, 4.7763700485229],
                ],
            ],
        ];
    }
}
