<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mayotte.
 * @internal
 */
class Extent1159
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [46.692948543, -11.336828895], [43.680286541, -11.336828895], [43.680286541, -14.485496898], [46.692948543, -14.485496898], [46.692948543, -11.336828895],
                ],
            ],
        ];
    }
}
