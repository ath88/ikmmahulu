<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Decatur and Rush.
 * @internal
 */
class Extent4299
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.296132000003, 39.78753099999], [-85.687448000001, 39.78753099999], [-85.687448000001, 39.130631000003], [-85.296132000003, 39.130631000003], [-85.296132000003, 39.78753099999],
                ],
            ],
        ];
    }
}
