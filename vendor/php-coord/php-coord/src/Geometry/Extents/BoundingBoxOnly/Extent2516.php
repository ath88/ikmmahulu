<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 32°40'N to 33°20'N; 129°E to 130°E.
 * @internal
 */
class Extent2516
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [130, 33.332666666667], [129.30164552051, 33.332666666667], [129.30164552051, 32.511307155685], [130, 32.511307155685], [130, 33.332666666667],
                ],
            ],
        ];
    }
}
