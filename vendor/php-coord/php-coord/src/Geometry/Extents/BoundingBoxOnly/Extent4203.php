<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Cottage Grove-Canyonville.
 * @internal
 */
class Extent4203
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-122.40066796208, 43.874139801489], [-123.9553982581, 43.874139801489], [-123.9553982581, 42.82966264932], [-122.40066796208, 42.82966264932], [-122.40066796208, 43.874139801489],
                ],
            ],
        ];
    }
}
