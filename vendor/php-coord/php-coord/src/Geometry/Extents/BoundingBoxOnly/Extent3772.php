<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Hawkes Bay mc Napier vcrs.
 * @internal
 */
class Extent3772
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [178.06217002869, -38.87341245], [175.80335868, -38.87341245], [175.80335868, -40.568576161631], [178.06217002869, -40.568576161631], [178.06217002869, -38.87341245],
                ],
            ],
        ];
    }
}
