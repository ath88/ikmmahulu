<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Albania - north of 41°18'N and east of 19°09'E.
 * @internal
 */
class Extent4446
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [20.621730000035, 42.660340000061], [19.15, 42.660340000061], [19.15, 41.3], [20.621730000035, 41.3], [20.621730000035, 42.660340000061],
                ],
            ],
        ];
    }
}
