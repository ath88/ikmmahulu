<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand.
 * @internal
 */
class Extent1175
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [188.7992603412, -25.888258907811], [160.60983065, -25.888258907811], [160.60983065, -55.949294936], [188.7992603412, -55.949294936], [188.7992603412, -25.888258907811],
                ],
            ],
        ];
    }
}
