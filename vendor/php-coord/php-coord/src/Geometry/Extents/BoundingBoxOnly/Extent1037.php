<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Austria.
 * @internal
 */
class Extent1037
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [17.166385650635, 49.018745422363], [9.5335693359375, 49.018745422363], [9.5335693359375, 46.407493591309], [17.166385650635, 46.407493591309], [17.166385650635, 49.018745422363],
                ],
            ],
        ];
    }
}
