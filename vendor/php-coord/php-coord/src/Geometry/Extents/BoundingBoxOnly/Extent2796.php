<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Foula onshore.
 * @internal
 */
class Extent2796
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-1.9528083610803, 60.191665220851], [-2.2011822976852, 60.191665220851], [-2.2011822976852, 60.069720493618], [-1.9528083610803, 60.069720493618], [-1.9528083610803, 60.191665220851],
                ],
            ],
        ];
    }
}
