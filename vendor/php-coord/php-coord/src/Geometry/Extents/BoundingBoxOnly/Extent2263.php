<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Washington - SPCS27 - S.
 * @internal
 */
class Extent2263
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-116.9191324613, 47.959522247314], [-124.39991188582, 47.959522247314], [-124.39991188582, 45.543251037598], [-116.9191324613, 45.543251037598], [-116.9191324613, 47.959522247314],
                ],
            ],
        ];
    }
}
