<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Martinique.
 * @internal
 */
class Extent1156
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.525645554049, 16.35403798876], [-62.81388855897, 16.35403798876], [-62.81388855897, 14.080555129072], [-57.525645554049, 14.080555129072], [-57.525645554049, 16.35403798876],
                ],
            ],
        ];
    }
}
