<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Victoria.
 * @internal
 */
class Extent2285
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [150.03729947381, -33.982208], [140.960037, -33.982208], [140.960037, -39.197008132935], [150.03729947381, -39.197008132935], [150.03729947381, -33.982208],
                ],
            ],
        ];
    }
}
