<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - 97.5°E to 100.5°E.
 * @internal
 */
class Extent2719
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [100.5, 42.750728696541], [97.5, 42.750728696541], [97.5, 21.435461044312], [100.5, 21.435461044312], [100.5, 42.750728696541],
                ],
            ],
        ];
    }
}
