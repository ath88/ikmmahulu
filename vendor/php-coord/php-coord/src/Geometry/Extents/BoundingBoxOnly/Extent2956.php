<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Kuwait - north of 29.25°N.
 * @internal
 */
class Extent2956
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.416745457509, 30.084159851074], [46.546943664551, 30.084159851074], [46.546943664551, 29.104198455811], [48.416745457509, 29.104198455811], [48.416745457509, 30.084159851074],
                ],
            ],
        ];
    }
}
