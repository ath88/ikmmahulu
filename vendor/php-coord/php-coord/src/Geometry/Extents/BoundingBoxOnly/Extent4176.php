<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australasia - Australia and Christmas Island - 108°E to 114°E.
 * @internal
 */
class Extent4176
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [109.02382130187, -10.729190663006], [108, -10.729190663006], [108, -13.002589497983], [109.02382130187, -13.002589497983], [109.02382130187, -10.729190663006],
                ],
            ],
            [
                [
                    [114, -14.055505768], [108, -14.055505768], [108, -37.832136354074], [114, -37.832136354074], [114, -14.055505768],
                ],
            ],
        ];
    }
}
