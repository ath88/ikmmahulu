<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Costa Rica - onshore.
 * @internal
 */
class Extent3232
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.534971334026, 11.212845802092], [-85.960070996658, 11.212845802092], [-85.960070996658, 7.9817113128033], [-82.534971334026, 7.9817113128033], [-82.534971334026, 11.212845802092],
                ],
            ],
        ];
    }
}
