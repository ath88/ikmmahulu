<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/St Pierre and Miquelon - onshore.
 * @internal
 */
class Extent3299
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-56.072673638895, 47.183776823702], [-56.471020559399, 47.183776823702], [-56.471020559399, 46.699251240852], [-56.072673638895, 46.699251240852], [-56.072673638895, 47.183776823702],
                ],
            ],
        ];
    }
}
