<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Solomon Islands - Guadalcanal Island.
 * @internal
 */
class Extent3197
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [160.87524010597, -9.2060302435872], [159.55194687328, -9.2060302435872], [159.55194687328, -9.9788643707067], [160.87524010597, -9.9788643707067], [160.87524010597, -9.2060302435872],
                ],
            ],
        ];
    }
}
