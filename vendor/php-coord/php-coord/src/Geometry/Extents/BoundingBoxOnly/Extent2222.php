<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Nebraska - SPCS27 - S.
 * @internal
 */
class Extent2222
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.30829, 42.001588889276], [-104.05530280265, 42.001588889276], [-104.05530280265, 39.999932], [-95.30829, 39.999932], [-95.30829, 42.001588889276],
                ],
            ],
        ];
    }
}
