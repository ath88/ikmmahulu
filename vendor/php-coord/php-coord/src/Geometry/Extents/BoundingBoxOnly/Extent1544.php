<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Oman - onshore west of 54°E.
 * @internal
 */
class Extent1544
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [54, 19.666526827507], [51.999290466309, 19.666526827507], [51.999290466309, 16.599396050847], [54, 16.599396050847], [54, 19.666526827507],
                ],
            ],
        ];
    }
}
