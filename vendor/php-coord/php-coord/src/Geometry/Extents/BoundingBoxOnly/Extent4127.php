<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - OML 124.
 * @internal
 */
class Extent4127
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.96177466, 5.7386075650001], [6.7259707600001, 5.7386075650001], [6.7259707600001, 5.5669782], [6.96177466, 5.5669782], [6.96177466, 5.7386075650001],
                ],
            ],
        ];
    }
}
