<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Jennings.
 * @internal
 */
class Extent4304
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.439966, 39.195616999998], [-85.799833999996, 39.195616999998], [-85.799833999996, 38.806986000014], [-85.439966, 38.806986000014], [-85.439966, 39.195616999998],
                ],
            ],
        ];
    }
}
