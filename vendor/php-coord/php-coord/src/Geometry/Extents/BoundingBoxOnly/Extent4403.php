<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Jammu and Kashmir.
 * @internal
 */
class Extent4403
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [79.564292907715, 35.501331329346], [73.763221740723, 35.501331329346], [73.763221740723, 32.275329589844], [79.564292907715, 32.275329589844], [79.564292907715, 35.501331329346],
                ],
            ],
        ];
    }
}
