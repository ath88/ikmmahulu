<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New Mexico - SPCS83 - C.
 * @internal
 */
class Extent2231
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.84023421652, 36.999082929088], [-107.72022297058, 36.999082929088], [-107.72022297058, 31.781495725094], [-104.84023421652, 31.781495725094], [-104.84023421652, 36.999082929088],
                ],
            ],
        ];
    }
}
