<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/South America - 72°W to 66°W, N hemisphere.
 * @internal
 */
class Extent1827
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66, 15.632165908813], [-72, 15.632165908813], [-72, 0], [-66, 0], [-66, 15.632165908813],
                ],
            ],
        ];
    }
}
