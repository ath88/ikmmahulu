<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Schleswig-Holstein - east of 10.5°E.
 * @internal
 */
class Extent3644
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.398032883606, 54.584508937875], [10.5, 54.584508937875], [10.5, 53.361938476562], [11.398032883606, 53.361938476562], [11.398032883606, 54.584508937875],
                ],
            ],
        ];
    }
}
