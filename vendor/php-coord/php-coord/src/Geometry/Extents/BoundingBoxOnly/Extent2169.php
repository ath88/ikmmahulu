<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arkansas - SPCS - N.
 * @internal
 */
class Extent2169
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.646304468239, 36.492811128552], [-94.617257095851, 36.492811128552], [-94.617257095851, 34.678464351042], [-89.646304468239, 34.678464351042], [-89.646304468239, 36.492811128552],
                ],
            ],
        ];
    }
}
