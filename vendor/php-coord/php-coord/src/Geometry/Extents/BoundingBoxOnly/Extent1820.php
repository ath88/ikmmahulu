<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Falkland Islands - onshore west of 60°W.
 * @internal
 */
class Extent1820
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-59.999984148583, -50.961920339909], [-61.544382354947, -50.961920339909], [-61.544382354947, -52.323610434059], [-59.999984148583, -52.323610434059], [-59.999984148583, -50.961920339909],
                ],
            ],
        ];
    }
}
