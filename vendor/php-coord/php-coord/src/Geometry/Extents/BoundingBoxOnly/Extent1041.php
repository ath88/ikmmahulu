<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bangladesh.
 * @internal
 */
class Extent1041
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [92.669342041016, 26.634122848511], [88.010566711426, 26.634122848511], [88.010566711426, 18.565193616816], [92.669342041016, 18.565193616816], [92.669342041016, 26.634122848511],
                ],
            ],
        ];
    }
}
