<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Maine.
 * @internal
 */
class Extent1388
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.917007446289, 47.467893600464], [-71.08750924735, 47.467893600464], [-71.08750924735, 43.044502266819], [-66.917007446289, 43.044502266819], [-66.917007446289, 47.467893600464],
                ],
            ],
        ];
    }
}
