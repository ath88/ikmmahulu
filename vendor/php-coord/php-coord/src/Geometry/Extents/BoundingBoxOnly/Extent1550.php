<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Hawaii - Niihau - onshore.
 * @internal
 */
class Extent1550
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-159.99653816223, 22.067358016968], [-160.29792404175, 22.067358016968], [-160.29792404175, 21.733602523804], [-159.99653816223, 21.733602523804], [-159.99653816223, 22.067358016968],
                ],
            ],
        ];
    }
}
