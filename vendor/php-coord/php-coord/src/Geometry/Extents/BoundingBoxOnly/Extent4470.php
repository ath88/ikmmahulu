<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Ukiah-Fox.
 * @internal
 */
class Extent4470
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-118.64476080759, 45.207595862267], [-119.34302131331, 45.207595862267], [-119.34302131331, 44.521991723274], [-118.64476080759, 44.521991723274], [-118.64476080759, 45.207595862267],
                ],
            ],
        ];
    }
}
