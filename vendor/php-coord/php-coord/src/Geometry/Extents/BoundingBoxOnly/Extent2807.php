<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Comoros - Njazidja (Grande Comore).
 * @internal
 */
class Extent2807
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [43.547667457271, -11.318732405805], [43.164150819483, -11.318732405805], [43.164150819483, -11.984303269154], [43.547667457271, -11.984303269154], [43.547667457271, -11.318732405805],
                ],
            ],
        ];
    }
}
