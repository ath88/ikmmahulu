<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Montgomery and Putnam.
 * @internal
 */
class Extent4262
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.641865000004, 40.215049999988], [-87.092207999996, 40.215049999988], [-87.092207999996, 39.470177000047], [-86.641865000004, 39.470177000047], [-86.641865000004, 40.215049999988],
                ],
            ],
        ];
    }
}
