<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Adams and Juneau.
 * @internal
 */
class Extent4360
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.597312602353, 44.249486175743], [-90.313352898489, 44.249486175743], [-90.313352898489, 43.64093652907], [-89.597312602353, 43.64093652907], [-89.597312602353, 44.249486175743],
                ],
            ],
        ];
    }
}
