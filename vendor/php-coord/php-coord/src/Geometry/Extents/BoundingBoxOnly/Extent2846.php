<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 5 gon W.
 * @internal
 */
class Extent2846
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.433691841046, 64.385586765553], [11.81595993042, 64.385586765553], [11.81595993042, 55.289268540114], [15.433691841046, 55.289268540114], [15.433691841046, 64.385586765553],
                ],
            ],
        ];
    }
}
