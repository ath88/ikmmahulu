<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kyrgyzstan - east of 79°01'E.
 * @internal
 */
class Extent4389
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [80.28158569336, 42.797595977783], [79.016666666667, 42.797595977783], [79.016666666667, 41.66513409471], [80.28158569336, 41.66513409471], [80.28158569336, 42.797595977783],
                ],
            ],
        ];
    }
}
