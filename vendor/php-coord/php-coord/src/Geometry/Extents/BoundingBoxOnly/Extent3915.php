<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Reunion - west of 54ºE.
 * @internal
 */
class Extent3915
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [54, -18.525711271777], [51.835076548, -18.525711271777], [51.835076548, -24.363127152898], [54, -24.363127152898], [54, -18.525711271777],
                ],
            ],
        ];
    }
}
