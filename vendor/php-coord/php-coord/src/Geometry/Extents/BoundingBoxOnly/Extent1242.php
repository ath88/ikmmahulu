<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Ukraine.
 * @internal
 */
class Extent1242
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.178745269775, 52.378601074219], [22.151441574097, 52.378601074219], [22.151441574097, 43.188055156001], [40.178745269775, 43.188055156001], [40.178745269775, 52.378601074219],
                ],
            ],
        ];
    }
}
