<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Ukraine - west of 25°E.
 * @internal
 */
class Extent4435
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25, 51.914658126817], [22.151441574097, 51.914658126817], [22.151441574097, 47.71314239502], [25, 47.71314239502], [25, 51.914658126817],
                ],
            ],
        ];
    }
}
