<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 9ºE to 10ºE.
 * @internal
 */
class Extent3649
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10, 64.157242120714], [9, 64.157242120714], [9, 58.521690510469], [10, 58.521690510469], [10, 64.157242120714],
                ],
            ],
        ];
    }
}
