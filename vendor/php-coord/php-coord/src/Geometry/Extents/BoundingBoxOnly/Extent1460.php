<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - 26°E to 28°E.
 * @internal
 */
class Extent1460
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [27.999999942813, -22.920504941735], [26.00001856162, -22.920504941735], [26.00001856162, -33.822040557861], [27.999999942813, -33.822040557861], [27.999999942813, -22.920504941735],
                ],
            ],
        ];
    }
}
