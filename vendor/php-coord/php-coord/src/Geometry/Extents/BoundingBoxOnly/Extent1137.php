<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kyrgyzstan.
 * @internal
 */
class Extent1137
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [80.281585693359, 43.216903686523], [69.249496459961, 43.216903686523], [69.249496459961, 39.195468902588], [80.281585693359, 39.195468902588], [80.281585693359, 43.216903686523],
                ],
            ],
        ];
    }
}
