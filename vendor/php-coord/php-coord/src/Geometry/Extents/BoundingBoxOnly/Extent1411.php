<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Tennessee.
 * @internal
 */
class Extent1411
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.652272181383, 36.67968325634], [-90.305448332911, 36.67968325634], [-90.305448332911, 34.988759767446], [-81.652272181383, 34.988759767446], [-81.652272181383, 36.67968325634],
                ],
            ],
        ];
    }
}
