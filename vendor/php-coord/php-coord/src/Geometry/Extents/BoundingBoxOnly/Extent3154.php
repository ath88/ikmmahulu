<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 19°E to 21°E.
 * @internal
 */
class Extent3154
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21.000003004735, 5.1505551338197], [18.999982718822, 5.1505551338197], [18.999982718822, -7.9994449615477], [21.000003004735, -7.9994449615477], [21.000003004735, 5.1505551338197],
                ],
            ],
        ];
    }
}
