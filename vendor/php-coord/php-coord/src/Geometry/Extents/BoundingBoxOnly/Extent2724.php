<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - 112.5°E to 115.5°E onshore.
 * @internal
 */
class Extent2724
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [115.5, 45.440547943115], [112.5, 45.440547943115], [112.5, 21.52397019656], [115.5, 21.52397019656], [115.5, 45.440547943115],
                ],
            ],
        ];
    }
}
