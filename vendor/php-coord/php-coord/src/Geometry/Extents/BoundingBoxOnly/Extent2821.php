<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - Belep.
 * @internal
 */
class Extent2821
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [163.7499295286, -19.505828366069], [163.54235002707, -19.505828366069], [163.54235002707, -19.844914708111], [163.7499295286, -19.844914708111], [163.7499295286, -19.505828366069],
                ],
            ],
        ];
    }
}
