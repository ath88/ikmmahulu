<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Tripura.
 * @internal
 */
class Extent4418
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [92.3369140625, 24.534021377563], [91.154479980469, 24.534021377563], [91.154479980469, 22.944150924683], [92.3369140625, 22.944150924683], [92.3369140625, 24.534021377563],
                ],
            ],
        ];
    }
}
