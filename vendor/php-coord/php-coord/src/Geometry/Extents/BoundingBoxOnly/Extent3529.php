<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/South Georgia - onshore.
 * @internal
 */
class Extent3529
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-35.743959974171, -53.939722608448], [-38.073754716992, -53.939722608448], [-38.073754716992, -54.940282274365], [-35.743959974171, -54.940282274365], [-35.743959974171, -53.939722608448],
                ],
            ],
        ];
    }
}
