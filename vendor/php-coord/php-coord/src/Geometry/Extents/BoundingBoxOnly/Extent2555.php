<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cameroon - coastal area.
 * @internal
 */
class Extent2555
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.392972743594, 4.983180978332], [8.4523414768613, 4.983180978332], [8.4523414768613, 2.1676787961496], [10.392972743594, 2.1676787961496], [10.392972743594, 4.983180978332],
                ],
            ],
        ];
    }
}
