<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - Basra area.
 * @internal
 */
class Extent3397
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.608653779497, 31.082338711733], [46.466714625068, 31.082338711733], [46.466714625068, 29.877218367148], [48.608653779497, 29.877218367148], [48.608653779497, 31.082338711733],
                ],
            ],
        ];
    }
}
