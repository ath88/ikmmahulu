<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - 44°S to 47.5°S.
 * @internal
 */
class Extent4572
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-63.25, -44], [-72.359313964844, -44], [-72.359313964844, -47.5], [-63.25, -47.5], [-63.25, -44],
                ],
            ],
        ];
    }
}
