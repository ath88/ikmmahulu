<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Collie.
 * @internal
 */
class Extent4443
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [116.4, -33.25], [115.73333333333, -33.25], [115.73333333333, -34.666666666667], [116.4, -34.666666666667], [116.4, -33.25],
                ],
            ],
        ];
    }
}
