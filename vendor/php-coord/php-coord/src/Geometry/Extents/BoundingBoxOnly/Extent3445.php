<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - 48°W to 42°W.
 * @internal
 */
class Extent3445
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-42, 5.1226062774658], [-48, 5.1226062774658], [-48, -33.491727828979], [-42, -33.491727828979], [-42, 5.1226062774658],
                ],
            ],
        ];
    }
}
