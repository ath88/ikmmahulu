<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - Cusiana.
 * @internal
 */
class Extent2315
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72.25, 5.6700000762939], [-73, 5.6700000762939], [-73, 4.7500019073486], [-72.25, 4.7500019073486], [-72.25, 5.6700000762939],
                ],
            ],
        ];
    }
}
