<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia.
 * @internal
 */
class Extent1098
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-131.978610623, -4.5264778879997], [-158.128055647, -4.5264778879997], [-158.128055647, -31.236190913], [-131.978610623, -31.236190913], [-131.978610623, -4.5264778879997],
                ],
            ],
        ];
    }
}
