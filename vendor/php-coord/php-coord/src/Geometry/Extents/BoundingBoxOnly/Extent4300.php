<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Fayette, Franklin, Union.
 * @internal
 */
class Extent4300
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.814129000003, 39.788630000028], [-85.301524999997, 39.788630000028], [-85.301524999997, 39.268291000015], [-84.814129000003, 39.268291000015], [-84.814129000003, 39.788630000028],
                ],
            ],
        ];
    }
}
