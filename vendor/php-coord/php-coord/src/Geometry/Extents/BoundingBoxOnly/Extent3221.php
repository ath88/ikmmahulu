<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Bermuda - onshore.
 * @internal
 */
class Extent3221
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-64.618106162357, 32.428320738644], [-64.881682238676, 32.428320738644], [-64.881682238676, 32.211729021989], [-64.618106162357, 32.211729021989], [-64.618106162357, 32.428320738644],
                ],
            ],
        ];
    }
}
