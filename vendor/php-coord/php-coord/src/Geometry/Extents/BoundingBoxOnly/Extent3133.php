<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Fatu Hiva.
 * @internal
 */
class Extent3133
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-138.54450335169, -10.364875982968], [-138.74683224948, -10.364875982968], [-138.74683224948, -10.599193033075], [-138.54450335169, -10.599193033075], [-138.54450335169, -10.364875982968],
                ],
            ],
        ];
    }
}
