<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Mendoza and Neuquen.
 * @internal
 */
class Extent4562
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-65.865758342405, -31.914892582143], [-72.138900756836, -31.914892582143], [-72.138900756836, -43.405476435138], [-65.865758342405, -43.405476435138], [-65.865758342405, -31.914892582143],
                ],
            ],
        ];
    }
}
