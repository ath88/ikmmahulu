<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Brown.
 * @internal
 */
class Extent4301
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.079030000005, 39.344189000013], [-86.381394999996, 39.344189000013], [-86.381394999996, 39.04837599993], [-86.079030000005, 39.04837599993], [-86.079030000005, 39.344189000013],
                ],
            ],
        ];
    }
}
