<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Uruguay - east of 54°W.
 * @internal
 */
class Extent3828
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-50.014827728271, -31.906721115112], [-54, -31.906721115112], [-54, -37.765113830566], [-50.014827728271, -37.765113830566], [-50.014827728271, -31.906721115112],
                ],
            ],
        ];
    }
}
