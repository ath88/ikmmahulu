<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Jharkhand.
 * @internal
 */
class Extent4404
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [87.973342895508, 25.348390579224], [83.32356262207, 25.348390579224], [83.32356262207, 21.966590881348], [87.973342895508, 21.966590881348], [87.973342895508, 25.348390579224],
                ],
            ],
        ];
    }
}
