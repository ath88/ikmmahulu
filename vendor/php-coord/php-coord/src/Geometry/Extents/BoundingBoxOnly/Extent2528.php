<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas - SPCS83 - S.
 * @internal
 */
class Extent2528
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.852147329036, 28.20391330063], [-100.19227559513, 28.20391330063], [-100.19227559513, 25.839860916138], [-96.852147329036, 25.839860916138], [-96.852147329036, 28.20391330063],
                ],
            ],
        ];
    }
}
