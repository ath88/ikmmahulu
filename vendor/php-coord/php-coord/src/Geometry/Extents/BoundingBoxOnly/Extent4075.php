<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 81°10'N to 76°10'N,  24°W to 3°E.
 * @internal
 */
class Extent4075
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [1.8809606137072, 81.166666030884], [-24, 81.166666030884], [-24, 76.166666030884], [1.8809606137072, 76.166666030884], [1.8809606137072, 81.166666030884],
                ],
            ],
        ];
    }
}
