<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wyoming - Wind River reservation.
 * @internal
 */
class Extent4319
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-107.95, 43.85], [-109.5, 43.85], [-109.5, 42.7], [-107.95, 42.7], [-107.95, 43.85],
                ],
            ],
        ];
    }
}
