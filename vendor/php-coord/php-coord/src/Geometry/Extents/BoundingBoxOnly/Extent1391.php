<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Michigan.
 * @internal
 */
class Extent1391
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.130279541016, 48.312211990357], [-90.418865851624, 48.312211990357], [-90.418865851624, 41.697121363375], [-82.130279541016, 41.697121363375], [-82.130279541016, 48.312211990357],
                ],
            ],
        ];
    }
}
