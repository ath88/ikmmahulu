<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Spencer.
 * @internal
 */
class Extent4164
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.970147, 43.500334036467], [-96.598315428219, 43.500334036467], [-96.598315428219, 42.907743], [-93.970147, 42.907743], [-93.970147, 43.500334036467],
                ],
            ],
        ];
    }
}
