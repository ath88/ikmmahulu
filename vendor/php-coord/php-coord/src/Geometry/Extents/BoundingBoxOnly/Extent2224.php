<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Nevada - SPCS - E.
 * @internal
 */
class Extent2224
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-114.03739207419, 41.996506455953], [-117.00702915924, 41.996506455953], [-117.00702915924, 34.998914428613], [-114.03739207419, 34.998914428613], [-114.03739207419, 41.996506455953],
                ],
            ],
        ];
    }
}
