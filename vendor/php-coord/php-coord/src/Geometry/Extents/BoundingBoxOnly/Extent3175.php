<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - FSU - CS63 zone C2.
 * @internal
 */
class Extent3175
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28.235967636109, 59.606948396925], [26.45, 59.606948396925], [26.45, 55.150272369385], [28.235967636109, 55.150272369385], [28.235967636109, 59.606948396925],
                ],
            ],
        ];
    }
}
