<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Salina.
 * @internal
 */
class Extent4500
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.805693, 40.002259], [-97.933418, 40.002259], [-97.933418, 38.08561], [-96.805693, 38.08561], [-96.805693, 40.002259],
                ],
            ],
        ];
    }
}
