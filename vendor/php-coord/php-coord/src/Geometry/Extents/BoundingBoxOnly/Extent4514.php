<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Pacific - Guam and NMI west of 144°E.
 * @internal
 */
class Extent4514
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [144, 23.035319133749], [141.195102631, 23.035319133749], [141.195102631, 10.950833126], [144, 10.950833126], [144, 23.035319133749],
                ],
            ],
        ];
    }
}
