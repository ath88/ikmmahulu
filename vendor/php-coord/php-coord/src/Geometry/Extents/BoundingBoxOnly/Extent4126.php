<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores E - Santa Maria and Formigas onshore.
 * @internal
 */
class Extent4126
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-24.962847303272, 37.073186394561], [-25.250558305859, 37.073186394561], [-25.250558305859, 36.879160474659], [-24.962847303272, 36.879160474659], [-24.962847303272, 37.073186394561],
                ],
            ],
            [
                [
                    [-24.72130342841, 37.334838486207], [-24.839956378284, 37.334838486207], [-24.839956378284, 37.210993002825], [-24.72130342841, 37.210993002825], [-24.72130342841, 37.334838486207],
                ],
            ],
        ];
    }
}
