<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Prekmurje.
 * @internal
 */
class Extent3346
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.607872009278, 46.876247406006], [15.987915992775, 46.876247406006], [15.987915992775, 46.474006652832], [16.607872009278, 46.474006652832], [16.607872009278, 46.876247406006],
                ],
            ],
        ];
    }
}
