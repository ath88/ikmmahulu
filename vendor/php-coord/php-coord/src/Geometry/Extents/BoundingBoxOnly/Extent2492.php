<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 34°40'N to 35°20'N; 134°E to 135°E.
 * @internal
 */
class Extent2492
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [135, 35.332766666667], [134, 35.332766666667], [134, 34.666066666667], [135, 34.666066666667], [135, 35.332766666667],
                ],
            ],
        ];
    }
}
