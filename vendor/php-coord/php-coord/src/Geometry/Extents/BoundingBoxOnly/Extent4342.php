<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Eau Claire.
 * @internal
 */
class Extent4342
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.921139477527, 44.857728297085], [-91.650493072687, 44.857728297085], [-91.650493072687, 44.595800000675], [-90.921139477527, 44.595800000675], [-90.921139477527, 44.857728297085],
                ],
            ],
        ];
    }
}
