<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Ghana - offshore.
 * @internal
 */
class Extent1505
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [2.0991015020005, 6.0566617507182], [-3.7891135039995, 6.0566617507182], [-3.7891135039995, 1.4037081170007], [2.0991015020005, 1.4037081170007], [2.0991015020005, 6.0566617507182],
                ],
            ],
        ];
    }
}
