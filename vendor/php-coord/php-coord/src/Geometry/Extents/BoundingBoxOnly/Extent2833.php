<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 12 00.
 * @internal
 */
class Extent2833
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.104319571886, 60.128482263697], [10.937693412446, 60.128482263697], [10.937693412446, 56.745040982121], [13.104319571886, 56.745040982121], [13.104319571886, 60.128482263697],
                ],
            ],
        ];
    }
}
