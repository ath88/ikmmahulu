<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Hiva Oa.
 * @internal
 */
class Extent3131
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-138.75975282085, -9.6454177607307], [-139.22225706685, -9.6454177607307], [-139.22225706685, -9.8809650793816], [-138.75975282085, -9.8809650793816], [-138.75975282085, -9.6454177607307],
                ],
            ],
        ];
    }
}
