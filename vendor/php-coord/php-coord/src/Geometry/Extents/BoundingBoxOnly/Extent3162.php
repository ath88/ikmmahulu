<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 6th parallel south 19°E to 21°E.
 * @internal
 */
class Extent3162
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21, -4.0195315692529], [19, -4.0195315692529], [19, -7.283616065979], [21, -7.283616065979], [21, -4.0195315692529],
                ],
            ],
        ];
    }
}
