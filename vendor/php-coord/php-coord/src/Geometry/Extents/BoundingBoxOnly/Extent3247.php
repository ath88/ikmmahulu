<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Falkland Islands - onshore.
 * @internal
 */
class Extent3247
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.60570099751, -50.961920339909], [-61.544382354947, -50.961920339909], [-61.544382354947, -52.509350206498], [-57.60570099751, -52.509350206498], [-57.60570099751, -50.961920339909],
                ],
            ],
        ];
    }
}
