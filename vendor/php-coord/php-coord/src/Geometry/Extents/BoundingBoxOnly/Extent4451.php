<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Karratha.
 * @internal
 */
class Extent4451
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [117.25, -20.25], [116.58333333333, -20.25], [116.58333333333, -20.916666666667], [117.25, -20.916666666667], [117.25, -20.25],
                ],
            ],
        ];
    }
}
