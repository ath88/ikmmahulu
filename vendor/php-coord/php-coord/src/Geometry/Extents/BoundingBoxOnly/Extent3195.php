<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Fiji - Viti Levu.
 * @internal
 */
class Extent3195
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [178.74484062195, -17.252500534058], [177.19856452942, -17.252500534058], [177.19856452942, -18.318332672119], [178.74484062195, -18.318332672119], [178.74484062195, -17.252500534058],
                ],
            ],
        ];
    }
}
