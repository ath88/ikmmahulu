<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Poland - zone IV.
 * @internal
 */
class Extent1518
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.08333333333, 53.3333333333], [14.147637367248, 53.3333333333], [14.147637367248, 49.391937255859], [19.08333333333, 49.391937255859], [19.08333333333, 53.3333333333],
                ],
            ],
        ];
    }
}
