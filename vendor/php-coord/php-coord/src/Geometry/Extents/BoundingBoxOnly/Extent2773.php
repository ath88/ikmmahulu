<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - CS63 zone A2.
 * @internal
 */
class Extent2773
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [46.0333333333, 43.046474431494], [43.0333333333, 43.046474431494], [43.0333333333, 38.877037478944], [46.0333333333, 38.877037478944], [46.0333333333, 43.046474431494],
                ],
            ],
        ];
    }
}
