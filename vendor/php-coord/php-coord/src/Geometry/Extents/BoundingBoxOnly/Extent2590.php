<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cameroon - Garoua area.
 * @internal
 */
class Extent2590
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.183802559482, 9.8681668285237], [12.906438127228, 9.8681668285237], [12.906438127228, 8.926350855919], [14.183802559482, 8.926350855919], [14.183802559482, 9.8681668285237],
                ],
            ],
        ];
    }
}
