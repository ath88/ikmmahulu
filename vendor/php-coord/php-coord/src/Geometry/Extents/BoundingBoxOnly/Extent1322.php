<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Trinidad and Tobago - Tobago - onshore.
 * @internal
 */
class Extent1322
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60.442054677978, 11.409305223126], [-60.896162592119, 11.409305223126], [-60.896162592119, 11.084513524369], [-60.442054677978, 11.084513524369], [-60.442054677978, 11.409305223126],
                ],
            ],
        ];
    }
}
