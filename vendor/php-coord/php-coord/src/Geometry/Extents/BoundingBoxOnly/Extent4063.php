<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 81°10'N to 76°10'N,  160°E to 169°W.
 * @internal
 */
class Extent4063
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [191, 81.166666030884], [160.00000190735, 81.166666030884], [160.00000190735, 76.166666030884], [191, 76.166666030884], [191, 81.166666030884],
                ],
            ],
        ];
    }
}
