<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Tippecanoe and White.
 * @internal
 */
class Extent4257
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.580368000008, 40.912576999998], [-87.099799999996, 40.912576999998], [-87.099799999996, 40.214373000011], [-86.580368000008, 40.214373000011], [-86.580368000008, 40.912576999998],
                ],
            ],
        ];
    }
}
