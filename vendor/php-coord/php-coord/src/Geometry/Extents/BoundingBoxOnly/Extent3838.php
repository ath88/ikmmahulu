<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Peru - 78°W to 72°W.
 * @internal
 */
class Extent3838
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, -0.036874771118164], [-78, -0.036874771118164], [-78, -21.04967880249], [-72, -21.04967880249], [-72, -0.036874771118164],
                ],
            ],
        ];
    }
}
