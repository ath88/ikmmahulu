<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Bolivia - east of 60°W.
 * @internal
 */
class Extent3733
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.521118164062, -16.270225524902], [-60, -16.270225524902], [-60, -20.168056488037], [-57.521118164062, -20.168056488037], [-57.521118164062, -16.270225524902],
                ],
            ],
        ];
    }
}
