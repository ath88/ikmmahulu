<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Sierra Leone - west of 12°W.
 * @internal
 */
class Extent1509
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-12, 9.9352073669435], [-13.342508711618, 9.9352073669435], [-13.342508711618, 7.1551508470818], [-12, 7.1551508470818], [-12, 9.9352073669435],
                ],
            ],
        ];
    }
}
