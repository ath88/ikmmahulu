<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Wallis and Futuna - Wallis.
 * @internal
 */
class Extent2815
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-176.072039, -13.164862], [-176.241102, -13.164862], [-176.241102, -13.403053], [-176.072039, -13.403053], [-176.072039, -13.164862],
                ],
            ],
        ];
    }
}
