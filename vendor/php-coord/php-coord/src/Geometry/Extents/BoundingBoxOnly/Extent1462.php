<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - 30°E to 32°E.
 * @internal
 */
class Extent1462
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [32.019373074053, -22.222060709738], [29.999997222862, -22.222060709738], [29.999997222862, -31.372258958666], [32.019373074053, -31.372258958666], [32.019373074053, -22.222060709738],
                ],
            ],
        ];
    }
}
