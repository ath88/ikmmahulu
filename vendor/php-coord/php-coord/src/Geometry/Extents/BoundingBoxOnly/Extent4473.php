<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arizona - Pima county Mt. Lemmon.
 * @internal
 */
class Extent4473
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-110.617, 32.483333333333], [-110.867, 32.483333333333], [-110.867, 32.331], [-110.617, 32.331], [-110.617, 32.483333333333],
                ],
            ],
        ];
    }
}
