<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Gibson.
 * @internal
 */
class Extent4273
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.315652000022, 38.533518999997], [-87.977702958006, 38.533518999997], [-87.977702958006, 38.165462000032], [-87.315652000022, 38.165462000032], [-87.315652000022, 38.533518999997],
                ],
            ],
        ];
    }
}
