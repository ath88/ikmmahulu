<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Waterloo.
 * @internal
 */
class Extent4234
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.597009, 43.082794], [-93.027115, 43.082794], [-93.027115, 42.209293], [-91.597009, 42.209293], [-91.597009, 43.082794],
                ],
            ],
        ];
    }
}
