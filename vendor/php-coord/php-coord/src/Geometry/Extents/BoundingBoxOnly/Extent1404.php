<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Ohio.
 * @internal
 */
class Extent1404
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-80.519996234962, 42.3274373242], [-84.82737860536, 42.3274373242], [-84.82737860536, 38.400510978509], [-80.519996234962, 38.400510978509], [-80.519996234962, 42.3274373242],
                ],
            ],
        ];
    }
}
