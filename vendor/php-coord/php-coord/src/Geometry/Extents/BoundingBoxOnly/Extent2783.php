<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran - South Pars blocks 2 and 3.
 * @internal
 */
class Extent2783
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [52.274831, 26.709803], [52.074963, 26.709803], [52.074963, 26.589953], [52.274831, 26.589953], [52.274831, 26.709803],
                ],
            ],
        ];
    }
}
