<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - onshore west of 66°E.
 * @internal
 */
class Extent1687
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [66, 29.863655090332], [60.866302490235, 29.863655090332], [60.866302490235, 24.982081288873], [66, 24.982081288873], [66, 29.863655090332],
                ],
            ],
        ];
    }
}
