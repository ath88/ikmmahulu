<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 6ºE to 7ºE.
 * @internal
 */
class Extent3639
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7, 63.016419327217], [6, 63.016419327217], [6, 57.93826750784], [7, 57.93826750784], [7, 63.016419327217],
                ],
            ],
        ];
    }
}
