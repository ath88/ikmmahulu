<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Lower Saxony 7.5°E to 10.5°E.
 * @internal
 */
class Extent3707
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.5, 53.943790305647], [7.5, 53.943790305647], [7.5, 51.289993286133], [10.5, 51.289993286133], [10.5, 53.943790305647],
                ],
            ],
        ];
    }
}
