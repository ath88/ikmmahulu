<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - west of 6.5°E.
 * @internal
 */
class Extent1715
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.5, 13.891498565674], [2.692499637604, 13.891498565674], [2.692499637604, 3.5757728940883], [6.5, 3.5757728940883], [6.5, 13.891498565674],
                ],
            ],
        ];
    }
}
