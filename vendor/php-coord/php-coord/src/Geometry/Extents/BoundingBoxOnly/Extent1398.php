<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New Hampshire.
 * @internal
 */
class Extent1398
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-70.631540679866, 45.303743362427], [-72.553428463971, 45.303743362427], [-72.553428463971, 42.69860344651], [-70.631540679866, 42.69860344651], [-70.631540679866, 45.303743362427],
                ],
            ],
        ];
    }
}
