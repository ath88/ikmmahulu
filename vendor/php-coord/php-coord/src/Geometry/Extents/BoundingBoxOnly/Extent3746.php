<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Paro district.
 * @internal
 */
class Extent3746
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [89.551923105283, 27.788726396006], [89.128036499023, 27.788726396006], [89.128036499023, 27.188430271658], [89.551923105283, 27.188430271658], [89.551923105283, 27.788726396006],
                ],
            ],
        ];
    }
}
