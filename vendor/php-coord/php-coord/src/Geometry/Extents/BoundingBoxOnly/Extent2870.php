<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Madeira and Porto Santo islands onshore.
 * @internal
 */
class Extent2870
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-16.66555650127, 32.920278001904], [-17.304517198681, 32.920278001904], [-17.304517198681, 32.587496351124], [-16.66555650127, 32.587496351124], [-16.66555650127, 32.920278001904],
                ],
            ],
            [
                [
                    [-16.231814329957, 33.14011162332], [-16.431488538844, 33.14011162332], [-16.431488538844, 32.969706828738], [-16.231814329957, 32.969706828738], [-16.231814329957, 33.14011162332],
                ],
            ],
        ];
    }
}
