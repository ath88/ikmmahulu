<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 36°40'N to 37°20'N; east of 140°E.
 * @internal
 */
class Extent2475
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.09534153313, 37.332866666667], [140, 37.332866666667], [140, 36.666166666667], [141.09534153313, 36.666166666667], [141.09534153313, 37.332866666667],
                ],
            ],
        ];
    }
}
