<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Tahuata.
 * @internal
 */
class Extent3132
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-138.98160028071, -9.8672671259391], [-139.18515588592, -9.8672671259391], [-139.18515588592, -10.074655286637], [-138.98160028071, -10.074655286637], [-138.98160028071, -9.8672671259391],
                ],
            ],
        ];
    }
}
