<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - mainland - onshore.
 * @internal
 */
class Extent1294
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-6.1904506683347, 42.150672912598], [-9.5542180422045, 42.150672912598], [-9.5542180422045, 36.958776078401], [-6.1904506683347, 36.958776078401], [-6.1904506683347, 42.150672912598],
                ],
            ],
        ];
    }
}
