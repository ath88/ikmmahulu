<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore block 3.
 * @internal
 */
class Extent2318
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.49765527465, -6.6681075742568], [11.747045864938, -6.6681075742568], [11.747045864938, -7.3347140916391], [12.49765527465, -7.3347140916391], [12.49765527465, -6.6681075742568],
                ],
            ],
        ];
    }
}
