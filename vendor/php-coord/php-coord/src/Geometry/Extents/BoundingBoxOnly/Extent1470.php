<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - west of 10°E.
 * @internal
 */
class Extent1470
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10, 30.483598506197], [9.3113880157472, 30.483598506197], [9.3113880157472, 25.372812442106], [10, 25.372812442106], [10, 30.483598506197],
                ],
            ],
        ];
    }
}
