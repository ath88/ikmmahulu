<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Buffalo.
 * @internal
 */
class Extent4337
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.528747928897, 44.596725415447], [-92.084147412294, 44.596725415447], [-92.084147412294, 44.025378214577], [-91.528747928897, 44.025378214577], [-91.528747928897, 44.596725415447],
                ],
            ],
        ];
    }
}
