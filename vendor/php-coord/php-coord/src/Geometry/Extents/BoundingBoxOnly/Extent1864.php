<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - zone XI.
 * @internal
 */
class Extent1864
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [139.62924250597, 42.285966451091], [139.34049315019, 42.285966451091], [139.34049315019, 41.99845949146], [139.62924250597, 41.99845949146], [139.62924250597, 42.285966451091],
                ],
            ],
            [
                [
                    [141.45838072827, 43.41497487312], [139.70234680179, 43.41497487312], [139.70234680179, 41.348161211323], [141.45838072827, 41.348161211323], [141.45838072827, 43.41497487312],
                ],
            ],
        ];
    }
}
