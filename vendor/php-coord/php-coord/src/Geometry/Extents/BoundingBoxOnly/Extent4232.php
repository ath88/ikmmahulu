<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Chile - onshore north of 32°S.
 * @internal
 */
class Extent4232
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.000839233398, -17.505279541016], [-71.761096954346, -17.505279541016], [-71.761096954346, -32], [-67.000839233398, -32], [-67.000839233398, -17.505279541016],
                ],
            ],
        ];
    }
}
