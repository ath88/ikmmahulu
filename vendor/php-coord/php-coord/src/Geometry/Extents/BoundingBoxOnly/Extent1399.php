<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New Jersey.
 * @internal
 */
class Extent1399
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-73.886865615845, 41.350573117305], [-75.599386384369, 41.350573117305], [-75.599386384369, 38.873514175415], [-73.886865615845, 38.873514175415], [-73.886865615845, 41.350573117305],
                ],
            ],
        ];
    }
}
