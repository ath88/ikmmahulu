<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Kuwait - Kuwait City.
 * @internal
 */
class Extent1310
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.15992671363, 29.441845939836], [47.780720152451, 29.441845939836], [47.780720152451, 29.171624879369], [48.15992671363, 29.171624879369], [48.15992671363, 29.441845939836],
                ],
            ],
        ];
    }
}
