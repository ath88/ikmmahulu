<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - Katanga - Lubumbashi area.
 * @internal
 */
class Extent3614
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [27.746977278548, -11.136090324449], [26.380901348568, -11.136090324449], [26.380901348568, -12.001662594746], [27.746977278548, -12.001662594746], [27.746977278548, -11.136090324449],
                ],
            ],
        ];
    }
}
