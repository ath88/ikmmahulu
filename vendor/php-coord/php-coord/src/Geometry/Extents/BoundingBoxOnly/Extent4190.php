<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Cocos (Keeling) Islands - east of 96°E.
 * @internal
 */
class Extent4190
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [100.334349593, -8.4720628919998], [96, -8.4720628919998], [96, -15.558839899], [100.334349593, -15.558839899], [100.334349593, -8.4720628919998],
                ],
            ],
        ];
    }
}
