<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Buller mc.
 * @internal
 */
class Extent3786
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [172.40989554, -41.42154760411], [171.27818707194, -41.42154760411], [171.27818707194, -42.18633846], [172.40989554, -42.18633846], [172.40989554, -41.42154760411],
                ],
            ],
        ];
    }
}
