<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Latvia.
 * @internal
 */
class Extent1139
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28.235967636109, 58.083255767822], [19.067483518001, 58.083255767822], [19.067483518001, 55.674835205078], [28.235967636109, 55.674835205078], [28.235967636109, 58.083255767822],
                ],
            ],
        ];
    }
}
