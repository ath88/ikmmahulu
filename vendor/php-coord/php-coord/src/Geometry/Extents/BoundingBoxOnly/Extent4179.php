<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Norfolk Island - east of 168°E.
 * @internal
 */
class Extent4179
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [173.34037034822, -25.615208309], [168, -25.615208309], [168, -32.477208452075], [173.34037034822, -32.477208452075], [173.34037034822, -25.615208309],
                ],
            ],
        ];
    }
}
