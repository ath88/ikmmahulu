<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Kenosha, Milwaukee, Ozaukee and Racine.
 * @internal
 */
class Extent4370
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.803779271598, 42.492674534325], [-87.804772509684, 42.492674534325], [-87.804772509684, 42.492489413967], [-87.803779271598, 42.492489413967], [-87.803779271598, 42.492674534325],
                ],
            ],
            [
                [
                    [-87.841736738335, 42.906061947665], [-87.84297561103, 42.906061947665], [-87.84297561103, 42.904724859879], [-87.841736738335, 42.904724859879], [-87.841736738335, 42.906061947665],
                ],
            ],
            [
                [
                    [-87.757509548984, 43.543440273789], [-88.307956267951, 43.543440273789], [-88.307956267951, 42.492654814143], [-87.757509548984, 42.492654814143], [-87.757509548984, 43.543440273789],
                ],
            ],
        ];
    }
}
