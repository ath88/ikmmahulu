<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Waukesha.
 * @internal
 */
class Extent4380
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.063370648237, 43.196103264446], [-88.542203708137, 43.196103264446], [-88.542203708137, 42.841944799632], [-88.063370648237, 42.841944799632], [-88.063370648237, 43.196103264446],
                ],
            ],
        ];
    }
}
