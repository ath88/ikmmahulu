<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Wichita.
 * @internal
 */
class Extent4510
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.522782, 38.175081], [-98.473104, 38.175081], [-98.473104, 37.383953], [-96.522782, 37.383953], [-96.522782, 38.175081],
                ],
            ],
        ];
    }
}
