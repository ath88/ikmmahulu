<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Mount Pleasant mc.
 * @internal
 */
class Extent3790
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [173.37728897607, -42.69374244], [171.11302254, -42.69374244], [171.11302254, -43.950050354004], [173.37728897607, -43.950050354004], [173.37728897607, -42.69374244],
                ],
            ],
        ];
    }
}
