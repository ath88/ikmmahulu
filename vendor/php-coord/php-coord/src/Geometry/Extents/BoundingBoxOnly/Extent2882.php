<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Adriatic - North Ancona.
 * @internal
 */
class Extent2882
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.959679663208, 45.727698858067], [12.227358342644, 45.727698858067], [12.227358342644, 43.629703212193], [13.959679663208, 43.629703212193], [13.959679663208, 45.727698858067],
                ],
            ],
        ];
    }
}
