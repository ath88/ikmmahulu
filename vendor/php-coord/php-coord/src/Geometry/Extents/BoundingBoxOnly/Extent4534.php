<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - British Columbia - north Vancouver Is.
 * @internal
 */
class Extent4534
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-123.4987239514, 50.920369025479], [-128.49195165313, 50.920369025479], [-128.49195165313, 48.485721838574], [-123.4987239514, 48.485721838574], [-123.4987239514, 50.920369025479],
                ],
            ],
        ];
    }
}
