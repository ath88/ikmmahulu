<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Java and Java Sea - 108°E to 114°E.
 * @internal
 */
class Extent1586
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [114, -4.2774317645322], [108, -4.2774317645322], [108, -8.6655220959061], [114, -8.6655220959061], [114, -4.2774317645322],
                ],
            ],
        ];
    }
}
