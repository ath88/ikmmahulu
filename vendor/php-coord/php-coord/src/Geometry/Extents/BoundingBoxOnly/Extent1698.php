<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines - zone I.
 * @internal
 */
class Extent1698
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [118, 18.633587129809], [116.041076608, 18.633587129809], [116.041076608, 6.8242899630683], [118, 6.8242899630683], [118, 18.633587129809],
                ],
            ],
            [
                [
                    [118, 6.5940411115322], [117.86730661, 6.5940411115322], [117.86730661, 6.2100852443713], [118, 6.2100852443713], [118, 6.5940411115322],
                ],
            ],
        ];
    }
}
