<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Azerbaijan - coastal area Baku to Astara.
 * @internal
 */
class Extent2594
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [50.394678917974, 40.327391054257], [48.93414009982, 40.327391054257], [48.93414009982, 38.311596596512], [50.394678917974, 38.311596596512], [50.394678917974, 40.327391054257],
                ],
            ],
        ];
    }
}
