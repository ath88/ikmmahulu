<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - North Rona onshore.
 * @internal
 */
class Extent2798
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.732259413097, 59.185135478251], [-5.9187429309085, 59.185135478251], [-5.9187429309085, 59.07449230777], [-5.732259413097, 59.07449230777], [-5.732259413097, 59.185135478251],
                ],
            ],
        ];
    }
}
