<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Taiwan.
 * @internal
 */
class Extent1228
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [123.609937615, 26.950310141], [114.324549606, 26.950310141], [114.324549606, 17.366870132], [123.609937615, 17.366870132], [123.609937615, 26.950310141],
                ],
            ],
        ];
    }
}
