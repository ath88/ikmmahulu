<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 126°E to 132°E.
 * @internal
 */
class Extent3960
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [132, 38.620321079043], [126, 38.620321079043], [126, 21.128454135], [132, 21.128454135], [132, 38.620321079043],
                ],
            ],
        ];
    }
}
