<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - Pulau Pinang.
 * @internal
 */
class Extent3381
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [100.55039978027, 5.5862664277103], [100.1297160343, 5.5862664277103], [100.1297160343, 5.1297698020936], [100.55039978027, 5.1297698020936], [100.55039978027, 5.5862664277103],
                ],
            ],
        ];
    }
}
