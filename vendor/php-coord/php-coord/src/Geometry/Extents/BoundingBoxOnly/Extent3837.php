<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Peru - 84°W to 78°W.
 * @internal
 */
class Extent3837
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78, -3.1194667816162], [-84, -3.1194667816162], [-84, -17.326360702515], [-78, -17.326360702515], [-78, -3.1194667816162],
                ],
            ],
        ];
    }
}
