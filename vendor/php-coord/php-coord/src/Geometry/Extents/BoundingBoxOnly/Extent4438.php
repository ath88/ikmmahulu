<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Barrow.
 * @internal
 */
class Extent4438
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [115.58333333333, -20.216666666667], [114.9, -20.216666666667], [114.9, -22.2], [115.58333333333, -22.2], [115.58333333333, -20.216666666667],
                ],
            ],
        ];
    }
}
