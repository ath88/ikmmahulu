<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Jackson.
 * @internal
 */
class Extent4343
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.312527779558, 44.596914851638], [-91.166305734094, 44.596914851638], [-91.166305734094, 44.070893640755], [-90.312527779558, 44.070893640755], [-90.312527779558, 44.596914851638],
                ],
            ],
        ];
    }
}
