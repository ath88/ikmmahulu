<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Zhemgang district.
 * @internal
 */
class Extent3761
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.185059658316, 27.388110800163], [90.533940671691, 27.388110800163], [90.533940671691, 26.77586059184], [91.185059658316, 26.77586059184], [91.185059658316, 27.388110800163],
                ],
            ],
        ];
    }
}
