<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - West Virginia - SPCS - N.
 * @internal
 */
class Extent2264
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-77.727467480919, 40.637203177913], [-81.758910326595, 40.637203177913], [-81.758910326595, 38.762053671554], [-77.727467480919, 38.762053671554], [-77.727467480919, 40.637203177913],
                ],
            ],
        ];
    }
}
