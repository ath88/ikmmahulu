<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Suriname - onshore.
 * @internal
 */
class Extent3312
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-53.959300994873, 6.0520305633545], [-58.071395874023, 6.0520305633545], [-58.071395874023, 1.8362522125244], [-53.959300994873, 1.8362522125244], [-53.959300994873, 6.0520305633545],
                ],
            ],
        ];
    }
}
