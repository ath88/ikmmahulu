<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Papua New Guinea - 144°E to 150°E.
 * @internal
 */
class Extent3885
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [150, 2.5743051180004], [144, 2.5743051180004], [144, -13.876721534028], [150, -13.876721534028], [150, 2.5743051180004],
                ],
            ],
        ];
    }
}
