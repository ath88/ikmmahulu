<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - East Sind.
 * @internal
 */
class Extent2983
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [71.134101867676, 28.60189662315], [68.27094228766, 28.60189662315], [68.27094228766, 24.164199829102], [71.134101867676, 24.164199829102], [71.134101867676, 28.60189662315],
                ],
            ],
        ];
    }
}
