<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New Mexico.
 * @internal
 */
class Extent1400
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-102.99740099902, 36.999760183727], [-109.05072784424, 36.999760183727], [-109.05072784424, 31.332603451188], [-102.99740099902, 31.332603451188], [-102.99740099902, 36.999760183727],
                ],
            ],
        ];
    }
}
