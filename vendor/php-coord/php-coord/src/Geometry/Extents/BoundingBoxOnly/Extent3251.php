<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Georgia - onshore.
 * @internal
 */
class Extent3251
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [46.710815429688, 43.584716796875], [39.995952861748, 43.584716796875], [39.995952861748, 41.048042297363], [46.710815429688, 41.048042297363], [46.710815429688, 43.584716796875],
                ],
            ],
        ];
    }
}
