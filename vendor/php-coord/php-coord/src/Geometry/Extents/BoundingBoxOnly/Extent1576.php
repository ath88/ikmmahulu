<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Botswana, Zambia and Zimbabwe - 24°E to 30°E.
 * @internal
 */
class Extent1576
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, -8.3102701528334], [24, -8.3102701528334], [24, -25.832225799561], [30, -25.832225799561], [30, -8.3102701528334],
                ],
            ],
        ];
    }
}
