<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/St Helena - St Helena Island.
 * @internal
 */
class Extent3183
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.5950117324745, -15.854617848413], [-5.8430612783534, -15.854617848413], [-5.8430612783534, -16.071074270017], [-5.5950117324745, -16.071074270017], [-5.5950117324745, -15.854617848413],
                ],
            ],
        ];
    }
}
