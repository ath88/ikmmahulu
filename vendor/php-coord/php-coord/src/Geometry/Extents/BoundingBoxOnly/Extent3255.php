<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Guam - onshore.
 * @internal
 */
class Extent3255
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [145.00462630025, 13.698994382618], [144.58288440149, 13.698994382618], [144.58288440149, 13.188263429323], [145.00462630025, 13.188263429323], [145.00462630025, 13.698994382618],
                ],
            ],
        ];
    }
}
