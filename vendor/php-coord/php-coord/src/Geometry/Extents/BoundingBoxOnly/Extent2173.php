<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - GoM OCS - 90°W to 84°W.
 * @internal
 */
class Extent2173
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-83.919926363401, 30.246306685173], [-90.00180060424, 30.246306685173], [-90.00180060424, 23.957833031565], [-83.919926363401, 23.957833031565], [-83.919926363401, 30.246306685173],
                ],
            ],
        ];
    }
}
