<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - Grande Terre - Noumea.
 * @internal
 */
class Extent2823
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [166.53214092497, -22.19941549356], [166.35465531005, -22.19941549356], [166.35465531005, -22.364831706869], [166.53214092497, -22.364831706869], [166.53214092497, -22.19941549356],
                ],
            ],
        ];
    }
}
