<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 120°E to 126°E, N hemisphere.
 * @internal
 */
class Extent1657
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [126, 5.4757875126779], [120, 5.4757875126779], [120, 0], [126, 0], [126, 5.4757875126779],
                ],
            ],
        ];
    }
}
