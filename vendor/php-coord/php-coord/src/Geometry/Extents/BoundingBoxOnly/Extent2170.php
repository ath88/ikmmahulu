<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arkansas - SPCS - S.
 * @internal
 */
class Extent2170
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.404030602704, 35.095677661084], [-94.476691262309, 35.095677661084], [-94.476691262309, 33.010151549578], [-90.404030602704, 33.010151549578], [-90.404030602704, 35.095677661084],
                ],
            ],
        ];
    }
}
