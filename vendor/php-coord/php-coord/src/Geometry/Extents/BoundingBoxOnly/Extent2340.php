<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Sicily onshore.
 * @internal
 */
class Extent2340
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.70074199913, 38.347021836871], [12.368174451313, 38.347021836871], [12.368174451313, 36.599062651922], [15.70074199913, 36.599062651922], [15.70074199913, 38.347021836871],
                ],
            ],
        ];
    }
}
