<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Northeast.
 * @internal
 */
class Extent4392
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [97.415161132813, 29.463344573975], [89.694839477539, 29.463344573975], [89.694839477539, 21.946229934692], [97.415161132813, 21.946229934692], [97.415161132813, 29.463344573975],
                ],
            ],
        ];
    }
}
