<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia region 4.
 * @internal
 */
class Extent3085
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, 9.3999996185303], [-74.39999961853, 9.3999996185303], [-74.39999961853, 5.0000019073486], [-72, 5.0000019073486], [-72, 9.3999996185303],
                ],
            ],
        ];
    }
}
