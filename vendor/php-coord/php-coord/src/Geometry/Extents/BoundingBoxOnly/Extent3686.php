<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - mainland and offshore Caribbean.
 * @internal
 */
class Extent3686
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.870452880859, 13.674841145725], [-79.098392486572, 13.674841145725], [-79.098392486572, -4.2279938238069], [-66.870452880859, -4.2279938238069], [-66.870452880859, 13.674841145725],
                ],
            ],
        ];
    }
}
