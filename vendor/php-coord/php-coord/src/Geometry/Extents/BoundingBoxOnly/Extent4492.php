<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - Adelie Land coastal area east of 138°E.
 * @internal
 */
class Extent4492
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142, -66.108621136224], [138, -66.108621136224], [138, -67.128750020038], [142, -67.128750020038], [142, -66.108621136224],
                ],
            ],
        ];
    }
}
