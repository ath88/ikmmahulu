<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - Snares and Auckland Islands.
 * @internal
 */
class Extent3554
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [166.92875194742, -47.808635996531], [166.19141941383, -47.808635996531], [166.19141941383, -48.265724103902], [166.92875194742, -48.265724103902], [166.92875194742, -47.808635996531],
                ],
            ],
            [
                [
                    [166.67649981948, -50.285681514909], [165.55327243047, -50.285681514909], [165.55327243047, -51.1287299593], [166.67649981948, -51.1287299593], [166.67649981948, -50.285681514909],
                ],
            ],
        ];
    }
}
