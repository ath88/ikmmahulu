<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Kewaunee, Manitowoc and Sheboygan.
 * @internal
 */
class Extent4371
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.375067536313, 44.676972618105], [-88.162229927347, 44.676972618105], [-88.162229927347, 43.542421809279], [-87.375067536313, 43.542421809279], [-87.375067536313, 44.676972618105],
                ],
            ],
        ];
    }
}
