<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - 138°E to 144°E.
 * @internal
 */
class Extent1562
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [144, -9.0880124819998], [138, -9.0880124819998], [138, -48.181383119729], [144, -48.181383119729], [144, -9.0880124819998],
                ],
            ],
        ];
    }
}
