<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Oman - block 4.
 * @internal
 */
class Extent2404
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [59.019635266778, 21.166667], [56.5, 21.166667], [56.5, 19.583333], [59.019635266778, 19.583333], [59.019635266778, 21.166667],
                ],
            ],
        ];
    }
}
