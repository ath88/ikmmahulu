<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - Angola proper.
 * @internal
 */
class Extent1288
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.084442138672, -5.8277087211607], [8.2018785080004, -5.8277087211607], [8.2018785080004, -18.01639175415], [24.084442138672, -18.01639175415], [24.084442138672, -5.8277087211607],
                ],
            ],
        ];
    }
}
