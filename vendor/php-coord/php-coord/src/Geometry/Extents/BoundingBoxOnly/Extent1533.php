<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Prince Edward Island.
 * @internal
 */
class Extent1533
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.902259795466, 47.08724966936], [-64.488328900612, 47.08724966936], [-64.488328900612, 45.905612713677], [-61.902259795466, 45.905612713677], [-61.902259795466, 47.08724966936],
                ],
            ],
        ];
    }
}
