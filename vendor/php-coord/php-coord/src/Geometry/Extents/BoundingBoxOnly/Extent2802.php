<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Scilly Isles onshore.
 * @internal
 */
class Extent2802
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-6.236685, 49.984625], [-6.40923, 49.984625], [-6.40923, 49.868691], [-6.236685, 49.868691], [-6.236685, 49.984625],
                ],
            ],
        ];
    }
}
