<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Africa - Botswana, Eswatini, Lesotho and South Africa.
 * @internal
 */
class Extent1290
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [32.942251205444, -17.782085418701], [16.451303482056, -17.782085418701], [16.451303482056, -34.872043609619], [32.942251205444, -34.872043609619], [32.942251205444, -17.782085418701],
                ],
            ],
        ];
    }
}
