<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Hokitika mc.
 * @internal
 */
class Extent3789
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [171.88771158, -42.41915415735], [170.39554725493, -42.41915415735], [170.39554725493, -43.22662146], [171.88771158, -43.22662146], [171.88771158, -42.41915415735],
                ],
            ],
        ];
    }
}
