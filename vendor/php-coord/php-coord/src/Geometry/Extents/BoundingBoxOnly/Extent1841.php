<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - 16°E to 18°E.
 * @internal
 */
class Extent1841
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.000000389126, -17.386175032976], [15.999996033683, -17.386175032976], [15.999996033683, -28.825938087284], [18.000000389126, -28.825938087284], [18.000000389126, -17.386175032976],
                ],
            ],
        ];
    }
}
