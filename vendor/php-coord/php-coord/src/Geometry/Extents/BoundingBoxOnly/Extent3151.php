<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 13°E to 15°E.
 * @internal
 */
class Extent3151
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.00000011659, -4.2827782630918], [13.000001397815, -4.2827782630918], [13.000001397815, -5.9080562591553], [15.00000011659, -5.9080562591553], [15.00000011659, -4.2827782630918],
                ],
            ],
        ];
    }
}
