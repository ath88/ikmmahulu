<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Bartholomew.
 * @internal
 */
class Extent4302
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.684736000003, 39.350067000014], [-86.08560099999, 39.350067000014], [-86.08560099999, 39.035863999976], [-85.684736000003, 39.035863999976], [-85.684736000003, 39.350067000014],
                ],
            ],
        ];
    }
}
