<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Siskiyou Pass.
 * @internal
 */
class Extent4463
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.9639348158, 42.453706553821], [-122.70488143121, 42.453706553821], [-122.70488143121, 41.958264754836], [-121.9639348158, 41.958264754836], [-121.9639348158, 42.453706553821],
                ],
            ],
        ];
    }
}
