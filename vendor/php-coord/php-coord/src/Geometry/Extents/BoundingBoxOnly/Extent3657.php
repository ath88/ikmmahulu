<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 16ºE to 17ºE.
 * @internal
 */
class Extent3657
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [17, 69.447220676808], [16.630573126593, 69.447220676808], [16.630573126593, 68.956271640413], [17, 68.956271640413], [17, 69.447220676808],
                ],
            ],
            [
                [
                    [17, 69.041212820219], [16, 69.041212820219], [16, 66.883444389896], [17, 66.883444389896], [17, 69.041212820219],
                ],
            ],
            [
                [
                    [16.29299721915, 69.362605047413], [16, 69.362605047413], [16, 69.015518287602], [16.29299721915, 69.015518287602], [16.29299721915, 69.362605047413],
                ],
            ],
        ];
    }
}
