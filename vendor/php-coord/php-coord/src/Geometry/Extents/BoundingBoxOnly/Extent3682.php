<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores - west of 30°W.
 * @internal
 */
class Extent3682
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-30, 43.067630156], [-35.579299533, 43.067630156], [-35.579299533, 35.25808676532], [-30, 35.25808676532], [-30, 43.067630156],
                ],
            ],
        ];
    }
}
