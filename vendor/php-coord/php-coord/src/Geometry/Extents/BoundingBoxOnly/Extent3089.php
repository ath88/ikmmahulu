<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia region 8.
 * @internal
 */
class Extent3089
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.870452880859, 7.0977268430002], [-74.39999961853, 7.0977268430002], [-74.39999961853, -4.2279940424075], [-66.870452880859, -4.2279940424075], [-66.870452880859, 7.0977268430002],
                ],
            ],
        ];
    }
}
