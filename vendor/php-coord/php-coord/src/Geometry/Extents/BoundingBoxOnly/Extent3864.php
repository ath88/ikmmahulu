<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/North America - 126°W to 120°W and NAD83 by country.
 * @internal
 */
class Extent3864
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-120, 81.798763834903], [-126, 81.798763834903], [-126, 30.542000143946], [-120, 30.542000143946], [-120, 81.798763834903],
                ],
            ],
        ];
    }
}
