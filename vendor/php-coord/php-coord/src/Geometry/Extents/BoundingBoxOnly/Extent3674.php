<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 29ºE to 30ºE.
 * @internal
 */
class Extent3674
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, 70.927835209975], [29, 70.927835209975], [29, 69.028739929199], [30, 69.028739929199], [30, 70.927835209975],
                ],
            ],
        ];
    }
}
