<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - zone VI.
 * @internal
 */
class Extent1746
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.889583333, 70.805023704351], [18.889583333, 70.805023704351], [18.889583333, 68.337074279785], [22.889583333, 68.337074279785], [22.889583333, 70.805023704351],
                ],
            ],
        ];
    }
}
