<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Tsirang district.
 * @internal
 */
class Extent3757
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [90.349504222567, 27.191756141044], [90.008664311223, 27.191756141044], [90.008664311223, 26.817285664467], [90.349504222567, 26.817285664467], [90.349504222567, 27.191756141044],
                ],
            ],
        ];
    }
}
