<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Timaru mc.
 * @internal
 */
class Extent3793
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [171.54153018774, -43.35315849], [169.82900262, -43.35315849], [169.82900262, -44.974247170518], [171.54153018774, -44.974247170518], [171.54153018774, -43.35315849],
                ],
            ],
        ];
    }
}
