<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska - St. Lawrence Island.
 * @internal
 */
class Extent1332
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-168.59025692553, 63.834352691777], [-171.96211001282, 63.834352691777], [-171.96211001282, 62.89984065183], [-168.59025692553, 62.89984065183], [-168.59025692553, 63.834352691777],
                ],
            ],
        ];
    }
}
