<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/French Southern Territories - east of 78°E.
 * @internal
 */
class Extent4252
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [81.820772576, -34.509254723385], [78, -34.509254723385], [78, -42.039242563961], [81.820772576, -42.039242563961], [81.820772576, -34.509254723385],
                ],
            ],
        ];
    }
}
