<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Iowa.
 * @internal
 */
class Extent4369
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.837998319851, 43.209766004484], [-90.429984352824, 43.209766004484], [-90.429984352824, 42.813031215745], [-89.837998319851, 42.813031215745], [-89.837998319851, 43.209766004484],
                ],
            ],
        ];
    }
}
