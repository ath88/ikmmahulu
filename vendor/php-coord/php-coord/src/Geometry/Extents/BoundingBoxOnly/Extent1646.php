<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - Estonia; Latvia; Lithuania.
 * @internal
 */
class Extent1646
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28.235967636109, 59.994633172029], [19.022537517959, 59.994633172029], [19.022537517959, 53.890338897705], [28.235967636109, 53.890338897705], [28.235967636109, 59.994633172029],
                ],
            ],
        ];
    }
}
