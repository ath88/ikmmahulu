<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Bangladesh; India; Myanmar; Pakistan - zone Ilb.
 * @internal
 */
class Extent1671
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [101.16942596436, 29.463344573975], [82, 29.463344573975], [82, 21], [101.16942596436, 21], [101.16942596436, 29.463344573975],
                ],
            ],
            [
                [
                    [91.622152239155, 22.672392204355], [91.348913527782, 22.672392204355], [91.348913527782, 22.295853673039], [91.622152239155, 22.295853673039], [91.622152239155, 22.672392204355],
                ],
            ],
        ];
    }
}
