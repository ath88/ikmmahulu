<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Cuba.
 * @internal
 */
class Extent1077
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-73.572218569, 25.500339602881], [-87.009609527639, 25.500339602881], [-87.009609527639, 18.832222133], [-73.572218569, 18.832222133], [-73.572218569, 25.500339602881],
                ],
            ],
        ];
    }
}
