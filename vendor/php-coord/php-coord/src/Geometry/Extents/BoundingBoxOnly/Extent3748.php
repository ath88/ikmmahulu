<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Latin America - 120°W to 114°W.
 * @internal
 */
class Extent3748
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-114, 32.718458175659], [-120, 32.718458175659], [-120, 15.011625289917], [-114, 15.011625289917], [-114, 32.718458175659],
                ],
            ],
        ];
    }
}
