<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas - SPCS27 - S.
 * @internal
 */
class Extent2255
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.365963310294, 28.20391330063], [-100.19227559513, 28.20391330063], [-100.19227559513, 25.839860916138], [-95.365963310294, 25.839860916138], [-95.365963310294, 28.20391330063],
                ],
            ],
        ];
    }
}
