<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 42°N to 42°40'N; 141°E to 142°E.
 * @internal
 */
class Extent2446
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142, 42.666466666667], [141, 42.666466666667], [141, 42.248454388506], [142, 42.248454388506], [142, 42.666466666667],
                ],
            ],
        ];
    }
}
