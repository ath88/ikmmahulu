<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - McMurdo Sound region.
 * @internal
 */
class Extent3853
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [173, -76], [153, -76], [153, -81], [173, -81], [173, -76],
                ],
            ],
        ];
    }
}
