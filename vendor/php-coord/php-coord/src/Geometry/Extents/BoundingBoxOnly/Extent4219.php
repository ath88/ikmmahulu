<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Mason City.
 * @internal
 */
class Extent4219
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.604007, 43.501457434001], [-93.971482, 43.501457434001], [-93.971482, 42.906741], [-91.604007, 42.906741], [-91.604007, 43.501457434001],
                ],
            ],
        ];
    }
}
