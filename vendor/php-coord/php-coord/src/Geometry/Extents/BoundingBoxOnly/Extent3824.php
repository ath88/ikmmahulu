<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - Gongola Basin.
 * @internal
 */
class Extent3824
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.12118, 11.62504], [9.41228, 11.62504], [9.41228, 8.78269], [12.12118, 8.78269], [12.12118, 11.62504],
                ],
            ],
        ];
    }
}
