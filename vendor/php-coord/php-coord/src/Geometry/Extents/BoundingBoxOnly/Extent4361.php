<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Calumet, Fond du Lac, Outagamie and Winnebago.
 * @internal
 */
class Extent4361
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.041607007942, 44.590677234867], [-88.886718567257, 44.590677234867], [-88.886718567257, 43.542853309959], [-88.041607007942, 43.542853309959], [-88.041607007942, 44.590677234867],
                ],
            ],
        ];
    }
}
