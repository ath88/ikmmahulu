<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Campos; Espirito Santo and Santos basins.
 * @internal
 */
class Extent2307
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-35.18865776062, -17.592367172241], [-48.793334960938, -17.592367172241], [-48.793334960938, -28.401002883911], [-35.18865776062, -28.401002883911], [-35.18865776062, -17.592367172241],
                ],
            ],
        ];
    }
}
