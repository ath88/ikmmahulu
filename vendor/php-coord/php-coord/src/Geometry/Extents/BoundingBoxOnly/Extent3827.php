<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Bolivia - west of 66°W.
 * @internal
 */
class Extent3827
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66, -9.7755565643311], [-69.656188964844, -9.7755565643311], [-69.656188964844, -22.901111602783], [-66, -22.901111602783], [-66, -9.7755565643311],
                ],
            ],
        ];
    }
}
