<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/French Guiana - coastal area.
 * @internal
 */
class Extent3105
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-51.616205215454, 5.8056468963623], [-54.44966506958, 5.8056468963623], [-54.44966506958, 3.4336261749268], [-51.616205215454, 3.4336261749268], [-51.616205215454, 5.8056468963623],
                ],
            ],
        ];
    }
}
