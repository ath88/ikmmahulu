<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - mainland.
 * @internal
 */
class Extent3229
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.870452880859, 12.513334274292], [-79.098392486572, 12.513334274292], [-79.098392486572, -4.2279940424075], [-66.870452880859, -4.2279940424075], [-66.870452880859, 12.513334274292],
                ],
            ],
        ];
    }
}
