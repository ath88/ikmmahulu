<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Columbia River West.
 * @internal
 */
class Extent4202
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.47117748136, 46.553480930995], [-124.32971422862, 46.553480930995], [-124.32971422862, 45.172979036413], [-121.47117748136, 45.172979036413], [-121.47117748136, 46.553480930995],
                ],
            ],
        ];
    }
}
