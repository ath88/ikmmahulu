<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 34°40N' to 35°20'N; 135°E to 136°E.
 * @internal
 */
class Extent2493
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [136, 35.332766666667], [135, 35.332766666667], [135, 34.666066666667], [136, 34.666066666667], [136, 35.332766666667],
                ],
            ],
        ];
    }
}
