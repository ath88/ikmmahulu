<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - offshore - Bei Bu.
 * @internal
 */
class Extent3561
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [110.16690141505, 21.683578453365], [107.1594446, 21.683578453365], [107.1594446, 17.816047017172], [110.16690141505, 17.816047017172], [110.16690141505, 21.683578453365],
                ],
            ],
        ];
    }
}
