<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Guadeloupe - St Martin - onshore.
 * @internal
 */
class Extent2890
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-62.963245454304, 18.167318067415], [-63.204824578387, 18.167318067415], [-63.204824578387, 18.016445594798], [-62.963245454304, 18.016445594798], [-62.963245454304, 18.167318067415],
                ],
            ],
        ];
    }
}
