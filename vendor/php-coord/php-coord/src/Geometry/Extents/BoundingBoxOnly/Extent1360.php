<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Kalimantan E.
 * @internal
 */
class Extent1360
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [119.05724835258, 4.2869301613658], [114.55614397811, 4.2869301613658], [114.55614397811, -4.2344223765284], [119.05724835258, -4.2344223765284], [119.05724835258, 4.2869301613658],
                ],
            ],
        ];
    }
}
