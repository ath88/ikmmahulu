<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Pelotas.
 * @internal
 */
class Extent2965
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-44.71445274353, -28.119760513306], [-53.374298095703, -28.119760513306], [-53.374298095703, -35.708560943604], [-44.71445274353, -35.708560943604], [-44.71445274353, -28.119760513306],
                ],
            ],
        ];
    }
}
