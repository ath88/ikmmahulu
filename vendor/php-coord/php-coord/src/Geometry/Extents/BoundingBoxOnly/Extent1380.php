<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Georgia.
 * @internal
 */
class Extent1380
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-80.776805877685, 35.000366708131], [-85.60896021625, 35.000366708131], [-85.60896021625, 30.361291834126], [-80.776805877685, 30.361291834126], [-80.776805877685, 35.000366708131],
                ],
            ],
        ];
    }
}
