<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Posey.
 * @internal
 */
class Extent4275
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.688191000008, 38.230312999998], [-88.058499371214, 38.230312999998], [-88.058499371214, 37.771742000004], [-87.688191000008, 37.771742000004], [-87.688191000008, 38.230312999998],
                ],
            ],
        ];
    }
}
