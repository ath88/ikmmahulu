<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - onshore and EEZ.
 * @internal
 */
class Extent1036
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [163.192086652, -8.8818808919997], [109.233479602, -8.8818808919997], [109.233479602, -47.193634928], [163.192086652, -47.193634928], [163.192086652, -8.8818808919997],
                ],
            ],
        ];
    }
}
