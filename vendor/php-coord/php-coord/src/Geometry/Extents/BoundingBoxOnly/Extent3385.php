<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Finland - east of 31.5°E.
 * @internal
 */
class Extent3385
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [31.58196258545, 62.995045939351], [31.5, 62.995045939351], [31.5, 62.83438575901], [31.58196258545, 62.83438575901], [31.58196258545, 62.995045939351],
                ],
            ],
        ];
    }
}
