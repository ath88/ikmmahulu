<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - 108°E to 114°E (EEZ).
 * @internal
 */
class Extent1557
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [114, -17.194024334999], [109.233479602, -17.194024334999], [109.233479602, -37.832136354074], [114, -37.832136354074], [114, -17.194024334999],
                ],
            ],
        ];
    }
}
