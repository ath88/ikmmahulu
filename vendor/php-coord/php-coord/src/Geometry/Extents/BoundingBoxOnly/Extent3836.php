<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Peru - east of 72°W.
 * @internal
 */
class Extent3836
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-68.673904418945, -9.4280014038085], [-72, -9.4280014038085], [-72, -20.436101913452], [-68.673904418945, -20.436101913452], [-68.673904418945, -9.4280014038085],
                ],
            ],
            [
                [
                    [-69.947516273694, -2.1479167937237], [-72, -2.1479167937237], [-72, -4.591425423141], [-69.947516273694, -4.591425423141], [-69.947516273694, -2.1479167937237],
                ],
            ],
        ];
    }
}
