<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Illinois.
 * @internal
 */
class Extent1382
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.020687037627, 42.508394227081], [-91.516284050609, 42.508394227081], [-91.516284050609, 36.986822280657], [-87.020687037627, 36.986822280657], [-87.020687037627, 42.508394227081],
                ],
            ],
        ];
    }
}
