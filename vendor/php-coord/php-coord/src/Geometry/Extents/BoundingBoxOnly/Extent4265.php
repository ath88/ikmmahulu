<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Clay.
 * @internal
 */
class Extent4265
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.939981000002, 39.607136999998], [-87.240736999996, 39.607136999998], [-87.240736999996, 39.167950999986], [-86.939981000002, 39.167950999986], [-86.939981000002, 39.607136999998],
                ],
            ],
        ];
    }
}
