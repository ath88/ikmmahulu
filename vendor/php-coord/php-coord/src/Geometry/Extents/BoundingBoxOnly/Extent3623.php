<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 25°E to 27°E.
 * @internal
 */
class Extent3623
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [27, -4.9943834938083], [25, -4.9943834938083], [25, -11.989168167114], [27, -11.989168167114], [27, -4.9943834938083],
                ],
            ],
        ];
    }
}
