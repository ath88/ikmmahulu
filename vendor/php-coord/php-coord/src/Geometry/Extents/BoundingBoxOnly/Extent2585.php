<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Pohorje.
 * @internal
 */
class Extent2585
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.5387066729, 46.579776428266], [15.151939373376, 46.579776428266], [15.151939373376, 46.287210963177], [15.5387066729, 46.287210963177], [15.5387066729, 46.579776428266],
                ],
            ],
        ];
    }
}
