<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Haryana.
 * @internal
 */
class Extent4401
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [77.592018127442, 30.930379867554], [74.465263366699, 30.930379867554], [74.465263366699, 27.655969619751], [77.592018127442, 27.655969619751], [77.592018127442, 30.930379867554],
                ],
            ],
        ];
    }
}
