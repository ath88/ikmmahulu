<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Bermuda.
 * @internal
 */
class Extent1047
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60.705473557, 35.722388149], [-68.826780564, 35.722388149], [-68.826780564, 28.913212143], [-60.705473557, 28.913212143], [-60.705473557, 35.722388149],
                ],
            ],
        ];
    }
}
