<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - east of 133.5°E.
 * @internal
 */
class Extent2731
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [134.76846313477, 48.391120910645], [133.5, 48.391120910645], [133.5, 45.855344696717], [134.76846313477, 45.855344696717], [134.76846313477, 48.391120910645],
                ],
            ],
        ];
    }
}
