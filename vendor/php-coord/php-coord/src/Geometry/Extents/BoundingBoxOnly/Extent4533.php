<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - British Columbia - CRD.
 * @internal
 */
class Extent4533
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-123.009065329, 49.052463617768], [-124.51084339062, 49.052463617768], [-124.51084339062, 48.252279520576], [-123.009065329, 48.252279520576], [-123.009065329, 49.052463617768],
                ],
            ],
        ];
    }
}
