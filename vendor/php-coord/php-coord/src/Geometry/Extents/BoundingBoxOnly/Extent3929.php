<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mozambique - west of 36°E.
 * @internal
 */
class Extent3929
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [36, -11.416875839233], [30.21301651001, -11.416875839233], [30.21301651001, -27.576717558202], [36, -27.576717558202], [36, -11.416875839233],
                ],
            ],
        ];
    }
}
