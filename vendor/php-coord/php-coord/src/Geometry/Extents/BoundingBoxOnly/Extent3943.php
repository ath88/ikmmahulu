<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bahrain - onshore.
 * @internal
 */
class Extent3943
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [50.844433899114, 25.774399846668], [50.686646495478, 25.774399846668], [50.686646495478, 25.535000140001], [50.844433899114, 25.535000140001], [50.844433899114, 25.774399846668],
                ],
            ],
            [
                [
                    [50.709553428133, 26.335950098032], [50.398108120008, 26.335950098032], [50.398108120008, 25.762645980493], [50.709553428133, 25.762645980493], [50.709553428133, 26.335950098032],
                ],
            ],
        ];
    }
}
