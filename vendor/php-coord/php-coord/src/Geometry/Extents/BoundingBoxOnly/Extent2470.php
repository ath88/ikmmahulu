<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 37°20'N to 38°N; 141°E to 142°E.
 * @internal
 */
class Extent2470
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.10182532476, 37.863104299607], [141, 37.863104299607], [141, 37.332866666667], [141.10182532476, 37.332866666667], [141.10182532476, 37.863104299607],
                ],
            ],
        ];
    }
}
