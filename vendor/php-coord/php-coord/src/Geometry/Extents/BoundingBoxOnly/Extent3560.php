<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Dolenjska - central.
 * @internal
 */
class Extent3560
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.42372701104, 46.006447067851], [14.973873652437, 46.006447067851], [14.973873652437, 45.814220800918], [15.42372701104, 45.814220800918], [15.42372701104, 46.006447067851],
                ],
            ],
        ];
    }
}
