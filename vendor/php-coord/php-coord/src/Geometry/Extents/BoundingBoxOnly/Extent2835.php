<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 15 00.
 * @internal
 */
class Extent2835
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.143320281571, 61.613361426102], [13.547092376657, 61.613361426102], [13.547092376657, 55.950338886076], [16.143320281571, 55.950338886076], [16.143320281571, 61.613361426102],
                ],
            ],
        ];
    }
}
