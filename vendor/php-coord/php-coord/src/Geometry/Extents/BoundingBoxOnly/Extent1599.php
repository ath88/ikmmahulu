<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - 75°35'W to 72°35'W.
 * @internal
 */
class Extent1599
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72.583000183105, 11.816415786743], [-75.583000183105, 11.816415786743], [-75.583000183105, -2.5063896179199], [-72.583000183105, -2.5063896179199], [-72.583000183105, 11.816415786743],
                ],
            ],
        ];
    }
}
