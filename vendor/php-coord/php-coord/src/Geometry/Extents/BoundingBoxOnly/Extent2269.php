<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wyoming - SPCS - E.
 * @internal
 */
class Extent2269
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.05170553525, 45.00109148433], [-106.32911805306, 45.00109148433], [-106.32911805306, 40.994289143578], [-104.05170553525, 40.994289143578], [-104.05170553525, 45.00109148433],
                ],
            ],
        ];
    }
}
