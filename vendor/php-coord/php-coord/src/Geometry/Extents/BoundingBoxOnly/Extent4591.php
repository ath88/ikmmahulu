<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - Lanzarote.
 * @internal
 */
class Extent4591
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-13.371389389038, 29.466936779776], [-13.940210086775, 29.466936779776], [-13.940210086775, 28.789266652891], [-13.371389389038, 28.789266652891], [-13.371389389038, 29.466936779776],
                ],
            ],
        ];
    }
}
