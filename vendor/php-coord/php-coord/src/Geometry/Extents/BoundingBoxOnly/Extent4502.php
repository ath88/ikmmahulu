<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Emporia.
 * @internal
 */
class Extent4502
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.945661, 38.870352], [-96.933924, 38.870352], [-96.933924, 38.085554], [-95.945661, 38.085554], [-95.945661, 38.870352],
                ],
            ],
        ];
    }
}
