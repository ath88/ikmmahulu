<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arkansas.
 * @internal
 */
class Extent1374
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.646304468239, 36.492811128552], [-94.617257095851, 36.492811128552], [-94.617257095851, 33.010151549578], [-89.646304468239, 33.010151549578], [-89.646304468239, 36.492811128552],
                ],
            ],
        ];
    }
}
