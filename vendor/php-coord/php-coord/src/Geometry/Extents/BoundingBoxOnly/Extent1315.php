<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mozambique - west - Tete province.
 * @internal
 */
class Extent1315
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [35.368370056153, -14.012565612793], [30.21301651001, -14.012565612793], [30.21301651001, -17.758169174194], [35.368370056153, -17.758169174194], [35.368370056153, -14.012565612793],
                ],
            ],
        ];
    }
}
