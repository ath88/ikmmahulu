<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 144°E to 150°E onshore.
 * @internal
 */
class Extent2955
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [145.86544719357, 44.392852845077], [144, 44.392852845077], [144, 42.849354336787], [145.86544719357, 42.849354336787], [145.86544719357, 44.392852845077],
                ],
            ],
        ];
    }
}
