<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 36°N to 36°40'N; 137°E to 138°E.
 * @internal
 */
class Extent2477
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [138, 36.666166666667], [137, 36.666166666667], [137, 35.999466666667], [138, 35.999466666667], [138, 36.666166666667],
                ],
            ],
        ];
    }
}
