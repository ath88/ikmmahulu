<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Dearborn, Ohio, Switzerland.
 * @internal
 */
class Extent4306
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.784579000003, 39.307053999998], [-85.203588999977, 39.307053999998], [-85.203588999977, 38.687609000003], [-84.784579000003, 38.687609000003], [-84.784579000003, 39.307053999998],
                ],
            ],
        ];
    }
}
