<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - offshore Indus.
 * @internal
 */
class Extent2985
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [68.233236, 25.383642], [64.000432, 25.383642], [64.000432, 21.059143135], [68.233236, 21.059143135], [68.233236, 25.383642],
                ],
            ],
        ];
    }
}
