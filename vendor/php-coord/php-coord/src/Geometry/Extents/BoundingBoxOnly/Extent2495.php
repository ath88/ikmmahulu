<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 34°40'N to 35°20'N; 137°E to 138°E.
 * @internal
 */
class Extent2495
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [138, 35.332766666667], [137, 35.332766666667], [137, 34.666066666667], [138, 34.666066666667], [138, 35.332766666667],
                ],
            ],
        ];
    }
}
