<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Sachsen - west of 13.5°E.
 * @internal
 */
class Extent3395
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.5, 51.658332824707], [11.896389961243, 51.658332824707], [11.896389961243, 50.206665039062], [13.5, 50.206665039062], [13.5, 51.658332824707],
                ],
            ],
        ];
    }
}
