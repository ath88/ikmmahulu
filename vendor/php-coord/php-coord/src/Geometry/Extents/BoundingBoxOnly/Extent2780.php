<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - East Malaysia - offshore SCS.
 * @internal
 */
class Extent2780
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [114.09764318389, 7.3625241230005], [109.315949602, 7.3625241230005], [109.315949602, 1.568692935796], [114.09764318389, 1.568692935796], [114.09764318389, 7.3625241230005],
                ],
            ],
            [
                [
                    [117.30370291423, 7.6624381230005], [113.762615606, 7.6624381230005], [113.762615606, 4.9465663509974], [117.30370291423, 4.9465663509974], [117.30370291423, 7.6624381230005],
                ],
            ],
        ];
    }
}
