<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 6th parallel south 29°E to 31°E.
 * @internal
 */
class Extent3167
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.631664276123, -4.3496500413833], [29, -4.3496500413833], [29, -6.0356455032504], [29.631664276123, -6.0356455032504], [29.631664276123, -4.3496500413833],
                ],
            ],
        ];
    }
}
