<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Taiwan - 118°E to 120°E.
 * @internal
 */
class Extent3563
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [120, 24.645543268924], [118, 24.645543268924], [118, 18.633587129809], [120, 18.633587129809], [120, 24.645543268924],
                ],
            ],
        ];
    }
}
