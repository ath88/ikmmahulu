<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arizona - SPCS - W.
 * @internal
 */
class Extent2168
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-112.52326292753, 36.997994486444], [-114.80982971191, 36.997994486444], [-114.80982971191, 32.056336388326], [-112.52326292753, 32.056336388326], [-112.52326292753, 36.997994486444],
                ],
            ],
        ];
    }
}
