<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Syria - Deir area.
 * @internal
 */
class Extent2329
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.801442, 35.899503], [39.301395, 35.899503], [39.301395, 34.499543], [40.801442, 34.499543], [40.801442, 35.899503],
                ],
            ],
        ];
    }
}
