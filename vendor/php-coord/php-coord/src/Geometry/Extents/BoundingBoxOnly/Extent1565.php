<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - 156°E to 162°E.
 * @internal
 */
class Extent1565
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [162, -14.081677431735], [156, -14.081677431735], [156, -35.127326917], [162, -35.127326917], [162, -14.081677431735],
                ],
            ],
        ];
    }
}
