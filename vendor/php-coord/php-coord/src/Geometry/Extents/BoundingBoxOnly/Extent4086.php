<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 77°50'N to 72°50'N,  70°E to 94°E.
 * @internal
 */
class Extent4086
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [94.000001907349, 77.833333969116], [70.000001907349, 77.833333969116], [70.000001907349, 72.833333969116], [94.000001907349, 72.833333969116], [94.000001907349, 77.833333969116],
                ],
            ],
        ];
    }
}
