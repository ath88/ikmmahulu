<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Emilia-Romagna.
 * @internal
 */
class Extent4035
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.755932731946, 45.139077338283], [9.1983585259899, 45.139077338283], [9.1983585259899, 43.739946582414], [12.755932731946, 43.739946582414], [12.755932731946, 45.139077338283],
                ],
            ],
        ];
    }
}
