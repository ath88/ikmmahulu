<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 34°40'N to 35°20'N; 133°E to 134°E.
 * @internal
 */
class Extent2491
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [134, 35.332766666667], [133, 35.332766666667], [133, 34.666066666667], [134, 34.666066666667], [134, 35.332766666667],
                ],
            ],
        ];
    }
}
