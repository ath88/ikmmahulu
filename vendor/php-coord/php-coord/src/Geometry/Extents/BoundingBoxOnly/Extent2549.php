<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - AOF 10°W to 3.5°W.
 * @internal
 */
class Extent2549
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-3.4999999999999, 27.2904586792], [-10, 27.2904586792], [-10, 4.2956916735183], [-3.4999999999999, 4.2956916735183], [-3.4999999999999, 27.2904586792],
                ],
            ],
        ];
    }
}
