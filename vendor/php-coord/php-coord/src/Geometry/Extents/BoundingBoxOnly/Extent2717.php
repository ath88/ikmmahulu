<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - 91.5°E to 94.5°E.
 * @internal
 */
class Extent2717
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [94.5, 45.126197710922], [91.5, 45.126197710922], [91.5, 27.715101242065], [94.5, 27.715101242065], [94.5, 45.126197710922],
                ],
            ],
        ];
    }
}
