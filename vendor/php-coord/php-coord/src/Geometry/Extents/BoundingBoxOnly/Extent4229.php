<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/UAE - Abu Dhabi island.
 * @internal
 */
class Extent4229
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [54.700378644753, 24.630616095819], [54.204799401077, 24.630616095819], [54.204799401077, 24.24142046859], [54.700378644753, 24.24142046859], [54.700378644753, 24.630616095819],
                ],
            ],
        ];
    }
}
