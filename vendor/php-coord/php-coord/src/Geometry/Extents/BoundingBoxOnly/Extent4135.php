<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - Bogota D.C. city.
 * @internal
 */
class Extent4135
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-73.989999771118, 4.8400020599365], [-74.260000228882, 4.8400020599365], [-74.260000228882, 4.4500026702881], [-73.989999771118, 4.4500026702881], [-73.989999771118, 4.8400020599365],
                ],
            ],
        ];
    }
}
