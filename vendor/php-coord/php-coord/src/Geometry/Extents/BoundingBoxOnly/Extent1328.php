<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Kalimantan - Mahakam delta.
 * @internal
 */
class Extent1328
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [117.98918800739, -0.00019403745289992], [116.72716569158, -0.00019403745289992], [116.72716569158, -1.2350899503943], [117.98918800739, -1.2350899503943], [117.98918800739, -0.00019403745289992],
                ],
            ],
        ];
    }
}
