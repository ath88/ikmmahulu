<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - 78°W to 72°W.
 * @internal
 */
class Extent3417
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, 84], [-78, 84], [-78, 43.633327484131], [-72, 43.633327484131], [-72, 84],
                ],
            ],
        ];
    }
}
