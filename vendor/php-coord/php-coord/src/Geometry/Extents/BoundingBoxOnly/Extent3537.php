<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - mainland - offshore.
 * @internal
 */
class Extent3537
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-7.2499999999999, 41.87157915503], [-13.86155951309, 41.87157915503], [-13.86155951309, 34.916670147973], [-7.2499999999999, 34.916670147973], [-7.2499999999999, 41.87157915503],
                ],
            ],
        ];
    }
}
