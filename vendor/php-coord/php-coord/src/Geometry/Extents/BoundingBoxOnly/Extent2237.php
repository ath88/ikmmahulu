<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - North Dakota - SPCS - N.
 * @internal
 */
class Extent2237
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.837065332077, 49.000276565552], [-104.06003363014, 49.000276565552], [-104.06003363014, 47.157795471035], [-96.837065332077, 47.157795471035], [-96.837065332077, 49.000276565552],
                ],
            ],
        ];
    }
}
