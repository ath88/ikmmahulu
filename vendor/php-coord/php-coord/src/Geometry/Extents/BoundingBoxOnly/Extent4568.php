<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - Marion and Prince Edward islands - onshore and offshore.
 * @internal
 */
class Extent4568
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [42.84529854, -43.268758924], [32.71139753, -43.268758924], [32.71139753, -50.313743931], [42.84529854, -50.313743931], [42.84529854, -43.268758924],
                ],
            ],
        ];
    }
}
