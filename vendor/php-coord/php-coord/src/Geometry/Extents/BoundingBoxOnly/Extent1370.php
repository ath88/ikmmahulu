<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Deltana.
 * @internal
 */
class Extent1370
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-59.803058624268, 10.452222824097], [-62.799999237061, 10.452222824097], [-62.799999237061, 7.8999996185303], [-59.803058624268, 7.8999996185303], [-59.803058624268, 10.452222824097],
                ],
            ],
        ];
    }
}
