<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - east of 75°W.
 * @internal
 */
class Extent1429
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.358075474967, 45.644954681396], [-75, 45.644954681396], [-75, 44.981708362931], [-74.358075474967, 44.981708362931], [-74.358075474967, 45.644954681396],
                ],
            ],
        ];
    }
}
