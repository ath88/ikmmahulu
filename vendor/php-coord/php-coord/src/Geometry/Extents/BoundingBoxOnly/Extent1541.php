<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mozambique - onshore east of 36°E.
 * @internal
 */
class Extent1541
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.896276998286, -10.427348080528], [35.999999282491, -10.427348080528], [35.999999282491, -18.974682752685], [40.896276998286, -18.974682752685], [40.896276998286, -10.427348080528],
                ],
            ],
        ];
    }
}
