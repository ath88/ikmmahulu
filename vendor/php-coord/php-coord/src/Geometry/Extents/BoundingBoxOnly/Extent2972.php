<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritania - Nouakchutt.
 * @internal
 */
class Extent2972
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-15.832942584848, 18.248592281619], [-16.100691023548, 18.248592281619], [-16.100691023548, 17.899994776859], [-15.832942584848, 17.899994776859], [-15.832942584848, 18.248592281619],
                ],
            ],
        ];
    }
}
