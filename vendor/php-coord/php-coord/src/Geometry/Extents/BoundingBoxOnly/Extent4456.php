<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - Lower Austria and Moravia.
 * @internal
 */
class Extent4456
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.85221862793, 50.440132141113], [14.41055393219, 50.440132141113], [14.41055393219, 47.424365997315], [18.85221862793, 47.424365997315], [18.85221862793, 50.440132141113],
                ],
            ],
        ];
    }
}
