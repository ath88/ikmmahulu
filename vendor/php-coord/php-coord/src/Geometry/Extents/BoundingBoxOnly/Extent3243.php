<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/El Salvador - onshore.
 * @internal
 */
class Extent3243
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.694671630975, 14.431982040517], [-90.108062743801, 14.431982040517], [-90.108062743801, 13.106214430553], [-87.694671630975, 13.106214430553], [-87.694671630975, 14.431982040517],
                ],
            ],
        ];
    }
}
