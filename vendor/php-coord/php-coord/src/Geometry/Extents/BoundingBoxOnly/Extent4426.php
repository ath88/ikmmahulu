<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Bulgaria - east of 30°E.
 * @internal
 */
class Extent4426
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [31.345277529, 43.662324791464], [30, 43.662324791464], [30, 42.56773396953], [31.345277529, 42.56773396953], [31.345277529, 43.662324791464],
                ],
            ],
        ];
    }
}
