<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - South Dakota - SPCS - S.
 * @internal
 */
class Extent2250
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.439394750965, 44.781671392587], [-104.05973138169, 44.781671392587], [-104.05973138169, 42.488459217507], [-96.439394750965, 42.488459217507], [-96.439394750965, 44.781671392587],
                ],
            ],
        ];
    }
}
