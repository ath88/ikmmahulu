<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Binh Dinh, Khanh Hoa, Ninh Thuan.
 * @internal
 */
class Extent4559
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [109.52034984659, 12.868671417], [108.551803589, 12.868671417], [108.551803589, 11.257011010345], [109.52034984659, 11.257011010345], [109.52034984659, 12.868671417],
                ],
            ],
            [
                [
                    [109.42226226796, 14.702920914], [108.601097107, 14.702920914], [108.601097107, 13.51513958], [109.42226226796, 13.51513958], [109.42226226796, 14.702920914],
                ],
            ],
        ];
    }
}
