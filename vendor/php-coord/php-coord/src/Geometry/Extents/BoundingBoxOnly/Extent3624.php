<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 27°E to 29°E.
 * @internal
 */
class Extent3624
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29, -6.4354417758492], [27, -6.4354417758492], [27, -13.383604606532], [29, -13.383604606532], [29, -6.4354417758492],
                ],
            ],
        ];
    }
}
