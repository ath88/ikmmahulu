<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Egypt.
 * @internal
 */
class Extent1086
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [37.908917535, 33.811342147009], [24.706804275513, 33.811342147009], [24.706804275513, 21.898683136], [37.908917535, 21.898683136], [37.908917535, 33.811342147009],
                ],
            ],
        ];
    }
}
