<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Channel Islands - Guernsey, Alderney, Sark.
 * @internal
 */
class Extent2989
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-2.0234775019999, 50.154417163], [-3.7224865029997, 50.154417163], [-3.7224865029997, 49.115105162], [-2.0234775019999, 49.115105162], [-2.0234775019999, 50.154417163],
                ],
            ],
        ];
    }
}
