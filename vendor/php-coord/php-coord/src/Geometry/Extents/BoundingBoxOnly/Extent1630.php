<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Netherlands - offshore.
 * @internal
 */
class Extent1630
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.4047234106296, 55.765000167947], [2.5394435020008, 55.765000167947], [2.5394435020008, 51.45000016403], [6.4047234106296, 51.45000016403], [6.4047234106296, 55.765000167947],
                ],
            ],
        ];
    }
}
