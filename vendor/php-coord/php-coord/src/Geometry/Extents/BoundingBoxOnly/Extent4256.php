<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Benton.
 * @internal
 */
class Extent4256
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.093561000003, 40.736949999982], [-87.526501999998, 40.736949999982], [-87.526501999998, 40.47562899999], [-87.093561000003, 40.47562899999], [-87.093561000003, 40.736949999982],
                ],
            ],
        ];
    }
}
