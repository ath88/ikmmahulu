<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Balearic Islands - Menorca.
 * @internal
 */
class Extent4603
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [4.3815250396731, 40.144616146839], [3.7321834564212, 40.144616146839], [3.7321834564212, 39.756441116333], [4.3815250396731, 39.756441116333], [4.3815250396731, 40.144616146839],
                ],
            ],
        ];
    }
}
