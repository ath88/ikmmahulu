<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Ha district.
 * @internal
 */
class Extent3742
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [89.388858185791, 27.615421018364], [88.908153472203, 27.615421018364], [88.908153472203, 27.026616682093], [89.388858185791, 27.026616682093], [89.388858185791, 27.615421018364],
                ],
            ],
        ];
    }
}
