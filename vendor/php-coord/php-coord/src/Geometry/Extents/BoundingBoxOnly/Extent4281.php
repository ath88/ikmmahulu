<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Lagrange and Noble.
 * @internal
 */
class Extent4281
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.192097000003, 41.760097000019], [-85.659844999996, 41.760097000019], [-85.659844999996, 41.263804999995], [-85.192097000003, 41.263804999995], [-85.192097000003, 41.760097000019],
                ],
            ],
        ];
    }
}
