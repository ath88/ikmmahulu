<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cote d'Ivoire (Ivory Coast) - Abidjan area.
 * @internal
 */
class Extent2282
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-3.8541535393604, 5.5351704854435], [-4.2161184531265, 5.5351704854435], [-4.2161184531265, 5.1583285160356], [-3.8541535393604, 5.1583285160356], [-3.8541535393604, 5.5351704854435],
                ],
            ],
        ];
    }
}
