<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - Hassi Messaoud.
 * @internal
 */
class Extent2393
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.5, 32.083333], [5.593389, 32.083333], [5.593389, 31.481389], [6.5, 31.481389], [6.5, 32.083333],
                ],
            ],
        ];
    }
}
