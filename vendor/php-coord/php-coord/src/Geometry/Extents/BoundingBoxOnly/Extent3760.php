<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Yangtse district.
 * @internal
 */
class Extent3760
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.767585754395, 27.991382598877], [91.340369552147, 27.991382598877], [91.340369552147, 27.373314365527], [91.767585754395, 27.373314365527], [91.767585754395, 27.991382598877],
                ],
            ],
        ];
    }
}
