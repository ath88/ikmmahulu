<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Latin America - 102°W to 96°W.
 * @internal
 */
class Extent3763
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96, 29.806112289429], [-102.0000038147, 29.806112289429], [-102.0000038147, 12.307741165161], [-96, 12.307741165161], [-96, 29.806112289429],
                ],
            ],
        ];
    }
}
