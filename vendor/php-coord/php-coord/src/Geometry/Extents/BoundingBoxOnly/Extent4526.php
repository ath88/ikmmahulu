<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Saudi Arabia - 36°E to 42°E.
 * @internal
 */
class Extent4526
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [42, 32.154941558838], [36, 32.154941558838], [36, 16.290000131], [42, 16.290000131], [42, 32.154941558838],
                ],
            ],
        ];
    }
}
