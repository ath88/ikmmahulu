<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - Karachi.
 * @internal
 */
class Extent2982
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [67.999937629758, 25.750277], [66.833413, 25.750277], [66.833413, 24.691667], [67.999937629758, 24.691667], [67.999937629758, 25.750277],
                ],
            ],
        ];
    }
}
