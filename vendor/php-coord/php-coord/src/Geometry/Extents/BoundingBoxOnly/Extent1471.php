<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 10°E to 12°E onshore.
 * @internal
 */
class Extent1471
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, 33.220332548245], [10, 33.220332548245], [10, 23.51904454762], [12, 23.51904454762], [12, 33.220332548245],
                ],
            ],
        ];
    }
}
