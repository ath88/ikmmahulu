<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Burnett.
 * @internal
 */
class Extent4325
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-92.031242657098, 46.15765427754], [-92.886908236887, 46.15765427754], [-92.886908236887, 45.639712332223], [-92.031242657098, 45.639712332223], [-92.031242657098, 46.15765427754],
                ],
            ],
        ];
    }
}
