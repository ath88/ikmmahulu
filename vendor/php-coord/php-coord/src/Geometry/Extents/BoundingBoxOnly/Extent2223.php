<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Nevada - SPCS - C.
 * @internal
 */
class Extent2223
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-114.99416318757, 40.994197885223], [-118.18282809687, 40.994197885223], [-118.18282809687, 36.001225957272], [-114.99416318757, 36.001225957272], [-114.99416318757, 40.994197885223],
                ],
            ],
        ];
    }
}
