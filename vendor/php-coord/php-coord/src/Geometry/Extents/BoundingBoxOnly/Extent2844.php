<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 23 15.
 * @internal
 */
class Extent2844
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.167007446289, 68.135003913608], [21.852496654286, 68.135003913608], [21.852496654286, 65.49320057508], [24.167007446289, 65.49320057508], [24.167007446289, 68.135003913608],
                ],
            ],
        ];
    }
}
