<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Egypt - 29°E to 33°E.
 * @internal
 */
class Extent1643
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [34.261806739587, 33.811342147009], [29, 33.811342147009], [29, 21.995552062989], [34.261806739587, 21.995552062989], [34.261806739587, 33.811342147009],
                ],
            ],
        ];
    }
}
