<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Neuquen basin.
 * @internal
 */
class Extent1292
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.525388752357, -34.269729614258], [-71.182373046875, -34.269729614258], [-71.182373046875, -40.161862105097], [-66.525388752357, -40.161862105097], [-66.525388752357, -34.269729614258],
                ],
            ],
        ];
    }
}
