<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arizona - Pima county central.
 * @internal
 */
class Extent4460
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-111.56666666667, 32.507678985596], [-112.51666666667, 32.507678985596], [-112.51666666667, 31.501955372568], [-111.56666666667, 31.501955372568], [-111.56666666667, 32.507678985596],
                ],
            ],
        ];
    }
}
