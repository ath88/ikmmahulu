<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Pitcairn - Pitcairn Island.
 * @internal
 */
class Extent3208
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-130.01384217632, -25.005695890308], [-130.15505735981, -25.005695890308], [-130.15505735981, -25.132226396679], [-130.01384217632, -25.132226396679], [-130.01384217632, -25.005695890308],
                ],
            ],
        ];
    }
}
