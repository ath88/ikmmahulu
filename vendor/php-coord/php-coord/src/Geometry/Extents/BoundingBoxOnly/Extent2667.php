<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - 61.5°E to 64.5°E onshore.
 * @internal
 */
class Extent2667
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [64.5, 81.244095777342], [61.5, 81.244095777342], [61.5, 80.413810536577], [64.5, 80.413810536577], [64.5, 81.244095777342],
                ],
            ],
            [
                [
                    [64.5, 76.452641580371], [61.5, 76.452641580371], [61.5, 75.171112858258], [64.5, 75.171112858258], [64.5, 76.452641580371],
                ],
            ],
            [
                [
                    [64.5, 69.833664876015], [61.5, 69.833664876015], [61.5, 35.145988464355], [64.5, 35.145988464355], [64.5, 69.833664876015],
                ],
            ],
            [
                [
                    [64.135408401489, 81.769250869751], [61.5, 81.769250869751], [61.5, 81.493837356567], [64.135408401489, 81.493837356567], [64.135408401489, 81.769250869751],
                ],
            ],
        ];
    }
}
