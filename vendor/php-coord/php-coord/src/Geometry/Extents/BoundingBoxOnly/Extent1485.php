<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Tierra del Fuego onshore west of 67.5°W.
 * @internal
 */
class Extent1485
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.5, -52.594263076782], [-68.637889862061, -52.594263076782], [-68.637889862061, -54.899435043335], [-67.5, -54.899435043335], [-67.5, -52.594263076782],
                ],
            ],
        ];
    }
}
