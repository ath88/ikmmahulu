<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Denmark - onshore Jutland west of 10°E.
 * @internal
 */
class Extent3631
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.9999999999999, 57.639613052371], [8.004987964101, 57.639613052371], [8.004987964101, 54.80910346386], [9.9999999999999, 54.80910346386], [9.9999999999999, 57.639613052371],
                ],
            ],
        ];
    }
}
