<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Colby.
 * @internal
 */
class Extent4496
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-100.719902, 40.002702], [-101.484383, 40.002702], [-101.484383, 38.698922], [-100.719902, 38.698922], [-100.719902, 40.002702],
                ],
            ],
        ];
    }
}
