<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Greenland - east coast - 68°N to 69°N.
 * @internal
 */
class Extent3369
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-25.148826118595, 69], [-26.98456977535, 69], [-26.98456977535, 68.661911341263], [-25.148826118595, 68.661911341263], [-25.148826118595, 69],
                ],
            ],
        ];
    }
}
