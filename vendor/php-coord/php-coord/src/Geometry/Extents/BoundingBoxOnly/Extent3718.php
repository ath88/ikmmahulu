<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - 34.6°N to 36.2°N, east of 45°E (map 7).
 * @internal
 */
class Extent3718
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [46.345199584962, 36.23487467901], [45, 36.23487467901], [45, 34.609709253617], [46.345199584962, 34.609709253617], [46.345199584962, 36.23487467901],
                ],
            ],
        ];
    }
}
