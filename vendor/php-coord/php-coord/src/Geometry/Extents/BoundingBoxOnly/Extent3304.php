<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Senegal - onshore.
 * @internal
 */
class Extent3304
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-11.369931221008, 16.690622329712], [-17.584400304716, 16.690622329712], [-17.584400304716, 12.297217186898], [-11.369931221008, 12.297217186898], [-11.369931221008, 16.690622329712],
                ],
            ],
        ];
    }
}
