<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - 84°W to 78°W.
 * @internal
 */
class Extent1441
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78, 55.36905465237], [-84, 55.36905465237], [-84, 41.675552368164], [-78, 41.675552368164], [-78, 55.36905465237],
                ],
            ],
        ];
    }
}
