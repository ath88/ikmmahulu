<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 19ºE to 20ºE.
 * @internal
 */
class Extent3661
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [20, 70.337573794196], [19, 70.337573794196], [19, 68.337074279785], [20, 68.337074279785], [20, 70.337573794196],
                ],
            ],
        ];
    }
}
