<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 38°N to 38°40'N; 140°E to 141°E.
 * @internal
 */
class Extent2463
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141, 38.666266666667], [140, 38.666266666667], [140, 37.999566666667], [141, 37.999566666667], [141, 38.666266666667],
                ],
            ],
        ];
    }
}
