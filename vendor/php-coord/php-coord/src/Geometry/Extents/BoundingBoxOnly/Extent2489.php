<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 35°20'N to 36°N; 140°E to 141°E.
 * @internal
 */
class Extent2489
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140.89850244011, 35.999466666667], [140, 35.999466666667], [140, 35.332766666667], [140.89850244011, 35.332766666667], [140.89850244011, 35.999466666667],
                ],
            ],
        ];
    }
}
