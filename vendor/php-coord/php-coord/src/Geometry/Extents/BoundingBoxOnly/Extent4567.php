<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - mainland - onshore and offshore.
 * @internal
 */
class Extent4567
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [36.532966534, -22.136390686035], [13.334894512001, -22.136390686035], [13.334894512001, -38.16003692], [36.532966534, -38.16003692], [36.532966534, -22.136390686035],
                ],
            ],
        ];
    }
}
