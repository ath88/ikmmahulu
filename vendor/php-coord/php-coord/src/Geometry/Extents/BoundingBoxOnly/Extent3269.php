<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Lebanon - onshore.
 * @internal
 */
class Extent3269
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [36.623741149903, 34.647864148002], [35.043029978154, 34.647864148002], [35.043029978154, 33.062080383301], [36.623741149903, 33.062080383301], [36.623741149903, 34.647864148002],
                ],
            ],
        ];
    }
}
