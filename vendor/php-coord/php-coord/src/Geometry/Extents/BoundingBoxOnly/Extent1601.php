<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - east of 69°35'W.
 * @internal
 */
class Extent1601
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-69.378547668457, -0.81134986877441], [-69.583000183105, -0.81134986877441], [-69.583000183105, -2.2492065429687], [-69.378547668457, -2.2492065429687], [-69.378547668457, -0.81134986877441],
                ],
            ],
            [
                [
                    [-69.573547363281, -0.60195922851562], [-69.583000183105, -0.60195922851562], [-69.583000183105, -0.6685848236084], [-69.573547363281, -0.6685848236084], [-69.573547363281, -0.60195922851562],
                ],
            ],
            [
                [
                    [-69.127815246582, 1.0729160308839], [-69.583000183105, 1.0729160308839], [-69.583000183105, 0.61631202697765], [-69.127815246582, 0.61631202697765], [-69.127815246582, 1.0729160308839],
                ],
            ],
            [
                [
                    [-66.870452880859, 6.3099994659424], [-69.583000183105, 6.3099994659424], [-69.583000183105, 1.1725025177003], [-66.870452880859, 1.1725025177003], [-66.870452880859, 6.3099994659424],
                ],
            ],
        ];
    }
}
