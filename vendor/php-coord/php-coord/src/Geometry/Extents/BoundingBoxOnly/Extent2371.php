<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - south.
 * @internal
 */
class Extent2371
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.4461202388691, 6.9488989874631], [4.3573360717893, 6.9488989874631], [4.3573360717893, 4.2227754918812], [9.4461202388691, 4.2227754918812], [9.4461202388691, 6.9488989874631],
                ],
            ],
        ];
    }
}
