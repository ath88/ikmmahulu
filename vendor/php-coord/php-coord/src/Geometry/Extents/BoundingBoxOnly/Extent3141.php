<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran - Tombak LNG plant.
 * @internal
 */
class Extent3141
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [52.250510693114, 27.800391248886], [52.098752338171, 27.800391248886], [52.098752338171, 27.630862119237], [52.250510693114, 27.630862119237], [52.250510693114, 27.800391248886],
                ],
            ],
        ];
    }
}
