<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 42°N to 42°40'N; 142°E to 143°E.
 * @internal
 */
class Extent2447
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [143, 42.666466666667], [142, 42.666466666667], [142, 42.022000152932], [143, 42.022000152932], [143, 42.666466666667],
                ],
            ],
        ];
    }
}
