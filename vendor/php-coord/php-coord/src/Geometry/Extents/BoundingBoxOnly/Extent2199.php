<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - SPCS - S.
 * @internal
 */
class Extent2199
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.142796750337, 42.035687836041], [-96.135623583461, 42.035687836041], [-96.135623583461, 40.371946618264], [-90.142796750337, 40.371946618264], [-90.142796750337, 42.035687836041],
                ],
            ],
        ];
    }
}
