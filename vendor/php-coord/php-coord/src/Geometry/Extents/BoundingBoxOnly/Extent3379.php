<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - Selangor.
 * @internal
 */
class Extent3379
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [101.96990203857, 3.8656620979311], [100.76899055581, 3.8656620979311], [100.76899055581, 2.5431738890286], [101.96990203857, 2.5431738890286], [101.96990203857, 3.8656620979311],
                ],
            ],
        ];
    }
}
