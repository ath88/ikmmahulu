<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Quebec - 78°W to 75°W.
 * @internal
 */
class Extent1427
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-75, 62.61106035396], [-78, 62.61106035396], [-78, 45.373764038086], [-75, 45.373764038086], [-75, 62.61106035396],
                ],
            ],
        ];
    }
}
