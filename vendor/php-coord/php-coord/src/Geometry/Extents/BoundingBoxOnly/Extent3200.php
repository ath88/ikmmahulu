<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - Iwo Jima.
 * @internal
 */
class Extent3200
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.4195859568, 24.88181316568], [141.20562044435, 24.88181316568], [141.20562044435, 24.678027623376], [141.4195859568, 24.678027623376], [141.4195859568, 24.88181316568],
                ],
            ],
        ];
    }
}
