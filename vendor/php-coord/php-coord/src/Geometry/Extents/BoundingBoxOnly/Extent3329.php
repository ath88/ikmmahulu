<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Virgin Islands, British - onshore.
 * @internal
 */
class Extent3329
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-64.27379868985, 18.561816058163], [-64.879742816033, 18.561816058163], [-64.879742816033, 18.281991213488], [-64.27379868985, 18.281991213488], [-64.27379868985, 18.561816058163],
                ],
            ],
            [
                [
                    [-64.250663003074, 18.77198985672], [-64.432394034159, 18.77198985672], [-64.432394034159, 18.667264842612], [-64.250663003074, 18.667264842612], [-64.250663003074, 18.77198985672],
                ],
            ],
        ];
    }
}
