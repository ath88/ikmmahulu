<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Scotland.
 * @internal
 */
class Extent2397
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-0.65599944564559, 60.892941926883], [-8.7307159830185, 60.892941926883], [-8.7307159830185, 54.57767917307], [-0.65599944564559, 54.57767917307], [-0.65599944564559, 60.892941926883],
                ],
            ],
        ];
    }
}
