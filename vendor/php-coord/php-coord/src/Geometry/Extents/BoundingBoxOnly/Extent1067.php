<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China.
 * @internal
 */
class Extent1067
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [134.76846313477, 53.553741455078], [73.620048522949, 53.553741455078], [73.620048522949, 16.701346131], [134.76846313477, 16.701346131], [134.76846313477, 53.553741455078],
                ],
            ],
        ];
    }
}
