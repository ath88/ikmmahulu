<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Wairarapa mc.
 * @internal
 */
class Extent3775
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [176.54935745789, -40.297745749809], [175.01079654, -40.297745749809], [175.01079654, -41.667663574219], [176.54935745789, -41.667663574219], [176.54935745789, -40.297745749809],
                ],
            ],
        ];
    }
}
