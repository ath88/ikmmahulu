<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - west of 12°E.
 * @internal
 */
class Extent3949
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, 33.916667724528], [9.3113880157471, 33.916667724528], [9.3113880157471, 23.519044547619], [12, 23.519044547619], [12, 33.916667724528],
                ],
            ],
        ];
    }
}
