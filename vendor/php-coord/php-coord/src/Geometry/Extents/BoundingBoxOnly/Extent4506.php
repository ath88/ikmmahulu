<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Garden City.
 * @internal
 */
class Extent4506
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-100.089155, 38.700603], [-101.128393, 38.700603], [-101.128393, 36.997922], [-100.089155, 36.997922], [-100.089155, 38.700603],
                ],
            ],
        ];
    }
}
