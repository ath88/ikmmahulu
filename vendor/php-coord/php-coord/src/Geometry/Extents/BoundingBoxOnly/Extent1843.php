<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - 20°E to 22°E.
 * @internal
 */
class Extent1843
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21.999999886549, -17.857223510742], [19.999994503125, -17.857223510742], [19.999994503125, -22.001996035821], [21.999999886549, -22.001996035821], [21.999999886549, -17.857223510742],
                ],
            ],
        ];
    }
}
