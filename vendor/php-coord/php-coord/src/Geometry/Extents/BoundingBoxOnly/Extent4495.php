<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Goodland.
 * @internal
 */
class Extent4495
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-101.389068, 40.003166], [-102.051769, 40.003166], [-102.051769, 38.697567], [-101.389068, 38.697567], [-101.389068, 40.003166],
                ],
            ],
        ];
    }
}
