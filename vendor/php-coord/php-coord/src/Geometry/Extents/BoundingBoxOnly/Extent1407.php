<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Pennsylvania.
 * @internal
 */
class Extent1407
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.700062439729, 42.524938414524], [-80.526045190189, 42.524938414524], [-80.526045190189, 39.719313603532], [-74.700062439729, 39.719313603532], [-74.700062439729, 42.524938414524],
                ],
            ],
        ];
    }
}
