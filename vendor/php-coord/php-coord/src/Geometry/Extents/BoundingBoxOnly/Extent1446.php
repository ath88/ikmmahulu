<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Quebec - east of 60°W.
 * @internal
 */
class Extent1446
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.109805945638, 52.000537872314], [-60, 52.000537872314], [-60, 50.200161667718], [-57.109805945638, 50.200161667718], [-57.109805945638, 52.000537872314],
                ],
            ],
        ];
    }
}
