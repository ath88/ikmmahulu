<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - onshore 66°E to 72°E.
 * @internal
 */
class Extent1688
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [72, 36.558300190065], [66, 36.558300190065], [66, 23.643277736247], [72, 23.643277736247], [72, 36.558300190065],
                ],
            ],
        ];
    }
}
