<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Cabimas.
 * @internal
 */
class Extent1311
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.299999237061, 10.60000038147], [-71.549999237061, 10.60000038147], [-71.549999237061, 10.14999961853], [-71.299999237061, 10.14999961853], [-71.299999237061, 10.60000038147],
                ],
            ],
        ];
    }
}
