<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/South America - 66°W to 60°W, S hemisphere and SIRGAS 2000 by country.
 * @internal
 */
class Extent3442
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60, 5.2727069854736], [-66, 5.2727069854736], [-66, -58.386787414551], [-60, -58.386787414551], [-60, 5.2727069854736],
                ],
            ],
        ];
    }
}
