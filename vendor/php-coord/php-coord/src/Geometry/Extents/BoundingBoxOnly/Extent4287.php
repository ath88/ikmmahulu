<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Howard and Miami.
 * @internal
 */
class Extent4287
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.862227000003, 40.999197], [-86.375761999997, 40.999197], [-86.375761999997, 40.373829999992], [-85.862227000003, 40.373829999992], [-85.862227000003, 40.999197],
                ],
            ],
        ];
    }
}
