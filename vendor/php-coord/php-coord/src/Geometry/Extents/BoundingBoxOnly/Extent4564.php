<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - 42.5°S to 50.3°S and west of 70.5°W.
 * @internal
 */
class Extent4564
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-70.5, -44.948575110559], [-73.582298278809, -44.948575110559], [-73.582298278809, -50.333334334117], [-70.5, -50.333334334117], [-70.5, -44.948575110559],
                ],
            ],
        ];
    }
}
