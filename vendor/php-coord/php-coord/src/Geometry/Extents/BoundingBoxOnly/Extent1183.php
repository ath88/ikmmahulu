<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Oman.
 * @internal
 */
class Extent1183
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [56.351070249297, 25.329778936091], [56.208600200307, 25.329778936091], [56.208600200307, 25.227608811382], [56.351070249297, 25.227608811382], [56.351070249297, 25.329778936091],
                ],
            ],
            [
                [
                    [63.370277559, 25.114681139], [51.999290466309, 25.114681139], [51.999290466309, 14.335012129], [63.370277559, 14.335012129], [63.370277559, 25.114681139],
                ],
            ],
            [
                [
                    [56.855814553, 26.737500141], [55.757958552001, 26.737500141], [55.757958552001, 25.626110076905], [56.855814553, 25.626110076905], [56.855814553, 26.737500141],
                ],
            ],
        ];
    }
}
