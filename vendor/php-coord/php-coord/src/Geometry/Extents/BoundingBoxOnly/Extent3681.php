<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores C - Graciosa onshore.
 * @internal
 */
class Extent3681
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-27.887165479928, 39.135032438538], [-28.121203422811, 39.135032438538], [-28.121203422811, 38.974252517481], [-27.887165479928, 38.974252517481], [-27.887165479928, 39.135032438538],
                ],
            ],
        ];
    }
}
