<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Carroll-Atlantic.
 * @internal
 */
class Extent4236
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.164101, 42.210425], [-95.155792, 42.210425], [-95.155792, 41.157105], [-94.164101, 41.157105], [-94.164101, 42.210425],
                ],
            ],
        ];
    }
}
