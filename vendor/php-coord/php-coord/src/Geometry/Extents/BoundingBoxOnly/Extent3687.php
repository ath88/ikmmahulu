<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - SA and WA 126°E to 132°E.
 * @internal
 */
class Extent3687
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [132, -9.379968258], [125.999999617, -9.379968258], [125.999999617, -37.046020480924], [132, -37.046020480924], [132, -9.379968258],
                ],
            ],
        ];
    }
}
