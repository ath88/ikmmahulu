<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Burns-Harper.
 * @internal
 */
class Extent4459
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-117.49619625116, 44.185588763265], [-118.60149318503, 44.185588763265], [-118.60149318503, 43.496264992681], [-117.49619625116, 43.496264992681], [-117.49619625116, 44.185588763265],
                ],
            ],
        ];
    }
}
