<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bangladesh - onshore west of 90°E.
 * @internal
 */
class Extent1674
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [90, 26.634122848511], [88.010566711426, 26.634122848511], [88.010566711426, 21.593317031292], [90, 21.593317031292], [90, 26.634122848511],
                ],
            ],
        ];
    }
}
