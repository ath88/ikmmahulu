<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wyoming.
 * @internal
 */
class Extent1419
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.05170553525, 45.002792925692], [-111.05265044017, 45.002792925692], [-111.05265044017, 40.994289143578], [-104.05170553525, 40.994289143578], [-104.05170553525, 45.002792925692],
                ],
            ],
        ];
    }
}
