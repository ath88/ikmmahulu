<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - 132°E to 138°E.
 * @internal
 */
class Extent1561
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [138, -8.8818808919997], [132, -8.8818808919997], [132, -40.705131082776], [138, -40.705131082776], [138, -8.8818808919997],
                ],
            ],
        ];
    }
}
