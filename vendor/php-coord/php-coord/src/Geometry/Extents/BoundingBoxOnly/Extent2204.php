<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Louisiana - SPCS - N.
 * @internal
 */
class Extent2204
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.866846591263, 33.023422407749], [-94.041785426663, 33.023422407749], [-94.041785426663, 30.851645143697], [-90.866846591263, 30.851645143697], [-90.866846591263, 33.023422407749],
                ],
            ],
        ];
    }
}
