<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas - SPCS83 - SC.
 * @internal
 */
class Extent2527
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.765612062946, 30.667956339675], [-104.99240820814, 30.667956339675], [-104.99240820814, 27.783231004575], [-93.765612062946, 27.783231004575], [-93.765612062946, 30.667956339675],
                ],
            ],
        ];
    }
}
