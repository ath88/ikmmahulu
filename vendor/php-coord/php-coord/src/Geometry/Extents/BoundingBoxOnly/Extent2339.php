<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Sardinia onshore.
 * @internal
 */
class Extent2339
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.8894743265294, 41.303393359001], [8.0827992697368, 41.303393359001], [8.0827992697368, 38.827870377845], [9.8894743265294, 38.827870377845], [9.8894743265294, 41.303393359001],
                ],
            ],
        ];
    }
}
