<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Egypt - west of 29°E; south of 28°11'N.
 * @internal
 */
class Extent1645
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.000000015987, 28.183333725848], [24.99694442749, 28.183333725848], [24.99694442749, 21.994163513184], [29.000000015987, 21.994163513184], [29.000000015987, 28.183333725848],
                ],
            ],
        ];
    }
}
