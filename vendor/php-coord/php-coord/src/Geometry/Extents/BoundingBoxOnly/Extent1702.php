<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines - zone V.
 * @internal
 */
class Extent1702
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [126.64827894904, 21.96564402959], [123.73103731563, 21.96564402959], [123.73103731563, 4.7609305852143], [126.64827894904, 4.7609305852143], [126.64827894904, 21.96564402959],
                ],
            ],
        ];
    }
}
