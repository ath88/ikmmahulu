<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Saudi Arabia - 42°E to 48°E.
 * @internal
 */
class Extent4527
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [47.999091699022, 31.146472396977], [41.999091699022, 31.146472396977], [41.999091699022, 16.357758800813], [47.999091699022, 16.357758800813], [47.999091699022, 31.146472396977],
                ],
            ],
        ];
    }
}
