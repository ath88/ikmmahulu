<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - SA.
 * @internal
 */
class Extent2986
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.001709, -25.995911], [128.99992393473, -25.995911], [128.99992393473, -38.123891830444], [141.001709, -38.123891830444], [141.001709, -25.995911],
                ],
            ],
        ];
    }
}
