<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Adriatic - South Gargano.
 * @internal
 */
class Extent2884
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.626732661429, 42.278639155], [15.956938342691, 42.278639155], [15.956938342691, 40.725743162717], [18.626732661429, 40.725743162717], [18.626732661429, 42.278639155],
                ],
            ],
        ];
    }
}
