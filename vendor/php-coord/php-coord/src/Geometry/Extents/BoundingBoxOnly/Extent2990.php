<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Brisbane.
 * @internal
 */
class Extent2990
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [153.68103981018, -24.649667739868], [151.19734164513, -24.649667739868], [151.19734164513, -29.876778165932], [153.68103981018, -29.876778165932], [153.68103981018, -24.649667739868],
                ],
            ],
        ];
    }
}
