<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - north of 7°45'N.
 * @internal
 */
class Extent1313
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-59.803058624268, 12.247201919556], [-73.378067016602, 12.247201919556], [-73.378067016602, 7.7500019073487], [-59.803058624268, 7.7500019073487], [-59.803058624268, 12.247201919556],
                ],
            ],
        ];
    }
}
