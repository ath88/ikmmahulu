<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - Hassi Mouina licence area.
 * @internal
 */
class Extent3402
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [1.25, 31], [0, 31], [0, 29.25], [1.25, 29.25], [1.25, 31],
                ],
            ],
        ];
    }
}
