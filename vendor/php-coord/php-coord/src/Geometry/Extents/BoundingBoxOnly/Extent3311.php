<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - South Sudan and Sudan onshore.
 * @internal
 */
class Extent3311
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [38.65739272689, 22.232219696045], [21.826565196364, 22.232219696045], [21.826565196364, 3.4933943748477], [38.65739272689, 3.4933943748477], [38.65739272689, 22.232219696045],
                ],
            ],
        ];
    }
}
