<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Papua New Guinea - onshore - Central province and Gulf province east of 144°E.
 * @internal
 */
class Extent4425
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [149.66709156965, -6.676726922399], [144.40296183407, -6.676726922399], [144.40296183407, -10.411466480395], [149.66709156965, -10.411466480395], [149.66709156965, -6.676726922399],
                ],
            ],
        ];
    }
}
