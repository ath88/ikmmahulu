<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Lithuania - onshore.
 * @internal
 */
class Extent3272
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [26.813051223755, 56.449851989746], [20.868163397815, 56.449851989746], [20.868163397815, 53.890338897705], [26.813051223755, 53.890338897705], [26.813051223755, 56.449851989746],
                ],
            ],
        ];
    }
}
