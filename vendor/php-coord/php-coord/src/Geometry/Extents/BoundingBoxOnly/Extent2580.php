<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - upper Savinja.
 * @internal
 */
class Extent2580
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.990941188392, 46.559985205663], [14.559635280985, 46.559985205663], [14.559635280985, 46.215839357891], [14.990941188392, 46.215839357891], [14.990941188392, 46.559985205663],
                ],
            ],
        ];
    }
}
