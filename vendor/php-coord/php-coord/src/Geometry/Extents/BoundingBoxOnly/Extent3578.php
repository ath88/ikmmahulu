<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - 18°E to 24°E onshore and S-42(83) by country.
 * @internal
 */
class Extent3578
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.894804000854, 50.055931091309], [18, 50.055931091309], [18, 45.748329162598], [22.894804000854, 45.748329162598], [22.894804000854, 50.055931091309],
                ],
            ],
        ];
    }
}
