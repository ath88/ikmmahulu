<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - east of 18°E.
 * @internal
 */
class Extent4539
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.084442138672, -6.913882255554], [18, -6.913882255554], [18, -18.01639175415], [24.084442138672, -18.01639175415], [24.084442138672, -6.913882255554],
                ],
            ],
        ];
    }
}
