<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 27°E to 29°E.
 * @internal
 */
class Extent3158
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.000003583783, 5.2034716606142], [26.999999903576, 5.2034716606142], [26.999999903576, -13.38360744895], [29.000003583783, -13.38360744895], [29.000003583783, 5.2034716606142],
                ],
            ],
        ];
    }
}
