<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Tamil Nadu.
 * @internal
 */
class Extent4417
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [80.39620909844, 13.582734211156], [76.225746154786, 13.582734211156], [76.225746154786, 8.027118347545], [80.39620909844, 8.027118347545], [80.39620909844, 13.582734211156],
                ],
            ],
        ];
    }
}
