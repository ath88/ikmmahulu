<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - west of 18°W.
 * @internal
 */
class Extent3629
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-18, 31.185582145], [-21.92695552, 31.185582145], [-21.92695552, 24.603629139001], [-18, 24.603629139001], [-18, 31.185582145],
                ],
            ],
        ];
    }
}
