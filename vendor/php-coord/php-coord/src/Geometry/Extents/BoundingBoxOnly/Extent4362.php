<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Columbia.
 * @internal
 */
class Extent4362
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.004508138827, 43.643670845731], [-89.785499095577, 43.643670845731], [-89.785499095577, 43.280905225993], [-89.004508138827, 43.280905225993], [-89.004508138827, 43.643670845731],
                ],
            ],
        ];
    }
}
