<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Guadeloupe - St Barthelemy - onshore.
 * @internal
 */
class Extent2891
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-62.731054010386, 17.975135787049], [-62.915922998481, 17.975135787049], [-62.915922998481, 17.829565186257], [-62.731054010386, 17.829565186257], [-62.731054010386, 17.975135787049],
                ],
            ],
        ];
    }
}
