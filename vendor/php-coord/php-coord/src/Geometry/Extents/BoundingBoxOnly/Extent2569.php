<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Greenland - south of 63°N.
 * @internal
 */
class Extent2569
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-41.331428420971, 63], [-50.712219208805, 63], [-50.712219208805, 59.74371386496], [-41.331428420971, 59.74371386496], [-41.331428420971, 63],
                ],
            ],
        ];
    }
}
