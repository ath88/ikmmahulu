<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Hungary.
 * @internal
 */
class Extent1119
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.894804000854, 48.576175689697], [16.111804962158, 48.576175689697], [16.111804962158, 45.748329162598], [22.894804000854, 45.748329162598], [22.894804000854, 48.576175689697],
                ],
            ],
        ];
    }
}
