<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Andhra Pradesh and Telangana.
 * @internal
 */
class Extent4394
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [84.807367785066, 19.916088104248], [76.756988525391, 19.916088104248], [76.756988525391, 12.611840248108], [84.807367785066, 12.611840248108], [84.807367785066, 19.916088104248],
                ],
            ],
        ];
    }
}
