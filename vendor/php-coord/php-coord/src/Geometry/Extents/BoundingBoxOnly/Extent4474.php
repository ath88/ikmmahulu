<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Dayville-Prairie City.
 * @internal
 */
class Extent4474
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-118.61264696925, 44.935358290875], [-119.88667453994, 44.935358290875], [-119.88667453994, 44.245545863722], [-118.61264696925, 44.245545863722], [-118.61264696925, 44.935358290875],
                ],
            ],
        ];
    }
}
