<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Maryland.
 * @internal
 */
class Extent1389
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.977053421604, 39.72546106874], [-79.489864795642, 39.72546106874], [-79.489864795642, 37.979780465479], [-74.977053421604, 37.979780465479], [-74.977053421604, 39.72546106874],
                ],
            ],
        ];
    }
}
