<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Togo.
 * @internal
 */
class Extent1232
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [2.410321502, 11.138540267945], [-0.14976200461376, 11.138540267945], [-0.14976200461376, 2.9116531190004], [2.410321502, 2.9116531190004], [2.410321502, 11.138540267945],
                ],
            ],
        ];
    }
}
