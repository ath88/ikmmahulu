<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Papua New Guinea - 150°E to 156°E.
 * @internal
 */
class Extent3888
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [156, 1.9759908533999], [150, 1.9759908533999], [150, -14.748474898], [156, -14.748474898], [156, 1.9759908533999],
                ],
            ],
        ];
    }
}
