<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Thailand - Bongkot field.
 * @internal
 */
class Extent2358
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [103.0416665893, 8.15217], [102.163169, 8.15217], [102.163169, 6.7499999999999], [103.0416665893, 6.7499999999999], [103.0416665893, 8.15217],
                ],
            ],
        ];
    }
}
