<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Wallowa.
 * @internal
 */
class Extent4480
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-116.42465078989, 46.043246688117], [-118.15690182591, 46.043246688117], [-118.15690182591, 45.272494220174], [-116.42465078989, 45.272494220174], [-116.42465078989, 46.043246688117],
                ],
            ],
        ];
    }
}
