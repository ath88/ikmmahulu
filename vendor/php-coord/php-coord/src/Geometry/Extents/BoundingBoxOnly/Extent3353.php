<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Velenje.
 * @internal
 */
class Extent3353
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.257617668682, 46.509854322545], [14.903976014683, 46.509854322545], [14.903976014683, 46.287519964834], [15.257617668682, 46.287519964834], [15.257617668682, 46.509854322545],
                ],
            ],
        ];
    }
}
