<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Sao Tome and Principe - onshore - Principe.
 * @internal
 */
class Extent3646
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.5132586050281, 1.7513744620448], [7.2781351252473, 1.7513744620448], [7.2781351252473, 1.4804244489495], [7.5132586050281, 1.4804244489495], [7.5132586050281, 1.7513744620448],
                ],
            ],
        ];
    }
}
