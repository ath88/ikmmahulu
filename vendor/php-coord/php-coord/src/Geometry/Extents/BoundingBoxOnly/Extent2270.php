<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wyoming - SPCS - EC.
 * @internal
 */
class Extent2270
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-106.00404209161, 45.000115150176], [-108.62525622197, 45.000115150176], [-108.62525622197, 40.998457386108], [-106.00404209161, 40.998457386108], [-106.00404209161, 45.000115150176],
                ],
            ],
        ];
    }
}
