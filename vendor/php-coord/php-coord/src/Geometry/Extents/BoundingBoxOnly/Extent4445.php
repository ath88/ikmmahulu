<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Esperance.
 * @internal
 */
class Extent4445
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [122.2, -33.333333333333], [121.56666666667, -33.333333333333], [121.56666666667, -34.5], [122.2, -34.5], [122.2, -33.333333333333],
                ],
            ],
        ];
    }
}
