<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Barbados - onshore.
 * @internal
 */
class Extent3218
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-59.378075808933, 13.385458559], [-59.708505558981, 13.385458559], [-59.708505558981, 13.002187615634], [-59.378075808933, 13.002187615634], [-59.378075808933, 13.385458559],
                ],
            ],
        ];
    }
}
