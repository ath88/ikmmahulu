<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Gisborne vcrs.
 * @internal
 */
class Extent3771
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [178.62698173523, -37.496059417725], [176.4114727534, -37.496059417725], [176.4114727534, -39.03923043], [178.62698173523, -39.03923043], [178.62698173523, -37.496059417725],
                ],
            ],
        ];
    }
}
