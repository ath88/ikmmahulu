<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Mexico - 102°W to 96°W.
 * @internal
 */
class Extent3426
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96, 29.806110382234], [-102, 29.806110382234], [-102, 12.307739127359], [-96, 12.307739127359], [-96, 29.806110382234],
                ],
            ],
        ];
    }
}
