<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Alberta - 115.5°W to 112.5°W.
 * @internal
 */
class Extent3542
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-112.5, 60], [-115.5, 60], [-115.5, 48.999435424805], [-112.5, 48.999435424805], [-112.5, 60],
                ],
            ],
        ];
    }
}
