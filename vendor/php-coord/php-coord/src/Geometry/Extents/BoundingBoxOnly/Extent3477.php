<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - Cyrenaica.
 * @internal
 */
class Extent3477
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.999501857433, 32.793348831885], [22.499506261291, 32.793348831885], [22.499506261291, 32.001209475229], [22.999501857433, 32.001209475229], [22.999501857433, 32.793348831885],
                ],
            ],
        ];
    }
}
