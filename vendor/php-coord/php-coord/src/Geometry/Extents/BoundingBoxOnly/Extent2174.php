<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - GoM OCS - east of 84°W.
 * @internal
 */
class Extent2174
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.170347468075, 29.939847249426], [-84.0815161851, 29.939847249426], [-84.0815161851, 23.823225057201], [-81.170347468075, 23.823225057201], [-81.170347468075, 29.939847249426],
                ],
            ],
        ];
    }
}
