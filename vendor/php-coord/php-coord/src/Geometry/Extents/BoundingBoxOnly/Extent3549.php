<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/France - mainland - 45°N to 47°N.
 * @internal
 */
class Extent3549
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.1586103439331, 47], [-2.2061376139679, 47], [-2.2061376139679, 45], [7.1586103439331, 45], [7.1586103439331, 47],
                ],
            ],
        ];
    }
}
