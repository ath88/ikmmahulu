<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Irian Jaya - Tangguh.
 * @internal
 */
class Extent2589
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [133.81547106658, -1.9730581300612], [131.89628685546, -1.9730581300612], [131.89628685546, -2.930681134133], [133.81547106658, -2.930681134133], [133.81547106658, -1.9730581300612],
                ],
            ],
        ];
    }
}
