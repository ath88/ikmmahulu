<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Crawford, Lawrence, Orange.
 * @internal
 */
class Extent4272
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.245555000036, 38.992403999994], [-86.683576999973, 38.992403999994], [-86.683576999973, 38.105396000004], [-86.245555000036, 38.105396000004], [-86.245555000036, 38.992403999994],
                ],
            ],
        ];
    }
}
