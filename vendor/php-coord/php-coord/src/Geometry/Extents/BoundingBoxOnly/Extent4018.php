<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 29°E to 31°E.
 * @internal
 */
class Extent4018
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.806663513184, -12.154167175293], [29, -12.154167175293], [29, -13.458057403564], [29.806663513184, -13.458057403564], [29.806663513184, -12.154167175293],
                ],
            ],
        ];
    }
}
