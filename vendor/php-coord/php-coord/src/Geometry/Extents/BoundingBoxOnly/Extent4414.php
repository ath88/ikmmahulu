<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Punjab.
 * @internal
 */
class Extent4414
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [76.931396484376, 32.576190948486], [73.870887756348, 32.576190948486], [73.870887756348, 29.546171188355], [76.931396484376, 29.546171188355], [76.931396484376, 32.576190948486],
                ],
            ],
        ];
    }
}
