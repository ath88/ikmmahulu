<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia.
 * @internal
 */
class Extent3955
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [105.819999599, 7.8000001206704], [98.024999591001, 7.8000001206704], [98.024999591001, 1.1354031170005], [105.819999599, 1.1354031170005], [105.819999599, 7.8000001206704],
                ],
            ],
        ];
    }
}
