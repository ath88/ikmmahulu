<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kyrgyzstan - west of 70°01'E.
 * @internal
 */
class Extent4385
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [70.016666666667, 40.217990875244], [69.249496459961, 40.217990875244], [69.249496459961, 39.518810272217], [70.016666666667, 39.518810272217], [70.016666666667, 40.217990875244],
                ],
            ],
        ];
    }
}
