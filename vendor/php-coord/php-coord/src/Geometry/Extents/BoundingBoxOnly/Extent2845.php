<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 7.5 gon W.
 * @internal
 */
class Extent2845
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.902104306879, 59.727572868036], [10.937693412446, 59.727572868036], [10.937693412446, 57.294961163556], [12.902104306879, 57.294961163556], [12.902104306879, 59.727572868036],
                ],
            ],
        ];
    }
}
