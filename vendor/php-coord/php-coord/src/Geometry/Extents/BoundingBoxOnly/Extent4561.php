<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Mendoza - Cuyo basin.
 * @internal
 */
class Extent4561
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.426736662971, -31.966146894239], [-69.393200973457, -31.966146894239], [-69.393200973457, -36.363493989782], [-66.426736662971, -36.363493989782], [-66.426736662971, -31.966146894239],
                ],
            ],
        ];
    }
}
