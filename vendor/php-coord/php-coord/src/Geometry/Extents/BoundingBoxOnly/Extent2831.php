<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Atlantic offshore.
 * @internal
 */
class Extent2831
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-47.743430543984, 64.205208824162], [-67.743055559783, 64.205208824162], [-67.743055559783, 40.040199904302], [-47.743430543984, 40.040199904302], [-47.743430543984, 64.205208824162],
                ],
            ],
        ];
    }
}
