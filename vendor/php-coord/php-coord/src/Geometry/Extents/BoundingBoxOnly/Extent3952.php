<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - 6°W to 0°W.
 * @internal
 */
class Extent3952
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [0, 37.004790042337], [-6, 37.004790042337], [-6, 21.825342514383], [0, 21.825342514383], [0, 37.004790042337],
                ],
            ],
        ];
    }
}
