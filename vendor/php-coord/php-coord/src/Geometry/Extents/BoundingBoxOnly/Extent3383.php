<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - Perak.
 * @internal
 */
class Extent3383
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [100.13658922265, 4.0073502451558], [100.07718146631, 4.0073502451558], [100.07718146631, 3.9543998536413], [100.13658922265, 3.9543998536413], [100.13658922265, 4.0073502451558],
                ],
            ],
            [
                [
                    [100.6157256922, 4.1119595552209], [100.49949312546, 4.1119595552209], [100.49949312546, 3.9492339617863], [100.6157256922, 3.9492339617863], [100.6157256922, 4.1119595552209],
                ],
            ],
            [
                [
                    [101.99829864502, 5.915207386017], [100.3123848916, 5.915207386017], [100.3123848916, 3.6654210090637], [101.99829864502, 3.6654210090637], [101.99829864502, 5.915207386017],
                ],
            ],
        ];
    }
}
