<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Guadeloupe - Marie-Galante - onshore.
 * @internal
 */
class Extent2894
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.138257341127, 16.043638576607], [-61.380121510975, 16.043638576607], [-61.380121510975, 15.800965054082], [-61.138257341127, 15.800965054082], [-61.138257341127, 16.043638576607],
                ],
            ],
        ];
    }
}
