<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Tararu vcrs.
 * @internal
 */
class Extent3818
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [175.98868456017, -36.789116845697], [175.44475298545, -36.789116845697], [175.44475298545, -37.208543325907], [175.98868456017, -37.208543325907], [175.98868456017, -36.789116845697],
                ],
            ],
        ];
    }
}
