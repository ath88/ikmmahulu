<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 32°N to 32°40'N; 131°E to 132°E.
 * @internal
 */
class Extent2523
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [131.90422558187, 32.665966666667], [131, 32.665966666667], [131, 31.999266666667], [131.90422558187, 31.999266666667], [131.90422558187, 32.665966666667],
                ],
            ],
        ];
    }
}
