<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Korea, Republic of (South Korea) - east of 128°E onshore.
 * @internal
 */
class Extent1496
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [129.64844701141, 38.636078482961], [128, 38.636078482961], [128, 34.493968005016], [129.64844701141, 34.493968005016], [129.64844701141, 38.636078482961],
                ],
            ],
        ];
    }
}
