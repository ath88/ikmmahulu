<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana  Milk River.
 * @internal
 */
class Extent4312
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-108.74999994059, 49.000604629517], [-112.5, 49.000604629517], [-112.5, 47.783], [-108.74999994059, 47.783], [-108.74999994059, 49.000604629517],
                ],
            ],
        ];
    }
}
