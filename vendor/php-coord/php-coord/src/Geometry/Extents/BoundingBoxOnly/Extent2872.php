<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores C - Terceira onshore.
 * @internal
 */
class Extent2872
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-26.970141004444, 38.853055406689], [-27.438613344311, 38.853055406689], [-27.438613344311, 38.579439710499], [-26.970141004444, 38.579439710499], [-26.970141004444, 38.853055406689],
                ],
            ],
        ];
    }
}
