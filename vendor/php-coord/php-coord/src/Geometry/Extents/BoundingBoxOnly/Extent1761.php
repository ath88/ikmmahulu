<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Bolivia - 66°W to 60°W.
 * @internal
 */
class Extent1761
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60, -9.6791954040527], [-66, -9.6791954040527], [-66, -22.869792938232], [-60, -22.869792938232], [-60, -9.6791954040527],
                ],
            ],
        ];
    }
}
