<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Singapore.
 * @internal
 */
class Extent1210
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [104.061780597, 1.4611528183483], [103.596043596, 1.4611528183483], [103.596043596, 1.1303611170003], [104.061780597, 1.1303611170003], [104.061780597, 1.4611528183483],
                ],
            ],
        ];
    }
}
