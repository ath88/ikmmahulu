<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Qld east of 150°E.
 * @internal
 */
class Extent3691
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [153.60364043442, -22.000647739079], [150, -22.000647739079], [150, -29.180741], [153.60364043442, -29.180741], [153.60364043442, -22.000647739079],
                ],
            ],
        ];
    }
}
