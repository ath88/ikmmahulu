<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Michigan - SPCS - E.
 * @internal
 */
class Extent1720
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.130279541016, 46.035918164794], [-84.862749548668, 46.035918164794], [-84.862749548668, 41.697121363375], [-82.130279541016, 41.697121363375], [-82.130279541016, 46.035918164794],
                ],
            ],
        ];
    }
}
