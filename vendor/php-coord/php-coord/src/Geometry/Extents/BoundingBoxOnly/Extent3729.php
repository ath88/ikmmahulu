<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - 31.4°N to 33°N, west of 40.1°E (map 13).
 * @internal
 */
class Extent3729
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.070261288128, 32.988368664456], [38.924868068421, 32.988368664456], [38.924868068421, 32.008587527903], [40.070261288128, 32.008587527903], [40.070261288128, 32.988368664456],
                ],
            ],
        ];
    }
}
