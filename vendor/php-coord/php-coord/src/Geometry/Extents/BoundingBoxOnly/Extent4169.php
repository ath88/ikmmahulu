<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Christmas Island - onshore.
 * @internal
 */
class Extent4169
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [105.76357545354, -10.362143305044], [105.48237539053, -10.362143305044], [105.48237539053, -10.620806543856], [105.76357545354, -10.620806543856], [105.76357545354, -10.362143305044],
                ],
            ],
        ];
    }
}
