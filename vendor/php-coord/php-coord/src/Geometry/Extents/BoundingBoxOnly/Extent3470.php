<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - offshore - Pearl River basin.
 * @internal
 */
class Extent3470
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [116.75058792921, 22.887388319263], [110.13758782887, 22.887388319263], [110.13758782887, 18.318771275796], [116.75058792921, 18.318771275796], [116.75058792921, 22.887388319263],
                ],
            ],
        ];
    }
}
