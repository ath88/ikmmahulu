<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 11°E to 13°E onshore.
 * @internal
 */
class Extent3150
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.000001623565, -4.6790910807696], [12.173489324635, -4.6790910807696], [12.173489324635, -6.0374333606684], [13.000001623565, -6.0374333606684], [13.000001623565, -4.6790910807696],
                ],
            ],
        ];
    }
}
