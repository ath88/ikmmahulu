<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Littoral onshore.
 * @internal
 */
class Extent2583
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.233183703503, 45.721744732595], [13.501505945986, 45.721744732595], [13.501505945986, 45.444160461426], [14.233183703503, 45.444160461426], [14.233183703503, 45.721744732595],
                ],
            ],
        ];
    }
}
