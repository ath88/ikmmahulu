<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - Sirte NC192.
 * @internal
 */
class Extent3142
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21.58333, 28.06667], [21.25, 28.06667], [21.25, 27.5], [21.58333, 27.5], [21.58333, 28.06667],
                ],
            ],
        ];
    }
}
