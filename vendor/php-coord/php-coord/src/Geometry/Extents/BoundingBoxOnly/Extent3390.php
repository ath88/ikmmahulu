<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Middle East -SE Iraq and SW Iran.
 * @internal
 */
class Extent3390
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [51.055322050464, 33.490205063031], [44.308799786611, 33.490205063031], [44.308799786611, 29.061660766602], [51.055322050464, 29.061660766602], [51.055322050464, 33.490205063031],
                ],
            ],
        ];
    }
}
