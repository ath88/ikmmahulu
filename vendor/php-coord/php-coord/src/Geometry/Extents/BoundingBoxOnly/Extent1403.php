<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - North Dakota.
 * @internal
 */
class Extent1403
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.551930995181, 49.000276565552], [-104.06003363014, 49.000276565552], [-104.06003363014, 45.930822152112], [-96.551930995181, 45.930822152112], [-96.551930995181, 49.000276565552],
                ],
            ],
        ];
    }
}
