<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Pepin and Pierce.
 * @internal
 */
class Extent4350
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.650285080473, 44.862660540645], [-92.808331250155, 44.862660540645], [-92.808331250155, 44.407400062808], [-91.650285080473, 44.407400062808], [-91.650285080473, 44.862660540645],
                ],
            ],
        ];
    }
}
