<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Uruguay.
 * @internal
 */
class Extent1247
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-50.014827728271, -30.096668243408], [-58.485967636108, -30.096668243408], [-58.485967636108, -37.765113830566], [-50.014827728271, -37.765113830566], [-50.014827728271, -30.096668243408],
                ],
            ],
        ];
    }
}
