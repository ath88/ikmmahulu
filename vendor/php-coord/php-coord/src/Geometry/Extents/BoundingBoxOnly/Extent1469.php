<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Guinea - east of 12°W.
 * @internal
 */
class Extent1469
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-7.6533703804016, 12.504695892334], [-12, 12.504695892334], [-12, 7.1939268112183], [-7.6533703804016, 7.1939268112183], [-7.6533703804016, 12.504695892334],
                ],
            ],
        ];
    }
}
