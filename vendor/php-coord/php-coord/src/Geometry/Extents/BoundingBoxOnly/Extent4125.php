<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Madeira and Desertas islands - onshore.
 * @internal
 */
class Extent4125
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-16.400776990847, 32.606148544994], [-16.58628214383, 32.606148544994], [-16.58628214383, 32.35725156871], [-16.400776990847, 32.35725156871], [-16.400776990847, 32.606148544994],
                ],
            ],
            [
                [
                    [-16.66555650127, 32.920278001904], [-17.304517198681, 32.920278001904], [-17.304517198681, 32.587496351124], [-16.66555650127, 32.587496351124], [-16.66555650127, 32.920278001904],
                ],
            ],
        ];
    }
}
