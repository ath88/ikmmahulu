<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Perry.
 * @internal
 */
class Extent4278
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.430091000004, 38.266328000008], [-86.813919652555, 38.266328000008], [-86.813919652555, 37.841425000005], [-86.430091000004, 37.841425000005], [-86.430091000004, 38.266328000008],
                ],
            ],
        ];
    }
}
