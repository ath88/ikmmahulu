<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - west of 12°E.
 * @internal
 */
class Extent1838
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, -17.157435474291], [11.666008252717, -17.157435474291], [11.666008252717, -18.522335395982], [12, -18.522335395982], [12, -17.157435474291],
                ],
            ],
        ];
    }
}
