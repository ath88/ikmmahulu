<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - offshore.
 * @internal
 */
class Extent1822
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.451303864997, -17.24929442314], [8.2435035080003, -17.24929442314], [8.2435035080003, -30.635802913], [16.451303864997, -30.635802913], [16.451303864997, -17.24929442314],
                ],
            ],
        ];
    }
}
