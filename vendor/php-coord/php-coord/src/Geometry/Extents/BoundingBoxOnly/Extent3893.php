<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Wytch Farm.
 * @internal
 */
class Extent3893
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-1.685945283614, 50.799543249494], [-2.1915794828604, 50.799543249494], [-2.1915794828604, 50.536035757429], [-1.685945283614, 50.536035757429], [-1.685945283614, 50.799543249494],
                ],
            ],
        ];
    }
}
