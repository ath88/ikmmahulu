<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - GoM - east of 87.25°W.
 * @internal
 */
class Extent3358
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.170347468075, 30.246306685173], [-87.25, 30.246306685173], [-87.25, 23.823225057201], [-81.170347468075, 23.823225057201], [-81.170347468075, 30.246306685173],
                ],
            ],
        ];
    }
}
