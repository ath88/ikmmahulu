<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - Chatham Islands group.
 * @internal
 */
class Extent2889
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-175.545037, -43.307589], [-177.244666, -43.307589], [-177.244666, -44.63813], [-175.545037, -44.63813], [-175.545037, -43.307589],
                ],
            ],
        ];
    }
}
