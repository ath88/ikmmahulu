<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Romania.
 * @internal
 */
class Extent1197
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [31.409714529, 48.263885498047], [20.261024475098, 48.263885498047], [20.261024475098, 43.446944156001], [31.409714529, 43.446944156001], [31.409714529, 48.263885498047],
                ],
            ],
        ];
    }
}
