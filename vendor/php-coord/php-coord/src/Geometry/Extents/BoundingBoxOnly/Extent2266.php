<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - SPCS - C.
 * @internal
 */
class Extent2266
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.25064894405, 45.795617611819], [-92.889241325672, 45.795617611819], [-92.889241325672, 43.984180436731], [-86.25064894405, 43.984180436731], [-86.25064894405, 45.795617611819],
                ],
            ],
        ];
    }
}
