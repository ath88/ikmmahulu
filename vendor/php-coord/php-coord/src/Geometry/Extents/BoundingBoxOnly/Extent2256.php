<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas - SPCS27 - SC.
 * @internal
 */
class Extent2256
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.41047426799, 30.667956339675], [-104.99240820814, 30.667956339675], [-104.99240820814, 27.783231005176], [-93.41047426799, 27.783231005176], [-93.41047426799, 30.667956339675],
                ],
            ],
        ];
    }
}
