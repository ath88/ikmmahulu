<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/UAE - Abu al Bu Khoosh.
 * @internal
 */
class Extent2392
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.395650366689, 25.53055614], [53.034314021555, 25.53055614], [53.034314021555, 25.333044893453], [53.395650366689, 25.333044893453], [53.395650366689, 25.53055614],
                ],
            ],
        ];
    }
}
