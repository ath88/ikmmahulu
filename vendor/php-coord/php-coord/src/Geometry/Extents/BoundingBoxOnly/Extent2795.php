<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Shetland Islands onshore.
 * @internal
 */
class Extent2795
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-0.674088, 60.862596], [-1.773894, 60.862596], [-1.773894, 59.833216], [-0.674088, 59.833216], [-0.674088, 60.862596],
                ],
            ],
        ];
    }
}
