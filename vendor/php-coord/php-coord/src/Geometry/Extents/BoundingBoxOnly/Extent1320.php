<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Maturin.
 * @internal
 */
class Extent1320
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-63, 10.129999160767], [-64.299999237061, 10.129999160767], [-64.299999237061, 9.1000003814697], [-63, 9.1000003814697], [-63, 10.129999160767],
                ],
            ],
        ];
    }
}
