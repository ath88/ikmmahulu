<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Reconcavo and Jacuipe.
 * @internal
 */
class Extent3692
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-35.310708999634, -11.1875], [-39.080284118652, -11.1875], [-39.080284118652, -13.565958023071], [-35.310708999634, -13.565958023071], [-35.310708999634, -11.1875],
                ],
            ],
        ];
    }
}
