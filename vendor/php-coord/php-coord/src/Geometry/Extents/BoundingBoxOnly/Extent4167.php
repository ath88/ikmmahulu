<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Mariana plate.
 * @internal
 */
class Extent4167
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [136.975509628, 11.441767126], [129.489699621, 11.441767126], [129.489699621, 1.6487131170002], [136.975509628, 1.6487131170002], [136.975509628, 11.441767126],
                ],
            ],
            [
                [
                    [149.544455639, 23.893055138], [141.195102631, 23.893055138], [141.195102631, 10.950833126], [149.544455639, 10.950833126], [149.544455639, 23.893055138],
                ],
            ],
        ];
    }
}
