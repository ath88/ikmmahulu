<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - Mare.
 * @internal
 */
class Extent2819
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [168.18407591879, -21.327415185976], [167.75622008261, -21.327415185976], [167.75622008261, -21.708812462512], [168.18407591879, -21.708812462512], [168.18407591879, -21.327415185976],
                ],
            ],
        ];
    }
}
