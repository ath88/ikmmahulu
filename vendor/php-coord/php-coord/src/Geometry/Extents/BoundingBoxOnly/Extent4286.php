<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Cass.
 * @internal
 */
class Extent4286
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.165082000002, 40.910942000016], [-86.581738999997, 40.910942000016], [-86.581738999997, 40.561280999995], [-86.165082000002, 40.561280999995], [-86.165082000002, 40.910942000016],
                ],
            ],
        ];
    }
}
