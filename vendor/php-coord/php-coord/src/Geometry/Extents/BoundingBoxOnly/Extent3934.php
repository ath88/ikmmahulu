<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/French Southern Territories - Europa.
 * @internal
 */
class Extent3934
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [41.813239539, -20.913837904], [37.986952535, -20.913837904], [37.986952535, -25.699276908], [41.813239539, -25.699276908], [41.813239539, -20.913837904],
                ],
            ],
        ];
    }
}
