<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Syria - Al Whaleed area.
 * @internal
 */
class Extent2327
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.401439, 35.899493], [39.1514, 35.899493], [39.1514, 35.332856], [40.401439, 35.332856], [40.401439, 35.899493],
                ],
            ],
        ];
    }
}
