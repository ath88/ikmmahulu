<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Iraq and SW Iran.
 * @internal
 */
class Extent4238
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [51.055322050464, 37.383674621582], [38.794700622559, 37.383674621582], [38.794700622559, 29.061660766602], [51.055322050464, 29.061660766602], [51.055322050464, 37.383674621582],
                ],
            ],
        ];
    }
}
