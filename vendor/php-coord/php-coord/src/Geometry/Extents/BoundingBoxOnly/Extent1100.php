<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Gabon.
 * @internal
 */
class Extent1100
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.519581794739, 2.3178982734682], [7.0349995070007, 2.3178982734682], [7.0349995070007, -6.3693738899995], [14.519581794739, -6.3693738899995], [14.519581794739, 2.3178982734682],
                ],
            ],
        ];
    }
}
