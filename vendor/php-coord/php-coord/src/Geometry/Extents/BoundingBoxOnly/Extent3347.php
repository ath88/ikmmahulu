<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Novo Mesto.
 * @internal
 */
class Extent3347
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.460817842079, 45.864427794306], [15.063502253028, 45.864427794306], [15.063502253028, 45.663473806421], [15.460817842079, 45.663473806421], [15.460817842079, 45.864427794306],
                ],
            ],
        ];
    }
}
