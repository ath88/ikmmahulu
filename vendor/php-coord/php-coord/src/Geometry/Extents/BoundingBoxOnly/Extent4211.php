<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Portland.
 * @internal
 */
class Extent4211
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-122.11282393877, 46.008188838916], [-123.52949710642, 46.008188838916], [-123.52949710642, 45.239490065507], [-122.11282393877, 45.239490065507], [-122.11282393877, 46.008188838916],
                ],
            ],
        ];
    }
}
