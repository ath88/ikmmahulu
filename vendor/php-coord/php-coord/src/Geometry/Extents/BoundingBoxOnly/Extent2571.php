<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Greenland - Ammassalik area.
 * @internal
 */
class Extent2571
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-36.818891947688, 65.900506663905], [-38.858982313529, 65.900506663905], [-38.858982313529, 65.520424887779], [-36.818891947688, 65.520424887779], [-36.818891947688, 65.900506663905],
                ],
            ],
        ];
    }
}
