<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Medford-Diamond Lake.
 * @internal
 */
class Extent4477
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.71214437827, 43.334683748127], [-122.72661598128, 43.334683748127], [-122.72661598128, 42.538910778217], [-121.71214437827, 42.538910778217], [-121.71214437827, 43.334683748127],
                ],
            ],
        ];
    }
}
