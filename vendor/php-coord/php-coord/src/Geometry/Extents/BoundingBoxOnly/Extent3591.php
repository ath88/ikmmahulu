<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Taiwan - onshore  Penghu.
 * @internal
 */
class Extent3591
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [119.77184137601, 23.810504313136], [119.25811024188, 23.810504313136], [119.25811024188, 23.137742347341], [119.77184137601, 23.137742347341], [119.77184137601, 23.810504313136],
                ],
            ],
        ];
    }
}
