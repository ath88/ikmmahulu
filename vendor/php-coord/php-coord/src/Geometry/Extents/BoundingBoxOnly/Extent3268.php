<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Latvia - onshore.
 * @internal
 */
class Extent3268
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28.235967636109, 58.083255767822], [20.878847727894, 58.083255767822], [20.878847727894, 55.674835205078], [28.235967636109, 55.674835205078], [28.235967636109, 58.083255767822],
                ],
            ],
        ];
    }
}
