<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 74°30'N to 69°30'N,  4°E to 24°E.
 * @internal
 */
class Extent4091
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.000001907349, 74.500001907349], [4.0000019073486, 74.500001907349], [4.0000019073486, 69.500001907349], [24.000001907349, 69.500001907349], [24.000001907349, 74.500001907349],
                ],
            ],
        ];
    }
}
