<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Bahamas (San Salvador Island) - onshore.
 * @internal
 */
class Extent2414
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.37074387071, 24.189834128831], [-74.595192373684, 24.189834128831], [-74.595192373684, 23.90573042713], [-74.37074387071, 23.90573042713], [-74.37074387071, 24.189834128831],
                ],
            ],
        ];
    }
}
