<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - AEF 14°E to 21°E.
 * @internal
 */
class Extent2553
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21, 23.450553894043], [14, 23.450553894043], [14, -4.9072227478027], [21, -4.9072227478027], [21, 23.450553894043],
                ],
            ],
        ];
    }
}
