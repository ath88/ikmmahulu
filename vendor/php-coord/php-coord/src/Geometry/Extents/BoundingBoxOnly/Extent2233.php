<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New York - SPCS - C.
 * @internal
 */
class Extent2233
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-75.068162218745, 44.402776171315], [-77.745007937534, 44.402776171315], [-77.745007937534, 41.996367700579], [-75.068162218745, 41.996367700579], [-75.068162218745, 44.402776171315],
                ],
            ],
        ];
    }
}
