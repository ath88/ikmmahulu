<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kyrgyzstan - 76°01'E to 79°01'E.
 * @internal
 */
class Extent4388
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [79.016666666667, 42.995788574219], [76.016666666667, 42.995788574219], [76.016666666667, 40.3532371521], [79.016666666667, 40.3532371521], [79.016666666667, 42.995788574219],
                ],
            ],
        ];
    }
}
