<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Taranaki mc.
 * @internal
 */
class Extent3777
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [175.43929158, -38.4090111], [173.68738746643, -38.4090111], [173.68738746643, -39.779619111874], [175.43929158, -39.779619111874], [175.43929158, -38.4090111],
                ],
            ],
        ];
    }
}
