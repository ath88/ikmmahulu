<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Coast Range North.
 * @internal
 */
class Extent4471
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-123.01717360995, 45.978605112988], [-123.80377253718, 45.978605112988], [-123.80377253718, 45.402704247011], [-123.01717360995, 45.402704247011], [-123.01717360995, 45.978605112988],
                ],
            ],
        ];
    }
}
