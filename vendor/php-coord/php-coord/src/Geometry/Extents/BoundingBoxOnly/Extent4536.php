<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - Toronto.
 * @internal
 */
class Extent4536
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-79.115243191, 43.855457183], [-79.639264937, 43.855457183], [-79.639264937, 43.580995995], [-79.115243191, 43.580995995], [-79.115243191, 43.855457183],
                ],
            ],
        ];
    }
}
