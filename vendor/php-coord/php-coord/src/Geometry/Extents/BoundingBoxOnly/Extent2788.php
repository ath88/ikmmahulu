<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Western Sahara - north of 24.3°N.
 * @internal
 */
class Extent2788
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-8.6663894653319, 27.666957855225], [-15.416337960205, 27.666957855225], [-15.416337960205, 24.299999470526], [-8.6663894653319, 24.299999470526], [-8.6663894653319, 27.666957855225],
                ],
            ],
        ];
    }
}
