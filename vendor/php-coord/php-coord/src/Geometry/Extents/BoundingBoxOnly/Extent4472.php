<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arizona - Pima county east.
 * @internal
 */
class Extent4472
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-110.44649353698, 32.514678955078], [-111.56666666667, 32.514678955078], [-111.56666666667, 31.436804907092], [-110.44649353698, 31.436804907092], [-110.44649353698, 32.514678955078],
                ],
            ],
        ];
    }
}
