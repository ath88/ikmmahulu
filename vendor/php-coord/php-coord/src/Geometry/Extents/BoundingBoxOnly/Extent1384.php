<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa.
 * @internal
 */
class Extent1384
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.142796750337, 43.501457434001], [-96.640709192144, 43.501457434001], [-96.640709192144, 40.371946618264], [-90.142796750337, 40.371946618264], [-90.142796750337, 43.501457434001],
                ],
            ],
        ];
    }
}
