<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Hamilton and Tipton.
 * @internal
 */
class Extent4293
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.860298999991, 40.406862000011], [-86.243008999997, 40.406862000011], [-86.243008999997, 39.925939999984], [-85.860298999991, 39.925939999984], [-85.860298999991, 40.406862000011],
                ],
            ],
        ];
    }
}
