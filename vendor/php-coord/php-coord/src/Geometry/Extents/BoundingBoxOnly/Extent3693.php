<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Tucano and Jatoba.
 * @internal
 */
class Extent3693
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-37.093614578247, -8.3967514038086], [-39.132966995239, -8.3967514038086], [-39.132966995239, -12.261936187744], [-37.093614578247, -12.261936187744], [-37.093614578247, -8.3967514038086],
                ],
            ],
        ];
    }
}
