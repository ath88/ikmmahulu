<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Campos.
 * @internal
 */
class Extent2963
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-37.110092163086, -20.454732894897], [-42.034446716309, -20.454732894897], [-42.034446716309, -25.907276153564], [-37.110092163086, -25.907276153564], [-37.110092163086, -20.454732894897],
                ],
            ],
        ];
    }
}
