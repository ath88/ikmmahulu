<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alabama - SPCS - W.
 * @internal
 */
class Extent2155
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.302890362984, 35.016033638512], [-88.47295192106, 35.016033638512], [-88.47295192106, 30.143386136912], [-86.302890362984, 30.143386136912], [-86.302890362984, 35.016033638512],
                ],
            ],
        ];
    }
}
