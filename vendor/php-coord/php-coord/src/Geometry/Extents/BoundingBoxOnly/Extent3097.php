<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Finland - 23.5°E to 24.5°E onshore.
 * @internal
 */
class Extent3097
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.5, 68.833877563477], [23.5, 68.833877563477], [23.5, 65.672422814856], [24.5, 65.672422814856], [24.5, 68.833877563477],
                ],
            ],
            [
                [
                    [24.5, 65.077853575524], [24.434975992238, 65.077853575524], [24.434975992238, 64.937602434906], [24.5, 64.937602434906], [24.5, 65.077853575524],
                ],
            ],
            [
                [
                    [24.5, 64.84744216087], [23.5, 64.84744216087], [23.5, 59.862895861797], [24.5, 59.862895861797], [24.5, 64.84744216087],
                ],
            ],
        ];
    }
}
