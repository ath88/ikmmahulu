<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Nevada.
 * @internal
 */
class Extent1397
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-114.03739207419, 41.99663709824], [-119.99632466005, 41.99663709824], [-119.99632466005, 34.998914428613], [-114.03739207419, 34.998914428613], [-114.03739207419, 41.99663709824],
                ],
            ],
        ];
    }
}
