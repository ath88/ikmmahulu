<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Selvagens onshore.
 * @internal
 */
class Extent2779
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-15.794006677932, 30.20068354261], [-16.101188059995, 30.20068354261], [-16.101188059995, 29.980654149879], [-15.794006677932, 29.980654149879], [-15.794006677932, 30.20068354261],
                ],
            ],
        ];
    }
}
