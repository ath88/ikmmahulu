<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy.
 * @internal
 */
class Extent1127
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.986794518, 47.094581604004], [5.9349995060006, 47.094581604004], [5.9349995060006, 34.766805148001], [18.986794518, 34.766805148001], [18.986794518, 47.094581604004],
                ],
            ],
        ];
    }
}
