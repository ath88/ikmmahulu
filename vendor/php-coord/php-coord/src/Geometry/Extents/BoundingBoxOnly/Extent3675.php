<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Paraguay - north of 22°S.
 * @internal
 */
class Extent3675
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.814586639404, -19.296808242798], [-62.562103271484, -19.296808242798], [-62.562103271484, -22], [-57.814586639404, -22], [-57.814586639404, -19.296808242798],
                ],
            ],
        ];
    }
}
