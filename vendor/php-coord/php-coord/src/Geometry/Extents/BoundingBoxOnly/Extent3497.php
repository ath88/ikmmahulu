<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - 126°W to 120°W.
 * @internal
 */
class Extent3497
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-120, 49.087211608887], [-126, 49.087211608887], [-126, 30.542000143946], [-120, 30.542000143946], [-120, 49.087211608887],
                ],
            ],
        ];
    }
}
