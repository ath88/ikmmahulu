<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - Pennell Coast region.
 * @internal
 */
class Extent3855
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [172, -69.5], [160, -69.5], [160, -73], [172, -73], [172, -69.5],
                ],
            ],
        ];
    }
}
