<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 6th parallel south 17°E to 19°E.
 * @internal
 */
class Extent3161
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19, -3.4004286014746], [17, -3.4004286014746], [17, -5.3745184664702], [19, -5.3745184664702], [19, -3.4004286014746],
                ],
            ],
        ];
    }
}
