<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Beloit.
 * @internal
 */
class Extent4499
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-97.922116, 40.002662], [-98.505295, 40.002662], [-98.505295, 38.15875], [-97.922116, 38.15875], [-97.922116, 40.002662],
                ],
            ],
        ];
    }
}
