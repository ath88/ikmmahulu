<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - Sembilan and Melaka.
 * @internal
 */
class Extent3377
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [102.70050048828, 3.2784099578857], [101.70342144972, 3.2784099578857], [101.70342144972, 2.031938196087], [102.70050048828, 2.031938196087], [102.70050048828, 3.2784099578857],
                ],
            ],
        ];
    }
}
