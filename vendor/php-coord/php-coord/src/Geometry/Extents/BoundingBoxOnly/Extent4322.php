<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Oman - west of 54°E.
 * @internal
 */
class Extent4322
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [54, 19.666666666667], [51.999290466309, 19.666666666667], [51.999290466309, 14.947155315285], [54, 14.947155315285], [54, 19.666666666667],
                ],
            ],
        ];
    }
}
