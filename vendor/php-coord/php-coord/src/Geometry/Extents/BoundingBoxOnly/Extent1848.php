<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Madagascar - nearshore - west of 48°E.
 * @internal
 */
class Extent1848
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48, -13.00800028], [42.5331092528, -13.00800028], [42.5331092528, -26.5877609792], [48, -26.5877609792], [48, -13.00800028],
                ],
            ],
        ];
    }
}
