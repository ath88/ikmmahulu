<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Saudi Arabia - east of 54°E.
 * @internal
 */
class Extent3106
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [55.666107177735, 22.769686468669], [54, 22.769686468669], [54, 19.666526827507], [55.666107177735, 19.666526827507], [55.666107177735, 22.769686468669],
                ],
            ],
        ];
    }
}
