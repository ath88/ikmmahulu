<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Harrison and Washington.
 * @internal
 */
class Extent4307
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.847580000004, 38.783459999986], [-86.329910999993, 38.783459999986], [-86.329910999993, 37.958018000004], [-85.847580000004, 37.958018000004], [-85.847580000004, 38.783459999986],
                ],
            ],
        ];
    }
}
