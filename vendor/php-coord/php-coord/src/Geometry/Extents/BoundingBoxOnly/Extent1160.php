<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Mexico.
 * @internal
 */
class Extent1160
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.641849578996, 32.718456268526], [-122.18355761439, 32.718456268526], [-122.18355761439, 12.103074126835], [-84.641849578996, 12.103074126835], [-84.641849578996, 32.718456268526],
                ],
            ],
        ];
    }
}
