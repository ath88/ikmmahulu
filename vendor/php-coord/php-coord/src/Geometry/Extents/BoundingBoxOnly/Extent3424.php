<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Mexico - 114°W to 108°W.
 * @internal
 */
class Extent3424
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-108, 32.26117891515], [-114, 32.26117891515], [-114, 15.095595626396], [-108, 15.095595626396], [-108, 32.26117891515],
                ],
            ],
        ];
    }
}
