<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Guadeloupe - La Desirade - onshore.
 * @internal
 */
class Extent2893
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60.970051993848, 16.370709026981], [-61.127502046423, 16.370709026981], [-61.127502046423, 16.266285749846], [-60.970051993848, 16.266285749846], [-60.970051993848, 16.370709026981],
                ],
            ],
        ];
    }
}
