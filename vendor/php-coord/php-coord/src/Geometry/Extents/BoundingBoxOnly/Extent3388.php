<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - 42°E to 48°E.
 * @internal
 */
class Extent3388
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48, 37.383674621582], [42, 37.383674621582], [42, 29.061660766602], [48, 29.061660766602], [48, 37.383674621582],
                ],
            ],
        ];
    }
}
