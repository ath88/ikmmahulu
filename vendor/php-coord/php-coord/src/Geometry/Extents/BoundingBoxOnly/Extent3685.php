<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores W - Corvo onshore.
 * @internal
 */
class Extent3685
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-31.021375160971, 39.763624557822], [-31.170911070379, 39.763624557822], [-31.170911070379, 39.63689095055], [-31.021375160971, 39.63689095055], [-31.021375160971, 39.763624557822],
                ],
            ],
        ];
    }
}
