<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New Mexico - SPCS - E.
 * @internal
 */
class Extent2228
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-102.99740099902, 36.999760183727], [-105.71310487867, 36.999760183727], [-105.71310487867, 32.002022778779], [-102.99740099902, 32.002022778779], [-102.99740099902, 36.999760183727],
                ],
            ],
        ];
    }
}
