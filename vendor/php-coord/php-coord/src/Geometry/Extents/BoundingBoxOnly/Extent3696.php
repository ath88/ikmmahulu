<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Sergipe and Alagoas.
 * @internal
 */
class Extent3696
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-32.017768859863, -8.7333602905273], [-37.33452796936, -8.7333602905273], [-37.33452796936, -13.573160171509], [-32.017768859863, -13.573160171509], [-32.017768859863, -8.7333602905273],
                ],
            ],
        ];
    }
}
