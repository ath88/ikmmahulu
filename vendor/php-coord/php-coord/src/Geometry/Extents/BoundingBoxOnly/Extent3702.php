<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - SE.
 * @internal
 */
class Extent3702
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.608653779497, 32.505180406162], [43.985646130354, 32.505180406162], [43.985646130354, 29.061660766602], [48.608653779497, 29.061660766602], [48.608653779497, 32.505180406162],
                ],
            ],
        ];
    }
}
