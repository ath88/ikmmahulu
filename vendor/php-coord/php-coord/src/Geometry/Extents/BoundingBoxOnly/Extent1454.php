<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - Walvis Bay.
 * @internal
 */
class Extent1454
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.592483152149, -22.685833768403], [14.358000946624, -22.685833768403], [14.358000946624, -23.143828580916], [14.592483152149, -23.143828580916], [14.592483152149, -22.685833768403],
                ],
            ],
        ];
    }
}
