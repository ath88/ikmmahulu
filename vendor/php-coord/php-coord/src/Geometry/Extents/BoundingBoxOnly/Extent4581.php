<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Morocco and Western Sahara - onshore.
 * @internal
 */
class Extent4581
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-1.0118056535719, 35.968202682502], [-17.154740143063, 35.968202682502], [-17.154740143063, 20.716932163705], [-1.0118056535719, 20.716932163705], [-1.0118056535719, 35.968202682502],
                ],
            ],
        ];
    }
}
