<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Russia - 138°E to 144°E onshore.
 * @internal
 */
class Extent1783
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.58813667297, 54.362958908081], [138, 54.362958908081], [138, 46.044746196675], [141.58813667297, 46.044746196675], [141.58813667297, 54.362958908081],
                ],
            ],
            [
                [
                    [138.01403236389, 54.514290408363], [138, 54.514290408363], [138, 54.433080505137], [138.01403236389, 54.433080505137], [138.01403236389, 54.514290408363],
                ],
            ],
            [
                [
                    [138.29233360291, 55.139966614072], [138, 55.139966614072], [138, 54.749649437196], [138.29233360291, 54.749649437196], [138.29233360291, 55.139966614072],
                ],
            ],
            [
                [
                    [144, 72.941865921021], [138, 72.941865921021], [138, 56.317799827274], [144, 56.317799827274], [144, 72.941865921021],
                ],
            ],
            [
                [
                    [144, 76.268617630005], [138, 76.268617630005], [138, 74.598596572876], [144, 74.598596572876], [144, 76.268617630005],
                ],
            ],
            [
                [
                    [144, 54.472833633423], [141.55761146545, 54.472833633423], [141.55761146545, 45.843297958374], [144, 45.843297958374], [144, 54.472833633423],
                ],
            ],
            [
                [
                    [143.70223426819, 73.968149185181], [139.48131752014, 73.968149185181], [139.48131752014, 73.155237197876], [143.70223426819, 73.155237197876], [143.70223426819, 73.968149185181],
                ],
            ],
            [
                [
                    [141.29737663269, 74.33046913147], [139.89328956604, 74.33046913147], [139.89328956604, 73.853364944458], [141.29737663269, 73.853364944458], [141.29737663269, 74.33046913147],
                ],
            ],
        ];
    }
}
