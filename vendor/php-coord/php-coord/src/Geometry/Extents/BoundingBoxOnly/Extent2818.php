<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - Adelie Land coastal area.
 * @internal
 */
class Extent2818
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142, -65.619309342777], [136, -65.619309342777], [136, -67.128750020038], [142, -67.128750020038], [142, -65.619309342777],
                ],
            ],
        ];
    }
}
