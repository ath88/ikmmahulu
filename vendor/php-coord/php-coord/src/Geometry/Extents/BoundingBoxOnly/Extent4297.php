<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Johnson and Marion.
 * @internal
 */
class Extent4297
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.937381000003, 39.927137999997], [-86.328292999997, 39.927137999997], [-86.328292999997, 39.341671999978], [-85.937381000003, 39.341671999978], [-85.937381000003, 39.927137999997],
                ],
            ],
        ];
    }
}
