<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritania - north coast.
 * @internal
 */
class Extent2967
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-15.885378899157, 21.337653495847], [-17.07555770874, 21.337653495847], [-17.07555770874, 19.374012619516], [-15.885378899157, 19.374012619516], [-15.885378899157, 21.337653495847],
                ],
            ],
        ];
    }
}
