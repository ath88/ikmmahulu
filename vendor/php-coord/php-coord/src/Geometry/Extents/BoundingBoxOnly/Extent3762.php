<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - One Tree vcrs.
 * @internal
 */
class Extent3762
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [174.8205353008, -34.367473602295], [172.61206344845, -34.367473602295], [172.61206344845, -36.409630908518], [174.8205353008, -36.409630908518], [174.8205353008, -34.367473602295],
                ],
            ],
        ];
    }
}
