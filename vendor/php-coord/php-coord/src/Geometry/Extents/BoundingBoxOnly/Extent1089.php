<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Eritrea.
 * @internal
 */
class Extent1089
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [43.30073354, 18.091396133], [36.443283081055, 18.091396133], [36.443283081055, 12.363887786865], [43.30073354, 12.363887786865], [43.30073354, 18.091396133],
                ],
            ],
        ];
    }
}
