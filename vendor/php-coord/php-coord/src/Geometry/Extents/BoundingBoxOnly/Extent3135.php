<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Society Islands - Huahine.
 * @internal
 */
class Extent3135
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-150.89612128371, -16.634889615423], [-151.10339846275, -16.634889615423], [-151.10339846275, -16.862173884655], [-150.89612128371, -16.862173884655], [-150.89612128371, -16.634889615423],
                ],
            ],
        ];
    }
}
