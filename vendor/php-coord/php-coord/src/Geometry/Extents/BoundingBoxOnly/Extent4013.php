<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Papua New Guinea - PFTB.
 * @internal
 */
class Extent4013
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [144.74420215904, -5.596682578901], [142.24343529801, -5.596682578901], [142.24343529801, -8.2766088568401], [144.74420215904, -8.2766088568401], [144.74420215904, -5.596682578901],
                ],
            ],
        ];
    }
}
