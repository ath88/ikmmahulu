<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Busselton.
 * @internal
 */
class Extent4437
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [115.86666666667, -33.4], [115.18333333333, -33.4], [115.18333333333, -33.75], [115.86666666667, -33.75], [115.86666666667, -33.4],
                ],
            ],
        ];
    }
}
