<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Central America - Guatemala to Costa Rica.
 * @internal
 */
class Extent3876
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.534971334026, 17.821109771599], [-92.285793526611, 17.821109771599], [-92.285793526611, 7.9817113128033], [-82.534971334026, 7.9817113128033], [-82.534971334026, 17.821109771599],
                ],
            ],
            [
                [
                    [-86.212754271182, 16.485944768305], [-86.684970590137, 16.485944768305], [-86.684970590137, 16.220144758943], [-86.212754271182, 16.220144758943], [-86.212754271182, 16.485944768305],
                ],
            ],
        ];
    }
}
