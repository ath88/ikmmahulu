<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Equatorial Guinea - Bioko.
 * @internal
 */
class Extent4220
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.0121603872447, 3.813511458366], [8.3742783007448, 3.813511458366], [8.3742783007448, 3.1469076950358], [9.0121603872447, 3.1469076950358], [9.0121603872447, 3.813511458366],
                ],
            ],
        ];
    }
}
