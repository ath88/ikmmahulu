<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Mitchell.
 * @internal
 */
class Extent4478
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-119.82037984083, 44.773321589816], [-120.55197872177, 44.773321589816], [-120.55197872177, 44.388954414991], [-119.82037984083, 44.388954414991], [-119.82037984083, 44.773321589816],
                ],
            ],
        ];
    }
}
