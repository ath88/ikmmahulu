<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Berlin.
 * @internal
 */
class Extent2898
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.754165649414, 52.640830993652], [13.091102600098, 52.640830993652], [13.091102600098, 52.338340759277], [13.754165649414, 52.338340759277], [13.754165649414, 52.640830993652],
                ],
            ],
        ];
    }
}
