<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - onshore Vung Tau area.
 * @internal
 */
class Extent1494
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [107.57830446396, 11.032092094], [105.499343872, 11.032092094], [105.499343872, 9.0320217546688], [107.57830446396, 9.0320217546688], [107.57830446396, 11.032092094],
                ],
            ],
        ];
    }
}
