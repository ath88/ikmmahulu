<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Kuwait - south of 29.25°N.
 * @internal
 */
class Extent2957
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.470911889888, 29.441845939837], [46.546943664551, 29.441845939837], [46.546943664551, 28.538883209229], [48.470911889888, 28.538883209229], [48.470911889888, 29.441845939837],
                ],
            ],
        ];
    }
}
