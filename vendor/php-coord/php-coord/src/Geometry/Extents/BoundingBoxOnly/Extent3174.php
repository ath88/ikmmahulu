<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - FSU - CS63 zone C1.
 * @internal
 */
class Extent3174
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [26.45, 59.714493912448], [23.45, 59.714493912448], [23.45, 53.890338897705], [26.45, 53.890338897705], [26.45, 59.714493912448],
                ],
            ],
        ];
    }
}
