<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Kenya - onshore.
 * @internal
 */
class Extent3264
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [41.905166625977, 4.6224994659427], [33.907218933106, 4.6224994659427], [33.907218933106, -4.7170229625269], [41.905166625977, -4.7170229625269], [41.905166625977, 4.6224994659427],
                ],
            ],
        ];
    }
}
