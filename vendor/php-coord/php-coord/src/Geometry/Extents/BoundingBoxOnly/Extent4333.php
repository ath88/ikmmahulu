<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Sawyer.
 * @internal
 */
class Extent4333
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.676333830754, 46.157908407625], [-91.551891023282, 46.157908407625], [-91.551891023282, 45.637535073078], [-90.676333830754, 45.637535073078], [-90.676333830754, 46.157908407625],
                ],
            ],
        ];
    }
}
