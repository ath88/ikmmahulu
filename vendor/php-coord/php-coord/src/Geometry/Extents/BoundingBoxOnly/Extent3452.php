<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Greenland - 60°W to 54°W.
 * @internal
 */
class Extent3452
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-54, 84], [-60, 84], [-60, 58.912819092379], [-54, 58.912819092379], [-54, 84],
                ],
            ],
        ];
    }
}
