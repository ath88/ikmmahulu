<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Reunion - onshore.
 * @internal
 */
class Extent3337
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [55.904615161719, -20.81052747904], [55.16904605371, -20.81052747904], [55.16904605371, -21.419826472427], [55.904615161719, -21.419826472427], [55.904615161719, -20.81052747904],
                ],
            ],
        ];
    }
}
