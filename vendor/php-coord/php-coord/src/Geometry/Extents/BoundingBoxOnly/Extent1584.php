<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Java and Java Sea - west of 108°E.
 * @internal
 */
class Extent1584
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [108, -4.0771869857419], [105.06512643024, -4.0771869857419], [105.06512643024, -7.7898400918831], [108, -7.7898400918831], [108, -4.0771869857419],
                ],
            ],
        ];
    }
}
