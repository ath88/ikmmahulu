<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 40°N to 40°40'N; 141°E to 142°E.
 * @internal
 */
class Extent2455
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142, 40.666366666667], [141, 40.666366666667], [141, 39.999666666667], [142, 39.999666666667], [142, 40.666366666667],
                ],
            ],
        ];
    }
}
