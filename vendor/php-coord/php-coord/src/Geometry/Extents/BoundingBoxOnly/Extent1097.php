<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/French Guiana.
 * @internal
 */
class Extent1097
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-49.459503173828, 8.8771533966064], [-54.603782653809, 8.8771533966064], [-54.603782653809, 2.1134738922119], [-49.459503173828, 2.1134738922119], [-49.459503173828, 8.8771533966064],
                ],
            ],
        ];
    }
}
