<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 81°10'N to 76°10'N,  54°W to 24°W.
 * @internal
 */
class Extent4074
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-24, 81.166666030884], [-54, 81.166666030884], [-54, 76.166666030884], [-24, 76.166666030884], [-24, 81.166666030884],
                ],
            ],
        ];
    }
}
