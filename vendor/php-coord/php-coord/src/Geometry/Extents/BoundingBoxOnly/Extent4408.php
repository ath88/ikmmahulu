<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Maharashtra.
 * @internal
 */
class Extent4408
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [80.892158508301, 22.03099822998], [72.601288460155, 22.03099822998], [72.601288460155, 15.604599952698], [80.892158508301, 15.604599952698], [80.892158508301, 22.03099822998],
                ],
            ],
        ];
    }
}
