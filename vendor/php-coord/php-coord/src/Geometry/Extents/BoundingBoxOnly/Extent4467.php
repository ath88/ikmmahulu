<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Trinidad and Tobago - offshore west of 60°W.
 * @internal
 */
class Extent4467
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60, 12.331426620483], [-62.083055496216, 12.331426620483], [-62.083055496216, 9.8319454193116], [-60, 9.8319454193116], [-60, 12.331426620483],
                ],
            ],
        ];
    }
}
