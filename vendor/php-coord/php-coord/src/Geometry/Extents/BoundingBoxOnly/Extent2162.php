<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska - 160°W to 156°W.
 * @internal
 */
class Extent2162
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-159.73984892371, 55.336001194385], [-160, 55.336001194385], [-160, 54.978274221391], [-159.73984892371, 54.978274221391], [-159.73984892371, 55.336001194385],
                ],
            ],
            [
                [
                    [-155.99224588699, 71.396817302681], [-160, 71.396817302681], [-160, 55.517866892773], [-155.99224588699, 55.517866892773], [-155.99224588699, 71.396817302681],
                ],
            ],
            [
                [
                    [-159.25200736045, 55.290749933303], [-159.74211380464, 55.290749933303], [-159.74211380464, 54.895365434657], [-159.25200736045, 54.895365434657], [-159.25200736045, 55.290749933303],
                ],
            ],
            [
                [
                    [-156.8853672959, 56.628477429061], [-157.42001350857, 56.628477429061], [-157.42001350857, 56.476800852183], [-156.8853672959, 56.476800852183], [-156.8853672959, 56.628477429061],
                ],
            ],
        ];
    }
}
