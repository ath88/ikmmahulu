<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Papua New Guinea - North Fly.
 * @internal
 */
class Extent4216
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.54, -5.0599999999999], [140.89437347449, -5.0599999999999], [140.89437347449, -6.5999999999999], [141.54, -6.5999999999999], [141.54, -5.0599999999999],
                ],
            ],
        ];
    }
}
