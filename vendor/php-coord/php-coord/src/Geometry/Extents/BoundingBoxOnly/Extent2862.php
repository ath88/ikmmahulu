<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - east of 12°E.
 * @internal
 */
class Extent2862
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.033818244934, 55.021361105108], [11.575180053711, 55.021361105108], [11.575180053711, 47.469787597656], [15.033818244934, 47.469787597656], [15.033818244934, 55.021361105108],
                ],
            ],
        ];
    }
}
