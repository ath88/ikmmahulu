<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/France - offshore Mediterranean.
 * @internal
 */
class Extent3897
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.37585651, 43.734315010011], [3.0478006417269, 43.734315010011], [3.0478006417269, 41.150833154], [10.37585651, 41.150833154], [10.37585651, 43.734315010011],
                ],
            ],
        ];
    }
}
