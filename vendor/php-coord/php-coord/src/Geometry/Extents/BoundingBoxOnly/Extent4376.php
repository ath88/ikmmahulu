<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Sauk.
 * @internal
 */
class Extent4376
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.599702883126, 43.642111341562], [-90.312632312847, 43.642111341562], [-90.312632312847, 43.14539735216], [-89.599702883126, 43.14539735216], [-89.599702883126, 43.642111341562],
                ],
            ],
        ];
    }
}
