<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Liberia - onshore.
 * @internal
 */
class Extent3270
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-7.3684029579161, 8.5127773284915], [-11.510099379743, 8.5127773284915], [-11.510099379743, 4.2945974392374], [-7.3684029579161, 4.2945974392374], [-7.3684029579161, 8.5127773284915],
                ],
            ],
        ];
    }
}
