<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Finland - 24.5°E to 25.5°E onshore.
 * @internal
 */
class Extent3098
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25.5, 68.89151763916], [24.5, 68.89151763916], [24.5, 59.942675933349], [25.5, 59.942675933349], [25.5, 68.89151763916],
                ],
            ],
        ];
    }
}
