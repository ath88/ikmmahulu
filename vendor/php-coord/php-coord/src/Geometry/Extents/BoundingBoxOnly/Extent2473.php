<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 36°40'N to 37°20'N; 138°E to 139°E.
 * @internal
 */
class Extent2473
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [139, 37.332866666667], [138, 37.332866666667], [138, 36.666166666667], [139, 36.666166666667], [139, 37.332866666667],
                ],
            ],
        ];
    }
}
