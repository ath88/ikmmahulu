<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Vigo.
 * @internal
 */
class Extent4264
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.199012000002, 39.608237999991], [-87.610049999997, 39.608237999991], [-87.610049999997, 39.258981999984], [-87.199012000002, 39.258981999984], [-87.199012000002, 39.608237999991],
                ],
            ],
        ];
    }
}
