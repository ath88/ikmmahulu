<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/American Samoa - Tutuila and Aunu'u islands.
 * @internal
 */
class Extent2288
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-170.51187379253, -14.204306196095], [-170.87322752583, -14.204306196095], [-170.87322752583, -14.425555635571], [-170.51187379253, -14.425555635571], [-170.51187379253, -14.204306196095],
                ],
            ],
        ];
    }
}
