<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Burkina Faso.
 * @internal
 */
class Extent1057
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [2.3979253768921, 15.082777023315], [-5.5208339691162, 15.082777023315], [-5.5208339691162, 9.3956928253174], [2.3979253768921, 9.3956928253174], [2.3979253768921, 15.082777023315],
                ],
            ],
        ];
    }
}
