<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Barinas.
 * @internal
 */
class Extent1267
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.580242156982, 9.0623683929443], [-71.487241744995, 9.0623683929443], [-71.487241744995, 7.3128871917725], [-67.580242156982, 7.3128871917725], [-67.580242156982, 9.0623683929443],
                ],
            ],
        ];
    }
}
