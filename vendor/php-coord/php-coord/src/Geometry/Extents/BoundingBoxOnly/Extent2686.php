<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Russia - 118.5°E to 121.5°E onshore.
 * @internal
 */
class Extent2686
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [121.5, 73.627965427133], [118.5, 73.627965427133], [118.5, 49.875992437907], [121.5, 49.875992437907], [121.5, 73.627965427133],
                ],
            ],
        ];
    }
}
