<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 35°20'N to 36°N; 134°E to 135°E.
 * @internal
 */
class Extent2483
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [135, 35.724017463681], [134, 35.724017463681], [134, 35.332766666667], [135, 35.332766666667], [135, 35.724017463681],
                ],
            ],
        ];
    }
}
