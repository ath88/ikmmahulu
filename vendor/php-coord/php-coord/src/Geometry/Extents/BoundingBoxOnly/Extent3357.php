<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - GoM OCS.
 * @internal
 */
class Extent3357
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.170347468075, 30.246306685173], [-97.211482716314, 30.246306685173], [-97.211482716314, 23.823225057201], [-81.170347468075, 23.823225057201], [-81.170347468075, 30.246306685173],
                ],
            ],
        ];
    }
}
