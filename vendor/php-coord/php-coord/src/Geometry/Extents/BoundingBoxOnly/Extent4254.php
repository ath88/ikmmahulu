<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Jasper and Porter.
 * @internal
 */
class Extent4254
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.929257000002, 41.760495103412], [-87.276573000019, 41.760495103412], [-87.276573000019, 40.736516000037], [-86.929257000002, 40.736516000037], [-86.929257000002, 41.760495103412],
                ],
            ],
        ];
    }
}
