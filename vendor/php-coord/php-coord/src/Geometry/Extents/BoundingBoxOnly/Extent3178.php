<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - east of 36°W onshore.
 * @internal
 */
class Extent3178
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-34.743459701538, -4.9997310638428], [-36, -4.9997310638428], [-36, -10.098299026489], [-34.743459701538, -10.098299026489], [-34.743459701538, -4.9997310638428],
                ],
            ],
        ];
    }
}
