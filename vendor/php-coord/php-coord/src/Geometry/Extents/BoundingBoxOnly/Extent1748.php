<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - zone VIII.
 * @internal
 */
class Extent1748
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [31.218499048217, 71.162711913802], [26.9729166666, 71.162711913802], [26.9729166666, 69.028739929199], [31.218499048217, 69.028739929199], [31.218499048217, 71.162711913802],
                ],
            ],
        ];
    }
}
