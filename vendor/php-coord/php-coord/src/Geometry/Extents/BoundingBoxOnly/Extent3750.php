<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Samdrup Jongkhar district.
 * @internal
 */
class Extent3750
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [92.122177124024, 27.24952428028], [91.394712410205, 27.24952428028], [91.394712410205, 26.791383743286], [92.122177124024, 26.791383743286], [92.122177124024, 27.24952428028],
                ],
            ],
        ];
    }
}
