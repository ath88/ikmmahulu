<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - 6.5°E to 10.5°E.
 * @internal
 */
class Extent1714
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.5, 13.527208080306], [6.5, 13.527208080306], [6.5, 3.5791386711019], [10.5, 3.5791386711019], [10.5, 13.527208080306],
                ],
            ],
        ];
    }
}
