<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Zambia.
 * @internal
 */
class Extent1260
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [33.702278137207, -8.1916675567627], [21.996387481689, -8.1916675567627], [21.996387481689, -18.074922561646], [33.702278137207, -18.074922561646], [33.702278137207, -8.1916675567627],
                ],
            ],
        ];
    }
}
