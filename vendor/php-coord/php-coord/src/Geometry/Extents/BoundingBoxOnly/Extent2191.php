<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Idaho - SPCS - C.
 * @internal
 */
class Extent2191
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-112.67850379354, 45.697376143977], [-115.29468656262, 45.697376143977], [-115.29468656262, 41.995390897469], [-112.67850379354, 41.995390897469], [-112.67850379354, 45.697376143977],
                ],
            ],
        ];
    }
}
