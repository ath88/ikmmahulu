<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Somalia - onshore east of 48°E.
 * @internal
 */
class Extent1555
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [51.462085942261, 12.026309979417], [48, 12.026309979417], [48, 4.4421817281337], [51.462085942261, 4.4421817281337], [51.462085942261, 12.026309979417],
                ],
            ],
        ];
    }
}
