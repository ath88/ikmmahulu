<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New Mexico - SPCS83 - W.
 * @internal
 */
class Extent2232
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-106.32337783221, 36.999471575633], [-109.05134615599, 36.999471575633], [-109.05134615599, 31.332603397144], [-106.32337783221, 31.332603397144], [-106.32337783221, 36.999471575633],
                ],
            ],
        ];
    }
}
