<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - South Sudan and Sudan - 24°E to 30°E.
 * @internal
 */
class Extent2827
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, 22.003608703614], [23.999644954683, 22.003608703614], [23.999644954683, 4.2169650010669], [30, 4.2169650010669], [30, 22.003608703614],
                ],
            ],
        ];
    }
}
