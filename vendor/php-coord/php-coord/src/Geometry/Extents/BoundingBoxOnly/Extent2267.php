<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - SPCS - N.
 * @internal
 */
class Extent2267
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.05822859479, 47.302520751953], [-92.886908236887, 47.302520751953], [-92.886908236887, 45.376802082385], [-88.05822859479, 45.376802082385], [-88.05822859479, 47.302520751953],
                ],
            ],
        ];
    }
}
