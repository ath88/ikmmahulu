<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Gresham-Warm Springs.
 * @internal
 */
class Extent4201
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.68081490326, 45.548449411868], [-122.42235322267, 45.548449411868], [-122.42235322267, 45.025595171478], [-121.68081490326, 45.025595171478], [-121.68081490326, 45.548449411868],
                ],
            ],
        ];
    }
}
