<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Maine - CS2000 - E.
 * @internal
 */
class Extent2960
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.917007446289, 47.36513710022], [-68.570422280546, 47.36513710022], [-68.570422280546, 44.187513351441], [-66.917007446289, 44.187513351441], [-66.917007446289, 47.36513710022],
                ],
            ],
        ];
    }
}
