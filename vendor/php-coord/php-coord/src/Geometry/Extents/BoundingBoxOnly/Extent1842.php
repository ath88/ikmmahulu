<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - 18°E to 20°E.
 * @internal
 */
class Extent1842
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.999999799347, -17.386169433594], [18.00000026067, -17.386169433594], [18.00000026067, -28.961875915527], [19.999999799347, -28.961875915527], [19.999999799347, -17.386169433594],
                ],
            ],
        ];
    }
}
