<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - offshore - Yellow Sea.
 * @internal
 */
class Extent3469
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [125.05812817459, 37.392241388536], [119.23746806571, 37.392241388536], [119.23746806571, 31.230509716207], [125.05812817459, 31.230509716207], [125.05812817459, 37.392241388536],
                ],
            ],
        ];
    }
}
