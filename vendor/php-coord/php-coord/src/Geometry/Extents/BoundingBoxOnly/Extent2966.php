<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - offshore 18°S to 34°S.
 * @internal
 */
class Extent2966
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-35.195253372192, -18], [-53.374298095703, -18], [-53.374298095703, -34], [-35.195253372192, -34], [-35.195253372192, -18],
                ],
            ],
        ];
    }
}
