<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Gambia - onshore.
 * @internal
 */
class Extent3250
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-13.798612594604, 13.826387405396], [-16.872958065175, 13.826387405396], [-16.872958065175, 13.055833128], [-13.798612594604, 13.055833128], [-13.798612594604, 13.826387405396],
                ],
            ],
        ];
    }
}
