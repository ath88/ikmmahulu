<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Ukraine - east of 40°E.
 * @internal
 */
class Extent4434
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.178745269775, 49.616661071777], [40, 49.616661071777], [40, 49.149212101712], [40.178745269775, 49.149212101712], [40.178745269775, 49.616661071777],
                ],
            ],
            [
                [
                    [40.074855804443, 48.893741607666], [40, 48.893741607666], [40, 48.808738708496], [40.074855804443, 48.808738708496], [40.074855804443, 48.893741607666],
                ],
            ],
        ];
    }
}
