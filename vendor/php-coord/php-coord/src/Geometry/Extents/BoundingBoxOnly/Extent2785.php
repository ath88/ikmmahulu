<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - Murzuq and En Naga.
 * @internal
 */
class Extent2785
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.717280701531, 27.660677626094], [18.370141772495, 27.660677626094], [18.370141772495, 27.327692430217], [18.717280701531, 27.327692430217], [18.717280701531, 27.660677626094],
                ],
            ],
        ];
    }
}
