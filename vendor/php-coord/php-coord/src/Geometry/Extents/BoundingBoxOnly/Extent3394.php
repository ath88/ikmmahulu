<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Sachsen - east of 13.5°E.
 * @internal
 */
class Extent3394
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.033818244934, 51.574760437012], [13.5, 51.574760437012], [13.5, 50.628385267947], [15.033818244934, 50.628385267947], [15.033818244934, 51.574760437012],
                ],
            ],
        ];
    }
}
