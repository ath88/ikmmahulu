<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Alberta.
 * @internal
 */
class Extent2376
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.98324515223, 60], [-120, 60], [-120, 48.999435424805], [-109.98324515223, 48.999435424805], [-109.98324515223, 60],
                ],
            ],
        ];
    }
}
