<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Seychelles - Mahe Island.
 * @internal
 */
class Extent2369
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [55.588998277606, -4.5091919978339], [55.309771396597, -4.5091919978339], [55.309771396597, -4.8578112362067], [55.588998277606, -4.8578112362067], [55.588998277606, -4.5091919978339],
                ],
            ],
        ];
    }
}
