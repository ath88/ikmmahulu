<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Qatar - offshore.
 * @internal
 */
class Extent2406
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.034721549, 27.043114141], [50.815833547, 27.043114141], [50.815833547, 24.64243333846], [53.034721549, 24.64243333846], [53.034721549, 27.043114141],
                ],
            ],
            [
                [
                    [50.807496927995, 25.58611114], [50.557395547, 25.58611114], [50.557395547, 24.794965400524], [50.807496927995, 24.794965400524], [50.807496927995, 25.58611114],
                ],
            ],
        ];
    }
}
