<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - onshore.
 * @internal
 */
class Extent3307
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.607872009278, 46.876247406006], [13.383471488953, 46.876247406006], [13.383471488953, 45.425819396973], [16.607872009278, 45.425819396973], [16.607872009278, 46.876247406006],
                ],
            ],
        ];
    }
}
