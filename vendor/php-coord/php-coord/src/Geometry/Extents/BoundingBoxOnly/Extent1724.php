<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Michigan - SPCS - C.
 * @internal
 */
class Extent1724
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.273961848427, 45.915586244236], [-87.050454813413, 45.915586244236], [-87.050454813413, 43.807921114189], [-82.273961848427, 43.807921114189], [-82.273961848427, 45.915586244236],
                ],
            ],
        ];
    }
}
