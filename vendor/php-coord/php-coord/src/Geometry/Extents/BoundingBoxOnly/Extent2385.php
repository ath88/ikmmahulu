<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Panama - Canal Zone.
 * @internal
 */
class Extent2385
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-79.46966630762, 9.4424714336175], [-80.060081537887, 9.4424714336175], [-80.060081537887, 8.8229306664539], [-79.46966630762, 8.8229306664539], [-79.46966630762, 9.4424714336175],
                ],
            ],
        ];
    }
}
