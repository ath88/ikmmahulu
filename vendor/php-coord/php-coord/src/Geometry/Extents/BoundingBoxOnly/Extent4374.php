<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Richland.
 * @internal
 */
class Extent4374
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.191695579288, 43.554983971824], [-90.671654960003, 43.554983971824], [-90.671654960003, 43.164022556165], [-90.191695579288, 43.164022556165], [-90.191695579288, 43.554983971824],
                ],
            ],
        ];
    }
}
