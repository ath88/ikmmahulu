<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Macedonia.
 * @internal
 */
class Extent1148
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [23.030969619751, 42.358951568604], [20.45881652832, 42.358951568604], [20.45881652832, 40.855888366699], [23.030969619751, 40.855888366699], [23.030969619751, 42.358951568604],
                ],
            ],
        ];
    }
}
