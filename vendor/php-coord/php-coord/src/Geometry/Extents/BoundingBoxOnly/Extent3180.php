<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Angola (Cabinda) and DR Congo (Zaire) - offshore.
 * @internal
 */
class Extent3180
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.366639842955, -5.0549634886659], [10.53152041439, -5.0549634886659], [10.53152041439, -6.0332808892048], [12.366639842955, -6.0332808892048], [12.366639842955, -5.0549634886659],
                ],
            ],
        ];
    }
}
