<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - northeast Kalimantan.
 * @internal
 */
class Extent2770
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [119.05724835258, 4.2869301613658], [116.96570970357, 4.2869301613658], [116.96570970357, -0.057257596136822], [119.05724835258, -0.057257596136822], [119.05724835258, 4.2869301613658],
                ],
            ],
        ];
    }
}
