<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Jan Mayen - onshore.
 * @internal
 */
class Extent2869
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-7.8785124529609, 71.230817247509], [-9.1699060689202, 71.230817247509], [-9.1699060689202, 70.753862928272], [-7.8785124529609, 70.753862928272], [-7.8785124529609, 71.230817247509],
                ],
            ],
        ];
    }
}
