<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Rusk.
 * @internal
 */
class Extent4353
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.678033720404, 45.639098611962], [-91.541198754713, 45.639098611962], [-91.541198754713, 45.29150977165], [-90.678033720404, 45.29150977165], [-90.678033720404, 45.639098611962],
                ],
            ],
        ];
    }
}
