<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - Upper Austria, Salzberg and Bohemia.
 * @internal
 */
class Extent4455
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.821344207434, 51.052494049073], [12.075905799866, 51.052494049073], [12.075905799866, 46.93803024292], [16.821344207434, 46.93803024292], [16.821344207434, 51.052494049073],
                ],
            ],
        ];
    }
}
