<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas.
 * @internal
 */
class Extent1385
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.588387, 40.003166], [-102.051769, 40.003166], [-102.051769, 36.993016], [-94.588387, 36.993016], [-94.588387, 40.003166],
                ],
            ],
        ];
    }
}
