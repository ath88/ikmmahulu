<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Maribor and Ptuj.
 * @internal
 */
class Extent2587
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.995043677151, 46.735086935727], [15.441324754563, 46.735086935727], [15.441324754563, 46.332177217381], [15.995043677151, 46.332177217381], [15.995043677151, 46.735086935727],
                ],
            ],
        ];
    }
}
