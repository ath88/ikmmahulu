<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - Angola proper - offshore - west of 12°E.
 * @internal
 */
class Extent1606
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, -6.0332721686867], [8.2018785080004, -6.0332721686867], [8.2018785080004, -17.254833218282], [12, -17.254833218282], [12, -6.0332721686867],
                ],
            ],
        ];
    }
}
