<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Faroe Islands.
 * @internal
 */
class Extent1093
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-0.48672750004351, 65.694080176966], [-13.909696512969, 65.694080176966], [-13.909696512969, 59.940833172001], [-0.48672750004351, 59.940833172001], [-0.48672750004351, 65.694080176966],
                ],
            ],
        ];
    }
}
