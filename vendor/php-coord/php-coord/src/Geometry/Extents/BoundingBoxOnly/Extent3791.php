<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Okarito mc.
 * @internal
 */
class Extent3791
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [170.88759954, -43.005965468677], [169.21611412286, -43.005965468677], [169.21611412286, -43.84880442], [170.88759954, -43.84880442], [170.88759954, -43.005965468677],
                ],
            ],
        ];
    }
}
