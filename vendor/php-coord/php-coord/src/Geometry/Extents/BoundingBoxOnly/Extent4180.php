<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Baker.
 * @internal
 */
class Extent4180
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-117.37280716486, 45.185147355712], [-118.14832577146, 45.185147355712], [-118.14832577146, 44.608788504405], [-117.37280716486, 44.608788504405], [-117.37280716486, 45.185147355712],
                ],
            ],
        ];
    }
}
