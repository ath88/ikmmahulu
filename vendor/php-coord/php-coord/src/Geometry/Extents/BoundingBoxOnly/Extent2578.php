<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - upper Soca Valley.
 * @internal
 */
class Extent2578
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.861061102488, 46.486815790018], [13.383471500017, 46.486815790018], [13.383471500017, 46.162197291499], [13.861061102488, 46.162197291499], [13.861061102488, 46.486815790018],
                ],
            ],
        ];
    }
}
