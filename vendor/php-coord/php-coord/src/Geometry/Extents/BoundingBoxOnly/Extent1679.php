<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - onshore west of 72°E.
 * @internal
 */
class Extent1679
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [72, 28.210445902031], [68.136841438671, 28.210445902031], [68.136841438671, 20.64073433723], [72, 20.64073433723], [72, 28.210445902031],
                ],
            ],
        ];
    }
}
