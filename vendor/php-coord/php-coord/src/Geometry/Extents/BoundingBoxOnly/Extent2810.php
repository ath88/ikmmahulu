<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Nuku Hiva.
 * @internal
 */
class Extent2810
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-139.96196229351, -8.7281947840461], [-140.30613463032, -8.7281947840461], [-140.30613463032, -9.0063909779777], [-139.96196229351, -9.0063909779777], [-139.96196229351, -8.7281947840461],
                ],
            ],
        ];
    }
}
