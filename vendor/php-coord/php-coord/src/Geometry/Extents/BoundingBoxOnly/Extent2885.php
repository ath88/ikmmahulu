<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Otranto channel.
 * @internal
 */
class Extent2885
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.986794518, 41.024080386725], [17.959197301353, 41.024080386725], [17.959197301353, 39.779383963748], [18.986794518, 39.779383963748], [18.986794518, 41.024080386725],
                ],
            ],
        ];
    }
}
