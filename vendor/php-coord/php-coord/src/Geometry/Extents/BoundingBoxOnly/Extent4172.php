<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Falklands - 63W to 57 W - offshore.
 * @internal
 */
class Extent4172
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-56.999652830545, -47.680107116699], [-63.000237861125, -47.680107116699], [-63.000237861125, -56.245471954346], [-56.999652830545, -56.245471954346], [-56.999652830545, -47.680107116699],
                ],
            ],
        ];
    }
}
