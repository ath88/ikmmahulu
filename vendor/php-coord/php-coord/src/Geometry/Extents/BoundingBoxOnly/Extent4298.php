<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Shelby.
 * @internal
 */
class Extent4298
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.629395999992, 39.699082999978], [-85.953530999997, 39.699082999978], [-85.953530999997, 39.34726900002], [-85.629395999992, 39.34726900002], [-85.629395999992, 39.699082999978],
                ],
            ],
        ];
    }
}
