<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - onshore.
 * @internal
 */
class Extent3625
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.608537698861, 37.383674621582], [38.794700622559, 37.383674621582], [38.794700622559, 29.061660766602], [48.608537698861, 29.061660766602], [48.608537698861, 37.383674621582],
                ],
            ],
        ];
    }
}
