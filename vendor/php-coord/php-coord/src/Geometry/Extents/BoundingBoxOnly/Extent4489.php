<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - Adelie Land coastal area west of 138°E.
 * @internal
 */
class Extent4489
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [138, -65.619309342777], [136, -65.619309342777], [136, -66.726669681698], [138, -66.726669681698], [138, -65.619309342777],
                ],
            ],
        ];
    }
}
