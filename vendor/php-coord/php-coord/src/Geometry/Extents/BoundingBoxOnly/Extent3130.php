<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Hiva Oa and Tahuata.
 * @internal
 */
class Extent3130
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-138.75975282085, -9.6454177607307], [-139.22225706684, -9.6454177607307], [-139.22225706684, -10.074655286637], [-138.75975282085, -10.074655286637], [-138.75975282085, -9.6454177607307],
                ],
            ],
        ];
    }
}
