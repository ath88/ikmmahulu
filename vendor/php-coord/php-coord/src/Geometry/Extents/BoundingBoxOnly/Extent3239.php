<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Dominica - onshore.
 * @internal
 */
class Extent3239
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.200892216513, 15.680483541089], [-61.541286900401, 15.680483541089], [-61.541286900401, 15.149499972573], [-61.200892216513, 15.149499972573], [-61.200892216513, 15.680483541089],
                ],
            ],
        ];
    }
}
