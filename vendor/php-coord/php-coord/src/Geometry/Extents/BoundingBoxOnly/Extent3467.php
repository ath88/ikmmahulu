<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/North America - Great Lakes basin.
 * @internal
 */
class Extent3467
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.471648263408, 50.7320797127], [-93.162978293834, 50.7320797127], [-93.162978293834, 40.999080421049], [-74.471648263408, 40.999080421049], [-74.471648263408, 50.7320797127],
                ],
            ],
        ];
    }
}
