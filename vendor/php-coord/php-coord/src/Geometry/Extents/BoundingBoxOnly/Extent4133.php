<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - offshore between 42°W to 36°W, northern hemisphere.
 * @internal
 */
class Extent4133
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-38.220708847046, 0.73265266418468], [-42, 0.73265266418468], [-42, 0], [-38.220708847046, 0], [-38.220708847046, 0.73265266418468],
                ],
            ],
        ];
    }
}
