<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Taiwan - 120°E to 122°E.
 * @internal
 */
class Extent3562
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [122.05485028066, 26.710867655348], [119.99569467659, 26.710867655348], [119.99569467659, 20.418337346308], [122.05485028066, 20.418337346308], [122.05485028066, 26.710867655348],
                ],
            ],
        ];
    }
}
