<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Korea N and S - east of 130°E.
 * @internal
 */
class Extent3727
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [130.74442180298, 42.972778242338], [130, 42.972778242338], [130, 41.849527261518], [130.74442180298, 41.849527261518], [130.74442180298, 42.972778242338],
                ],
            ],
            [
                [
                    [131.00453972388, 37.611497070086], [130.71862565547, 37.611497070086], [130.71862565547, 37.390709684502], [131.00453972388, 37.390709684502], [131.00453972388, 37.611497070086],
                ],
            ],
        ];
    }
}
