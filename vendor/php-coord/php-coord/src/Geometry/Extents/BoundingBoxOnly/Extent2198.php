<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - SPCS - N.
 * @internal
 */
class Extent2198
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.150663060238, 43.501457434001], [-96.640709192144, 43.501457434001], [-96.640709192144, 41.851689572396], [-90.150663060238, 41.851689572396], [-90.150663060238, 43.501457434001],
                ],
            ],
        ];
    }
}
