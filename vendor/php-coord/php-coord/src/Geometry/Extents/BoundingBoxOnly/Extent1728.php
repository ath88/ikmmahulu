<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - north of 34°39'N.
 * @internal
 */
class Extent1728
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8.6380538940432, 37.139756994041], [-2.213111075743, 37.139756994041], [-2.213111075743, 34.649999490861], [8.6380538940432, 34.649999490861], [8.6380538940432, 37.139756994041],
                ],
            ],
        ];
    }
}
