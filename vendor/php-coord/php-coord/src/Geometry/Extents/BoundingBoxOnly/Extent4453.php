<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Lancelin.
 * @internal
 */
class Extent4453
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [115.61666666667, -30.716666666667], [115.15, -30.716666666667], [115.15, -31.483333333333], [115.61666666667, -31.483333333333], [115.61666666667, -30.716666666667],
                ],
            ],
        ];
    }
}
