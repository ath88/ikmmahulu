<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Minnesota - SPCS - C.
 * @internal
 */
class Extent2213
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-92.291597305372, 47.478361818002], [-96.854989849337, 47.478361818002], [-96.854989849337, 45.284586580222], [-92.291597305372, 45.284586580222], [-92.291597305372, 47.478361818002],
                ],
            ],
        ];
    }
}
