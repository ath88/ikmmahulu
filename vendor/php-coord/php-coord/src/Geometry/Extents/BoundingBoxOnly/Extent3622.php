<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 23°E to 25°E.
 * @internal
 */
class Extent3622
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25, -4.5887231164498], [23, -4.5887231164498], [23, -11.462639808655], [25, -11.462639808655], [25, -4.5887231164498],
                ],
            ],
        ];
    }
}
