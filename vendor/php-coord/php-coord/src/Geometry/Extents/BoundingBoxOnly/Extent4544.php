<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/North America - Canada, US (Conus+AK), PRVI.
 * @internal
 */
class Extent4544
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-63.88888856, 21.854251136094], [-68.481888563935, 21.854251136094], [-68.481888563935, 14.928194130078], [-63.88888856, 14.928194130078], [-63.88888856, 21.854251136094],
                ],
            ],
            [
                [
                    [-47.743430543984, 86.453745196084], [-192.34928894043, 86.453745196084], [-192.34928894043, 23.81750013802], [-47.743430543984, 23.81750013802], [-47.743430543984, 86.453745196084],
                ],
            ],
        ];
    }
}
