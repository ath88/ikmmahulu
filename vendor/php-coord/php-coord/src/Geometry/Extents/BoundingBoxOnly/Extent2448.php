<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - south of 42°40'N; 143°E to 144°E.
 * @internal
 */
class Extent2448
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [143.75595037076, 42.666466666667], [143, 42.666466666667], [143, 41.875806260696], [143.75595037076, 41.875806260696], [143.75595037076, 42.666466666667],
                ],
            ],
        ];
    }
}
