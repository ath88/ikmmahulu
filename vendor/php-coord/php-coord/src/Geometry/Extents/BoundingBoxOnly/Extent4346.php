<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Marathon.
 * @internal
 */
class Extent4346
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.223405070113, 45.120559393437], [-90.316952127107, 45.120559393437], [-90.316952127107, 44.681264027334], [-89.223405070113, 44.681264027334], [-89.223405070113, 45.120559393437],
                ],
            ],
        ];
    }
}
