<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Prairie City-Brogan.
 * @internal
 */
class Extent4484
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-117.48532435601, 45.013518559584], [-118.76089255428, 45.013518559584], [-118.76089255428, 44.162105133975], [-117.48532435601, 44.162105133975], [-117.48532435601, 45.013518559584],
                ],
            ],
        ];
    }
}
