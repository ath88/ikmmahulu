<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Burundi.
 * @internal
 */
class Extent1058
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30.853191375732, -2.3015625476837], [28.984998703003, -2.3015625476837], [28.984998703003, -4.4480562210083], [30.853191375732, -4.4480562210083], [30.853191375732, -2.3015625476837],
                ],
            ],
        ];
    }
}
