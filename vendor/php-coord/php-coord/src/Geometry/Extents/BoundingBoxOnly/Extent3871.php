<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - 72°W to 66°W onshore.
 * @internal
 */
class Extent3871
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.800647735596, 41.131361007691], [-72, 41.131361007691], [-72, 40.960814055168], [-71.800647735596, 40.960814055168], [-71.800647735596, 41.131361007691],
                ],
            ],
            [
                [
                    [-71.534226031553, 41.244524866935], [-71.625121441976, 41.244524866935], [-71.625121441976, 41.136497337271], [-71.534226031553, 41.136497337271], [-71.534226031553, 41.244524866935],
                ],
            ],
            [
                [
                    [-69.89352607727, 41.433023452759], [-70.298570632934, 41.433023452759], [-70.298570632934, 41.197793960571], [-69.89352607727, 41.197793960571], [-69.89352607727, 41.433023452759],
                ],
            ],
            [
                [
                    [-66.917007446289, 47.467893600464], [-72, 47.467893600464], [-72, 41.236063964578], [-66.917007446289, 41.236063964578], [-66.917007446289, 47.467893600464],
                ],
            ],
        ];
    }
}
