<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - west of 6°W.
 * @internal
 */
class Extent1735
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-6, 29.848888397217], [-8.6672229766844, 29.848888397217], [-8.6672229766844, 25.731513431949], [-6, 25.731513431949], [-6, 29.848888397217],
                ],
            ],
        ];
    }
}
