<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - onshore.
 * @internal
 */
class Extent3213
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.986474990845, 37.139756994042], [-8.6672229766844, 37.139756994042], [-8.6672229766844, 18.976387023926], [11.986474990845, 18.976387023926], [11.986474990845, 37.139756994042],
                ],
            ],
        ];
    }
}
