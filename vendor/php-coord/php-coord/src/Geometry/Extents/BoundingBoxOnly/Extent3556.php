<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - Antipodes and Bounty Islands.
 * @internal
 */
class Extent3556
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [179.36444736072, -47.545987709092], [178.70659952012, -47.545987709092], [178.70659952012, -47.968078835902], [179.36444736072, -47.968078835902], [179.36444736072, -47.545987709092],
                ],
            ],
            [
                [
                    [179.13166711075, -49.437139768249], [178.40811727319, -49.437139768249], [178.40811727319, -49.91930561785], [179.13166711075, -49.91930561785], [179.13166711075, -49.437139768249],
                ],
            ],
        ];
    }
}
