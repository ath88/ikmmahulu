<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - Gran Canaria.
 * @internal
 */
class Extent4593
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-15.315279006958, 28.22162601847], [-15.872640609741, 28.22162601847], [-15.872640609741, 27.681108474732], [-15.315279006958, 27.681108474732], [-15.315279006958, 28.22162601847],
                ],
            ],
        ];
    }
}
