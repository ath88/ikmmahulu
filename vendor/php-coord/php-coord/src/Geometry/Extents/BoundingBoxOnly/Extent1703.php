<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Morocco - north of 31.5°N.
 * @internal
 */
class Extent1703
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-1.0118056535719, 35.968202682502], [-9.8428326384857, 35.968202682502], [-9.8428326384857, 31.499998444304], [-1.0118056535719, 31.499998444304], [-1.0118056535719, 35.968202682502],
                ],
            ],
        ];
    }
}
