<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Ames-Des Moines.
 * @internal
 */
class Extent4237
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.231443, 42.210094], [-94.280862, 42.210094], [-94.280862, 41.156356], [-93.231443, 41.156356], [-93.231443, 42.210094],
                ],
            ],
        ];
    }
}
