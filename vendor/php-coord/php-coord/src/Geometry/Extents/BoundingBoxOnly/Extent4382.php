<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - Ain Tsila.
 * @internal
 */
class Extent4382
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8.2661028750328, 28.096483802656], [7.6610146531578, 28.096483802656], [7.6610146531578, 27.400980489694], [8.2661028750328, 27.400980489694], [8.2661028750328, 28.096483802656],
                ],
            ],
        ];
    }
}
