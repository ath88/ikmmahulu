<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Willamette Pass.
 * @internal
 */
class Extent4488
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.48789474425, 44.179112326448], [-122.25005516595, 44.179112326448], [-122.25005516595, 42.998898247309], [-121.48789474425, 42.998898247309], [-121.48789474425, 44.179112326448],
                ],
            ],
        ];
    }
}
