<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Tunisia - offshore.
 * @internal
 */
class Extent1489
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.661269513, 38.401667152], [7.8166665070002, 38.401667152], [7.8166665070002, 33.220332548244], [13.661269513, 33.220332548244], [13.661269513, 38.401667152],
                ],
            ],
        ];
    }
}
