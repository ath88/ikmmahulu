<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - SPCS - S.
 * @internal
 */
class Extent2201
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.60908, 38.870054], [-102.045253, 38.870054], [-102.045253, 36.993016], [-94.60908, 36.993016], [-94.60908, 38.870054],
                ],
            ],
        ];
    }
}
