<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 6th parallel south 23°E to 25°E.
 * @internal
 */
class Extent3164
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.999998450007, -5.0177259358015], [23.000000184814, -5.0177259358015], [23.000000184814, -6.9885498243353], [24.999998450007, -6.9885498243353], [24.999998450007, -5.0177259358015],
                ],
            ],
        ];
    }
}
