<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - 12°E to 14°E.
 * @internal
 */
class Extent1839
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.000013164642, -16.954170227051], [12.000001997016, -16.954170227051], [12.000001997016, -21.891936294152], [14.000013164642, -21.891936294152], [14.000013164642, -16.954170227051],
                ],
            ],
        ];
    }
}
