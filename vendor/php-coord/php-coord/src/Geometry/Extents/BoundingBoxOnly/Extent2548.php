<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - AOF west of 10°W.
 * @internal
 */
class Extent2548
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-10, 26], [-17.584400304716, 26], [-17.584400304716, 8.2986106872559], [-10, 8.2986106872559], [-10, 26],
                ],
            ],
        ];
    }
}
