<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Louisiana - SPCS83 - S.
 * @internal
 */
class Extent2529
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.758344024157, 31.060144435799], [-93.938061882605, 31.060144435799], [-93.938061882605, 28.854289941421], [-88.758344024157, 28.854289941421], [-88.758344024157, 31.060144435799],
                ],
            ],
        ];
    }
}
