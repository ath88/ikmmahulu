<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Arunachal Pradesh.
 * @internal
 */
class Extent4395
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [97.415161132813, 29.463344573975], [91.554824829102, 29.463344573975], [91.554824829102, 26.656021118164], [97.415161132813, 26.656021118164], [97.415161132813, 29.463344573975],
                ],
            ],
        ];
    }
}
