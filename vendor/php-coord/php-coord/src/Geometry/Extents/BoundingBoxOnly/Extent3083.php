<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia region 2.
 * @internal
 */
class Extent3083
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-73, 11.588243484497], [-76.075048446655, 11.588243484497], [-76.075048446655, 9.3999996185303], [-73, 9.3999996185303], [-73, 11.588243484497],
                ],
            ],
        ];
    }
}
