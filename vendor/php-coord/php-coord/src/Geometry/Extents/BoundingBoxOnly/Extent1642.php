<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Egypt - east of 33°E onshore.
 * @internal
 */
class Extent1642
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [36.949366870388, 31.35714682938], [33, 31.35714682938], [33, 21.978206575604], [36.949366870388, 21.978206575604], [36.949366870388, 31.35714682938],
                ],
            ],
        ];
    }
}
