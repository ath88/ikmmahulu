<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Jay.
 * @internal
 */
class Extent4292
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.802265000017, 40.572214999998], [-85.219946999998, 40.572214999998], [-85.219946999998, 40.306705999992], [-84.802265000017, 40.306705999992], [-84.802265000017, 40.572214999998],
                ],
            ],
        ];
    }
}
