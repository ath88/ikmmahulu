<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska - 144°W to 141°W.
 * @internal
 */
class Extent2158
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-140.9802604146, 70.159222444052], [-144.00298418045, 70.159222444052], [-144.00298418045, 59.725013222547], [-140.9802604146, 59.725013222547], [-140.9802604146, 70.159222444052],
                ],
            ],
        ];
    }
}
