<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/French Southern Territories - Crozet 48°E to 54°E.
 * @internal
 */
class Extent4247
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [54, -42.616907924], [48, -42.616907924], [48, -49.819224931], [54, -49.819224931], [54, -42.616907924],
                ],
            ],
        ];
    }
}
