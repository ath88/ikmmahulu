<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - Gambat.
 * @internal
 */
class Extent3589
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [69.29754, 27.666781], [68.249898, 27.666781], [68.249898, 25.883599], [69.29754, 25.883599], [69.29754, 27.666781],
                ],
            ],
        ];
    }
}
