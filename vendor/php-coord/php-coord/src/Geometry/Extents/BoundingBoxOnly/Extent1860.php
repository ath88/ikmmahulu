<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - zone VII.
 * @internal
 */
class Extent1860
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [137.831023, 37.571300416443], [136.22099366857, 37.571300416443], [136.22099366857, 34.518468298108], [137.831023, 34.518468298108], [137.831023, 37.571300416443],
                ],
            ],
        ];
    }
}
