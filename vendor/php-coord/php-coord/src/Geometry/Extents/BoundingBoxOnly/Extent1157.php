<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritania.
 * @internal
 */
class Extent1157
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-4.8061113357543, 27.290458679199], [-20.038915519, 27.290458679199], [-20.038915519, 14.725639343262], [-4.8061113357543, 14.725639343262], [-4.8061113357543, 27.290458679199],
                ],
            ],
        ];
    }
}
