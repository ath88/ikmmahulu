<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Maracaibo area.
 * @internal
 */
class Extent1319
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.5, 10.999681472778], [-72.245609283447, 10.999681472778], [-72.245609283447, 10.000001907349], [-71.5, 10.000001907349], [-71.5, 10.999681472778],
                ],
            ],
        ];
    }
}
