<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Fiji - Vanua Levu and Taveuni.
 * @internal
 */
class Extent3401
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [180.22889518738, -16.103469848633], [178.42787361145, -16.103469848633], [178.42787361145, -17.061946868896], [180.22889518738, -17.061946868896], [180.22889518738, -16.103469848633],
                ],
            ],
        ];
    }
}
