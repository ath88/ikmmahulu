<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Spencer.
 * @internal
 */
class Extent4277
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.765122000003, 38.205130000006], [-87.268848269093, 38.205130000006], [-87.268848269093, 37.782512000004], [-86.765122000003, 37.782512000004], [-86.765122000003, 38.205130000006],
                ],
            ],
        ];
    }
}
