<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Paraguay.
 * @internal
 */
class Extent1188
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-54.243896484375, -19.296808242798], [-62.643768310547, -19.296808242798], [-62.643768310547, -27.58472442627], [-54.243896484375, -27.58472442627], [-54.243896484375, -19.296808242798],
                ],
            ],
        ];
    }
}
