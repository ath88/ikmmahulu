<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Pemagatshel district.
 * @internal
 */
class Extent3747
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.552841445693, 27.179386668736], [91.003868321484, 27.179386668736], [91.003868321484, 26.781234741211], [91.552841445693, 26.781234741211], [91.552841445693, 27.179386668736],
                ],
            ],
        ];
    }
}
