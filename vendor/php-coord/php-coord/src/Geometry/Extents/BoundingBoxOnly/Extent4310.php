<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - St Mary valley.
 * @internal
 */
class Extent4310
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-113, 49.000001907349], [-113.967, 49.000001907349], [-113.967, 48.55], [-113, 48.55], [-113, 49.000001907349],
                ],
            ],
        ];
    }
}
