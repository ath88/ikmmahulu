<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 12°E to 18°E.
 * @internal
 */
class Extent3626
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18, -3.417722073872], [12, -3.417722073872], [12, -8.1029014055696], [18, -8.1029014055696], [18, -3.417722073872],
                ],
            ],
        ];
    }
}
