<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Queensland - Weipa.
 * @internal
 */
class Extent4491
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142.5, -11.5], [141.50130894219, -11.5], [141.50130894219, -13.5], [142.5, -13.5], [142.5, -11.5],
                ],
            ],
        ];
    }
}
