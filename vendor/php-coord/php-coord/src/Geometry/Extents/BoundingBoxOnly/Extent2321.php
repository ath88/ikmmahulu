<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore blocks 1 and 16.
 * @internal
 */
class Extent2321
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.080393328374, -6.0332697387694], [11.080366376929, -6.0332697387694], [11.080366376929, -7.2513862263063], [12.080393328374, -7.2513862263063], [12.080393328374, -6.0332697387694],
                ],
            ],
        ];
    }
}
