<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Walworth.
 * @internal
 */
class Extent4378
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.304648166607, 42.84302906765], [-88.778846753406, 42.84302906765], [-88.778846753406, 42.491966313006], [-88.304648166607, 42.491966313006], [-88.304648166607, 42.84302906765],
                ],
            ],
        ];
    }
}
