<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - Katanga.
 * @internal
 */
class Extent3147
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30.771240234375, -4.999000072479], [21.749025344849, -4.999000072479], [21.749025344849, -13.458057403564], [30.771240234375, -13.458057403564], [30.771240234375, -4.999000072479],
                ],
            ],
        ];
    }
}
