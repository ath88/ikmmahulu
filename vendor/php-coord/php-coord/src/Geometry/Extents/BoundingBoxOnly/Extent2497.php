<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 34°40'N to 35°20'N; 139°E to 140°E.
 * @internal
 */
class Extent2497
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140, 35.332766666667], [139, 35.332766666667], [139, 34.666066666667], [140, 34.666066666667], [140, 35.332766666667],
                ],
            ],
        ];
    }
}
