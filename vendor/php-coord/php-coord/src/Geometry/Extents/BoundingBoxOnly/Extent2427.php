<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 44°40'N to 45°20'N; 141°E to 142°E.
 * @internal
 */
class Extent2427
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142, 45.333266666667], [141.50285621668, 45.333266666667], [141.50285621668, 44.666566666667], [142, 44.666566666667], [142, 45.333266666667],
                ],
            ],
        ];
    }
}
