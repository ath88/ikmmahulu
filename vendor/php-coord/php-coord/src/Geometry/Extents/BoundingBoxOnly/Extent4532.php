<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Costa Rica - offshore Pacific.
 * @internal
 */
class Extent4532
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.920275043418, 11.105667125969], [-90.444061584, 11.105667125969], [-90.444061584, 2.1507381179902], [-82.920275043418, 2.1507381179902], [-82.920275043418, 11.105667125969],
                ],
            ],
        ];
    }
}
