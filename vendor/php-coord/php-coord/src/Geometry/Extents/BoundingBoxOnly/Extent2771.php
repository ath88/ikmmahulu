<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Niger - southeast.
 * @internal
 */
class Extent2771
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.897544084231, 16.691966898722], [7.8169262766427, 16.691966898722], [7.8169262766427, 12.802434921265], [14.897544084231, 12.802434921265], [14.897544084231, 16.691966898722],
                ],
            ],
        ];
    }
}
