<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Oberlin.
 * @internal
 */
class Extent4497
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-100.14794, 40.002269], [-100.818795, 40.002269], [-100.818795, 38.697341], [-100.14794, 38.697341], [-100.14794, 40.002269],
                ],
            ],
        ];
    }
}
