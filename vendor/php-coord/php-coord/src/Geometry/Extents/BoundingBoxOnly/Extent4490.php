<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Hamburg.
 * @internal
 */
class Extent4490
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.402684742865, 53.764917141546], [9.6645228186926, 53.764917141546], [9.6645228186926, 53.366733020942], [10.402684742865, 53.366733020942], [10.402684742865, 53.764917141546],
                ],
            ],
            [
                [
                    [8.5918850456206, 53.989402635407], [8.3308705562628, 53.989402635407], [8.3308705562628, 53.884836389321], [8.5918850456206, 53.884836389321], [8.5918850456206, 53.989402635407],
                ],
            ],
        ];
    }
}
