<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Bela Krajina.
 * @internal
 */
class Extent2584
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.356630325318, 45.734302520795], [15.0654710566, 45.734302520795], [15.0654710566, 45.425819396973], [15.356630325318, 45.425819396973], [15.356630325318, 45.734302520795],
                ],
            ],
        ];
    }
}
