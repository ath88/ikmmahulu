<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - South Dakota.
 * @internal
 */
class Extent1410
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.439394750965, 45.943547318258], [-104.06103614077, 45.943547318258], [-104.06103614077, 42.488459217507], [-96.439394750965, 42.488459217507], [-96.439394750965, 45.943547318258],
                ],
            ],
        ];
    }
}
