<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Vermont.
 * @internal
 */
class Extent1414
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.505372300629, 45.020830154419], [-73.436000711279, 45.020830154419], [-73.436000711279, 42.725852529085], [-71.505372300629, 42.725852529085], [-71.505372300629, 45.020830154419],
                ],
            ],
        ];
    }
}
