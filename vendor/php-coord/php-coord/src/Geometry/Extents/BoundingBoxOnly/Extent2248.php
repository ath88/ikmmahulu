<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - South Carolina - SPCS27 - S.
 * @internal
 */
class Extent2248
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78.956189674722, 33.946929931641], [-82.020301818848, 33.946929931641], [-82.020301818848, 32.052025355722], [-78.956189674722, 32.052025355722], [-78.956189674722, 33.946929931641],
                ],
            ],
        ];
    }
}
