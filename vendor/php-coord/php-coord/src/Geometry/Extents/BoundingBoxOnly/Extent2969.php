<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritania - east of 6°W.
 * @internal
 */
class Extent2969
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-4.8061113357542, 25.731513431948], [-6, 25.731513431948], [-6, 24.9994430542], [-4.8061113357542, 24.9994430542], [-4.8061113357542, 25.731513431948],
                ],
            ],
            [
                [
                    [-5.3350000381469, 20.091156399207], [-6, 20.091156399207], [-6, 15.498371124268], [-5.3350000381469, 15.498371124268], [-5.3350000381469, 20.091156399207],
                ],
            ],
        ];
    }
}
