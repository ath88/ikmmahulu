<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Huntington and Whitley.
 * @internal
 */
class Extent4284
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.307748000003, 41.295428000007], [-85.686602999997, 41.295428000007], [-85.686602999997, 40.652944999996], [-85.307748000003, 40.652944999996], [-85.307748000003, 41.295428000007],
                ],
            ],
        ];
    }
}
