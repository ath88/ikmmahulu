<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Bend-Klamath Falls.
 * @internal
 */
class Extent4192
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-120.77339598601, 43.887594325703], [-122.44196838679, 43.887594325703], [-122.44196838679, 41.883291161731], [-120.77339598601, 41.883291161731], [-120.77339598601, 43.887594325703],
                ],
            ],
        ];
    }
}
