<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - west of 78°35'W.
 * @internal
 */
class Extent3091
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78.58302116394, 2.4757900238037], [-79.098392486572, 2.4757900238037], [-79.098392486572, 1.2312450408936], [-78.58302116394, 1.2312450408936], [-78.58302116394, 2.4757900238037],
                ],
            ],
        ];
    }
}
