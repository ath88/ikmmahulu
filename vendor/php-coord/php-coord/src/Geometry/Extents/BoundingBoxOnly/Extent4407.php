<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Madhya Pradesh.
 * @internal
 */
class Extent4407
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [82.807777404785, 26.874370574951], [74.03466796875, 26.874370574951], [74.03466796875, 21.075309753418], [82.807777404785, 21.075309753418], [82.807777404785, 26.874370574951],
                ],
            ],
        ];
    }
}
