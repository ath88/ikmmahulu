<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 17°E to 19°E.
 * @internal
 */
class Extent3153
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.000000696762, 4.7626634176904], [17.000000546113, 4.7626634176904], [17.000000546113, -8.1077785491942], [19.000000696762, -8.1077785491942], [19.000000696762, 4.7626634176904],
                ],
            ],
        ];
    }
}
