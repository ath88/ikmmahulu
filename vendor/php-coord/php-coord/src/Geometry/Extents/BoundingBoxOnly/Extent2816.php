<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/French Southern Territories - Kerguelen onshore.
 * @internal
 */
class Extent2816
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [70.617490221142, -48.601389478565], [68.690271924854, -48.601389478565], [68.690271924854, -49.775006700634], [70.617490221142, -49.775006700634], [70.617490221142, -48.601389478565],
                ],
            ],
        ];
    }
}
