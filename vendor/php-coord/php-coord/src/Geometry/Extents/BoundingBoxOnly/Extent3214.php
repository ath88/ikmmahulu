<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Anguilla - onshore.
 * @internal
 */
class Extent3214
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-62.921716525494, 18.321686235435], [-63.218734971454, 18.321686235435], [-63.218734971454, 18.115725167972], [-62.921716525494, 18.115725167972], [-62.921716525494, 18.321686235435],
                ],
            ],
        ];
    }
}
