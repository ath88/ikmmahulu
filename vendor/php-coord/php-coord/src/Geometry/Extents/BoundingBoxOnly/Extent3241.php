<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Ecuador - mainland onshore.
 * @internal
 */
class Extent3241
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-75.216842651367, 1.4496593475342], [-81.025228500366, 1.4496593475342], [-81.025228500366, -5.0003089904785], [-75.216842651367, -5.0003089904785], [-75.216842651367, 1.4496593475342],
                ],
            ],
        ];
    }
}
