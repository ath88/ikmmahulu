<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Mexico - offshore GoM - Campeche area N.
 * @internal
 */
class Extent3461
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.6754397737, 23.000082150155], [-94.321314690065, 23.000082150155], [-94.321314690065, 20.875820712196], [-88.6754397737, 20.875820712196], [-88.6754397737, 23.000082150155],
                ],
            ],
        ];
    }
}
