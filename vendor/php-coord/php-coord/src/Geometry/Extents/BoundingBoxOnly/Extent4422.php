<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Delhi.
 * @internal
 */
class Extent4422
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [77.33773803711, 28.884511947632], [76.832916259766, 28.884511947632], [76.832916259766, 28.408460617065], [77.33773803711, 28.408460617065], [77.33773803711, 28.884511947632],
                ],
            ],
        ];
    }
}
