<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Uganda - north of equator and west of 30°E.
 * @internal
 */
class Extent1578
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, 0.85646348543137], [29.718193054199, 0.85646348543137], [29.718193054199, 0], [30, 0], [30, 0.85646348543137],
                ],
            ],
        ];
    }
}
