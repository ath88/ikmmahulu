<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Reunion - east of 54ºE.
 * @internal
 */
class Extent3911
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [58.239722554, -18.286388901], [54, -18.286388901], [54, -24.710473907], [58.239722554, -24.710473907], [58.239722554, -18.286388901],
                ],
            ],
        ];
    }
}
