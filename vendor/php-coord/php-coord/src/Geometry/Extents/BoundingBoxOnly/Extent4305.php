<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Ripley.
 * @internal
 */
class Extent4305
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.065287000003, 39.310068999967], [-85.444911999997, 39.310068999967], [-85.444911999997, 38.912967999998], [-85.065287000003, 38.912967999998], [-85.065287000003, 39.310068999967],
                ],
            ],
        ];
    }
}
