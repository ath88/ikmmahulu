<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran - west of 48°E.
 * @internal
 */
class Extent1464
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48, 39.779155731202], [44.034954071045, 39.779155731202], [44.034954071045, 30.997189026679], [48, 30.997189026679], [48, 39.779155731202],
                ],
            ],
        ];
    }
}
