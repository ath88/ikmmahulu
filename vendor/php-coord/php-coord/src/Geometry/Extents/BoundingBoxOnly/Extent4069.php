<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 24°E to 30°E.
 * @internal
 */
class Extent4069
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, 71.204394181342], [24, 71.204394181342], [24, 68.580810546875], [30, 68.580810546875], [30, 71.204394181342],
                ],
            ],
        ];
    }
}
