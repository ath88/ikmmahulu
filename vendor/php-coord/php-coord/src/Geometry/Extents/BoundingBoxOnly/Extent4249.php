<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/French Southern Territories - 60°E to 66°E.
 * @internal
 */
class Extent4249
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [66, -45.731661853852], [62.966388559, -45.731661853852], [62.966388559, -53.021692544111], [66, -53.021692544111], [66, -45.731661853852],
                ],
            ],
        ];
    }
}
