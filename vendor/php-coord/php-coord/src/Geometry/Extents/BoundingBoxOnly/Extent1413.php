<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Utah.
 * @internal
 */
class Extent1413
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.04320640865, 42.002300584901], [-114.04727299918, 42.002300584901], [-114.04727299918, 36.991746303875], [-109.04320640865, 36.991746303875], [-109.04320640865, 42.002300584901],
                ],
            ],
        ];
    }
}
