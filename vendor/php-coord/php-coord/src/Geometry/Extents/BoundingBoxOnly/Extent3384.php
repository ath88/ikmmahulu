<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - Kelantan.
 * @internal
 */
class Extent3384
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [102.66780090332, 6.2848140378334], [101.33689880371, 6.2848140378334], [101.33689880371, 4.5491108894349], [102.66780090332, 4.5491108894349], [102.66780090332, 6.2848140378334],
                ],
            ],
        ];
    }
}
