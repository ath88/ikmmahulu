<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - 108°W to 102°W.
 * @internal
 */
class Extent3526
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-102, 84], [-108, 84], [-108, 48.999435424805], [-102, 48.999435424805], [-102, 84],
                ],
            ],
        ];
    }
}
