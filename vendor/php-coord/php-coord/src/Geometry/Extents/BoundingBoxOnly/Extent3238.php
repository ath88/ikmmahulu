<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Djibouti - onshore.
 * @internal
 */
class Extent3238
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [43.471460127472, 12.713750128001], [41.759857177735, 12.713750128001], [41.759857177735, 10.942220687866], [43.471460127472, 10.942220687866], [43.471460127472, 12.713750128001],
                ],
            ],
        ];
    }
}
