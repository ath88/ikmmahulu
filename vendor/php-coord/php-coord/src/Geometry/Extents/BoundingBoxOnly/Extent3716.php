<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Korea N and S - 126°E to 128°E.
 * @internal
 */
class Extent3716
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [128, 41.79610824585], [126, 41.79610824585], [126, 33.967201467628], [128, 33.967201467628], [128, 41.79610824585],
                ],
            ],
            [
                [
                    [127.00073793969, 33.601149002781], [126.09573068346, 33.601149002781], [126.09573068346, 33.142271233754], [127.00073793969, 33.142271233754], [127.00073793969, 33.601149002781],
                ],
            ],
        ];
    }
}
