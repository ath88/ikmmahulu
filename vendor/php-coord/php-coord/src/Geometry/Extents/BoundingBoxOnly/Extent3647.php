<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 7ºE to 8ºE.
 * @internal
 */
class Extent3647
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8, 63.517071135433], [7, 63.517071135433], [7, 57.93807588073], [8, 57.93807588073], [8, 63.517071135433],
                ],
            ],
        ];
    }
}
