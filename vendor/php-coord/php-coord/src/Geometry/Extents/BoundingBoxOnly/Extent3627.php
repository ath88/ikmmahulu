<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 18°E to 24°E.
 * @internal
 */
class Extent3627
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24, -3.5745721730536], [18, -3.5745721730536], [18, -11.237222671509], [24, -11.237222671509], [24, -3.5745721730536],
                ],
            ],
        ];
    }
}
