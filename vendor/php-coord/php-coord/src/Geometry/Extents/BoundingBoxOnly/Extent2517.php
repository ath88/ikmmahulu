<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 32°40'N to 33°20'N; 130°E to 131°E.
 * @internal
 */
class Extent2517
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [131, 33.332666666667], [130, 33.332666666667], [130, 32.665966666667], [131, 32.665966666667], [131, 33.332666666667],
                ],
            ],
        ];
    }
}
