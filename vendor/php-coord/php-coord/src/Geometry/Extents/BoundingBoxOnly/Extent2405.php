<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kazakhstan - Caspian Sea.
 * @internal
 */
class Extent2405
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.145848788499, 46.962942861514], [48.905799244616, 46.962942861514], [48.905799244616, 41.159086418066], [53.145848788499, 41.159086418066], [53.145848788499, 46.962942861514],
                ],
            ],
        ];
    }
}
