<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - California - SPCS - 2.
 * @internal
 */
class Extent2176
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-119.54677889377, 40.153923768601], [-124.0541471408, 40.153923768601], [-124.0541471408, 38.028146006556], [-119.54677889377, 38.028146006556], [-119.54677889377, 40.153923768601],
                ],
            ],
        ];
    }
}
