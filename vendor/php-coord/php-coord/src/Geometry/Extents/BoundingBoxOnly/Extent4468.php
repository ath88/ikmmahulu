<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Trinidad and Tobago - offshore east of 60°W.
 * @internal
 */
class Extent4468
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.286916732788, 12.187283750475], [-60, 12.187283750475], [-60, 9.9519150659196], [-57.286916732788, 9.9519150659196], [-57.286916732788, 12.187283750475],
                ],
            ],
        ];
    }
}
