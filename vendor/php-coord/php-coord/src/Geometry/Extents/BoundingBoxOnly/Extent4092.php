<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 74°30'N to 69°30'N,  24°E to 44°E.
 * @internal
 */
class Extent4092
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [44.000001907349, 74.500001907349], [24.000001907349, 74.500001907349], [24.000001907349, 69.500001907349], [44.000001907349, 69.500001907349], [44.000001907349, 74.500001907349],
                ],
            ],
        ];
    }
}
