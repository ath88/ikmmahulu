<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Russia - 121.5°E to 124.5°E onshore.
 * @internal
 */
class Extent2687
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [124.5, 73.994442258418], [121.5, 73.994442258418], [121.5, 53.188325881958], [124.5, 53.188325881958], [124.5, 73.994442258418],
                ],
            ],
        ];
    }
}
