<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Guyana.
 * @internal
 */
class Extent1114
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-55.775762557983, 10.693311691284], [-61.389724731445, 10.693311691284], [-61.389724731445, 1.1868762969971], [-55.775762557983, 1.1868762969971], [-55.775762557983, 10.693311691284],
                ],
            ],
        ];
    }
}
