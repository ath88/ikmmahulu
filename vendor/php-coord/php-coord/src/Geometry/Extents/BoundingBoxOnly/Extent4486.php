<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Warner Highway.
 * @internal
 */
class Extent4486
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-119.30104789209, 42.425601985483], [-120.41187469583, 42.425601985483], [-120.41187469583, 41.957859836749], [-119.30104789209, 41.957859836749], [-119.30104789209, 42.425601985483],
                ],
            ],
        ];
    }
}
