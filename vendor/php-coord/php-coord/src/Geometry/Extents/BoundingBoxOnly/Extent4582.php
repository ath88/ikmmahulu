<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - London to Birmingham and Crewe.
 * @internal
 */
class Extent4582
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-0.0026622599091583, 53.29877995544], [-2.7484664640652, 53.29877995544], [-2.7484664640652, 51.453370041071], [-0.0026622599091583, 51.453370041071], [-0.0026622599091583, 53.29877995544],
                ],
            ],
        ];
    }
}
