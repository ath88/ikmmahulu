<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - North Sea - offshore south of 62°N.
 * @internal
 */
class Extent2334
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.13155777908, 62], [1.3744536290441, 62], [1.3744536290441, 56.086666167996], [11.13155777908, 56.086666167996], [11.13155777908, 62],
                ],
            ],
        ];
    }
}
