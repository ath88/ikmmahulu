<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - east of 10.5°E.
 * @internal
 */
class Extent1713
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.649651527405, 13.719999313355], [10.499999482183, 13.719999313355], [10.499999482183, 6.4331936836245], [14.649651527405, 6.4331936836245], [14.649651527405, 13.719999313355],
                ],
            ],
        ];
    }
}
