<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Uttar Pradesh.
 * @internal
 */
class Extent4419
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [84.630599975586, 30.4124584198], [77.084907531739, 30.4124584198], [77.084907531739, 23.872770309448], [84.630599975586, 23.872770309448], [84.630599975586, 30.4124584198],
                ],
            ],
        ];
    }
}
