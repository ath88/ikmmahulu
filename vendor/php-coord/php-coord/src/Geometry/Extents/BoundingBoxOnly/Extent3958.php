<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines - zone I onshore.
 * @internal
 */
class Extent3958
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [118, 9.31880383402], [117.12822810216, 9.31880383402], [117.12822810216, 8.1317160497967], [118, 8.1317160497967], [118, 9.31880383402],
                ],
            ],
            [
                [
                    [117.14361404476, 8.1278775432757], [116.89968746764, 8.1278775432757], [116.89968746764, 7.7553109377861], [117.14361404476, 7.7553109377861], [117.14361404476, 8.1278775432757],
                ],
            ],
        ];
    }
}
