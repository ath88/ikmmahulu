<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - southeastern.
 * @internal
 */
class Extent3567
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.356630325318, 45.764429427576], [14.535186178434, 45.764429427576], [14.535186178434, 45.425819396973], [15.356630325318, 45.425819396973], [15.356630325318, 45.764429427576],
                ],
            ],
        ];
    }
}
