<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Santiam Pass.
 * @internal
 */
class Extent4213
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-121.69313196256, 44.659272605269], [-122.5, 44.659272605269], [-122.5, 44.107963781427], [-121.69313196256, 44.107963781427], [-121.69313196256, 44.659272605269],
                ],
            ],
        ];
    }
}
