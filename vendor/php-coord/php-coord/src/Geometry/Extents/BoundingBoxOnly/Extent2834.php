<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 13 30.
 * @internal
 */
class Extent2834
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.783298958838, 62.272886958762], [12.124443054199, 62.272886958762], [12.124443054199, 55.289268540114], [14.783298958838, 55.289268540114], [14.783298958838, 62.272886958762],
                ],
            ],
        ];
    }
}
