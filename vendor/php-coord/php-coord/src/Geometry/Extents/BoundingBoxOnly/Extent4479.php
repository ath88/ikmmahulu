<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - North Central.
 * @internal
 */
class Extent4479
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-119.03811127466, 45.941860136591], [-121.78963068194, 45.941860136591], [-121.78963068194, 44.89790836107], [-119.03811127466, 44.89790836107], [-119.03811127466, 45.941860136591],
                ],
            ],
        ];
    }
}
