<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Port Hedland.
 * @internal
 */
class Extent4466
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [118.96666666667, -20.1], [118.25, -20.1], [118.25, -20.783333333333], [118.96666666667, -20.783333333333], [118.96666666667, -20.1],
                ],
            ],
        ];
    }
}
