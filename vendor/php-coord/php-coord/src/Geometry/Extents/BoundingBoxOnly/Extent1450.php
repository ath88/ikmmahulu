<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cote d'Ivoire (Ivory Coast) - east of 6°W.
 * @internal
 */
class Extent1450
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-2.4877781867979, 10.457221031189], [-6, 10.457221031189], [-6, 4.92872051238], [-2.4877781867979, 4.92872051238], [-2.4877781867979, 10.457221031189],
                ],
            ],
        ];
    }
}
