<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Suha Krajina.
 * @internal
 */
class Extent2581
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.077183710177, 45.884090839619], [14.579255601409, 45.884090839619], [14.579255601409, 45.667185632595], [15.077183710177, 45.667185632595], [15.077183710177, 45.884090839619],
                ],
            ],
        ];
    }
}
