<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia region 5.
 * @internal
 */
class Extent3086
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.39999961853, 8.0000019073486], [-77.91255569458, 8.0000019073486], [-77.91255569458, 5.0000019073486], [-74.39999961853, 5.0000019073486], [-74.39999961853, 8.0000019073486],
                ],
            ],
        ];
    }
}
