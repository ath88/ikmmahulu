<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - south Santa Cruz west of 70.5°W.
 * @internal
 */
class Extent4570
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-70.5, -50.333332349748], [-73.275513821263, -50.333332349748], [-73.275513821263, -51.997505187998], [-70.5, -51.997505187998], [-70.5, -50.333332349748],
                ],
            ],
        ];
    }
}
