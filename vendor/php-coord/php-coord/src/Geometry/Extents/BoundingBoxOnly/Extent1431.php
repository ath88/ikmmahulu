<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - MTM zone 10.
 * @internal
 */
class Extent1431
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78, 47.321469139549], [-81, 47.321469139549], [-81, 42.26050805969], [-78, 42.26050805969], [-78, 47.321469139549],
                ],
            ],
        ];
    }
}
