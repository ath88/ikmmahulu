<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - Stockholm county.
 * @internal
 */
class Extent3608
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.605208690598, 60.268298538309], [17.252522371009, 60.268298538309], [17.252522371009, 58.690740558257], [19.605208690598, 58.690740558257], [19.605208690598, 60.268298538309],
                ],
            ],
        ];
    }
}
