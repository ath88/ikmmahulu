<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Costa Rica.
 * @internal
 */
class Extent1074
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.433332576, 11.767564127128], [-90.444061584, 11.767564127128], [-90.444061584, 2.1507381179902], [-81.433332576, 2.1507381179902], [-81.433332576, 11.767564127128],
                ],
            ],
        ];
    }
}
