<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - east of 66°W.
 * @internal
 */
class Extent3858
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-58.954944610596, 16.745054244995], [-66, 16.745054244995], [-66, 0.64916801452637], [-58.954944610596, 0.64916801452637], [-58.954944610596, 16.745054244995],
                ],
            ],
        ];
    }
}
