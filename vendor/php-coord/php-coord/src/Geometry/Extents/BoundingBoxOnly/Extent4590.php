<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Ceuta.
 * @internal
 */
class Extent4590
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.2468490600585, 35.965978620511], [-5.3955571008616, 35.965978620511], [-5.3955571008616, 35.829763412476], [-5.2468490600585, 35.829763412476], [-5.2468490600585, 35.965978620511],
                ],
            ],
        ];
    }
}
