<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - North Dakota - SPCS - S.
 * @internal
 */
class Extent2238
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.551930995181, 47.823984594412], [-104.04891397401, 47.823984594412], [-104.04891397401, 45.930822152112], [-96.551930995181, 45.930822152112], [-96.551930995181, 47.823984594412],
                ],
            ],
        ];
    }
}
