<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - FSU - 31.5°E to 34.5°E onshore.
 * @internal
 */
class Extent2657
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [34.5, 70.01092338562], [31.5, 70.01092338562], [31.5, 44.329493709157], [34.5, 44.329493709157], [34.5, 70.01092338562],
                ],
            ],
        ];
    }
}
