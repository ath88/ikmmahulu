<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Amazon cone shelf.
 * @internal
 */
class Extent1754
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-48, 5.593729019165], [-51.638261795044, 5.593729019165], [-51.638261795044, -1.0450267791748], [-48, -1.0450267791748], [-48, 5.593729019165],
                ],
            ],
        ];
    }
}
