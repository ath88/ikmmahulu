<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Wanganui mc.
 * @internal
 */
class Extent3776
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [176.26929858, -39.46363641], [174.40236023238, -39.46363641], [174.40236023238, -40.96252044], [176.26929858, -40.96252044], [176.26929858, -39.46363641],
                ],
            ],
        ];
    }
}
