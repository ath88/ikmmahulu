<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Saarland.
 * @internal
 */
class Extent4584
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.4037733078004, 49.639438629151], [6.3564863204956, 49.639438629151], [6.3564863204956, 49.111663818359], [7.4037733078004, 49.111663818359], [7.4037733078004, 49.639438629151],
                ],
            ],
        ];
    }
}
