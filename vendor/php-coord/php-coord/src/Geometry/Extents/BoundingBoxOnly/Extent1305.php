<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - Ireland (Republic and Ulster) - onshore.
 * @internal
 */
class Extent1305
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.3455815694454, 55.429414931381], [-10.555896938101, 55.429414931381], [-10.555896938101, 51.396195941322], [-5.3455815694454, 51.396195941322], [-5.3455815694454, 55.429414931381],
                ],
            ],
        ];
    }
}
