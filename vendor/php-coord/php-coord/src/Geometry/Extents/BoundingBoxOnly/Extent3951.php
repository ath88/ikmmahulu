<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 18°E to 24°E.
 * @internal
 */
class Extent3951
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.000000003382, 35.021065148001], [18, 35.021065148001], [18, 19.5], [24.000000003382, 19.5], [24.000000003382, 35.021065148001],
                ],
            ],
        ];
    }
}
