<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - offshore Pacific Ocean, Southern Ocean.
 * @internal
 */
class Extent3508
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [190, -25], [155, -25], [155, -60], [190, -60], [190, -25],
                ],
            ],
        ];
    }
}
