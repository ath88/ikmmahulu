<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Idaho - SPCS - W.
 * @internal
 */
class Extent2193
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-114.32471239328, 49.002550784294], [-117.23692089447, 49.002550784294], [-117.23692089447, 41.9945994629], [-114.32471239328, 41.9945994629], [-114.32471239328, 49.002550784294],
                ],
            ],
        ];
    }
}
