<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Laos.
 * @internal
 */
class Extent1138
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [107.634979248, 22.499927520752], [100.09136962891, 22.499927520752], [100.09136962891, 13.926664352417], [107.634979248, 13.926664352417], [107.634979248, 22.499927520752],
                ],
            ],
        ];
    }
}
