<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana.
 * @internal
 */
class Extent1395
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.04257965088, 49.000604629517], [-116.06511653997, 49.000604629517], [-116.06511653997, 44.353729248047], [-104.04257965088, 44.353729248047], [-104.04257965088, 49.000604629517],
                ],
            ],
        ];
    }
}
