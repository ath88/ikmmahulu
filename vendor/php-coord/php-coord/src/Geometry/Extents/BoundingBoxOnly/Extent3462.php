<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Mexico - offshore GoM - Campeche area S.
 * @internal
 */
class Extent3462
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.759020832757, 20.888059725136], [-94.787731277754, 20.888059725136], [-94.787731277754, 17.85274239106], [-89.759020832757, 17.85274239106], [-89.759020832757, 20.888059725136],
                ],
            ],
        ];
    }
}
