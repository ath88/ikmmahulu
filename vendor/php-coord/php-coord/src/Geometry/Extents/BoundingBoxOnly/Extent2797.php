<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Sule Skerry onshore.
 * @internal
 */
class Extent2797
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-4.3094405314609, 59.126580028992], [-4.4901835357884, 59.126580028992], [-4.4901835357884, 59.050755746114], [-4.3094405314609, 59.050755746114], [-4.3094405314609, 59.126580028992],
                ],
            ],
        ];
    }
}
