<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Jacksons Bay mc.
 * @internal
 */
class Extent3794
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [170.00946864, -43.670257415785], [168.02188367368, -43.670257415785], [168.02188367368, -44.39573343], [170.00946864, -44.39573343], [170.00946864, -43.670257415785],
                ],
            ],
        ];
    }
}
