<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Brunei - onshore.
 * @internal
 */
class Extent2349
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [115.36026000977, 5.1031008692146], [114.09507751465, 5.1031008692146], [114.09507751465, 4.0181937217714], [115.36026000977, 4.0181937217714], [115.36026000977, 5.1031008692146],
                ],
            ],
        ];
    }
}
