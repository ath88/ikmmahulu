<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - Bas Congo.
 * @internal
 */
class Extent3171
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.277299880982, -4.2827782630918], [12.173489324635, -4.2827782630918], [12.173489324635, -6.0374333606684], [16.277299880982, -6.0374333606684], [16.277299880982, -4.2827782630918],
                ],
            ],
        ];
    }
}
