<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Western Australia - Perth.
 * @internal
 */
class Extent4462
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [116.08333333333, -31.333333333333], [115.44166666667, -31.333333333333], [115.44166666667, -33.416666666667], [116.08333333333, -33.416666666667], [116.08333333333, -31.333333333333],
                ],
            ],
        ];
    }
}
