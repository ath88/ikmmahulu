<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Washington - SPCS83 - S.
 * @internal
 */
class Extent2274
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-116.91913242808, 47.60548916013], [-124.39990588667, 47.60548916013], [-124.39990588667, 45.543092834503], [-116.91913242808, 45.543092834503], [-116.91913242808, 47.60548916013],
                ],
            ],
        ];
    }
}
