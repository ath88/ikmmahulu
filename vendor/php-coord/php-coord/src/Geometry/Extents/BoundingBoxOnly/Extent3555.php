<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - Campbell Island.
 * @internal
 */
class Extent3555
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [169.59666356817, -52.266757112982], [168.65628973918, -52.266757112982], [168.65628973918, -52.821586483109], [169.59666356817, -52.821586483109], [169.59666356817, -52.266757112982],
                ],
            ],
        ];
    }
}
