<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Northern Ireland - onshore.
 * @internal
 */
class Extent2530
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.3455815694454, 55.352009898694], [-8.1716661453246, 55.352009898694], [-8.1716661453246, 53.963558133807], [-5.3455815694454, 53.963558133807], [-5.3455815694454, 55.352009898694],
                ],
            ],
        ];
    }
}
