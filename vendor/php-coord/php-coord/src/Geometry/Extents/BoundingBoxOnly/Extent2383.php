<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Idaho and Montana - west of 113°W.
 * @internal
 */
class Extent2383
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-113, 49.002551194312], [-117.23692089447, 49.002551194312], [-117.23692089447, 41.9945994629], [-113, 41.9945994629], [-113, 49.002551194312],
                ],
            ],
        ];
    }
}
