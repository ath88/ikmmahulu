<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - 4°N to 5°N, 6°E to 8°E.
 * @internal
 */
class Extent3590
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [8.0000391958412, 5.0000001411455], [5.999997539335, 5.0000001411455], [5.999997539335, 3.9999987409584], [8.0000391958412, 3.9999987409584], [8.0000391958412, 5.0000001411455],
                ],
            ],
        ];
    }
}
