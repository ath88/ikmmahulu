<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Poland.
 * @internal
 */
class Extent1192
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.143468856812, 55.921550168001], [14.147637367248, 55.921550168001], [14.147637367248, 49.002914428711], [24.143468856812, 49.002914428711], [24.143468856812, 55.921550168001],
                ],
            ],
        ];
    }
}
