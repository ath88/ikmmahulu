<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - east of 138°E onshore.
 * @internal
 */
class Extent3975
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.00701904297, -1.4946480820813], [138, -1.4946480820813], [138, -9.1841535662995], [141.00701904297, -9.1841535662995], [141.00701904297, -1.4946480820813],
                ],
            ],
        ];
    }
}
