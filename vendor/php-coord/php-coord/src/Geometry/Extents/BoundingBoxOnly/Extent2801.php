<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Flannan Isles onshore.
 * @internal
 */
class Extent2801
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-7.4607606942879, 58.346855649561], [-7.7456024959696, 58.346855649561], [-7.7456024959696, 58.21769652454], [-7.4607606942879, 58.21769652454], [-7.4607606942879, 58.346855649561],
                ],
            ],
        ];
    }
}
