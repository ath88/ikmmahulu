<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Corrego Alegre 1961.
 * @internal
 */
class Extent3874
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-38.820402145386, -14.999958478139], [-58.158889770508, -14.999958478139], [-58.158889770508, -27.499952627735], [-38.820402145386, -27.499952627735], [-38.820402145386, -14.999958478139],
                ],
            ],
        ];
    }
}
