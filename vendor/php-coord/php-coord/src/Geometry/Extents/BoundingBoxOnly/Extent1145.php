<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Lithuania.
 * @internal
 */
class Extent1145
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [26.813051223755, 56.449851989746], [19.022537517959, 56.449851989746], [19.022537517959, 53.890338897705], [26.813051223755, 53.890338897705], [26.813051223755, 56.449851989746],
                ],
            ],
        ];
    }
}
