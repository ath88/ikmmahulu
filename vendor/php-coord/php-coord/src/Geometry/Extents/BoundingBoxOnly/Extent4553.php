<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Bac Kan and Thai Nguyen.
 * @internal
 */
class Extent4553
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [106.247016907, 22.741199493], [105.431129456, 22.741199493], [105.431129456, 21.325265884], [106.247016907, 21.325265884], [106.247016907, 22.741199493],
                ],
            ],
        ];
    }
}
