<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - England and Wales, Isle of Man.
 * @internal
 */
class Extent2396
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-6.156445031993, 50.039856610364], [-6.4936773539885, 50.039856610364], [-6.4936773539885, 49.812477863374], [-6.156445031993, 49.812477863374], [-6.156445031993, 50.039856610364],
                ],
            ],
            [
                [
                    [1.8312159143487, 55.845146914051], [-5.7944309649949, 55.845146914051], [-5.7944309649949, 49.905707207256], [1.8312159143487, 49.905707207256], [1.8312159143487, 55.845146914051],
                ],
            ],
        ];
    }
}
