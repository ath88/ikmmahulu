<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - Puerto Carreno city.
 * @internal
 */
class Extent4153
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.41397857666, 6.2937553412837], [-67.7, 6.2937553412837], [-67.7, 5.98], [-67.41397857666, 5.98], [-67.41397857666, 6.2937553412837],
                ],
            ],
        ];
    }
}
