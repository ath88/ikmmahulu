<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 33°20'N to 34°N; 130°E to 131°E.
 * @internal
 */
class Extent2509
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [131, 33.999366666667], [130, 33.999366666667], [130, 33.332666666667], [131, 33.332666666667], [131, 33.999366666667],
                ],
            ],
        ];
    }
}
