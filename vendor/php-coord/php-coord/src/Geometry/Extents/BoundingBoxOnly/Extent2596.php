<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Tierra del Fuego offshore west of 66°W.
 * @internal
 */
class Extent2596
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66, -51.659149169922], [-68.617580413818, -51.659149169922], [-68.617580413818, -54.605590820312], [-66, -54.605590820312], [-66, -51.659149169922],
                ],
            ],
        ];
    }
}
