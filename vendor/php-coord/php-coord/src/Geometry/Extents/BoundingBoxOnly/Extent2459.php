<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 38°40'N to 39°20'N; 139°E to 140°E.
 * @internal
 */
class Extent2459
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140, 39.332966666667], [139.55055382336, 39.332966666667], [139.55055382336, 38.666266666667], [140, 38.666266666667], [140, 39.332966666667],
                ],
            ],
        ];
    }
}
