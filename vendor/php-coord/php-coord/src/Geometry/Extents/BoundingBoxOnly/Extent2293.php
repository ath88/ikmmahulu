<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Myanmar (Burma) - onshore south of 15°N.
 * @internal
 */
class Extent2293
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [99.657348632813, 15], [97.745613411729, 15], [97.745613411729, 9.9593938739583], [99.657348632813, 9.9593938739583], [99.657348632813, 15],
                ],
            ],
            [
                [
                    [98.340087728126, 10.107316665665], [98.063999420956, 10.107316665665], [98.063999420956, 9.7894877474486], [98.340087728126, 9.7894877474486], [98.340087728126, 10.107316665665],
                ],
            ],
            [
                [
                    [98.334623660023, 11.028973103878], [98.026193950775, 11.028973103878], [98.026193950775, 10.639760435877], [98.334623660023, 10.639760435877], [98.334623660023, 11.028973103878],
                ],
            ],
            [
                [
                    [98.190129120816, 12.466471075913], [97.886115793392, 12.466471075913], [97.886115793392, 12.091997710531], [98.190129120816, 12.091997710531], [98.190129120816, 12.466471075913],
                ],
            ],
            [
                [
                    [98.51888662055, 12.721198641412], [98.249397437498, 12.721198641412], [98.249397437498, 12.265880397849], [98.51888662055, 12.265880397849], [98.51888662055, 12.721198641412],
                ],
            ],
        ];
    }
}
