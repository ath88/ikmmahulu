<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - west of 70.5°W.
 * @internal
 */
class Extent1608
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-70.5, -36.162292480469], [-73.582298278809, -36.162292480469], [-73.582298278809, -51.997505187988], [-70.5, -51.997505187988], [-70.5, -36.162292480469],
                ],
            ],
        ];
    }
}
