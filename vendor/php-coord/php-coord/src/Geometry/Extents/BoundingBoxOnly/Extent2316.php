<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore block 5.
 * @internal
 */
class Extent2316
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.391804695129, -7.7501303065337], [12.580387692842, -7.7501303065337], [12.580387692842, -8.584600645472], [13.391804695129, -8.584600645472], [13.391804695129, -7.7501303065337],
                ],
            ],
        ];
    }
}
