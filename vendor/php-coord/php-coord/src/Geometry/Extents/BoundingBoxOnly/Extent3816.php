<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - offshore blocks OPL 217-223.
 * @internal
 */
class Extent3816
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.9960002863336, 3.8507509295598], [5.5835868717662, 3.8507509295598], [5.5835868717662, 3.2488342383138], [7.9960002863336, 3.2488342383138], [7.9960002863336, 3.8507509295598],
                ],
            ],
        ];
    }
}
