<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Gabon - north of equator and west of 12°E onshore.
 * @internal
 */
class Extent1696
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, 2.3152775764467], [9.2525118680282, 2.3152775764467], [9.2525118680282, 0], [12, 0], [12, 2.3152775764467],
                ],
            ],
        ];
    }
}
