<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Ireland - onshore.
 * @internal
 */
class Extent3767
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.9304159290667, 55.429414931381], [-10.555896938101, 55.429414931381], [-10.555896938101, 51.396195941322], [-5.9304159290667, 51.396195941322], [-5.9304159290667, 55.429414931381],
                ],
            ],
        ];
    }
}
