<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 96°E to 102°E, N hemisphere.
 * @internal
 */
class Extent1649
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [102, 7.4833515782209], [96, 7.4833515782209], [96, 0], [102, 0], [102, 7.4833515782209],
                ],
            ],
        ];
    }
}
