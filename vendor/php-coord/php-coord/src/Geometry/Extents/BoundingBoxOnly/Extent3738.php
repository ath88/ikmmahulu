<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Dagana district.
 * @internal
 */
class Extent3738
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [90.078369930569, 27.280470943086], [89.636840656412, 27.280470943086], [89.636840656412, 26.700654983521], [90.078369930569, 26.700654983521], [90.078369930569, 27.280470943086],
                ],
            ],
        ];
    }
}
