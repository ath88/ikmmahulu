<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Clark, Floyd, Scott.
 * @internal
 */
class Extent4308
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.415271999996, 38.831284999997], [-86.033174999997, 38.831284999997], [-86.033174999997, 38.178081529079], [-85.415271999996, 38.178081529079], [-85.415271999996, 38.831284999997],
                ],
            ],
        ];
    }
}
