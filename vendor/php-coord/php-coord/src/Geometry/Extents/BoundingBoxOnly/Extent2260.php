<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Virginia - SPCS - N.
 * @internal
 */
class Extent2260
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-76.511244405607, 39.456998174809], [-80.054807232997, 39.456998174809], [-80.054807232997, 37.776509142762], [-76.511244405607, 37.776509142762], [-76.511244405607, 39.456998174809],
                ],
            ],
        ];
    }
}
