<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/South America - 72°W to 66°W, S hemisphere and SIRGAS 2000 by country.
 * @internal
 */
class Extent3441
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66, 2.1428470611572], [-72, 2.1428470611572], [-72, -59.864894866943], [-66, -59.864894866943], [-66, 2.1428470611572],
                ],
            ],
        ];
    }
}
