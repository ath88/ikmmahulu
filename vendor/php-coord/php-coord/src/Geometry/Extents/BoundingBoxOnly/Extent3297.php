<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/St Kitts and Nevis - onshore.
 * @internal
 */
class Extent3297
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-62.500555520989, 17.457799639965], [-62.912849943623, 17.457799639965], [-62.912849943623, 17.066104882301], [-62.500555520989, 17.066104882301], [-62.500555520989, 17.457799639965],
                ],
            ],
        ];
    }
}
