<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Sioux City-Iowa Falls.
 * @internal
 */
class Extent4233
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.001636, 42.910423], [-96.640709192144, 42.910423], [-96.640709192144, 42.208694], [-93.001636, 42.208694], [-93.001636, 42.910423],
                ],
            ],
        ];
    }
}
