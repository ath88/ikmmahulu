<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - Comodoro Rivadavia - west of 67.5°W.
 * @internal
 */
class Extent4614
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-67.5, -45.199998855591], [-69.5, -45.199998855591], [-69.5, -46.699998855591], [-67.5, -46.699998855591], [-67.5, -45.199998855591],
                ],
            ],
        ];
    }
}
