<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Trinidad and Tobago - Trinidad - onshore.
 * @internal
 */
class Extent3143
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-60.857713899919, 10.893818422173], [-61.979790888492, 10.893818422173], [-61.979790888492, 9.9944628951316], [-60.857713899919, 9.9944628951316], [-60.857713899919, 10.893818422173],
                ],
            ],
        ];
    }
}
