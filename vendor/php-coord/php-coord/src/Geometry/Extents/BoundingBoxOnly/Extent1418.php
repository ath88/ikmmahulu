<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin.
 * @internal
 */
class Extent1418
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.25064894405, 47.302520751953], [-92.889241325672, 47.302520751953], [-92.889241325672, 42.489152849524], [-86.25064894405, 42.489152849524], [-86.25064894405, 47.302520751953],
                ],
            ],
        ];
    }
}
