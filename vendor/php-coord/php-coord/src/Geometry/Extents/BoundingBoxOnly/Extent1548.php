<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Hawaii - Oahu - onshore.
 * @internal
 */
class Extent1548
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-157.6155872345, 21.749437332153], [-158.32349777222, 21.749437332153], [-158.32349777222, 21.208883285523], [-157.6155872345, 21.208883285523], [-157.6155872345, 21.749437332153],
                ],
            ],
        ];
    }
}
