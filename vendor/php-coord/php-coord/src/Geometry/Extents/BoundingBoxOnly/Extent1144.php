<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Liechtenstein.
 * @internal
 */
class Extent1144
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.6338882446289, 47.274543762207], [9.4746370315552, 47.274543762207], [9.4746370315552, 47.057456970215], [9.6338882446289, 47.057456970215], [9.6338882446289, 47.274543762207],
                ],
            ],
        ];
    }
}
