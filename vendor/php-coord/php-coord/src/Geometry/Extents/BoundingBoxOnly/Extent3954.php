<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - east of 6°E.
 * @internal
 */
class Extent3954
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.986474990845, 38.797222152001], [6, 38.797222152001], [6, 19.609162411029], [11.986474990845, 19.609162411029], [11.986474990845, 38.797222152001],
                ],
            ],
        ];
    }
}
