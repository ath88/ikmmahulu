<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 32°40'N to 33°20'N; 134°E to 135°E.
 * @internal
 */
class Extent2521
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [134.26442957782, 33.332666666667], [134, 33.332666666667], [134, 33.192426580537], [134.26442957782, 33.192426580537], [134.26442957782, 33.332666666667],
                ],
            ],
        ];
    }
}
