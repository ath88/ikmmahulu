<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritania - 18°W to 12°W.
 * @internal
 */
class Extent3938
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-12, 23.454441070557], [-18, 23.454441070557], [-18, 14.725639343262], [-12, 14.725639343262], [-12, 23.454441070557],
                ],
            ],
        ];
    }
}
