<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - Tenerife.
 * @internal
 */
class Extent4594
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-16.086167007681, 28.622220993042], [-16.95937538147, 28.622220993042], [-16.95937538147, 27.934164047241], [-16.086167007681, 27.934164047241], [-16.086167007681, 28.622220993042],
                ],
            ],
        ];
    }
}
