<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - west of 174°E - AK, OCS.
 * @internal
 */
class Extent3372
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [174.00000000308, 56.276679734151], [167.65070915226, 56.276679734151], [167.65070915226, 49.019], [174.00000000308, 49.019], [174.00000000308, 56.276679734151],
                ],
            ],
        ];
    }
}
