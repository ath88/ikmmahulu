<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Nebraska - SPCS27 - N.
 * @internal
 */
class Extent2221
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.076437293329, 43.003087792437], [-104.05604392175, 43.003087792437], [-104.05604392175, 41.684098992388], [-96.076437293329, 41.684098992388], [-96.076437293329, 43.003087792437],
                ],
            ],
        ];
    }
}
