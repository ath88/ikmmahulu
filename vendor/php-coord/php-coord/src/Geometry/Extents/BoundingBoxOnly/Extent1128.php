<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Jamaica.
 * @internal
 */
class Extent1128
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.513888569, 19.356544134], [-80.591886575152, 19.356544134], [-80.591886575152, 14.083333129], [-74.513888569, 14.083333129], [-74.513888569, 19.356544134],
                ],
            ],
        ];
    }
}
