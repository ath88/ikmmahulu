<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Myanmar (Burma) - Moattama area.
 * @internal
 */
class Extent2361
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [99.657348632813, 17.868287981574], [93.942159516507, 17.868287981574], [93.942159516507, 9.4882391250003], [99.657348632813, 9.4882391250003], [99.657348632813, 17.868287981574],
                ],
            ],
        ];
    }
}
