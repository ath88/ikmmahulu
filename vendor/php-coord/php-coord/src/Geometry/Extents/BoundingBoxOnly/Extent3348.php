<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Gorenjska - upper.
 * @internal
 */
class Extent3348
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.171185720616, 46.526611327929], [13.708342966562, 46.526611327929], [13.708342966562, 46.092976570311], [14.171185720616, 46.092976570311], [14.171185720616, 46.526611327929],
                ],
            ],
        ];
    }
}
