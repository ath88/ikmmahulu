<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - North Carolina.
 * @internal
 */
class Extent1402
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-75.386260986328, 36.589767810635], [-84.323773491634, 36.589767810635], [-84.323773491634, 33.830278396607], [-75.386260986328, 33.830278396607], [-75.386260986328, 36.589767810635],
                ],
            ],
        ];
    }
}
