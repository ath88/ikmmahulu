<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Wood.
 * @internal
 */
class Extent4359
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.724489899588, 44.685376233529], [-90.317989018454, 44.685376233529], [-90.317989018454, 44.247631018272], [-89.724489899588, 44.247631018272], [-89.724489899588, 44.685376233529],
                ],
            ],
        ];
    }
}
