<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - Ross Sea Region.
 * @internal
 */
class Extent3558
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-150, -60], [-215.000039, -60], [-215.000039, -90], [-150, -90], [-150, -60],
                ],
            ],
        ];
    }
}
