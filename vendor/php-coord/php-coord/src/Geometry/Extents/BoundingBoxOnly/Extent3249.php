<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Gabon - onshore.
 * @internal
 */
class Extent3249
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.519581794739, 2.3178982734684], [8.6511727065643, 2.3178982734684], [8.6511727065643, -3.9743356848587], [14.519581794739, -3.9743356848587], [14.519581794739, 2.3178982734684],
                ],
            ],
        ];
    }
}
