<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Adams.
 * @internal
 */
class Extent4289
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.802094000008, 40.922376999998], [-85.07386799999, 40.922376999998], [-85.07386799999, 40.568240000003], [-84.802094000008, 40.568240000003], [-84.802094000008, 40.922376999998],
                ],
            ],
        ];
    }
}
