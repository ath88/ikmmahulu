<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Society Islands - Moorea and Tahiti.
 * @internal
 */
class Extent2811
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-149.09668214214, -17.44375092876], [-149.69169371235, -17.44375092876], [-149.69169371235, -17.920834947704], [-149.09668214214, -17.920834947704], [-149.09668214214, -17.44375092876],
                ],
            ],
            [
                [
                    [-149.73321778667, -17.416803907276], [-149.99168150532, -17.416803907276], [-149.99168150532, -17.624448228954], [-149.73321778667, -17.624448228954], [-149.73321778667, -17.416803907276],
                ],
            ],
        ];
    }
}
