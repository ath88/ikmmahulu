<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - NE.
 * @internal
 */
class Extent3565
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.607872009278, 46.876247406006], [14.544998168945, 46.876247406006], [14.544998168945, 46.14690879185], [16.607872009278, 46.14690879185], [16.607872009278, 46.876247406006],
                ],
            ],
        ];
    }
}
