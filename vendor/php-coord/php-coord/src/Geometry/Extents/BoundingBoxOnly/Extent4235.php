<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Council Bluffs.
 * @internal
 */
class Extent4235
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.040775, 42.215002], [-96.363511843129, 42.215002], [-96.363511843129, 40.584334009379], [-95.040775, 40.584334009379], [-95.040775, 42.215002],
                ],
            ],
        ];
    }
}
