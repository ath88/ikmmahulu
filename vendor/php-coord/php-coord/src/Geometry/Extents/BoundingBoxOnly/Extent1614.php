<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina - east of 55.5°W onshore.
 * @internal
 */
class Extent1614
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-53.650005340576, -25.496391296387], [-55.5, -25.496391296387], [-55.5, -28.104667663574], [-53.650005340576, -28.104667663574], [-53.650005340576, -25.496391296387],
                ],
            ],
        ];
    }
}
