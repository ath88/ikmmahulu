<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Dufur-Madras.
 * @internal
 */
class Extent4204
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-120.46332169098, 45.540842970045], [-121.94610750046, 45.540842970045], [-121.94610750046, 44.63333275034], [-120.46332169098, 44.63333275034], [-120.46332169098, 45.540842970045],
                ],
            ],
        ];
    }
}
