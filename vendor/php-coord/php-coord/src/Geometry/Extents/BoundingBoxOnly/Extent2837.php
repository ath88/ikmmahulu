<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 18 00.
 * @internal
 */
class Extent2837
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.605208690598, 60.693202419425], [17.087167834637, 60.693202419425], [17.087167834637, 58.660741798311], [19.605208690598, 58.660741798311], [19.605208690598, 60.693202419425],
                ],
            ],
        ];
    }
}
