<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Aratu.
 * @internal
 */
class Extent1274
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-26.00968170166, 4.2522640228271], [-53.374298095703, 4.2522640228271], [-53.374298095703, -35.708560943604], [-26.00968170166, -35.708560943604], [-26.00968170166, 4.2522640228271],
                ],
            ],
        ];
    }
}
