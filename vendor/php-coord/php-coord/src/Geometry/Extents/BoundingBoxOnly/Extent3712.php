<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - south of 29.7°N, east of 46.1°E (map 25).
 * @internal
 */
class Extent3712
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [47.016658880221, 29.736178431041], [46.027597769187, 29.736178431041], [46.027597769187, 29.061660766601], [47.016658880221, 29.061660766601], [47.016658880221, 29.736178431041],
                ],
            ],
        ];
    }
}
