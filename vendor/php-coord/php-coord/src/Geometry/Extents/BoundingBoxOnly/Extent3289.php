<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - onshore.
 * @internal
 */
class Extent3289
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [77.823926705845, 37.060787200928], [60.866302490235, 37.060787200928], [60.866302490235, 23.643277736247], [77.823926705845, 23.643277736247], [77.823926705845, 37.060787200928],
                ],
            ],
        ];
    }
}
