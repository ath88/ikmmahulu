<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 81°10'N to 76°10'N,  4°W to 36°E.
 * @internal
 */
class Extent4058
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [38, 81.166666030884], [-3.3437137437567, 81.166666030884], [-3.3437137437567, 76.166666030884], [38, 76.166666030884], [38, 81.166666030884],
                ],
            ],
        ];
    }
}
