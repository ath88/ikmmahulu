<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - mainland and Sicily.
 * @internal
 */
class Extent3736
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.5070015934, 42.920088434265], [10.03320981173, 42.920088434265], [10.03320981173, 42.658582882367], [10.5070015934, 42.658582882367], [10.5070015934, 42.920088434265],
                ],
            ],
            [
                [
                    [18.579282184839, 47.094581604004], [6.6239671707154, 47.094581604004], [6.6239671707154, 36.599062651922], [18.579282184839, 36.599062651922], [18.579282184839, 47.094581604004],
                ],
            ],
        ];
    }
}
