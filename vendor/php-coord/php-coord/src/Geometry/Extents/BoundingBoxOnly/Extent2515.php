<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 33°20'N to 34°N; 136°E to 137°E.
 * @internal
 */
class Extent2515
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [136.33774015765, 33.999366666667], [136, 33.999366666667], [136, 33.547495791294], [136.33774015765, 33.547495791294], [136.33774015765, 33.999366666667],
                ],
            ],
        ];
    }
}
