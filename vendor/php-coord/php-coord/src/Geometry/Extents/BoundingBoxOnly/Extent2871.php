<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores E - S Miguel onshore.
 * @internal
 */
class Extent2871
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-25.080973218799, 37.959302354931], [-25.914308000683, 37.959302354931], [-25.914308000683, 37.655550550343], [-25.080973218799, 37.655550550343], [-25.080973218799, 37.959302354931],
                ],
            ],
        ];
    }
}
