<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Finland - 22.5°E to 25.5°E onshore.
 * @internal
 */
class Extent1537
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25.5, 68.89151763916], [22.5, 68.89151763916], [22.5, 59.756970978171], [25.5, 59.756970978171], [25.5, 68.89151763916],
                ],
            ],
        ];
    }
}
