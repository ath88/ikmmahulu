<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Lesotho.
 * @internal
 */
class Extent1141
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.45555305481, -28.570695877075], [27.013971328735, -28.570695877075], [27.013971328735, -30.650527954102], [29.45555305481, -30.650527954102], [29.45555305481, -28.570695877075],
                ],
            ],
        ];
    }
}
