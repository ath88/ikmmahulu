<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - De Kalb.
 * @internal
 */
class Extent4283
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.803580999993, 41.530297999998], [-85.193925999997, 41.530297999998], [-85.193925999997, 41.264282999975], [-84.803580999993, 41.264282999975], [-84.803580999993, 41.530297999998],
                ],
            ],
        ];
    }
}
