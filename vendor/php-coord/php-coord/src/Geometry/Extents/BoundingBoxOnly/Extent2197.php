<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - SPCS - W.
 * @internal
 */
class Extent2197
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.240691000003, 41.760688386132], [-88.058499371214, 41.760688386132], [-88.058499371214, 37.771742000004], [-86.240691000003, 37.771742000004], [-86.240691000003, 41.760688386132],
                ],
            ],
        ];
    }
}
