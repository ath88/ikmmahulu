<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Green Lake and Marquette.
 * @internal
 */
class Extent4368
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.885185702245, 43.983468622723], [-89.599915330241, 43.983468622723], [-89.599915330241, 43.631813971296], [-88.885185702245, 43.631813971296], [-88.885185702245, 43.983468622723],
                ],
            ],
        ];
    }
}
