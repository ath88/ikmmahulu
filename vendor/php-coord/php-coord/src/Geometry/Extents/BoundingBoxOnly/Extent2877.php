<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Slovenska Bistrica.
 * @internal
 */
class Extent2877
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.998709382636, 46.45408776599], [15.310403571045, 46.45408776599], [15.310403571045, 46.14389738112], [15.998709382636, 46.14389738112], [15.998709382636, 46.45408776599],
                ],
            ],
        ];
    }
}
