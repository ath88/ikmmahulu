<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Lindis Peak mc.
 * @internal
 */
class Extent3795
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [170.23735368, -43.71951249], [168.62242158, -43.71951249], [168.62242158, -45.39688749], [170.23735368, -45.39688749], [170.23735368, -43.71951249],
                ],
            ],
        ];
    }
}
