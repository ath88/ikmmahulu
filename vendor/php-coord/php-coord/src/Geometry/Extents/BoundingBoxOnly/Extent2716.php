<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - 88.5°E to 91.5°E.
 * @internal
 */
class Extent2716
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.5, 48.412159875793], [88.5, 48.412159875793], [88.5, 27.327966125071], [91.5, 27.327966125071], [91.5, 48.412159875793],
                ],
            ],
        ];
    }
}
