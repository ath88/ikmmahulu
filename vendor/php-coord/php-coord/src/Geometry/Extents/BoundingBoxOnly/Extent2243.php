<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - SPCS - N.
 * @internal
 */
class Extent2243
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-116.47041116841, 46.251328623697], [-124.16673173077, 46.251328623697], [-124.16673173077, 43.95386869106], [-116.47041116841, 43.95386869106], [-116.47041116841, 46.251328623697],
                ],
            ],
        ];
    }
}
