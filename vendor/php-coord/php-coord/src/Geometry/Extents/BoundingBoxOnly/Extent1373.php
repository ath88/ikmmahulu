<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Arizona.
 * @internal
 */
class Extent1373
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.0455729355, 37.003926023797], [-114.80982971191, 37.003926023797], [-114.80982971191, 31.332603601804], [-109.0455729355, 31.332603601804], [-109.0455729355, 37.003926023797],
                ],
            ],
        ];
    }
}
