<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Idaho and Montana - east of 113°W.
 * @internal
 */
class Extent2382
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.04257965088, 49.000604629517], [-113, 49.000604629517], [-113, 41.996203349407], [-104.04257965088, 41.996203349407], [-104.04257965088, 49.000604629517],
                ],
            ],
        ];
    }
}
