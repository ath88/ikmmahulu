<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - California - SPCS27 - 7.
 * @internal
 */
class Extent2181
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-117.63890838623, 34.823867797852], [-118.95356852341, 34.823867797852], [-118.95356852341, 33.666772842407], [-117.63890838623, 33.666772842407], [-117.63890838623, 34.823867797852],
                ],
            ],
        ];
    }
}
