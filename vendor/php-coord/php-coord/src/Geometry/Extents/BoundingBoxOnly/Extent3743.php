<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Lhuentse district.
 * @internal
 */
class Extent3743
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [91.484918625097, 28.081108093262], [90.774394934901, 28.081108093262], [90.774394934901, 27.390347119408], [91.484918625097, 27.390347119408], [91.484918625097, 28.081108093262],
                ],
            ],
        ];
    }
}
