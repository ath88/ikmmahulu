<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas.
 * @internal
 */
class Extent1412
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-93.507388858387, 36.493912716666], [-106.65006189088, 36.493912716666], [-106.65006189088, 25.839860916138], [-93.507388858387, 25.839860916138], [-93.507388858387, 36.493912716666],
                ],
            ],
        ];
    }
}
