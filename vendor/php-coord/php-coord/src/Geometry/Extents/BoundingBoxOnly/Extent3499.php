<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - 114°W to 108°W.
 * @internal
 */
class Extent3499
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-108, 49.000604629517], [-114, 49.000604629517], [-114, 31.332605361939], [-108, 31.332605361939], [-108, 49.000604629517],
                ],
            ],
        ];
    }
}
