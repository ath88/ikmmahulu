<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 77°50'N to 72°50'N,  142°E to 166°E.
 * @internal
 */
class Extent4089
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [166.00000190735, 77.833333969116], [142.00000190735, 77.833333969116], [142.00000190735, 72.833333969116], [166.00000190735, 72.833333969116], [166.00000190735, 77.833333969116],
                ],
            ],
        ];
    }
}
