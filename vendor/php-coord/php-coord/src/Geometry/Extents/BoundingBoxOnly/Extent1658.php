<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 120°E to 126°E, S hemisphere.
 * @internal
 */
class Extent1658
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [126, 5.6843418860808E-14], [120, 5.6843418860808E-14], [120, -13.942142903], [126, -13.942142903], [126, 5.6843418860808E-14],
                ],
            ],
        ];
    }
}
