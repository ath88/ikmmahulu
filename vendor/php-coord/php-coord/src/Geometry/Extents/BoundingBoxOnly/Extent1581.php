<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Kenya, Tanzania and Uganda - south of equator and 30°E to 36°E.
 * @internal
 */
class Extent1581
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [36, 5.6843418860808E-14], [30, 5.6843418860808E-14], [30, -11.607084274292], [36, -11.607084274292], [36, 5.6843418860808E-14],
                ],
            ],
        ];
    }
}
