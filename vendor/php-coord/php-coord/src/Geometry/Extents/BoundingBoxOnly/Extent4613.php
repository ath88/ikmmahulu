<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe  Lyon-Turin.
 * @internal
 */
class Extent4613
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.8772423467624, 45.88320957683], [4.6505192204449, 45.88320957683], [4.6505192204449, 44.87236950014], [7.8772423467624, 44.87236950014], [7.8772423467624, 45.88320957683],
                ],
            ],
        ];
    }
}
