<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Finland.
 * @internal
 */
class Extent1095
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [31.58196258545, 70.088607788086], [19.083199518, 70.088607788086], [19.083199518, 58.844500171001], [31.58196258545, 58.844500171001], [31.58196258545, 70.088607788086],
                ],
            ],
        ];
    }
}
