<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Texas - SPCS - NC.
 * @internal
 */
class Extent2254
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.00458470013, 34.579634629863], [-103.0600181856, 34.579634629863], [-103.0600181856, 31.72075789374], [-94.00458470013, 31.72075789374], [-94.00458470013, 34.579634629863],
                ],
            ],
        ];
    }
}
