<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Grenada and southern Grenadines - onshore.
 * @internal
 */
class Extent1551
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.497830797415, 12.385686345155], [-61.833422487935, 12.385686345155], [-61.833422487935, 11.948903628207], [-61.497830797415, 11.948903628207], [-61.497830797415, 12.385686345155],
                ],
            ],
            [
                [
                    [-61.354365452014, 12.562479596962], [-61.527485327503, 12.562479596962], [-61.527485327503, 12.36780773743], [-61.354365452014, 12.36780773743], [-61.354365452014, 12.562479596962],
                ],
            ],
        ];
    }
}
