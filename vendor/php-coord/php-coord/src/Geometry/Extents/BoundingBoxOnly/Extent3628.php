<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 24°E to 30°E.
 * @internal
 */
class Extent3628
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.806663513184, -4.791553305129], [24, -4.791553305129], [24, -13.458057403564], [29.806663513184, -13.458057403564], [29.806663513184, -4.791553305129],
                ],
            ],
        ];
    }
}
