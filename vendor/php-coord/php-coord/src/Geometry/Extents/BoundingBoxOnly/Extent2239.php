<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Ohio - SPCS - N.
 * @internal
 */
class Extent2239
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-80.519996234962, 42.3274373242], [-84.806210000006, 42.3274373242], [-84.806210000006, 40.107018695345], [-80.519996234962, 40.107018695345], [-80.519996234962, 42.3274373242],
                ],
            ],
        ];
    }
}
