<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Himachal Pradesh.
 * @internal
 */
class Extent4402
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [78.996696472168, 33.255710601807], [75.578758239746, 33.255710601807], [75.578758239746, 30.384519577026], [78.996696472168, 30.384519577026], [78.996696472168, 33.255710601807],
                ],
            ],
        ];
    }
}
