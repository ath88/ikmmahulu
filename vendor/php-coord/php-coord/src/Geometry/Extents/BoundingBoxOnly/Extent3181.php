<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/USA - Hawaii - Tern Island and Sorel Atoll.
 * @internal
 */
class Extent3181
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-166.03103342852, 23.925963250381], [-166.35895046961, 23.925963250381], [-166.35895046961, 23.691110236585], [-166.03103342852, 23.691110236585], [-166.03103342852, 23.925963250381],
                ],
            ],
        ];
    }
}
