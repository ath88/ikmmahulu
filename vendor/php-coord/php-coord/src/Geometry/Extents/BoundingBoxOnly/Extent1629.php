<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - offshore - North Sea.
 * @internal
 */
class Extent1629
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [3.3999994410004, 62.02736337945], [-5.046669789728, 62.02736337945], [-5.046669789728, 51.039454652004], [3.3999994410004, 51.039454652004], [3.3999994410004, 62.02736337945],
                ],
            ],
        ];
    }
}
