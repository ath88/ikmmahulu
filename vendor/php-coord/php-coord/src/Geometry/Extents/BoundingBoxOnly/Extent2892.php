<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Guadeloupe - Grande-Terre and Basse-Terre - onshore.
 * @internal
 */
class Extent2892
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.155393250655, 16.544367894077], [-61.846283291331, 16.544367894077], [-61.846283291331, 15.882070384716], [-61.155393250655, 15.882070384716], [-61.155393250655, 16.544367894077],
                ],
            ],
        ];
    }
}
