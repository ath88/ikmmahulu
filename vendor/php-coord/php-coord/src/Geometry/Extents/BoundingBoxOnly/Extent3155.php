<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 21°E to 23°E.
 * @internal
 */
class Extent3155
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [23.000000079663, 4.8311796188356], [20.999998436787, 4.8311796188356], [20.999998436787, -11.237222671509], [23.000000079663, -11.237222671509], [23.000000079663, 4.8311796188356],
                ],
            ],
        ];
    }
}
