<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Minnesota.
 * @internal
 */
class Extent1392
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.492868219435, 49.376657485962], [-97.216369095832, 49.376657485962], [-97.216369095832, 43.498102092378], [-89.492868219435, 43.498102092378], [-89.492868219435, 49.376657485962],
                ],
            ],
        ];
    }
}
