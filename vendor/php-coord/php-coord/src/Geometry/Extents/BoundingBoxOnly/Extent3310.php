<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Sri Lanka - onshore.
 * @internal
 */
class Extent3310
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [81.941830837842, 9.8768510015349], [79.645616058968, 9.8768510015349], [79.645616058968, 5.8693416732964], [81.941830837842, 5.8693416732964], [81.941830837842, 9.8768510015349],
                ],
            ],
        ];
    }
}
