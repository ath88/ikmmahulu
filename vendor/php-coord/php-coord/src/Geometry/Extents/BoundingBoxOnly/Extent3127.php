<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - Marquesas Islands - Ua Huka.
 * @internal
 */
class Extent3127
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-139.44743954753, -8.8179749231336], [-139.65069781525, -8.8179749231336], [-139.65069781525, -8.9910110247832], [-139.44743954753, -8.9910110247832], [-139.44743954753, -8.8179749231336],
                ],
            ],
        ];
    }
}
