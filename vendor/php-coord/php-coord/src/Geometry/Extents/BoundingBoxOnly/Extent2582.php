<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Karst.
 * @internal
 */
class Extent2582
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.111511035235, 45.898866206687], [13.57833194749, 45.898866206687], [13.57833194749, 45.647367582143], [14.111511035235, 45.647367582143], [14.111511035235, 45.898866206687],
                ],
            ],
        ];
    }
}
