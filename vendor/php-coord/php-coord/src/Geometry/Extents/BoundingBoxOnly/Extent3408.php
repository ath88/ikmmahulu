<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - Stockholm.
 * @internal
 */
class Extent3408
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18.197682861985, 59.42496862145], [17.759316198065, 59.42496862145], [17.759316198065, 59.227770799865], [18.197682861985, 59.227770799865], [18.197682861985, 59.42496862145],
                ],
            ],
        ];
    }
}
