<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - 132°E to 138°E, 36°S to 40°S (SJ53) onshore.
 * @internal
 */
class Extent2943
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [137.67574882507, -36], [136.57348715741, -36], [136.57348715741, -36.135427474976], [137.67574882507, -36.135427474976], [137.67574882507, -36],
                ],
            ],
        ];
    }
}
