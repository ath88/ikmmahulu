<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Lang Son.
 * @internal
 */
class Extent4556
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [107.364128113, 22.461481094], [106.094955444, 22.461481094], [106.094955444, 21.325149536], [107.364128113, 21.325149536], [107.364128113, 22.461481094],
                ],
            ],
        ];
    }
}
