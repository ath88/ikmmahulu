<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Ulysses.
 * @internal
 */
class Extent4505
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-101.066451, 38.701608], [-102.045253, 38.701608], [-102.045253, 36.993016], [-101.066451, 36.993016], [-101.066451, 38.701608],
                ],
            ],
        ];
    }
}
