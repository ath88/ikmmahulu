<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/France - mainland south of 44°N.
 * @internal
 */
class Extent3546
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.6488676954726, 44], [-1.7833556985155, 44], [-1.7833556985155, 42.332912445069], [7.6488676954726, 42.332912445069], [7.6488676954726, 44],
                ],
            ],
        ];
    }
}
