<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Latin America - 108°W to 102°W.
 * @internal
 */
class Extent3759
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-102, 31.783887863159], [-108, 31.783887863159], [-108, 14.057550430298], [-102, 14.057550430298], [-102, 31.783887863159],
                ],
            ],
        ];
    }
}
