<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - 20°E to 22°E.
 * @internal
 */
class Extent1457
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.00000098838, -24.765407562256], [19.999999223691, -24.765407562256], [19.999999223691, -34.872043609619], [22.00000098838, -34.872043609619], [22.00000098838, -24.765407562256],
                ],
            ],
        ];
    }
}
