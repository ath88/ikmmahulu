<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Angola (Cabinda) and DR Congo (Zaire) - coastal.
 * @internal
 */
class Extent3179
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.092638015747, -4.3889913558959], [10.531467191977, -4.3889913558959], [10.531467191977, -6.0374333606684], [13.092638015747, -6.0374333606684], [13.092638015747, -4.3889913558959],
                ],
            ],
        ];
    }
}
