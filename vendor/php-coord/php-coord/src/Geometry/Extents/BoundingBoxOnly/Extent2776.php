<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - CS63 zone K2.
 * @internal
 */
class Extent2776
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [52.2666666667, 51.769180297852], [49.2666666667, 51.769180297852], [49.2666666667, 41.159086418066], [52.2666666667, 41.159086418066], [52.2666666667, 51.769180297852],
                ],
            ],
        ];
    }
}
