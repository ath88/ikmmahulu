<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Iceland.
 * @internal
 */
class Extent1120
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-5.5574455049721, 69.58333318096], [-30.863333528977, 69.58333318096], [-30.863333528977, 59.963398171922], [-5.5574455049721, 59.963398171922], [-5.5574455049721, 69.58333318096],
                ],
            ],
        ];
    }
}
