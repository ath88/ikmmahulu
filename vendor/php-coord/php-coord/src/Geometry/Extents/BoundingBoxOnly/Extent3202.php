<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Midway Islands - Sand and Eastern Islands.
 * @internal
 */
class Extent3202
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-177.31054932964, 28.271520067333], [-177.44584410298, 28.271520067333], [-177.44584410298, 28.134157728077], [-177.31054932964, 28.134157728077], [-177.31054932964, 28.271520067333],
                ],
            ],
        ];
    }
}
