<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Luxembourg.
 * @internal
 */
class Extent1146
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.5240273475647, 50.181804656982], [5.7344436645508, 50.181804656982], [5.7344436645508, 49.448467254639], [6.5240273475647, 49.448467254639], [6.5240273475647, 50.181804656982],
                ],
            ],
        ];
    }
}
