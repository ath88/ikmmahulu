<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Riley-Lakeview.
 * @internal
 */
class Extent4458
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-119.15701069382, 43.448589529527], [-120.96400702897, 43.448589529527], [-120.96400702897, 41.882234444198], [-119.15701069382, 41.882234444198], [-119.15701069382, 43.448589529527],
                ],
            ],
        ];
    }
}
