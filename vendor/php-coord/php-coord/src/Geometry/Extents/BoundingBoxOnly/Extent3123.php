<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - east of 138°W.
 * @internal
 */
class Extent3123
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-131.978610623, -5.5277335349945], [-138, -5.5277335349945], [-138, -26.575872909], [-131.978610623, -26.575872909], [-131.978610623, -5.5277335349945],
                ],
            ],
        ];
    }
}
