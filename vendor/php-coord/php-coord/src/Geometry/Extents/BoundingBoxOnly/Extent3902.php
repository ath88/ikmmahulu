<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Reunion.
 * @internal
 */
class Extent3902
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [58.239722554, -18.286388901], [51.835076548, -18.286388901], [51.835076548, -24.710473907], [58.239722554, -24.710473907], [58.239722554, -18.286388901],
                ],
            ],
        ];
    }
}
