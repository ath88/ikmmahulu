<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Observation Point mc.
 * @internal
 */
class Extent3796
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [171.23691114703, -44.61361542], [169.77527154, -44.61361542], [169.77527154, -45.81652446], [171.23691114703, -45.81652446], [171.23691114703, -44.61361542],
                ],
            ],
        ];
    }
}
