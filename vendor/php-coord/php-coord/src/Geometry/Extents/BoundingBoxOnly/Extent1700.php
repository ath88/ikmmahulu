<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines - zone III.
 * @internal
 */
class Extent1700
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [122.20241501202, 21.615912148537], [119.70085073431, 21.615912148537], [119.70085073431, 3.0003331190004], [122.20241501202, 3.0003331190004], [122.20241501202, 21.615912148537],
                ],
            ],
        ];
    }
}
