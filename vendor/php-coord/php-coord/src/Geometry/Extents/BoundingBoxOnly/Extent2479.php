<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 36°N to 36°40'N; 139°E to 140°E.
 * @internal
 */
class Extent2479
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140, 36.666166666667], [139, 36.666166666667], [139, 35.999466666667], [140, 35.999466666667], [140, 36.666166666667],
                ],
            ],
        ];
    }
}
