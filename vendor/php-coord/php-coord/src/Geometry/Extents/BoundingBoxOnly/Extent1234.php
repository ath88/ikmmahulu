<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Tonga.
 * @internal
 */
class Extent1234
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-171.28416666006, -14.143271897], [-179.07111066694, -14.143271897], [-179.07111066694, -25.674113907865], [-171.28416666006, -25.674113907865], [-171.28416666006, -14.143271897],
                ],
            ],
        ];
    }
}
