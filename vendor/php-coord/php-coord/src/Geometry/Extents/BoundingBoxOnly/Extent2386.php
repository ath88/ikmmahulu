<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Greenland - Hayes Peninsula.
 * @internal
 */
class Extent2386
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-69.840584568581, 77.515549044684], [-72.788950552864, 77.515549044684], [-72.788950552864, 77.254883421082], [-69.840584568581, 77.254883421082], [-69.840584568581, 77.515549044684],
                ],
            ],
            [
                [
                    [-60.987905578202, 79.192143561832], [-73.281564503721, 79.192143561832], [-73.281564503721, 75.860029784869], [-60.987905578202, 75.860029784869], [-60.987905578202, 79.192143561832],
                ],
            ],
        ];
    }
}
