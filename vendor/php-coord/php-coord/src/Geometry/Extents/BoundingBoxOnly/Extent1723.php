<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Michigan - SPCS - N.
 * @internal
 */
class Extent1723
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-83.447769116626, 48.312211990357], [-90.418865851624, 48.312211990357], [-90.418865851624, 45.088539182448], [-83.447769116626, 45.088539182448], [-83.447769116626, 48.312211990357],
                ],
            ],
        ];
    }
}
