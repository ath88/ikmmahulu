<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Washburn.
 * @internal
 */
class Extent4335
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.540255722978, 46.15790831556], [-92.05070648861, 46.15790831556], [-92.05070648861, 45.637620171308], [-91.540255722978, 45.637620171308], [-91.540255722978, 46.15790831556],
                ],
            ],
        ];
    }
}
