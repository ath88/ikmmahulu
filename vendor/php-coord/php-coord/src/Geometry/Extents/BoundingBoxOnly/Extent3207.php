<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Cayman Islands - Cayman Brac.
 * @internal
 */
class Extent3207
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-79.694070036458, 19.777438823733], [-79.914204414109, 19.777438823733], [-79.914204414109, 19.663514399404], [-79.694070036458, 19.663514399404], [-79.694070036458, 19.777438823733],
                ],
            ],
        ];
    }
}
