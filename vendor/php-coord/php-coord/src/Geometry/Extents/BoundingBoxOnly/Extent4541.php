<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Dien Bien and Lai Chau.
 * @internal
 */
class Extent4541
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [103.985015869, 22.81111908], [102.144584656, 22.81111908], [102.144584656, 20.892038345], [103.985015869, 20.892038345], [103.985015869, 22.81111908],
                ],
            ],
        ];
    }
}
