<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Michigan - SPCS - S.
 * @internal
 */
class Extent1725
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.130279541016, 44.210421062505], [-87.199149295191, 44.210421062505], [-87.199149295191, 41.697121363375], [-82.130279541016, 41.697121363375], [-82.130279541016, 44.210421062505],
                ],
            ],
        ];
    }
}
