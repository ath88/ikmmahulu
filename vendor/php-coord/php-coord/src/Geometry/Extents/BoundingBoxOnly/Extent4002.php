<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Yemen - east of 54°E.
 * @internal
 */
class Extent4002
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [57.956546554001, 14.947155315285], [54, 14.947155315285], [54, 8.9577171240002], [57.956546554001, 8.9577171240002], [57.956546554001, 14.947155315285],
                ],
            ],
        ];
    }
}
