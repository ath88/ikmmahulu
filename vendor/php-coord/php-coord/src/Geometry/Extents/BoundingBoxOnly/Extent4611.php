<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - East Malaysia - Sarawak onshore.
 * @internal
 */
class Extent4611
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [115.680999999, 5.0229672824754], [109.54734039307, 5.0229672824754], [109.54734039307, 0.85277760028896], [115.680999999, 0.85277760028896], [115.680999999, 5.0229672824754],
                ],
            ],
        ];
    }
}
