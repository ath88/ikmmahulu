<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Poland - zone III.
 * @internal
 */
class Extent1517
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [20, 54.88596594036], [14.147637367248, 54.88596594036], [14.147637367248, 52.1666666667], [20, 52.1666666667], [20, 54.88596594036],
                ],
            ],
        ];
    }
}
