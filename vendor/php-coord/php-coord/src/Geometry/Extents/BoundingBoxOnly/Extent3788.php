<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Amuri mc.
 * @internal
 */
class Extent3788
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [173.5430316927, -42.09576543], [171.88771158, -42.09576543], [171.88771158, -42.946605647732], [173.5430316927, -42.946605647732], [173.5430316927, -42.09576543],
                ],
            ],
        ];
    }
}
