<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Menominee.
 * @internal
 */
class Extent4348
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.483622851717, 45.118051194929], [-88.982200411232, 45.118051194929], [-88.982200411232, 44.855459626616], [-88.483622851717, 44.855459626616], [-88.483622851717, 45.118051194929],
                ],
            ],
        ];
    }
}
