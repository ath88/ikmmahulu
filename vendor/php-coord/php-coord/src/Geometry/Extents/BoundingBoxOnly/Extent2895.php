<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Guadeloupe - Les Saintes - onshore.
 * @internal
 */
class Extent2895
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.520332899748, 15.933294101711], [-61.677631746704, 15.933294101711], [-61.677631746704, 15.803686227698], [-61.520332899748, 15.803686227698], [-61.520332899748, 15.933294101711],
                ],
            ],
        ];
    }
}
