<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Tucano basin north.
 * @internal
 */
class Extent2308
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-37.093614578247, -8.3967514038086], [-39.031932830811, -8.3967514038086], [-39.031932830811, -9.7999992370605], [-37.093614578247, -9.7999992370605], [-37.093614578247, -8.3967514038086],
                ],
            ],
        ];
    }
}
