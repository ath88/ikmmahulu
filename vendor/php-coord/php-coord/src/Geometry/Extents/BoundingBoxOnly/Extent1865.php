<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - zone XII.
 * @internal
 */
class Extent1865
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141.40455437187, 45.512715234618], [140.89709669602, 45.512715234618], [140.89709669602, 45.04818927238], [141.40455437187, 45.04818927238], [141.40455437187, 45.512715234618],
                ],
            ],
            [
                [
                    [143.603369, 45.535358965168], [141.009301, 45.535358965168], [141.009301, 42.151622706651], [143.603369, 42.151622706651], [143.603369, 45.535358965168],
                ],
            ],
        ];
    }
}
