<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Red Oak-Ottumwa.
 * @internal
 */
class Extent4242
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-92.178142, 41.162647], [-95.385478, 41.162647], [-95.385478, 40.570966143683], [-92.178142, 40.570966143683], [-92.178142, 41.162647],
                ],
            ],
        ];
    }
}
