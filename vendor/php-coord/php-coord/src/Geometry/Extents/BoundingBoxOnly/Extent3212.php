<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Albania - onshore.
 * @internal
 */
class Extent3212
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21.053331375122, 42.660343170166], [19.224593543761, 42.660343170166], [19.224593543761, 39.643064023431], [21.053331375122, 39.643064023431], [21.053331375122, 42.660343170166],
                ],
            ],
        ];
    }
}
