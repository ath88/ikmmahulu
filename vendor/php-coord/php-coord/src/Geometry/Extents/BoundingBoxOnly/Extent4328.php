<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Forest.
 * @internal
 */
class Extent4328
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.425359268487, 46.073516132926], [-89.04791333783, 46.073516132926], [-89.04791333783, 45.376920169604], [-88.425359268487, 45.376920169604], [-88.425359268487, 46.073516132926],
                ],
            ],
        ];
    }
}
