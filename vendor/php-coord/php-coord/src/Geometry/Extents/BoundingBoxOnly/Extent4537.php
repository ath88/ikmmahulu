<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario ex. Toronto.
 * @internal
 */
class Extent4537
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.358075474967, 56.894989545085], [-95.157722473145, 56.894989545085], [-95.157722473145, 41.675552368164], [-74.358075474967, 41.675552368164], [-74.358075474967, 56.894989545085],
                ],
            ],
        ];
    }
}
