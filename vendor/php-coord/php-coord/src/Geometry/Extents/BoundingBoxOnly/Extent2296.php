<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cote d'Ivoire (Ivory Coast) - offshore.
 * @internal
 */
class Extent2296
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-3.1146571773713, 5.1876241864296], [-7.5458185069995, 5.1876241864296], [-7.5458185069995, 1.0216541170003], [-3.1146571773713, 1.0216541170003], [-3.1146571773713, 5.1876241864296],
                ],
            ],
        ];
    }
}
