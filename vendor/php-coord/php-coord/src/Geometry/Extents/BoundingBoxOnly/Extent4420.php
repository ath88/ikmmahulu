<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Uttarakhand.
 * @internal
 */
class Extent4420
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [81.019607543946, 31.472259521484], [77.562240600586, 31.472259521484], [77.562240600586, 28.715620040894], [81.019607543946, 28.715620040894], [81.019607543946, 31.472259521484],
                ],
            ],
        ];
    }
}
