<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Dane.
 * @internal
 */
class Extent4364
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.008692923011, 43.294177321608], [-89.838528267265, 43.294177321608], [-89.838528267265, 42.845135166038], [-89.008692923011, 42.845135166038], [-89.008692923011, 43.294177321608],
                ],
            ],
        ];
    }
}
