<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Idaho.
 * @internal
 */
class Extent1381
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-111.04677118118, 49.002551194312], [-117.23692089447, 49.002551194312], [-117.23692089447, 41.9945994629], [-111.04677118118, 41.9945994629], [-111.04677118118, 49.002551194312],
                ],
            ],
        ];
    }
}
