<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska including EEZ.
 * @internal
 */
class Extent2373
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-129.99365234375, 74.709768295288], [-192.34928894043, 74.709768295288], [-192.34928894043, 47.881341934204], [-129.99365234375, 47.881341934204], [-129.99365234375, 74.709768295288],
                ],
            ],
        ];
    }
}
