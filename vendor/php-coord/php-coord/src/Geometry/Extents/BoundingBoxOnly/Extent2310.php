<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Tucano basin south.
 * @internal
 */
class Extent2310
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-37.989717483521, -10.60000038147], [-39.066816329956, -10.60000038147], [-39.066816329956, -12.262121200562], [-37.989717483521, -12.262121200562], [-37.989717483521, -10.60000038147],
                ],
            ],
        ];
    }
}
