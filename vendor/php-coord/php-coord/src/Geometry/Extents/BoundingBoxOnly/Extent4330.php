<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Oneida.
 * @internal
 */
class Extent4330
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.046458596933, 45.901500200736], [-90.045323591618, 45.901500200736], [-90.045323591618, 45.464744063236], [-89.046458596933, 45.464744063236], [-89.046458596933, 45.901500200736],
                ],
            ],
        ];
    }
}
