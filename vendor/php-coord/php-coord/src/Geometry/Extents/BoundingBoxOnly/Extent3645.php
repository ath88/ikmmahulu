<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Sao Tome and Principe - onshore - Sao Tome.
 * @internal
 */
class Extent3645
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.8159500793043, 0.4588446010667], [6.415448532668, 0.4588446010667], [6.415448532668, -0.031757783779582], [6.8159500793043, -0.031757783779582], [6.8159500793043, 0.4588446010667],
                ],
            ],
        ];
    }
}
