<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - 91.5°W to 88.5°W.
 * @internal
 */
class Extent1436
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.5, 56.894989545085], [-91.5, 56.894989545085], [-91.5, 47.979713439942], [-88.5, 47.979713439942], [-88.5, 56.894989545085],
                ],
            ],
        ];
    }
}
