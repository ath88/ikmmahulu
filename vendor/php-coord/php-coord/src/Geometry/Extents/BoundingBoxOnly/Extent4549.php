<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - 104°15'E to 107°15'E by province - HCMC.
 * @internal
 */
class Extent4549
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [107.027506462, 11.500681877], [105.499343872, 11.500681877], [105.499343872, 9.7502120457813], [107.027506462, 9.7502120457813], [107.027506462, 11.500681877],
                ],
            ],
            [
                [
                    [107.79889888625, 20.220617867382], [107.62603622324, 20.220617867382], [107.62603622324, 20.06783941945], [107.79889888625, 20.06783941945], [107.79889888625, 20.220617867382],
                ],
            ],
            [
                [
                    [107.26572663513, 21.020458221], [106.400505066, 21.020458221], [106.400505066, 20.561971437601], [107.26572663513, 20.561971437601], [107.26572663513, 21.020458221],
                ],
            ],
            [
                [
                    [106.83769989, 23.119197845], [105.266281128, 23.119197845], [105.266281128, 22.356613159], [106.83769989, 22.356613159], [106.83769989, 23.119197845],
                ],
            ],
        ];
    }
}
