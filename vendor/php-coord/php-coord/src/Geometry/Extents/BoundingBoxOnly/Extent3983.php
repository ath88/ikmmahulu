<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 120°E to 126°E, N hemisphere onshore.
 * @internal
 */
class Extent3983
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [125.70686087398, 3.8362147918638], [125.36034870187, 3.8362147918638], [125.36034870187, 3.2855103482542], [125.70686087398, 3.2855103482542], [125.70686087398, 3.8362147918638],
                ],
            ],
            [
                [
                    [125.29719874447, 1.750055182589], [120, 1.750055182589], [120, 0], [125.29719874447, 0], [125.29719874447, 1.750055182589],
                ],
            ],
            [
                [
                    [125.49840077452, 2.8570120978077], [125.30664102891, 2.8570120978077], [125.30664102891, 2.5765966148084], [125.49840077452, 2.5765966148084], [125.49840077452, 2.8570120978077],
                ],
            ],
        ];
    }
}
