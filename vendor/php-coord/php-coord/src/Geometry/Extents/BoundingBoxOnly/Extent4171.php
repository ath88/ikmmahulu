<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Northern Mariana Islands - Rota, Saipan and Tinian.
 * @internal
 */
class Extent4171
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [145.34292230919, 14.251773077096], [145.0699936767, 14.251773077096], [145.0699936767, 14.060159606216], [145.34292230919, 14.060159606216], [145.34292230919, 14.251773077096],
                ],
            ],
            [
                [
                    [145.88338138703, 15.342607000464], [145.53029753601, 15.342607000464], [145.53029753601, 14.871419354282], [145.88338138703, 14.871419354282], [145.88338138703, 15.342607000464],
                ],
            ],
        ];
    }
}
