<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Shawano.
 * @internal
 */
class Extent4354
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.242715511373, 45.029595826655], [-89.2244770459, 45.029595826655], [-89.2244770459, 44.584735159798], [-88.242715511373, 44.584735159798], [-88.242715511373, 45.029595826655],
                ],
            ],
        ];
    }
}
