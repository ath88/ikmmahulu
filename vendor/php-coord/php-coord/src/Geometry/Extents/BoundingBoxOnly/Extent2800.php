<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - St. Kilda onshore.
 * @internal
 */
class Extent2800
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-8.4127157255197, 57.920997409423], [-8.7307159830185, 57.920997409423], [-8.7307159830185, 57.746183026091], [-8.4127157255197, 57.746183026091], [-8.4127157255197, 57.920997409423],
                ],
            ],
        ];
    }
}
