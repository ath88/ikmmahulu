<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - North Sea.
 * @internal
 */
class Extent2330
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.13155777908, 62.000067664245], [-5.046669789728, 62.000067664245], [-5.046669789728, 51.039454652004], [11.13155777908, 51.039454652004], [11.13155777908, 62.000067664245],
                ],
            ],
        ];
    }
}
