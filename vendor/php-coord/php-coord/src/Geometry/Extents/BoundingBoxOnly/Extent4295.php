<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Randolph and Wayne.
 * @internal
 */
class Extent4295
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-84.803917000002, 40.310114999983], [-85.221135999996, 40.310114999983], [-85.221135999996, 39.714580000021], [-84.803917000002, 39.714580000021], [-84.803917000002, 40.310114999983],
                ],
            ],
        ];
    }
}
