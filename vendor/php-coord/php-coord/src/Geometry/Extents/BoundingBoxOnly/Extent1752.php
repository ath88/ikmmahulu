<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Peru - 79°W to 73°W.
 * @internal
 */
class Extent1752
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-73, -0.036874771118164], [-79, -0.036874771118164], [-79, -16.561971664429], [-73, -16.561971664429], [-73, -0.036874771118164],
                ],
            ],
        ];
    }
}
