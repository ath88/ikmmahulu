<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Estonia.
 * @internal
 */
class Extent1090
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28.194093704224, 59.994633172029], [20.373333519, 59.994633172029], [20.373333519, 57.522632598877], [28.194093704224, 57.522632598877], [28.194093704224, 59.994633172029],
                ],
            ],
        ];
    }
}
