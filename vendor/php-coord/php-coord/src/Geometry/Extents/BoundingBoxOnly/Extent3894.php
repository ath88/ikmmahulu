<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - Chatham Island onshore.
 * @internal
 */
class Extent3894
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-176.2003, -43.672024], [-176.917692, -43.672024], [-176.917692, -44.170483], [-176.2003, -44.170483], [-176.2003, -43.672024],
                ],
            ],
        ];
    }
}
