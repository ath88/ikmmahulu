<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - 6th parallel south 27°E to 29°E.
 * @internal
 */
class Extent3166
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29, -4.2485997566918], [27, -4.2485997566918], [27, -7.3559915662654], [29, -7.3559915662654], [29, -4.2485997566918],
                ],
            ],
        ];
    }
}
