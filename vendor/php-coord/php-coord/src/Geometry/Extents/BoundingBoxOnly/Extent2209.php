<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Massachusetts - SPCS - mainland.
 * @internal
 */
class Extent2209
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-69.860315322876, 42.886877476298], [-73.498840002732, 42.886877476298], [-73.498840002732, 41.46132850647], [-69.860315322876, 41.46132850647], [-69.860315322876, 42.886877476298],
                ],
            ],
        ];
    }
}
