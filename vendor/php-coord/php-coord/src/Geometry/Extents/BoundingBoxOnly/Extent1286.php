<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - Liechtenstein and Switzerland.
 * @internal
 */
class Extent1286
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.488205909729, 47.806663513184], [5.9670133590698, 47.806663513184], [5.9670133590698, 45.829437255859], [10.488205909729, 45.829437255859], [10.488205909729, 47.806663513184],
                ],
            ],
        ];
    }
}
