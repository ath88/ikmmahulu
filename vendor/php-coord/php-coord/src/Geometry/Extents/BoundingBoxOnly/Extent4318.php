<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - Billings.
 * @internal
 */
class Extent4318
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-108, 47.4], [-109.416667, 47.4], [-109.416667, 44.99764251709], [-108, 44.99764251709], [-108, 47.4],
                ],
            ],
        ];
    }
}
