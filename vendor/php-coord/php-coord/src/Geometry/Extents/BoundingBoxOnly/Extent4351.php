<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Polk.
 * @internal
 */
class Extent4351
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-92.154356032425, 45.72871447054], [-92.889241325672, 45.72871447054], [-92.889241325672, 45.209147640774], [-92.154356032425, 45.209147640774], [-92.154356032425, 45.72871447054],
                ],
            ],
        ];
    }
}
