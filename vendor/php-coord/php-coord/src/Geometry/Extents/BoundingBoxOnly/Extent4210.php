<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Pendleton-La Grande.
 * @internal
 */
class Extent4210
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-118.09268579428, 45.631487392296], [-118.6398349774, 45.631487392296], [-118.6398349774, 45.137772284089], [-118.09268579428, 45.137772284089], [-118.09268579428, 45.631487392296],
                ],
            ],
        ];
    }
}
