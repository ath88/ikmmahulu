<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Goa.
 * @internal
 */
class Extent4399
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [74.341651916504, 15.797529220581], [73.618405993084, 15.797529220581], [73.618405993084, 14.86610746983], [74.341651916504, 14.86610746983], [74.341651916504, 15.797529220581],
                ],
            ],
        ];
    }
}
