<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/South America - 72°W to 66°W, S hemisphere and PSAD56 by country.
 * @internal
 */
class Extent1759
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.733337402344, -43.121116638183], [-72, -43.121116638183], [-72, -43.5], [-71.733337402344, -43.5], [-71.733337402344, -43.121116638183],
                ],
            ],
            [
                [
                    [-66, -9.4280014038085], [-72, -9.4280014038085], [-72, -42.176254272461], [-66, -42.176254272461], [-66, -9.4280014038085],
                ],
            ],
            [
                [
                    [-69.947516273694, -2.1479167938232], [-72, -2.1479167938232], [-72, -4.5914249420166], [-69.947516273694, -4.5914249420166], [-69.947516273694, -2.1479167938232],
                ],
            ],
        ];
    }
}
