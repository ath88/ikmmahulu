<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska - St. Paul Island.
 * @internal
 */
class Extent1333
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-170.04836962212, 57.27257554327], [-170.50439055936, 57.27257554327], [-170.50439055936, 57.060761348858], [-170.04836962212, 57.060761348858], [-170.04836962212, 57.27257554327],
                ],
            ],
        ];
    }
}
