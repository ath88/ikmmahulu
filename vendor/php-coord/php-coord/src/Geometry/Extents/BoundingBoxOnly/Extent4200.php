<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Columbia River East.
 * @internal
 */
class Extent4200
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-118.89299652568, 46.071948741305], [-122.04497270382, 46.071948741305], [-122.04497270382, 45.495336549602], [-118.89299652568, 45.495336549602], [-118.89299652568, 46.071948741305],
                ],
            ],
        ];
    }
}
