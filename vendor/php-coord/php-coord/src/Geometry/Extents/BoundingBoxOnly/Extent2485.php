<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 35°20'N to 36°N; 136°E to 137°E.
 * @internal
 */
class Extent2485
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [137, 35.999466666667], [136, 35.999466666667], [136, 35.332766666667], [137, 35.332766666667], [137, 35.999466666667],
                ],
            ],
        ];
    }
}
