<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Mississippi - SPCS - W.
 * @internal
 */
class Extent2217
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.378473481541, 35.00078876686], [-91.644988038412, 35.00078876686], [-91.644988038412, 31.000418117466], [-89.378473481541, 31.000418117466], [-89.378473481541, 35.00078876686],
                ],
            ],
        ];
    }
}
