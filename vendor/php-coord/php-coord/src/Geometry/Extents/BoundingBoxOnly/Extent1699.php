<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines - zone II.
 * @internal
 */
class Extent1699
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [120.06130123493, 20.418337346308], [118, 20.418337346308], [118, 3.0225022566842], [120.06130123493, 3.0225022566842], [120.06130123493, 20.418337346308],
                ],
            ],
        ];
    }
}
