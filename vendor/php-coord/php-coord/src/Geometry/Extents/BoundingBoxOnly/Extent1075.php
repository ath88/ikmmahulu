<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cote d'Ivoire (Ivory Coast).
 * @internal
 */
class Extent1075
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-2.487778186798, 10.735256195068], [-8.6063842773436, 10.735256195068], [-8.6063842773436, 1.0216541170003], [-2.487778186798, 1.0216541170003], [-2.487778186798, 10.735256195068],
                ],
            ],
        ];
    }
}
