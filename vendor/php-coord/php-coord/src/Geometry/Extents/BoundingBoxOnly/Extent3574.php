<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - onshore - eastern - S-42(58).
 * @internal
 */
class Extent3574
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21.053331375122, 42.660343170166], [18.461944517, 42.660343170166], [18.461944517, 39.638635153001], [21.053331375122, 39.638635153001], [21.053331375122, 42.660343170166],
                ],
            ],
            [
                [
                    [31.409714529, 54.88596594036], [9.9222221374512, 54.88596594036], [9.9222221374512, 41.243049621582], [31.409714529, 41.243049621582], [31.409714529, 54.88596594036],
                ],
            ],
        ];
    }
}
