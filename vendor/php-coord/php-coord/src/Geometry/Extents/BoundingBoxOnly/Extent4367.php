<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Green and Lafayette.
 * @internal
 */
class Extent4367
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-89.365805225126, 42.857662302571], [-90.426914000763, 42.857662302571], [-90.426914000763, 42.50006207388], [-89.365805225126, 42.50006207388], [-89.365805225126, 42.857662302571],
                ],
            ],
        ];
    }
}
