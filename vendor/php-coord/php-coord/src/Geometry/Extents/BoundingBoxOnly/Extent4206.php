<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - La Grande.
 * @internal
 */
class Extent4206
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-117.14252360092, 45.795106759201], [-118.16162794562, 45.795106759201], [-118.16162794562, 45.133970010819], [-117.14252360092, 45.133970010819], [-117.14252360092, 45.795106759201],
                ],
            ],
        ];
    }
}
