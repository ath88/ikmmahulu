<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Quang Ngai.
 * @internal
 */
class Extent4558
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [109.13482235214, 15.474226114288], [108.235107422, 15.474226114288], [108.235107422, 14.531601906], [109.13482235214, 14.531601906], [109.13482235214, 15.474226114288],
                ],
            ],
            [
                [
                    [109.19467197331, 15.48311145708], [109.02558580704, 15.48311145708], [109.02558580704, 15.318885212706], [109.19467197331, 15.318885212706], [109.19467197331, 15.48311145708],
                ],
            ],
        ];
    }
}
