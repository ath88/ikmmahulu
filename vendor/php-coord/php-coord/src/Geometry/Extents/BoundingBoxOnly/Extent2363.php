<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - east.
 * @internal
 */
class Extent2363
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-59.803058624268, 10.797822952271], [-67.489545129238, 10.797822952271], [-67.489545129238, 3.5601406097413], [-59.803058624268, 3.5601406097413], [-59.803058624268, 10.797822952271],
                ],
            ],
        ];
    }
}
