<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Falkland Islands - East Falkland Island.
 * @internal
 */
class Extent2355
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.60570099751, -51.162695679013], [-59.976335119791, -51.162695679013], [-59.976335119791, -52.509350206498], [-57.60570099751, -52.509350206498], [-57.60570099751, -51.162695679013],
                ],
            ],
        ];
    }
}
