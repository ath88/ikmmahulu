<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Louisiana.
 * @internal
 */
class Extent1387
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.758344024157, 33.023422407749], [-94.041785426663, 33.023422407749], [-94.041785426663, 28.854289941421], [-88.758344024157, 28.854289941421], [-88.758344024157, 33.023422407749],
                ],
            ],
        ];
    }
}
