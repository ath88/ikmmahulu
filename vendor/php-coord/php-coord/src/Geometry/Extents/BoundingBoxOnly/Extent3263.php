<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - onshore mainland.
 * @internal
 */
class Extent3263
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142.13432077242, 41.57864317941], [129.30164552051, 41.57864317941], [129.30164552051, 30.9498237462], [142.13432077242, 30.9498237462], [142.13432077242, 41.57864317941],
                ],
            ],
            [
                [
                    [145.86544719357, 45.535358965168], [139.70234680179, 45.535358965168], [139.70234680179, 41.348161211323], [145.86544719357, 41.348161211323], [145.86544719357, 45.535358965168],
                ],
            ],
        ];
    }
}
