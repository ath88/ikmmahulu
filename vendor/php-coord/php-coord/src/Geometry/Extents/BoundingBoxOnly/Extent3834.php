<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/South America - 72°W to 66°W, N hemisphere onshore.
 * @internal
 */
class Extent3834
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66, 12.513334274292], [-72, 12.513334274292], [-72, 0], [-66, 0], [-66, 12.513334274292],
                ],
            ],
        ];
    }
}
