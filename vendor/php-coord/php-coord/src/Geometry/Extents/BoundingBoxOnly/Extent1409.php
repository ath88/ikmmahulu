<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - South Carolina.
 * @internal
 */
class Extent1409
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78.526371340602, 35.208240509033], [-83.350799560547, 35.208240509033], [-83.350799560547, 32.052025355722], [-78.526371340602, 32.052025355722], [-78.526371340602, 35.208240509033],
                ],
            ],
        ];
    }
}
