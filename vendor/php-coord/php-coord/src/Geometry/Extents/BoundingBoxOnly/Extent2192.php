<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Idaho - SPCS - E.
 * @internal
 */
class Extent2192
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-111.04677118118, 44.745087042947], [-113.23663797349, 44.745087042947], [-113.23663797349, 41.996203349407], [-111.04677118118, 41.996203349407], [-111.04677118118, 44.745087042947],
                ],
            ],
        ];
    }
}
