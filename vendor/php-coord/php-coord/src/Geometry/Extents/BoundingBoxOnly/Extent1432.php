<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - MTM zone 11.
 * @internal
 */
class Extent1432
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81, 46], [-83.597778320312, 46], [-83.597778320312, 41.675552368164], [-81, 41.675552368164], [-81, 46],
                ],
            ],
        ];
    }
}
