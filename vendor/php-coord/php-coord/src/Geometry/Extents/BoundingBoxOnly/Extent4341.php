<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Dunn.
 * @internal
 */
class Extent4341
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.649656768247, 45.209469646551], [-92.157085958916, 45.209469646551], [-92.157085958916, 44.683580260692], [-91.649656768247, 44.683580260692], [-91.649656768247, 45.209469646551],
                ],
            ],
        ];
    }
}
