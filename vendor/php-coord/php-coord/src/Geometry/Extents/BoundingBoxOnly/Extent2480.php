<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 36°N to 36°40'N; 140°E to 141°E.
 * @internal
 */
class Extent2480
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140.7623185451, 36.666166666667], [140, 36.666166666667], [140, 35.999466666667], [140.7623185451, 35.999466666667], [140.7623185451, 36.666166666667],
                ],
            ],
        ];
    }
}
