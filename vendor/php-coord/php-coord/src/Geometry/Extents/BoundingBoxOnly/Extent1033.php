<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Argentina.
 * @internal
 */
class Extent1033
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-52.631031036377, -21.780521392822], [-73.582298278809, -21.780521392822], [-73.582298278809, -58.404598236084], [-52.631031036377, -58.404598236084], [-52.631031036377, -21.780521392822],
                ],
            ],
        ];
    }
}
