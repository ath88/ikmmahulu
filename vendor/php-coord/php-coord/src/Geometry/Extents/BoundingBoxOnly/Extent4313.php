<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana  Fort Belknap.
 * @internal
 */
class Extent4313
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-107, 49.000604629517], [-110.833333, 49.000604629517], [-110.833333, 47.783], [-107, 47.783], [-107, 49.000604629517],
                ],
            ],
        ];
    }
}
