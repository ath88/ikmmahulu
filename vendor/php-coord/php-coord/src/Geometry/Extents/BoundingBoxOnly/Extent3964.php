<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines - zone II onshore.
 * @internal
 */
class Extent3964
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [120.06130123493, 11.474056585697], [118, 11.474056585697], [118, 8.8263865804279], [120.06130123493, 8.8263865804279], [120.06130123493, 11.474056585697],
                ],
            ],
            [
                [
                    [119.92840660985, 11.57118978883], [119.66349760329, 11.57118978883], [119.66349760329, 11.326865942033], [119.92840660985, 11.326865942033], [119.92840660985, 11.57118978883],
                ],
            ],
        ];
    }
}
