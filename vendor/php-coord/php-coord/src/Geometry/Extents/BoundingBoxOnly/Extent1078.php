<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Cyprus.
 * @internal
 */
class Extent1078
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [35.191951533, 36.20754915], [29.952283527969, 36.20754915], [29.952283527969, 32.884955146012], [35.191951533, 32.884955146012], [35.191951533, 36.20754915],
                ],
            ],
        ];
    }
}
