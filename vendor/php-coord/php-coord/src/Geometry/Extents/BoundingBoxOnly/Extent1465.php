<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran - 48°E to 54°E.
 * @internal
 */
class Extent1465
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [54, 39.707053643924], [48, 39.707053643924], [48, 25.47024614], [54, 25.47024614], [54, 39.707053643924],
                ],
            ],
        ];
    }
}
