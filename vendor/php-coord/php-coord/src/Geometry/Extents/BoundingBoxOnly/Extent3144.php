<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/French Guiana - east of 54°W.
 * @internal
 */
class Extent3144
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-49.459503173828, 8.8771533966064], [-54, 8.8771533966064], [-54, 2.1756954193115], [-49.459503173828, 2.1756954193115], [-49.459503173828, 8.8771533966064],
                ],
            ],
        ];
    }
}
