<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Newfoundland - east of 54.5°W.
 * @internal
 */
class Extent2226
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-52.543563434093, 49.880440168616], [-54.5, 49.880440168616], [-54.5, 46.567800287196], [-52.543563434093, 46.567800287196], [-52.543563434093, 49.880440168616],
                ],
            ],
        ];
    }
}
