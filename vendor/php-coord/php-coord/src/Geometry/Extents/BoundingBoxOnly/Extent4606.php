<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - British Isles - Ireland onshore, UK onshore and UKCS.
 * @internal
 */
class Extent4606
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [2.0000000000011, 61], [-10.847173753249, 61], [-10.847173753249, 49.758303], [2.0000000000011, 49.758303], [2.0000000000011, 61],
                ],
            ],
        ];
    }
}
