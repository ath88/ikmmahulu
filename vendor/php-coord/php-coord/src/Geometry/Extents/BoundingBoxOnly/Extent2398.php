<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/UK - Wales.
 * @internal
 */
class Extent2398
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-2.65944480896, 53.474569331915], [-5.3396037623181, 53.474569331915], [-5.3396037623181, 51.284946450377], [-2.65944480896, 51.284946450377], [-2.65944480896, 53.474569331915],
                ],
            ],
        ];
    }
}
