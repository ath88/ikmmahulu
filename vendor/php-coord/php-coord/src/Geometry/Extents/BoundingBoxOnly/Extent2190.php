<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Georgia - SPCS - W.
 * @internal
 */
class Extent2190
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.997496906685, 35.000366708131], [-85.60896021625, 35.000366708131], [-85.60896021625, 30.621341398742], [-82.997496906685, 30.621341398742], [-82.997496906685, 35.000366708131],
                ],
            ],
        ];
    }
}
