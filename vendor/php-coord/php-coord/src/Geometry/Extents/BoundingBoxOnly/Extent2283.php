<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - Australian Capital Territory.
 * @internal
 */
class Extent2283
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [149.398086, -35.126067], [148.761582, -35.126067], [148.761582, -35.92208], [149.398086, -35.92208], [149.398086, -35.126067],
                ],
            ],
        ];
    }
}
