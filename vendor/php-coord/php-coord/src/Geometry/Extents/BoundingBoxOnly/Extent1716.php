<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - offshore deep water - west of 6°E.
 * @internal
 */
class Extent1716
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6, 6.1374495118623], [2.6674375020002, 6.1374495118623], [2.6674375020002, 1.9216671180004], [6, 1.9216671180004], [6, 6.1374495118623],
                ],
            ],
        ];
    }
}
