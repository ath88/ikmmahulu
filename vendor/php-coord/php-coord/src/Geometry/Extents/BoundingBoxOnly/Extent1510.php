<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Sierra Leone - east of 12°W.
 * @internal
 */
class Extent1510
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-10.264307022094, 9.9974994659424], [-12, 9.9974994659424], [-12, 6.8809612931623], [-10.264307022094, 6.8809612931623], [-10.264307022094, 9.9974994659424],
                ],
            ],
        ];
    }
}
