<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Gasa district.
 * @internal
 */
class Extent3740
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [90.46694193674, 28.324996948242], [89.44879742787, 28.324996948242], [89.44879742787, 27.723025394142], [90.46694193674, 27.723025394142], [90.46694193674, 28.324996948242],
                ],
            ],
        ];
    }
}
