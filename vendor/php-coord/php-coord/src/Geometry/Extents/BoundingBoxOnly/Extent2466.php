<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 37°20'N to 38°N; 137°E to 138°E.
 * @internal
 */
class Extent2466
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [137.42267711252, 37.571300416443], [137, 37.571300416443], [137, 37.332866666667], [137.42267711252, 37.332866666667], [137.42267711252, 37.571300416443],
                ],
            ],
        ];
    }
}
