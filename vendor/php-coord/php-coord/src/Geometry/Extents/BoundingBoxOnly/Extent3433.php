<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - east of 168°E.
 * @internal
 */
class Extent3433
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [174.275709662, -19.750086899002], [168, -19.750086899002], [168, -25.942013537378], [174.275709662, -25.942013537378], [174.275709662, -19.750086899002],
                ],
            ],
        ];
    }
}
