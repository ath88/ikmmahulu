<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Gujarat.
 * @internal
 */
class Extent4400
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [74.47891998291, 24.70578956604], [68.136841438671, 24.70578956604], [68.136841438671, 20.051620483398], [74.47891998291, 20.051620483398], [74.47891998291, 24.70578956604],
                ],
            ],
        ];
    }
}
