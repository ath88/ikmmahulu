<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Kon Tum.
 * @internal
 */
class Extent4557
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [108.547561646, 15.417463303], [107.332870483, 15.417463303], [107.332870483, 13.92152977], [108.547561646, 13.92152977], [108.547561646, 15.417463303],
                ],
            ],
        ];
    }
}
