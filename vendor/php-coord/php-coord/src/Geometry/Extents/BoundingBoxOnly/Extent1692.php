<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Malaysia - West Malaysia - east of 102°E.
 * @internal
 */
class Extent1692
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [103.31152615917, 1.5130992966913], [103.19098215743, 1.5130992966913], [103.19098215743, 1.4230376861951], [103.31152615917, 1.4230376861951], [103.31152615917, 1.5130992966913],
                ],
            ],
            [
                [
                    [105.819999599, 7.8000001201534], [102, 7.8000001201534], [102, 1.2192625415937], [105.819999599, 1.2192625415937], [105.819999599, 7.8000001201534],
                ],
            ],
        ];
    }
}
