<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - Fort Peck lower areas.
 * @internal
 */
class Extent4315
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.04747576021, 49.00010495367], [-107.7500000004, 49.00010495367], [-107.7500000004, 47.75], [-104.04747576021, 47.75], [-104.04747576021, 49.00010495367],
                ],
            ],
        ];
    }
}
