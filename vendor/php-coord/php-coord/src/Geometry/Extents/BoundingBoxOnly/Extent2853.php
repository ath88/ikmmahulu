<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Iceland - onshore east of 18°W.
 * @internal
 */
class Extent2853
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-13.381984495641, 66.580108967227], [-18, 66.580108967227], [-18, 63.456364281252], [-13.381984495641, 63.456364281252], [-13.381984495641, 66.580108967227],
                ],
            ],
        ];
    }
}
