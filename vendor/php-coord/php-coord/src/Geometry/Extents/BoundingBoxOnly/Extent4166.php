<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - onshore - Honshu, Shikoku, Kyushu.
 * @internal
 */
class Extent4166
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [142.13432077242, 41.57864317941], [129.30164552051, 41.57864317941], [129.30164552051, 30.9498237462], [142.13432077242, 30.9498237462], [142.13432077242, 41.57864317941],
                ],
            ],
        ];
    }
}
