<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Guarico state.
 * @internal
 */
class Extent1371
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-64.760749816895, 10.020002365112], [-68, 10.020002365112], [-68, 7.5404148101807], [-64.760749816895, 7.5404148101807], [-64.760749816895, 10.020002365112],
                ],
            ],
        ];
    }
}
