<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 35°20'N to 36°N; 132°E to 133°E.
 * @internal
 */
class Extent2481
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [133, 35.579444380496], [132.5688540009, 35.579444380496], [132.5688540009, 35.332766666667], [133, 35.332766666667], [133, 35.579444380496],
                ],
            ],
        ];
    }
}
