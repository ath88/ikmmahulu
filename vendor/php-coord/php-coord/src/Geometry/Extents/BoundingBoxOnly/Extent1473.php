<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 14°E to 16°E onshore.
 * @internal
 */
class Extent1473
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16, 32.78967672354], [14, 32.78967672354], [14, 22.614166259766], [16, 22.614166259766], [16, 32.78967672354],
                ],
            ],
        ];
    }
}
