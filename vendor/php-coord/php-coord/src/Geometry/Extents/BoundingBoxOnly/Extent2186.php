<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Florida - SPCS - E.
 * @internal
 */
class Extent2186
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-79.973489761352, 30.828308294787], [-82.326482184764, 30.828308294787], [-82.326482184764, 24.410230723243], [-79.973489761352, 24.410230723243], [-79.973489761352, 30.828308294787],
                ],
            ],
        ];
    }
}
