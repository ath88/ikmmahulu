<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 40°N to 40°48'N; 139°E to 140°E.
 * @internal
 */
class Extent2453
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [140, 40.792087206834], [139.63471746484, 40.792087206834], [139.63471746484, 39.999666666667], [140, 39.999666666667], [140, 40.792087206834],
                ],
            ],
        ];
    }
}
