<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - CS63 zone A4.
 * @internal
 */
class Extent2775
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [51.729748345079, 42.582570779519], [49.0333333333, 42.582570779519], [49.0333333333, 37.899973291396], [51.729748345079, 37.899973291396], [51.729748345079, 42.582570779519],
                ],
            ],
        ];
    }
}
