<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Pendleton.
 * @internal
 */
class Extent4209
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-118.17038302684, 46.017684265908], [-119.35106249888, 46.017684265908], [-119.35106249888, 45.465130738938], [-118.17038302684, 45.465130738938], [-118.17038302684, 46.017684265908],
                ],
            ],
        ];
    }
}
