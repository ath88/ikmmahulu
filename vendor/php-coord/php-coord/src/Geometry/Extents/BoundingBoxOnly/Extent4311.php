<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - Blackfeet reservation.
 * @internal
 */
class Extent4311
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-112, 49.000001907349], [-113.8330326745, 49.000001907349], [-113.8330326745, 48], [-112, 48], [-112, 49.000001907349],
                ],
            ],
        ];
    }
}
