<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Gorenjska - central.
 * @internal
 */
class Extent2422
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.602735349479, 46.441382815615], [14.019334013011, 46.441382815615], [14.019334013011, 46.148558434072], [14.602735349479, 46.148558434072], [14.602735349479, 46.441382815615],
                ],
            ],
        ];
    }
}
