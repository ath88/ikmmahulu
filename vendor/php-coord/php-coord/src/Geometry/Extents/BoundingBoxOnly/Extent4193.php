<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - west of 103.5°E onshore.
 * @internal
 */
class Extent4193
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [103.5, 22.81111908], [102.144584656, 22.81111908], [102.144584656, 20.775388270326], [103.5, 20.775388270326], [103.5, 22.81111908],
                ],
            ],
            [
                [
                    [103.5, 9.3712680083389], [103.40814953104, 9.3712680083389], [103.40814953104, 9.2042054488294], [103.5, 9.2042054488294], [103.5, 9.3712680083389],
                ],
            ],
        ];
    }
}
