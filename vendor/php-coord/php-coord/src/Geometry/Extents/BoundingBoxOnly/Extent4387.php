<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kyrgyzstan - 73°01'E to 76°01'E.
 * @internal
 */
class Extent4387
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [76.016666666667, 43.216903686524], [73.016666666667, 43.216903686524], [73.016666666667, 39.354515075684], [76.016666666667, 39.354515075684], [76.016666666667, 43.216903686524],
                ],
            ],
        ];
    }
}
