<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria.
 * @internal
 */
class Extent1026
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.986474990845, 38.797222152001], [-8.6672229766844, 38.797222152001], [-8.6672229766844, 18.976387023926], [11.986474990845, 18.976387023926], [11.986474990845, 38.797222152001],
                ],
            ],
        ];
    }
}
