<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - New South Wales.
 * @internal
 */
class Extent3139
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [153.68103981018, -28.156491789176], [140.998017, -28.156491789176], [140.998017, -37.528421759178], [153.68103981018, -37.528421759178], [153.68103981018, -28.156491789176],
                ],
            ],
        ];
    }
}
