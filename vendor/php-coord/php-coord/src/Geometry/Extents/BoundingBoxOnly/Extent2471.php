<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 36°40'N to 37°20'N; 136°E to 137°E.
 * @internal
 */
class Extent2471
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [137, 37.332866666667], [136.58257193239, 37.332866666667], [136.58257193239, 36.666166666667], [137, 36.666166666667], [137, 37.332866666667],
                ],
            ],
        ];
    }
}
