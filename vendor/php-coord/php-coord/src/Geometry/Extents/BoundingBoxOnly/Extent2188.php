<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Florida - SPCS - W.
 * @internal
 */
class Extent2188
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.133494140097, 29.599762925323], [-83.331190751966, 29.599762925323], [-83.331190751966, 26.271792473801], [-81.133494140097, 26.271792473801], [-81.133494140097, 29.599762925323],
                ],
            ],
        ];
    }
}
