<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 5 gon E.
 * @internal
 */
class Extent2850
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.167007446289, 68.573608398437], [21.343900176768, 68.573608398437], [21.343900176768, 65.245881608748], [24.167007446289, 65.245881608748], [24.167007446289, 68.573608398437],
                ],
            ],
        ];
    }
}
