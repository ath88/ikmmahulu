<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Alaska - Panhandle.
 * @internal
 */
class Extent2156
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-129.99365234375, 60.344715066076], [-140.99715600123, 60.344715066076], [-140.99715600123, 54.618135448376], [-129.99365234375, 54.618135448376], [-129.99365234375, 60.344715066076],
                ],
            ],
        ];
    }
}
