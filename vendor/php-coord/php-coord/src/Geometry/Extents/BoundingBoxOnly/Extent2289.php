<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/American Samoa - Ofu, Olesega and Ta'u islands.
 * @internal
 */
class Extent2289
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-169.38735542361, -14.118008545322], [-169.72809554498, -14.118008545322], [-169.72809554498, -14.304122921053], [-169.38735542361, -14.304122921053], [-169.38735542361, -14.118008545322],
                ],
            ],
        ];
    }
}
