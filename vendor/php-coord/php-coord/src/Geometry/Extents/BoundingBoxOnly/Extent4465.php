<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Canyon City-Burns.
 * @internal
 */
class Extent4465
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-118.52462525235, 44.350009706859], [-119.21256232778, 44.350009706859], [-119.21256232778, 43.526466686349], [-118.52462525235, 43.526466686349], [-118.52462525235, 44.350009706859],
                ],
            ],
        ];
    }
}
