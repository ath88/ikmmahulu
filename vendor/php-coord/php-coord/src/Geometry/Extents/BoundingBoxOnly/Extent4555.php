<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - 12°E to 18°E.
 * @internal
 */
class Extent4555
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18, -5.8277087211607], [12, -5.8277087211607], [12, -17.430002212524], [18, -17.430002212524], [18, -5.8277087211607],
                ],
            ],
            [
                [
                    [13.092638015747, -4.3889913558959], [12, -4.3889913558959], [12, -5.9135540811391], [13.092638015747, -5.9135540811391], [13.092638015747, -4.3889913558959],
                ],
            ],
        ];
    }
}
