<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana - SPCS27 - S.
 * @internal
 */
class Extent2212
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-104.04257965088, 46.865562438965], [-114.5637512207, 46.865562438965], [-114.5637512207, 44.353729248047], [-104.04257965088, 44.353729248047], [-104.04257965088, 46.865562438965],
                ],
            ],
        ];
    }
}
