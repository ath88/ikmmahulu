<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran.
 * @internal
 */
class Extent1123
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [63.330272674561, 39.779155731202], [44.034954071045, 39.779155731202], [44.034954071045, 23.346667138001], [63.330272674561, 23.346667138001], [63.330272674561, 39.779155731202],
                ],
            ],
        ];
    }
}
