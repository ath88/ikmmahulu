<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Korea, Republic of (South Korea) - 126°E to 128°E Jeju.
 * @internal
 */
class Extent3721
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [127.00073793969, 33.601149002781], [126.09573068346, 33.601149002781], [126.09573068346, 33.142271233754], [127.00073793969, 33.142271233754], [127.00073793969, 33.601149002781],
                ],
            ],
        ];
    }
}
