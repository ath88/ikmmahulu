<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Java Sea - offshore northwest Java.
 * @internal
 */
class Extent2577
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [110, -4.0771869857418], [105.77528924716, -4.0771869857418], [105.77528924716, -6.8809763419253], [110, -6.8809763419253], [110, -4.0771869857418],
                ],
            ],
        ];
    }
}
