<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 20°E to 22°E onshore.
 * @internal
 */
class Extent1476
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22, 32.994439798274], [20, 32.994439798274], [20, 20.544413287164], [22, 20.544413287164], [22, 32.994439798274],
                ],
            ],
        ];
    }
}
