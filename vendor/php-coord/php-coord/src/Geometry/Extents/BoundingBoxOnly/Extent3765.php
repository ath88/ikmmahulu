<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/French Guiana - coastal area west of 54°W.
 * @internal
 */
class Extent3765
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-54, 5.6894054412842], [-54.449659347534, 5.6894054412842], [-54.449659347534, 4.8412418365479], [-54, 4.8412418365479], [-54, 5.6894054412842],
                ],
            ],
        ];
    }
}
