<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Jefferson.
 * @internal
 */
class Extent4309
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-85.201476945133, 38.914014000019], [-85.683896999988, 38.914014000019], [-85.683896999988, 38.586178000002], [-85.201476945133, 38.586178000002], [-85.201476945133, 38.914014000019],
                ],
            ],
        ];
    }
}
