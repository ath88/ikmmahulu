<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - West Virginia.
 * @internal
 */
class Extent1417
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-77.727467480919, 40.637203177913], [-82.647158260325, 40.637203177913], [-82.647158260325, 37.204910015601], [-77.727467480919, 37.204910015601], [-77.727467480919, 40.637203177913],
                ],
            ],
        ];
    }
}
