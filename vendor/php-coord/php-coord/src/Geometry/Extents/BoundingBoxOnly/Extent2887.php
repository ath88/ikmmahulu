<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - Sicily Strait east of 13°E.
 * @internal
 */
class Extent2887
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.1554500861, 37.475901482663], [13.00000010579, 37.475901482663], [13.00000010579, 35.225193149], [15.1554500861, 35.225193149], [15.1554500861, 37.475901482663],
                ],
            ],
        ];
    }
}
