<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - Mabruk.
 * @internal
 */
class Extent2786
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [17.502965622242, 30.063448956096], [17.135040260359, 30.063448956096], [17.135040260359, 29.617801729011], [17.502965622242, 29.617801729011], [17.502965622242, 30.063448956096],
                ],
            ],
        ];
    }
}
