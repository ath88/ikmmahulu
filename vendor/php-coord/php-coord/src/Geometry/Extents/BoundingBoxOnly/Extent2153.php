<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - 48°W to 42°W.
 * @internal
 */
class Extent2153
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-47.743430543984, 49.177334775728], [-48, 49.177334775728], [-48, 46.467264763072], [-47.743430543984, 46.467264763072], [-47.743430543984, 49.177334775728],
                ],
            ],
        ];
    }
}
