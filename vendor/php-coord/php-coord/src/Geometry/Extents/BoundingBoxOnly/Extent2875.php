<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Azores C - S Jorge onshore.
 * @internal
 */
class Extent2875
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-27.711458753467, 38.798604417919], [-28.365556169628, 38.798604417919], [-28.365556169628, 38.489992689015], [-27.711458753467, 38.489992689015], [-27.711458753467, 38.798604417919],
                ],
            ],
        ];
    }
}
