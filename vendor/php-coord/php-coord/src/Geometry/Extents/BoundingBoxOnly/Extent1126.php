<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Israel.
 * @internal
 */
class Extent1126
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [35.681106567383, 33.524921147001], [32.990981530991, 33.524921147001], [32.990981530991, 29.453380143], [35.681106567383, 29.453380143], [35.681106567383, 33.524921147001],
                ],
            ],
        ];
    }
}
