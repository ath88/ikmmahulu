<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Slovenske Gorice.
 * @internal
 */
class Extent2878
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [16.292507171458, 46.755134582519], [15.907027544396, 46.755134582519], [15.907027544396, 46.292590521956], [16.292507171458, 46.292590521956], [16.292507171458, 46.755134582519],
                ],
            ],
        ];
    }
}
