<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia - offshore Caribbean west of 72°W.
 * @internal
 */
class Extent1603
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, 13.674840927124], [-77.366668701172, 13.674840927124], [-77.366668701172, 7.9050006866455], [-72, 7.9050006866455], [-72, 13.674840927124],
                ],
            ],
        ];
    }
}
