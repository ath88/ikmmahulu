<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Pittsburg.
 * @internal
 */
class Extent4513
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.60908, 38.738915], [-95.526136, 38.738915], [-95.526136, 36.99859], [-94.60908, 36.99859], [-94.60908, 38.738915],
                ],
            ],
        ];
    }
}
