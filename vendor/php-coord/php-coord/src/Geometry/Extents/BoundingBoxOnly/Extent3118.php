<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Grenada - main island - onshore.
 * @internal
 */
class Extent3118
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-61.547688260392, 12.285722951054], [-61.833856418667, 12.285722951054], [-61.833856418667, 11.948369791624], [-61.547688260392, 11.948369791624], [-61.547688260392, 12.285722951054],
                ],
            ],
        ];
    }
}
