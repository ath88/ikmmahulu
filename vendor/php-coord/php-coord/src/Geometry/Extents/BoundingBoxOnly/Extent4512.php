<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Coffeyville.
 * @internal
 */
class Extent4512
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.500439, 38.870054], [-96.526327, 38.870054], [-96.526327, 36.998643], [-95.500439, 36.998643], [-95.500439, 38.870054],
                ],
            ],
        ];
    }
}
