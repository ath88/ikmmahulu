<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Suriname - offshore.
 * @internal
 */
class Extent1727
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-52.664041519165, 9.348237991333], [-57.248504638672, 9.348237991333], [-57.248504638672, 5.3474025726318], [-52.664041519165, 5.3474025726318], [-52.664041519165, 9.348237991333],
                ],
            ],
        ];
    }
}
