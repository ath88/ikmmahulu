<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Yemen - South Yemen - mainland.
 * @internal
 */
class Extent1340
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.13315919636, 18.999343872071], [43.370927865134, 18.999343872071], [43.370927865134, 12.54647651247], [53.13315919636, 12.54647651247], [53.13315919636, 18.999343872071],
                ],
            ],
        ];
    }
}
