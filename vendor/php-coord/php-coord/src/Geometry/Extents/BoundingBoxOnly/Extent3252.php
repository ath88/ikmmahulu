<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Ghana - onshore.
 * @internal
 */
class Extent3252
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [1.2227986799401, 11.1556930542], [-3.2488892078399, 11.1556930542], [-3.2488892078399, 4.6775416850426], [1.2227986799401, 4.6775416850426], [1.2227986799401, 11.1556930542],
                ],
            ],
        ];
    }
}
