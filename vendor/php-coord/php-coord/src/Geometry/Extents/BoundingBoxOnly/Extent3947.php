<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - 120°E to 126°E.
 * @internal
 */
class Extent3947
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [126, 53.553741455078], [120, 53.553741455078], [120, 24.645543268924], [126, 24.645543268924], [126, 53.553741455078],
                ],
            ],
        ];
    }
}
