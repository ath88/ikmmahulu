<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore blocks 2  3 17-18 and 31-33.
 * @internal
 */
class Extent2324
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.830107761304, -6.0106854964942], [10.413694255881, -6.0106854964942], [10.413694255881, -8.5845912001739], [12.830107761304, -8.5845912001739], [12.830107761304, -6.0106854964942],
                ],
            ],
        ];
    }
}
