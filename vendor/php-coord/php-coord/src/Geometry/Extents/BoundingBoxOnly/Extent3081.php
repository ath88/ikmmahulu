<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Antarctic/Antarctica - Transantarctic mountains north of 80°S.
 * @internal
 */
class Extent3081
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [174.00426916834, -68.607560626954], [149.83665920844, -68.607560626954], [149.83665920844, -80], [174.00426916834, -80], [174.00426916834, -68.607560626954],
                ],
            ],
        ];
    }
}
