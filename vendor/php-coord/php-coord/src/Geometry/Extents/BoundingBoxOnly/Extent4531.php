<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Costa Rica - offshore Caribbean.
 * @internal
 */
class Extent4531
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-81.433332576, 11.767564127128], [-83.599835526476, 11.767564127128], [-83.599835526476, 9.606727491913], [-81.433332576, 9.606727491913], [-81.433332576, 11.767564127128],
                ],
            ],
        ];
    }
}
