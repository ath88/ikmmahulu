<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Ochoco Summit.
 * @internal
 */
class Extent4481
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-120.31281453942, 44.603741684393], [-121.00372306507, 44.603741684393], [-121.00372306507, 44.218866116764], [-120.31281453942, 44.218866116764], [-120.31281453942, 44.603741684393],
                ],
            ],
        ];
    }
}
