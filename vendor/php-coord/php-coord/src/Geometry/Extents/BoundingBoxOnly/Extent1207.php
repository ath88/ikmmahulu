<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Senegal.
 * @internal
 */
class Extent1207
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-11.369931221008, 16.690622329712], [-20.211138519, 16.690622329712], [-20.211138519, 10.644304126], [-11.369931221008, 10.644304126], [-11.369931221008, 16.690622329712],
                ],
            ],
        ];
    }
}
