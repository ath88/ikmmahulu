<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Bosnia and Herzegovina.
 * @internal
 */
class Extent1050
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.619785308838, 45.26594543457], [15.740589141846, 45.26594543457], [15.740589141846, 42.565818590643], [19.619785308838, 42.565818590643], [19.619785308838, 45.26594543457],
                ],
            ],
        ];
    }
}
