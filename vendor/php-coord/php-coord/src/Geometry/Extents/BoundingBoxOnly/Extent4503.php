<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Atchison.
 * @internal
 */
class Extent4503
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.859035, 40.001541], [-96.806742, 40.001541], [-96.806742, 39.216081], [-94.859035, 39.216081], [-94.859035, 40.001541],
                ],
            ],
        ];
    }
}
