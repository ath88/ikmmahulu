<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - west of 78°E.
 * @internal
 */
class Extent1587
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [78.000000000784, 41.069671630859], [73.62004852295, 41.069671630859], [73.62004852295, 35.427589416504], [78.000000000784, 35.427589416504], [78.000000000784, 41.069671630859],
                ],
            ],
        ];
    }
}
