<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia.
 * @internal
 */
class Extent1174
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [174.275709662, -14.834166898], [156.255709646, -14.834166898], [156.255709646, -26.441631911354], [174.275709662, -26.441631911354], [174.275709662, -14.834166898],
                ],
            ],
        ];
    }
}
