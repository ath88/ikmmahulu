<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia - east of 24°E.
 * @internal
 */
class Extent1845
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25.264430999756, -17.473472595215], [24.000000148309, -17.473472595215], [24.000000148309, -18.170687781051], [25.264430999756, -18.170687781051], [25.264430999756, -17.473472595215],
                ],
            ],
        ];
    }
}
