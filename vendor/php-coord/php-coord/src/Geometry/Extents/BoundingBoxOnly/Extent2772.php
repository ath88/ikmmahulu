<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - CS63 zone A1.
 * @internal
 */
class Extent2772
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [43.0333333333, 43.584716796875], [39.995952861748, 43.584716796875], [39.995952861748, 41.379343273907], [43.0333333333, 41.379343273907], [43.0333333333, 43.584716796875],
                ],
            ],
        ];
    }
}
