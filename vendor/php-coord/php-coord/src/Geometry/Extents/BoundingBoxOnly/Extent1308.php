<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Bangladesh; India; Myanmar; Pakistan  onshore.
 * @internal
 */
class Extent1308
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [74.379821158986, 14.069828368764], [74.273789070507, 14.069828368764], [74.273789070507, 13.96184220161], [74.379821158986, 13.96184220161], [74.379821158986, 14.069828368764],
                ],
            ],
            [
                [
                    [91.622152239155, 22.672392204355], [91.348913527782, 22.672392204355], [91.348913527782, 22.295853673039], [91.622152239155, 22.295853673039], [91.622152239155, 22.672392204355],
                ],
            ],
            [
                [
                    [101.16942596436, 37.060787200928], [60.866302490235, 37.060787200928], [60.866302490235, 8.027118347545], [101.16942596436, 8.027118347545], [101.16942596436, 37.060787200928],
                ],
            ],
        ];
    }
}
