<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - west of 12°E.
 * @internal
 */
class Extent4551
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, -5.0359577165266], [8.2018785080004, -5.0359577165266], [8.2018785080004, -17.270278930664], [12, -17.270278930664], [12, -5.0359577165266],
                ],
            ],
        ];
    }
}
