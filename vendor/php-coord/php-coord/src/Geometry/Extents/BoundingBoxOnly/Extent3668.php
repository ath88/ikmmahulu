<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 24ºE to 25ºE.
 * @internal
 */
class Extent3668
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25, 71.15415093977], [24, 71.15415093977], [24, 68.580810546875], [25, 68.580810546875], [25, 71.15415093977],
                ],
            ],
        ];
    }
}
