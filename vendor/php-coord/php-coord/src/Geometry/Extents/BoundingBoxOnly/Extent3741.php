<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Thailand - onshore and Gulf of Thailand.
 * @internal
 */
class Extent3741
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [99.752402082145, 6.7531645343201], [99.546520214606, 6.7531645343201], [99.546520214606, 6.4692233868241], [99.752402082145, 6.4692233868241], [99.752402082145, 6.7531645343201],
                ],
            ],
            [
                [
                    [105.63928985596, 20.454578399658], [97.347274780274, 20.454578399658], [97.347274780274, 5.6334710121156], [105.63928985596, 5.6334710121156], [105.63928985596, 20.454578399658],
                ],
            ],
        ];
    }
}
