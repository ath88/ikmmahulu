<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - 114°E to 120°E onshore.
 * @internal
 */
class Extent1594
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [120, 51.51975900214], [114, 51.51975900214], [114, 22.14452915736], [120, 22.14452915736], [120, 51.51975900214],
                ],
            ],
        ];
    }
}
