<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Italy - 12°E to 18°E.
 * @internal
 */
class Extent4186
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18, 47.094581604004], [12, 47.094581604004], [12, 34.794724878393], [18, 34.794724878393], [18, 47.094581604004],
                ],
            ],
        ];
    }
}
