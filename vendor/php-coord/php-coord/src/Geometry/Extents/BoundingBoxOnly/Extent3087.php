<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Colombia region 6.
 * @internal
 */
class Extent3087
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.39999961853, 5.0000019073486], [-77.670709609985, 5.0000019073486], [-77.670709609985, 3.0000019073486], [-74.39999961853, 3.0000019073486], [-74.39999961853, 5.0000019073486],
                ],
            ],
        ];
    }
}
