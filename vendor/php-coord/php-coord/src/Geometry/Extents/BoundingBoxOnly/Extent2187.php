<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Florida - SPCS - N.
 * @internal
 */
class Extent2187
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.043667181311, 31.003157231218], [-87.625711591394, 31.003157231218], [-87.625711591394, 29.213020594273], [-82.043667181311, 29.213020594273], [-82.043667181311, 31.003157231218],
                ],
            ],
        ];
    }
}
