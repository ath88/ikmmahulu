<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - west of 72°W.
 * @internal
 */
class Extent1693
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, 11.614625930786], [-73.378067016602, 11.614625930786], [-73.378067016602, 7.0219554901123], [-72, 7.0219554901123], [-72, 11.614625930786],
                ],
            ],
        ];
    }
}
