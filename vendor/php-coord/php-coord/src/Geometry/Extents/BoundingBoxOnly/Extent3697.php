<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Paraiba-Pernambuco.
 * @internal
 */
class Extent3697
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-29.136610031128, -4.6068058013916], [-35.090944290161, -4.6068058013916], [-35.090944290161, -10.163892745972], [-29.136610031128, -10.163892745972], [-29.136610031128, -4.6068058013916],
                ],
            ],
        ];
    }
}
