<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Bluff vcrs.
 * @internal
 */
class Extent3801
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [168.85813534432, -46.269614209287], [168.01652908259, -46.269614209287], [168.01652908259, -46.706718049203], [168.85813534432, -46.706718049203], [168.85813534432, -46.269614209287],
                ],
            ],
        ];
    }
}
