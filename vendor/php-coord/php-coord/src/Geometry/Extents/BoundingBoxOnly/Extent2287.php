<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - SE Australia (ACT NSW Vic).
 * @internal
 */
class Extent2287
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [153.68103981018, -28.156491789176], [140.960037, -28.156491789176], [140.960037, -39.197008132935], [153.68103981018, -39.197008132935], [153.68103981018, -28.156491789176],
                ],
            ],
        ];
    }
}
