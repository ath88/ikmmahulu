<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/St Helena - Ascension Island.
 * @internal
 */
class Extent3182
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-14.24841917806, -7.8388927530303], [-14.457532031877, -7.8388927530303], [-14.457532031877, -8.0273800177485], [-14.24841917806, -8.0273800177485], [-14.24841917806, -7.8388927530303],
                ],
            ],
        ];
    }
}
