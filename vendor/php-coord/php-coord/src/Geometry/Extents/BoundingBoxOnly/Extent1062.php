<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Cape Verde.
 * @internal
 */
class Extent1062
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-19.534999518, 20.537242135], [-28.849406527, 20.537242135], [-28.849406527, 11.475052126], [-19.534999518, 11.475052126], [-19.534999518, 20.537242135],
                ],
            ],
        ];
    }
}
