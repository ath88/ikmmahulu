<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Croatia.
 * @internal
 */
class Extent1076
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.424997329712, 46.535827636719], [13.008333512, 46.535827636719], [13.008333512, 41.622009155], [19.424997329712, 41.622009155], [19.424997329712, 46.535827636719],
                ],
            ],
        ];
    }
}
