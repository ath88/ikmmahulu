<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Pacific - Guam and NMI east of 144°E.
 * @internal
 */
class Extent4518
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [149.544455639, 23.893055138], [144, 23.893055138], [144, 11.051447924642], [149.544455639, 11.051447924642], [149.544455639, 23.893055138],
                ],
            ],
        ];
    }
}
