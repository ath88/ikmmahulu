<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - 31.4°N to 33°N, east of 46.1°E (map 17).
 * @internal
 */
class Extent3704
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [47.864440918007, 32.983804431108], [46.051438457888, 32.983804431108], [46.051438457888, 31.336165917998], [47.864440918007, 31.336165917998], [47.864440918007, 32.983804431108],
                ],
            ],
        ];
    }
}
