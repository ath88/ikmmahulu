<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Latin America - 114°W to 108°W.
 * @internal
 */
class Extent3756
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-108, 32.261178970337], [-114, 32.261178970337], [-114, 15.095598220825], [-108, 15.095598220825], [-108, 32.261178970337],
                ],
            ],
        ];
    }
}
