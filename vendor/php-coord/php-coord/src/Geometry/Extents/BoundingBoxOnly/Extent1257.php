<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Yemen.
 * @internal
 */
class Extent1257
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [57.956546554001, 18.999343872071], [41.082032538, 18.999343872071], [41.082032538, 8.9577171240002], [57.956546554001, 8.9577171240002], [57.956546554001, 18.999343872071],
                ],
            ],
        ];
    }
}
