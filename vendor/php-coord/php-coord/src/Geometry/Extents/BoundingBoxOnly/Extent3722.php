<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - 33°N to 34.6°N, west of 40.1°E (map 8).
 * @internal
 */
class Extent3722
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [40.082661009879, 33.986040386762], [38.794700622559, 33.986040386762], [38.794700622559, 32.983804431108], [40.082661009879, 32.983804431108], [40.082661009879, 33.986040386762],
                ],
            ],
        ];
    }
}
