<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Yukon.
 * @internal
 */
class Extent2417
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-123.91089630127, 69.697164513875], [-141.00285698924, 69.697164513875], [-141.00285698924, 59.996089212317], [-123.91089630127, 59.996089212317], [-123.91089630127, 69.697164513875],
                ],
            ],
        ];
    }
}
