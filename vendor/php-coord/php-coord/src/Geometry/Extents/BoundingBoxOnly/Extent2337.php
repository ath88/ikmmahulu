<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - mainland northwest.
 * @internal
 */
class Extent2337
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-4.5, 43.813798904419], [-9.3611106872559, 43.813798904419], [-9.3611106872559, 41.5], [-4.5, 41.5], [-4.5, 43.813798904419],
                ],
            ],
        ];
    }
}
