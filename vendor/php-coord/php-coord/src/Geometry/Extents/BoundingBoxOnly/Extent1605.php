<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore block 15.
 * @internal
 */
class Extent1605
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [11.663714, -6.033161], [10.830367, -6.033161], [10.830367, -6.584779], [11.663714, -6.584779], [11.663714, -6.033161],
                ],
            ],
        ];
    }
}
