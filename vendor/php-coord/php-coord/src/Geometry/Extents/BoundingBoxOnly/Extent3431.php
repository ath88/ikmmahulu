<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - west of 162°E.
 * @internal
 */
class Extent3431
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [162, -15.348633813428], [156.255709646, -15.348633813428], [156.255709646, -26.02806744592], [162, -26.02806744592], [162, -15.348633813428],
                ],
            ],
        ];
    }
}
