<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Iowa - Cedar Rapids.
 * @internal
 */
class Extent4240
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.89802, 42.298947], [-92.300137, 42.298947], [-92.300137, 41.423144], [-90.89802, 41.423144], [-90.89802, 42.298947],
                ],
            ],
        ];
    }
}
