<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/El Salvador.
 * @internal
 */
class Extent1087
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.65073858188, 14.431982040517], [-91.424937585, 14.431982040517], [-91.424937585, 9.9729261247608], [-87.65073858188, 9.9729261247608], [-87.65073858188, 14.431982040517],
                ],
            ],
        ];
    }
}
