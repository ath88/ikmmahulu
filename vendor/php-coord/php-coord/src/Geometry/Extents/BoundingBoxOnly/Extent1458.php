<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - 22°E to 24°E.
 * @internal
 */
class Extent1458
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.000001011164, -25.264167785645], [22.000000268145, -25.264167785645], [22.000000268145, -34.25869797487], [24.000001011164, -34.25869797487], [24.000001011164, -25.264167785645],
                ],
            ],
        ];
    }
}
