<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Trempealeau.
 * @internal
 */
class Extent4357
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-91.151988283952, 44.597040599658], [-91.612842045469, 44.597040599658], [-91.612842045469, 43.984180436731], [-91.151988283952, 43.984180436731], [-91.151988283952, 44.597040599658],
                ],
            ],
        ];
    }
}
