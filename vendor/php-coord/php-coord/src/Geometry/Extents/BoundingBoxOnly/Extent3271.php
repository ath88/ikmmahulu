<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - onshore.
 * @internal
 */
class Extent3271
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25.207877041493, 33.220332548245], [9.3113880157473, 33.220332548245], [9.3113880157473, 19.5], [25.207877041493, 19.5], [25.207877041493, 33.220332548245],
                ],
            ],
        ];
    }
}
