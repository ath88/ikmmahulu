<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Greenland.
 * @internal
 */
class Extent1107
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.9884162935953, 87.023943197], [-74.998683569945, 87.023943197], [-74.998683569945, 56.383177168], [7.9884162935953, 56.383177168], [7.9884162935953, 87.023943197],
                ],
            ],
        ];
    }
}
