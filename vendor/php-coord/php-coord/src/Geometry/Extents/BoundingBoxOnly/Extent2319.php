<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore block 7.
 * @internal
 */
class Extent2319
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.384290727214, -9.4161321848239], [12.663704353803, -9.4161321848239], [12.663704353803, -10.084460928307], [13.384290727214, -10.084460928307], [13.384290727214, -9.4161321848239],
                ],
            ],
        ];
    }
}
