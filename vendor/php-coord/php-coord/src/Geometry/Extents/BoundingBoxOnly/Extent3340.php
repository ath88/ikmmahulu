<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mayotte - onshore.
 * @internal
 */
class Extent3340
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [45.343345705972, -12.614548874985], [44.989173601337, -12.614548874985], [44.989173601337, -13.04043930249], [45.343345705972, -13.04043930249], [45.343345705972, -12.614548874985],
                ],
            ],
        ];
    }
}
