<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Zasavje.
 * @internal
 */
class Extent3354
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.341925781214, 46.319309785364], [14.843540900232, 46.319309785364], [14.843540900232, 46.005751118311], [15.341925781214, 46.005751118311], [15.341925781214, 46.319309785364],
                ],
            ],
        ];
    }
}
