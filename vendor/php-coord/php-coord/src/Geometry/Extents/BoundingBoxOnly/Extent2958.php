<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Maine - CS2000 - W.
 * @internal
 */
class Extent2958
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-69.604999408283, 46.571710103114], [-71.08750924735, 46.571710103114], [-71.08750924735, 43.073558191019], [-69.604999408283, 43.073558191019], [-69.604999408283, 46.571710103114],
                ],
            ],
        ];
    }
}
