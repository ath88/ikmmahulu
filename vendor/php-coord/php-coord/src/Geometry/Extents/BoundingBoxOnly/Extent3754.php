<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Trashigang district.
 * @internal
 */
class Extent3754
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [92.124755859375, 27.486255645752], [91.378343048156, 27.486255645752], [91.378343048156, 27.015105958685], [92.124755859375, 27.015105958685], [92.124755859375, 27.486255645752],
                ],
            ],
        ];
    }
}
