<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iraq - 31.4°N to 33°N, 43.9°E to 46.1°E (map 16).
 * @internal
 */
class Extent3695
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [46.070261288128, 32.9883912678], [43.929738711872, 32.9883912678], [43.929738711872, 31.360201796158], [46.070261288128, 31.360201796158], [46.070261288128, 32.9883912678],
                ],
            ],
        ];
    }
}
