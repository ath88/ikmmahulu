<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Cocos (Keeling) Islands - onshore.
 * @internal
 */
class Extent1069
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [96.982953879628, -12.018628528105], [96.762728895322, -12.018628528105], [96.762728895322, -12.261085190643], [96.982953879628, -12.261085190643], [96.982953879628, -12.018628528105],
                ],
            ],
            [
                [
                    [96.88342592159, -11.766347987706], [96.762967696579, -11.766347987706], [96.762967696579, -11.892507621708], [96.88342592159, -11.892507621708], [96.88342592159, -11.766347987706],
                ],
            ],
        ];
    }
}
