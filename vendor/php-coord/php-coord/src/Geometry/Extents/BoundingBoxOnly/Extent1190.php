<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Philippines.
 * @internal
 */
class Extent1190
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [129.943936621, 22.178960136], [116.041076608, 22.178960136], [116.041076608, 3.0003331190003], [129.943936621, 3.0003331190003], [129.943936621, 22.178960136],
                ],
            ],
        ];
    }
}
