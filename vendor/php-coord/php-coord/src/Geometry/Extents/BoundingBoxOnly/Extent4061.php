<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Arctic/Arctic - 81°10'N to 76°10'N,  98°E to 129°E.
 * @internal
 */
class Extent4061
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [129.00000190735, 81.166666030884], [98.000001907349, 81.166666030884], [98.000001907349, 76.166666030884], [129.00000190735, 76.166666030884], [129.00000190735, 81.166666030884],
                ],
            ],
        ];
    }
}
