<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 123°E to 126°E onshore.
 * @internal
 */
class Extent3519
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [125.70686087398, 3.8362147918638], [125.36034870187, 3.8362147918638], [125.36034870187, 3.2855103482542], [125.70686087398, 3.2855103482542], [125.70686087398, 3.8362147918638],
                ],
            ],
            [
                [
                    [126, -1.5852352474338], [124.27832672195, -1.5852352474338], [124.27832672195, -2.5139355140658], [126, -2.5139355140658], [126, -1.5852352474338],
                ],
            ],
            [
                [
                    [126, -3.1077262336634], [125.96009398022, -3.1077262336634], [125.96009398022, -3.4473994354142], [126, -3.4473994354142], [126, -3.1077262336634],
                ],
            ],
            [
                [
                    [126, -7.6085158128499], [125.7225463505, -7.6085158128499], [125.7225463505, -8.0705037067803], [126, -8.0705037067803], [126, -7.6085158128499],
                ],
            ],
            [
                [
                    [125.16590028421, -8.8959098278098], [123, -8.8959098278098], [123, -10.915349671423], [125.16590028421, -10.915349671423], [125.16590028421, -8.8959098278098],
                ],
            ],
            [
                [
                    [125.1894546108, -8.0827569433625], [123, -8.0827569433625], [123, -8.6469282806282], [125.1894546108, -8.6469282806282], [125.1894546108, -8.0827569433625],
                ],
            ],
            [
                [
                    [123.26598864264, -4.3305846617382], [123, -4.3305846617382], [123, -5.4763599707881], [123.26598864264, -5.4763599707881], [123.26598864264, -4.3305846617382],
                ],
            ],
            [
                [
                    [123.306962108, -3.9272484571432], [123, -3.9272484571432], [123, -4.2926600444646], [123.306962108, -4.2926600444646], [123.306962108, -3.9272484571432],
                ],
            ],
            [
                [
                    [123.60239141636, -1.0968326843324], [123, -1.0968326843324], [123, -1.6740033122136], [123.60239141636, -1.6740033122136], [123.60239141636, -1.0968326843324],
                ],
            ],
            [
                [
                    [123.50343470003, -0.50976102787956], [123, -0.50976102787956], [123, -1.1072007490528], [123.50343470003, -1.1072007490528], [123.50343470003, -0.50976102787956],
                ],
            ],
            [
                [
                    [125.29719874447, 1.750055182589], [123, 1.750055182589], [123, 0.23111177350825], [125.29719874447, 0.23111177350825], [125.29719874447, 1.750055182589],
                ],
            ],
            [
                [
                    [123.68608737503, -5.2005811669113], [123.47056546923, -5.2005811669113], [123.47056546923, -5.426660991189], [123.68608737503, -5.426660991189], [123.68608737503, -5.2005811669113],
                ],
            ],
            [
                [
                    [125.49840077452, 2.8570120978077], [125.30664102891, 2.8570120978077], [125.30664102891, 2.5765966148084], [125.49840077452, 2.5765966148084], [125.49840077452, 2.8570120978077],
                ],
            ],
        ];
    }
}
