<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Monroe.
 * @internal
 */
class Extent4373
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.31175644982, 44.161048012088], [-90.978394232913, 44.161048012088], [-90.978394232913, 43.725404378683], [-90.31175644982, 43.725404378683], [-90.31175644982, 44.161048012088],
                ],
            ],
        ];
    }
}
