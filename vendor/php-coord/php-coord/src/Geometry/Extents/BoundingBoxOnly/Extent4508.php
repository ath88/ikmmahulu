<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Larned.
 * @internal
 */
class Extent4508
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-98.91236, 38.697065], [-99.586604, 38.697065], [-99.586604, 36.999221], [-98.91236, 36.999221], [-98.91236, 38.697065],
                ],
            ],
        ];
    }
}
