<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Ireland - Corrib and Errigal.
 * @internal
 */
class Extent2961
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-9.499920896048, 55.750020340619], [-12.5, 55.750020340619], [-12.5, 53.75], [-9.499920896048, 53.75], [-9.499920896048, 55.750020340619],
                ],
            ],
        ];
    }
}
