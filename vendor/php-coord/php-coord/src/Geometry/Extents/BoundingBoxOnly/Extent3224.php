<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Bulgaria - onshore.
 * @internal
 */
class Extent3224
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [28.673716794504, 44.224716186523], [22.36527633667, 44.224716186523], [22.36527633667, 41.243049621582], [28.673716794504, 41.243049621582], [28.673716794504, 44.224716186523],
                ],
            ],
        ];
    }
}
