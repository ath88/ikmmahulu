<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Grants Pass-Ashland.
 * @internal
 */
class Extent4198
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-122.37902459372, 42.845915472643], [-123.94496864868, 42.845915472643], [-123.94496864868, 41.883358942837], [-122.37902459372, 41.883358942837], [-122.37902459372, 42.845915472643],
                ],
            ],
        ];
    }
}
