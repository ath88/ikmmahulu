<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/French Guiana - coastal area east of 54°W.
 * @internal
 */
class Extent3766
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-51.616199493408, 5.8056468963623], [-54, 5.8056468963623], [-54, 3.4336261749268], [-51.616199493408, 3.4336261749268], [-51.616199493408, 5.8056468963623],
                ],
            ],
        ];
    }
}
