<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Canyonville-Grants Pass.
 * @internal
 */
class Extent4199
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-122.43743512167, 43.23969161825], [-123.8257680356, 43.23969161825], [-123.8257680356, 42.496581017494], [-122.43743512167, 42.496581017494], [-122.43743512167, 43.23969161825],
                ],
            ],
        ];
    }
}
