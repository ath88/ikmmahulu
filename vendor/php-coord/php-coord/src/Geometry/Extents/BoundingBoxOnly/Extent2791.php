<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - Burkina Faso and SW Niger.
 * @internal
 */
class Extent2791
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [3.9952177206365, 14.224077146893], [-4.6391343791723, 14.224077146893], [-4.6391343791723, 11.834488500899], [3.9952177206365, 11.834488500899], [3.9952177206365, 14.224077146893],
                ],
            ],
        ];
    }
}
