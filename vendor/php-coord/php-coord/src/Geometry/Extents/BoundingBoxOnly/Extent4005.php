<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - Kalimantan W - coastal.
 * @internal
 */
class Extent4005
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [109.7755012309, 2.1222880425104], [108.79726372584, 2.1222880425104], [108.79726372584, 0.061219584083062], [109.7755012309, 0.061219584083062], [109.7755012309, 2.1222880425104],
                ],
            ],
        ];
    }
}
