<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Guyana - onshore.
 * @internal
 */
class Extent3259
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-56.470672607422, 8.570348739624], [-61.389724731445, 8.570348739624], [-61.389724731445, 1.1868762969971], [-56.470672607422, 1.1868762969971], [-56.470672607422, 8.570348739624],
                ],
            ],
        ];
    }
}
