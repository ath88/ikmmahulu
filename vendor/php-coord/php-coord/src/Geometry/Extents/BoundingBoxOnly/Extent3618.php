<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 17°E and 19°E.
 * @internal
 */
class Extent3618
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19, -3.4300644702517], [17, -3.4300644702517], [17, -8.1077785491942], [19, -8.1077785491942], [19, -3.4300644702517],
                ],
            ],
        ];
    }
}
