<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 15 45.
 * @internal
 */
class Extent2839
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [17.001392479635, 65.123961106714], [13.668262481689, 65.123961106714], [13.668262481689, 60.447059074921], [17.001392479635, 60.447059074921], [17.001392479635, 65.123961106714],
                ],
            ],
        ];
    }
}
