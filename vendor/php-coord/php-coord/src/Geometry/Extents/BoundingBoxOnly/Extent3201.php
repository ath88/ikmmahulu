<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Johnston Island.
 * @internal
 */
class Extent3201
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-169.47392518413, 16.780238150619], [-169.58894102681, 16.780238150619], [-169.58894102681, 16.674158643604], [-169.47392518413, 16.674158643604], [-169.47392518413, 16.780238150619],
                ],
            ],
        ];
    }
}
