<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Dubois and Martin.
 * @internal
 */
class Extent4271
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.678991999993, 38.904696999996], [-87.073270999977, 38.904696999996], [-87.073270999977, 38.20303100001], [-86.678991999993, 38.20303100001], [-86.678991999993, 38.904696999996],
                ],
            ],
        ];
    }
}
