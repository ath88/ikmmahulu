<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - MTM zone 13.
 * @internal
 */
class Extent1434
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-82.5, 55.58917154543], [-85.5, 55.58917154543], [-85.5, 46], [-82.5, 46], [-82.5, 55.58917154543],
                ],
            ],
        ];
    }
}
