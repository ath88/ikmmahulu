<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 37°20'N to 38°N; 140°E to 141°E.
 * @internal
 */
class Extent2469
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141, 37.999566666667], [140, 37.999566666667], [140, 37.332866666667], [141, 37.332866666667], [141, 37.999566666667],
                ],
            ],
        ];
    }
}
