<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Egypt - Western Desert.
 * @internal
 */
class Extent2595
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [30, 31.670600401384], [24.706804275513, 31.670600401384], [24.706804275513, 25.712493202945], [30, 25.712493202945], [30, 31.670600401384],
                ],
            ],
        ];
    }
}
