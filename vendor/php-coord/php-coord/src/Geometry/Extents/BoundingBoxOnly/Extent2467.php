<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 37°20'N to 38°N; 138°E to 139°E.
 * @internal
 */
class Extent2467
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [139, 37.96238294026], [138.39153389271, 37.96238294026], [138.39153389271, 37.332866666667], [139, 37.332866666667], [139, 37.96238294026],
                ],
            ],
        ];
    }
}
