<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - zone XIII.
 * @internal
 */
class Extent1866
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [145.86544719357, 44.392852845077], [142.61573749385, 44.392852845077], [142.61573749385, 41.875806260696], [145.86544719357, 41.875806260696], [145.86544719357, 44.392852845077],
                ],
            ],
        ];
    }
}
