<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Distrito Federal.
 * @internal
 */
class Extent3619
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-47.102388381958, -15.376224517822], [-48.090015411377, -15.376224517822], [-48.090015411377, -15.935964584351], [-47.102388381958, -15.935964584351], [-47.102388381958, -15.376224517822],
                ],
            ],
        ];
    }
}
