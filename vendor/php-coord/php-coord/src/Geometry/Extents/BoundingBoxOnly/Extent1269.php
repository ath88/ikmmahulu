<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Pedregal area of Falcon state.
 * @internal
 */
class Extent1269
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-69.699998855591, 11.250001907349], [-70.39999961853, 11.250001907349], [-70.39999961853, 10.800001144409], [-69.699998855591, 10.800001144409], [-69.699998855591, 11.250001907349],
                ],
            ],
        ];
    }
}
