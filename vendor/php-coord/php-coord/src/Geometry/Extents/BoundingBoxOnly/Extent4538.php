<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Vietnam - Son La.
 * @internal
 */
class Extent4538
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [105.025299072, 22.030763626], [103.213127136, 22.030763626], [103.213127136, 20.574827194], [105.025299072, 20.574827194], [105.025299072, 22.030763626],
                ],
            ],
        ];
    }
}
