<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - OML 58.
 * @internal
 */
class Extent3113
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [6.8327245491084, 5.3539704559659], [6.5396551599816, 5.3539704559659], [6.5396551599816, 5.0510067330454], [6.8327245491084, 5.0510067330454], [6.8327245491084, 5.3539704559659],
                ],
            ],
        ];
    }
}
