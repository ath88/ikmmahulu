<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Nagaland.
 * @internal
 */
class Extent4412
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [95.244918823242, 27.042949676514], [93.331741333008, 27.042949676514], [93.331741333008, 25.20205116272], [95.244918823242, 25.20205116272], [95.244918823242, 27.042949676514],
                ],
            ],
        ];
    }
}
