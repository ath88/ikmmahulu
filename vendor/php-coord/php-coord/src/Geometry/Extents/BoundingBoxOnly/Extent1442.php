<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Ontario - east of 78°W.
 * @internal
 */
class Extent1442
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.358075474967, 46.249781778414], [-78, 46.249781778414], [-78, 43.633327484131], [-74.358075474967, 43.633327484131], [-74.358075474967, 46.249781778414],
                ],
            ],
        ];
    }
}
