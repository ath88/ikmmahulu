<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Qatar.
 * @internal
 */
class Extent1195
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.034721549, 27.043114141], [50.557395547, 27.043114141], [50.557395547, 24.556041717529], [53.034721549, 24.556041717529], [53.034721549, 27.043114141],
                ],
            ],
        ];
    }
}
