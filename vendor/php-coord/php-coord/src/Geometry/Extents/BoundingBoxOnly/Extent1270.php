<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela - Maracaibo south.
 * @internal
 */
class Extent1270
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-70.787704467773, 10.000001907349], [-72.39079284668, 10.000001907349], [-72.39079284668, 8.7208919525146], [-70.787704467773, 8.7208919525146], [-70.787704467773, 10.000001907349],
                ],
            ],
        ];
    }
}
