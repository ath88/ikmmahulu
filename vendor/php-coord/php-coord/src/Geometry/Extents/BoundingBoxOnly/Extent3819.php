<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - offshore blocks OPL 215 and 221.
 * @internal
 */
class Extent3819
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.3046200662327, 3.8340749966044], [6.952082064829, 3.8340749966044], [6.952082064829, 3.2525500211446], [7.3046200662327, 3.2525500211446], [7.3046200662327, 3.8340749966044],
                ],
            ],
            [
                [
                    [5.5840150976129, 4.2239779120394], [5.0281798936906, 4.2239779120394], [5.0281798936906, 3.7403747621366], [5.5840150976129, 3.7403747621366], [5.5840150976129, 4.2239779120394],
                ],
            ],
        ];
    }
}
