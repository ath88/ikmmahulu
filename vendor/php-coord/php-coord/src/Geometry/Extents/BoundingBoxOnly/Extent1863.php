<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - zone X.
 * @internal
 */
class Extent1863
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [139.549729, 38.546421630114], [139.54957720984, 38.546421630114], [139.54957720984, 38.545166525504], [139.549729, 38.545166525504], [139.549729, 38.546421630114],
                ],
            ],
            [
                [
                    [142.13432077242, 41.57864317941], [139.49506013511, 41.57864317941], [139.49506013511, 37.730942], [142.13432077242, 37.730942], [142.13432077242, 41.57864317941],
                ],
            ],
        ];
    }
}
