<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Cayman Islands.
 * @internal
 */
class Extent1063
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78.723584573, 20.672444135], [-83.597221577971, 20.672444135], [-83.597221577971, 17.584084132039], [-78.723584573, 17.584084132039], [-78.723584573, 20.672444135],
                ],
            ],
        ];
    }
}
