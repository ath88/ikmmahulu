<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Botswana.
 * @internal
 */
class Extent1051
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [29.373622894287, -17.782085418701], [19.996109008789, -17.782085418701], [19.996109008789, -26.875556945801], [29.373622894287, -26.875556945801], [29.373622894287, -17.782085418701],
                ],
            ],
        ];
    }
}
