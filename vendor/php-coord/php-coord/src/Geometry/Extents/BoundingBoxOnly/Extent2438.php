<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 42°40'N to 43°25'N; 140°E to 141°E.
 * @internal
 */
class Extent2438
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [141, 43.41497487312], [140, 43.41497487312], [140, 42.666466666667], [141, 42.666466666667], [141, 43.41497487312],
                ],
            ],
        ];
    }
}
