<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mauritius - mainland.
 * @internal
 */
class Extent3209
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [57.846805500473, -19.940823166004], [57.254888546292, -19.940823166004], [57.254888546292, -20.566127493389], [57.846805500473, -20.566127493389], [57.846805500473, -19.940823166004],
                ],
            ],
        ];
    }
}
