<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Dolenjska.
 * @internal
 */
class Extent3568
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [15.724443435669, 46.115829467774], [14.474858183003, 46.115829467774], [14.474858183003, 45.700546264648], [15.724443435669, 45.700546264648], [15.724443435669, 46.115829467774],
                ],
            ],
        ];
    }
}
