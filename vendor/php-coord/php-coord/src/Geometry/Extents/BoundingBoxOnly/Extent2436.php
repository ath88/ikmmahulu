<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - 43°20'N to 44°N; 144°E to 145°E.
 * @internal
 */
class Extent2436
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [145, 43.999866666667], [144, 43.999866666667], [144, 43.333166666667], [145, 43.333166666667], [145, 43.999866666667],
                ],
            ],
        ];
    }
}
