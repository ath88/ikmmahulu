<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - onshore.
 * @internal
 */
class Extent3309
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [32.942251205444, -22.136390686035], [16.451303482056, -22.136390686035], [16.451303482056, -34.872043609619], [32.942251205444, -34.872043609619], [32.942251205444, -22.136390686035],
                ],
            ],
        ];
    }
}
