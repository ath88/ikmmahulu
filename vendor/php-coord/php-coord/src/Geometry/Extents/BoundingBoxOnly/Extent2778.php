<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Asia - FSU - CS63 zone K4.
 * @internal
 */
class Extent2778
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [58.2666666667, 51.133527360237], [55.2666666667, 51.133527360237], [55.2666666667, 41.26388168335], [58.2666666667, 41.26388168335], [58.2666666667, 51.133527360237],
                ],
            ],
        ];
    }
}
