<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - zone III.
 * @internal
 */
class Extent1856
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [133.48239575574, 35.632113324029], [130.81945142317, 35.632113324029], [130.81945142317, 33.721408694788], [133.48239575574, 33.721408694788], [133.48239575574, 35.632113324029],
                ],
            ],
            [
                [
                    [133.44703270182, 36.374611205439], [133.12069435549, 36.374611205439], [133.12069435549, 36.102322789001], [133.44703270182, 36.102322789001], [133.44703270182, 36.374611205439],
                ],
            ],
        ];
    }
}
