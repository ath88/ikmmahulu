<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Maine - SPCS - E.
 * @internal
 */
class Extent2206
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-66.917007446289, 47.467893600464], [-70.029436888477, 47.467893600464], [-70.029436888477, 43.88897895813], [-66.917007446289, 43.88897895813], [-66.917007446289, 47.467893600464],
                ],
            ],
        ];
    }
}
