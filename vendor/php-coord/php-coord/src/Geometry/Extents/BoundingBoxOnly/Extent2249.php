<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - South Dakota - SPCS - N.
 * @internal
 */
class Extent2249
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-96.454496608233, 45.943547318258], [-104.06103614077, 45.943547318258], [-104.06103614077, 44.145639432165], [-96.454496608233, 44.145639432165], [-96.454496608233, 45.943547318258],
                ],
            ],
        ];
    }
}
