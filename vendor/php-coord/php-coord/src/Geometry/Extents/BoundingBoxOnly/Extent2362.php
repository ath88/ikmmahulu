<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran - Kangan district.
 * @internal
 */
class Extent2362
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [53.001142258311, 28.198263895356], [51.8, 28.198263895356], [51.8, 27.300452168842], [53.001142258311, 27.300452168842], [53.001142258311, 28.198263895356],
                ],
            ],
        ];
    }
}
