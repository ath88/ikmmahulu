<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Europe - common offshore.
 * @internal
 */
class Extent2342
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [10.856713489753, 63.887480174939], [-16.096100515106, 63.887480174939], [-16.096100515106, 47.423337159941], [10.856713489753, 47.423337159941], [10.856713489753, 63.887480174939],
                ],
            ],
        ];
    }
}
