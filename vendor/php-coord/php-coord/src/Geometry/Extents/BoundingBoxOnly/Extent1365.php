<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Algeria - north of 32°N.
 * @internal
 */
class Extent1365
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.0825808439418, 37.139756994041], [-2.9446501070774, 37.139756994041], [-2.9446501070774, 31.999999836461], [9.0825808439418, 31.999999836461], [9.0825808439418, 37.139756994041],
                ],
            ],
        ];
    }
}
