<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - zone II.
 * @internal
 */
class Extent1742
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [9.5562166666667, 63.862594088372], [7.2229166666667, 63.862594088372], [7.2229166666667, 57.955711953821], [9.5562166666667, 57.955711953821], [9.5562166666667, 63.862594088372],
                ],
            ],
        ];
    }
}
