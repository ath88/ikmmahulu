<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Gabon - west of 12°E - onshore and offshore.
 * @internal
 */
class Extent1697
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12, 2.3152775764466], [7.0349995070007, 2.3152775764466], [7.0349995070007, -6.3693738899995], [12, -6.3693738899995], [12, 2.3152775764466],
                ],
            ],
        ];
    }
}
