<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Angola - offshore north of 8°S.
 * @internal
 */
class Extent4025
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.830107761304, -5.0549634886659], [10.413694255881, -5.0549634886659], [10.413694255881, -8.0013148473166], [12.830107761304, -8.0013148473166], [12.830107761304, -5.0549634886659],
                ],
            ],
        ];
    }
}
