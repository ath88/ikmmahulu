<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Alberta - east of 112.5°W.
 * @internal
 */
class Extent3543
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.98324515223, 60], [-112.5, 60], [-112.5, 48.999435424805], [-109.98324515223, 48.999435424805], [-109.98324515223, 60],
                ],
            ],
        ];
    }
}
