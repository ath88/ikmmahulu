<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 132°E to 138°E, S hemisphere onshore.
 * @internal
 */
class Extent3991
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [132.03754662302, -7.1252139845379], [132, -7.1252139845379], [132, -7.2933857237477], [132.03754662302, -7.2933857237477], [132.03754662302, -7.1252139845379],
                ],
            ],
            [
                [
                    [138, -0.29418345748581], [132, -0.29418345748581], [132, -5.5000233467554], [138, -5.5000233467554], [138, -0.29418345748581],
                ],
            ],
            [
                [
                    [138, -7.5716133479981], [137.58115438449, -7.5716133479981], [137.58115438449, -8.4853851412951], [138, -8.4853851412951], [138, -7.5716133479981],
                ],
            ],
            [
                [
                    [134.79106281131, -6.5368075946858], [134.57454122702, -6.5368075946858], [134.57454122702, -6.8247198038965], [134.79106281131, -6.8247198038965], [134.79106281131, -6.5368075946858],
                ],
            ],
            [
                [
                    [134.93825191223, -6.2391707812218], [134.73934612924, -6.2391707812218], [134.73934612924, -6.5202714179793], [134.93825191223, -6.5202714179793], [134.93825191223, -6.2391707812218],
                ],
            ],
            [
                [
                    [134.81959078055, -5.3790967768614], [134.00191236515, -5.3790967768614], [134.00191236515, -6.9717720931537], [134.81959078055, -6.9717720931537], [134.81959078055, -5.3790967768614],
                ],
            ],
            [
                [
                    [133.23902257163, -5.2419906153119], [132.57960689457, -5.2419906153119], [132.57960689457, -6.0535978729683], [133.23902257163, -6.0535978729683], [133.23902257163, -5.2419906153119],
                ],
            ],
            [
                [
                    [136.94928477643, -1.5422197580169], [135.37262212479, -1.5422197580169], [135.37262212479, -1.9540288811997], [136.94928477643, -1.9540288811997], [136.94928477643, -1.5422197580169],
                ],
            ],
            [
                [
                    [135.04082547996, -0.88974097771074], [134.75061978515, -0.88974097771074], [134.75061978515, -1.1911168826693], [135.04082547996, -1.1911168826693], [135.04082547996, -0.88974097771074],
                ],
            ],
            [
                [
                    [136.43489135688, -0.58631914052762], [135.31982237475, -0.58631914052762], [135.31982237475, -1.2758323430868], [136.43489135688, -1.2758323430868], [136.43489135688, -0.58631914052762],
                ],
            ],
        ];
    }
}
