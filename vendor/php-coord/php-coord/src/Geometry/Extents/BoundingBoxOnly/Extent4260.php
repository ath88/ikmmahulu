<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Clinton.
 * @internal
 */
class Extent4260
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.242296999981, 40.432013999998], [-86.695803999997, 40.432013999998], [-86.695803999997, 40.176886999975], [-86.242296999981, 40.176886999975], [-86.242296999981, 40.432013999998],
                ],
            ],
        ];
    }
}
