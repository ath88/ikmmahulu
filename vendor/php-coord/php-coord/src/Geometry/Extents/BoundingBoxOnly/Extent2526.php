<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Japan - onshore mainland south of 31°20'N.
 * @internal
 */
class Extent2526
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [130.72153885884, 31.332566666667], [130.14578965299, 31.332566666667], [130.14578965299, 31.105231131441], [130.72153885884, 31.105231131441], [130.72153885884, 31.332566666667],
                ],
            ],
            [
                [
                    [131.18719590508, 31.332566666667], [130.61033227341, 31.332566666667], [130.61033227341, 30.9498237462], [131.18719590508, 30.9498237462], [131.18719590508, 31.332566666667],
                ],
            ],
        ];
    }
}
