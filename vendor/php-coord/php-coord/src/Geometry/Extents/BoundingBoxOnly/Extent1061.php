<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada.
 * @internal
 */
class Extent1061
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-47.743430543984, 86.453745196084], [-141.00299072266, 86.453745196084], [-141.00299072266, 40.040199904302], [-47.743430543984, 40.040199904302], [-47.743430543984, 86.453745196084],
                ],
            ],
        ];
    }
}
