<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Potiguar, Ceara and Barreirinhas.
 * @internal
 */
class Extent3698
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-26.00968170166, 4.2522640228271], [-44.786392211914, 4.2522640228271], [-44.786392211914, -6.4963130950928], [-26.00968170166, -6.4963130950928], [-26.00968170166, 4.2522640228271],
                ],
            ],
        ];
    }
}
