<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Waupaca.
 * @internal
 */
class Extent4358
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.60517129823, 44.681362307293], [-89.224788463345, 44.681362307293], [-89.224788463345, 44.242611085067], [-88.60517129823, 44.242611085067], [-88.60517129823, 44.681362307293],
                ],
            ],
        ];
    }
}
