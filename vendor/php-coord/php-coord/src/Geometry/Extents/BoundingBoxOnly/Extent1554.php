<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Somalia - 42°E to 48°E, N hemisphere onshore.
 * @internal
 */
class Extent1554
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48, 11.510413226234], [42, 11.510413226234], [42, 0], [48, 0], [48, 11.510413226234],
                ],
            ],
        ];
    }
}
