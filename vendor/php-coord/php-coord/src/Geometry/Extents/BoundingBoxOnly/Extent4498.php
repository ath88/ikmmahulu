<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Great Bend.
 * @internal
 */
class Extent4498
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-98.479507, 40.002553], [-99.067128, 40.002553], [-99.067128, 38.26079], [-98.479507, 38.26079], [-98.479507, 40.002553],
                ],
            ],
        ];
    }
}
