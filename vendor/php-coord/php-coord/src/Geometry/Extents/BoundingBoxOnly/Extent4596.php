<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - La Palma.
 * @internal
 */
class Extent4596
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-17.666528701782, 28.898195266724], [-18.05305480957, 28.898195266724], [-18.05305480957, 28.403192520142], [-17.666528701782, 28.403192520142], [-17.666528701782, 28.898195266724],
                ],
            ],
        ];
    }
}
