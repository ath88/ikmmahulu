<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Mount Nicholas mc.
 * @internal
 */
class Extent3797
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [169.10411868, -44.293562794975], [167.72639117398, -44.293562794975], [167.72639117398, -45.57814146], [169.10411868, -45.57814146], [169.10411868, -44.293562794975],
                ],
            ],
        ];
    }
}
