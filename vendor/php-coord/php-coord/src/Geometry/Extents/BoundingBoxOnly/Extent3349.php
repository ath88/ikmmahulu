<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Slovenia - Ljubliana west.
 * @internal
 */
class Extent3349
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.517794091609, 46.20388582979], [13.955715938193, 46.20388582979], [13.955715938193, 45.867715601349], [14.517794091609, 45.867715601349], [14.517794091609, 46.20388582979],
                ],
            ],
        ];
    }
}
