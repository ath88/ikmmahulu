<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Boone and Hendricks.
 * @internal
 */
class Extent4263
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.240691000003, 40.180777999971], [-86.695880999997, 40.180777999971], [-86.695880999997, 39.60072800003], [-86.240691000003, 39.60072800003], [-86.240691000003, 40.180777999971],
                ],
            ],
        ];
    }
}
