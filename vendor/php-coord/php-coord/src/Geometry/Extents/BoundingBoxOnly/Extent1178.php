<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria.
 * @internal
 */
class Extent1178
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.649651527405, 13.891498565674], [2.6674375020002, 13.891498565674], [2.6674375020002, 1.9216671180004], [14.649651527405, 1.9216671180004], [14.649651527405, 13.891498565674],
                ],
            ],
        ];
    }
}
