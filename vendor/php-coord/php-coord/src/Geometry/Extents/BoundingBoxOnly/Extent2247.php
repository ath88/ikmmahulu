<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - South Carolina - SPCS27 - N.
 * @internal
 */
class Extent2247
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-78.526371340602, 35.208240509033], [-83.350799560547, 35.208240509033], [-83.350799560547, 33.464088439941], [-78.526371340602, 33.464088439941], [-78.526371340602, 35.208240509033],
                ],
            ],
        ];
    }
}
