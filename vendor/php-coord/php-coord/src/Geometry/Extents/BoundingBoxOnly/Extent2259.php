<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Utah - SPCS - S.
 * @internal
 */
class Extent2259
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.04320640865, 38.579028122414], [-114.04727299918, 38.579028122414], [-114.04727299918, 36.991746303875], [-109.04320640865, 36.991746303875], [-109.04320640865, 38.579028122414],
                ],
            ],
        ];
    }
}
