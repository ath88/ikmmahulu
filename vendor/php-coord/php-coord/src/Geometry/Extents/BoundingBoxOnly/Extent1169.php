<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Namibia.
 * @internal
 */
class Extent1169
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [25.264430999756, -16.954170227051], [8.2435035080003, -16.954170227051], [8.2435035080003, -30.635802913], [25.264430999756, -30.635802913], [25.264430999756, -16.954170227051],
                ],
            ],
        ];
    }
}
