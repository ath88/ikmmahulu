<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Lake and Newton.
 * @internal
 */
class Extent4253
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-87.21882500001, 41.760688386132], [-87.526768000023, 41.760688386132], [-87.526768000023, 40.736463999996], [-87.21882500001, 40.736463999996], [-87.21882500001, 41.760688386132],
                ],
            ],
        ];
    }
}
