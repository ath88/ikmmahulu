<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Congo DR (Zaire) - south and 15°E to 17°E.
 * @internal
 */
class Extent3617
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [17, -3.417722073872], [15, -3.417722073872], [15, -7.3005912293353], [17, -7.3005912293353], [17, -3.417722073872],
                ],
            ],
        ];
    }
}
