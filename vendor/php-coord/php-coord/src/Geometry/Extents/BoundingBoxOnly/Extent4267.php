<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Monroe and Morgan.
 * @internal
 */
class Extent4267
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.249889000008, 39.633645999998], [-86.687573999997, 39.633645999998], [-86.687573999997, 38.990870000011], [-86.249889000008, 38.990870000011], [-86.249889000008, 39.633645999998],
                ],
            ],
        ];
    }
}
