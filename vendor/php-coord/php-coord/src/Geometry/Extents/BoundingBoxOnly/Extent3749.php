<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Punakha district.
 * @internal
 */
class Extent3749
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [90.070550266374, 27.863157439012], [89.63542428122, 27.863157439012], [89.63542428122, 27.466980492091], [90.070550266374, 27.466980492091], [90.070550266374, 27.863157439012],
                ],
            ],
        ];
    }
}
