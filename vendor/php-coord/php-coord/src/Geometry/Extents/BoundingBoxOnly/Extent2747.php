<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Russia - 19.5°E to 22.5°E onshore.
 * @internal
 */
class Extent2747
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.5, 55.313291549683], [19.575708389282, 55.313291549683], [19.575708389282, 54.325555801392], [22.5, 54.325555801392], [22.5, 55.313291549683],
                ],
            ],
        ];
    }
}
