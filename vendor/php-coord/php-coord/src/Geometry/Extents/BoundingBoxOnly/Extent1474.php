<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 16°E to 18°E onshore.
 * @internal
 */
class Extent1474
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [18, 31.337401459249], [16, 31.337401459249], [16, 22.517430544906], [18, 22.517430544906], [18, 31.337401459249],
                ],
            ],
        ];
    }
}
