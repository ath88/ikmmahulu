<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - offshore deep water - east of 6°E.
 * @internal
 */
class Extent3812
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.8195275070005, 3.6734909064825], [6, 3.6734909064825], [6, 2.6190768586226], [7.8195275070005, 2.6190768586226], [7.8195275070005, 3.6734909064825],
                ],
            ],
        ];
    }
}
