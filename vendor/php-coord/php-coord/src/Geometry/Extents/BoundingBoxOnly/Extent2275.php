<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Newfoundland and Labrador - 60°W to 57.5°W.
 * @internal
 */
class Extent2275
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-57.5, 50.531986827464], [-59.477936442937, 50.531986827464], [-59.477936442937, 47.507338519471], [-57.5, 47.507338519471], [-57.5, 50.531986827464],
                ],
            ],
        ];
    }
}
