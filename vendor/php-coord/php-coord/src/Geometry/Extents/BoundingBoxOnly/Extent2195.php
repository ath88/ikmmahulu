<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Illinois - SPCS - W.
 * @internal
 */
class Extent2195
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.930699695823, 42.508394227081], [-91.516284050609, 42.508394227081], [-91.516284050609, 36.986822280657], [-88.930699695823, 36.986822280657], [-88.930699695823, 42.508394227081],
                ],
            ],
        ];
    }
}
