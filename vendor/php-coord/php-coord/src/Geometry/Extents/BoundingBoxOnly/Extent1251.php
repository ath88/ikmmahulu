<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Venezuela.
 * @internal
 */
class Extent1251
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-58.954944610596, 16.745054244995], [-73.378067016602, 16.745054244995], [-73.378067016602, 0.64916801452642], [-58.954944610596, 0.64916801452642], [-58.954944610596, 16.745054244995],
                ],
            ],
        ];
    }
}
