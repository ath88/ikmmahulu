<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oregon - Oregon Coast.
 * @internal
 */
class Extent4208
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-123.35311970722, 46.394548173465], [-124.83851922396, 46.394548173465], [-124.83851922396, 41.89797964785], [-123.35311970722, 41.89797964785], [-123.35311970722, 46.394548173465],
                ],
            ],
        ];
    }
}
