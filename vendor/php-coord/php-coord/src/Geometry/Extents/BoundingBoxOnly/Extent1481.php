<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 18°E to 24°E onshore.
 * @internal
 */
class Extent1481
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24, 32.994439798274], [18, 32.994439798274], [18, 19.5], [24, 19.5], [24, 32.994439798274],
                ],
            ],
        ];
    }
}
