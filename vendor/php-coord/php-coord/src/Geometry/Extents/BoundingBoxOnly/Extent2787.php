<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Morocco - south of 31.5°N.
 * @internal
 */
class Extent2787
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-3.5928475856778, 31.500000461125], [-13.233305093264, 31.500000461125], [-13.233305093264, 27.664237976075], [-3.5928475856778, 27.664237976075], [-3.5928475856778, 31.500000461125],
                ],
            ],
        ];
    }
}
