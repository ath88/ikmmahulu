<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - east of 18°W.
 * @internal
 */
class Extent3630
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-11.753166510994, 32.754493146022], [-18, 32.754493146022], [-18, 25.251838632821], [-11.753166510994, 25.251838632821], [-11.753166510994, 32.754493146022],
                ],
            ],
        ];
    }
}
