<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Cumuruxatiba, Jequitinhonha and Camamu-Almada.
 * @internal
 */
class Extent3699
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-34.606912612915, -13.018238067627], [-39.214729309082, -13.018238067627], [-39.214729309082, -17.696962356567], [-34.606912612915, -17.696962356567], [-34.606912612915, -13.018238067627],
                ],
            ],
        ];
    }
}
