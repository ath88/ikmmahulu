<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Nova Scotia - east of 63°W.
 * @internal
 */
class Extent1534
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-59.7372811359, 47.078471709113], [-63, 47.078471709113], [-63, 44.640944235011], [-59.7372811359, 44.640944235011], [-59.7372811359, 47.078471709113],
                ],
            ],
        ];
    }
}
