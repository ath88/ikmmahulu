<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Montana  Three Forks.
 * @internal
 */
class Extent4317
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-110.75, 46.583333], [-112.333333, 46.583333], [-112.333333, 45.366667], [-110.75, 45.366667], [-110.75, 46.583333],
                ],
            ],
        ];
    }
}
