<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Chile - Easter Island onshore.
 * @internal
 */
class Extent3188
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.1620464325, -27.019100189209], [-109.50198173523, -27.019100189209], [-109.50198173523, -27.246438980103], [-109.1620464325, -27.246438980103], [-109.1620464325, -27.019100189209],
                ],
            ],
        ];
    }
}
