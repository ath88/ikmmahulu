<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Albania.
 * @internal
 */
class Extent1025
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [21.053331375122, 42.660343170166], [18.461944517, 42.660343170166], [18.461944517, 39.638635153001], [21.053331375122, 39.638635153001], [21.053331375122, 42.660343170166],
                ],
            ],
        ];
    }
}
