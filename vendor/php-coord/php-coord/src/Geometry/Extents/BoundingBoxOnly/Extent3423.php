<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Mexico - west of 114°W.
 * @internal
 */
class Extent3423
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-114, 32.718456219614], [-122.18355761439, 32.718456219614], [-122.18355761439, 15.011626130342], [-114, 15.011626130342], [-114, 32.718456219614],
                ],
            ],
        ];
    }
}
