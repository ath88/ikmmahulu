<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Langlade.
 * @internal
 */
class Extent4344
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-88.638278466882, 45.470074737137], [-89.42596520987, 45.470074737137], [-89.42596520987, 45.02881692698], [-88.638278466882, 45.02881692698], [-88.638278466882, 45.470074737137],
                ],
            ],
        ];
    }
}
