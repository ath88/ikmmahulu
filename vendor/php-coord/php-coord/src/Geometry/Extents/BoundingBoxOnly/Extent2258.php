<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Utah - SPCS - N.
 * @internal
 */
class Extent2258
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-109.04615572619, 42.002300584901], [-114.03907266235, 42.002300584901], [-114.03907266235, 40.555102306287], [-109.04615572619, 40.555102306287], [-109.04615572619, 42.002300584901],
                ],
            ],
        ];
    }
}
