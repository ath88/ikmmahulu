<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden - 21 45.
 * @internal
 */
class Extent2843
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [22.903744488345, 66.425016638363], [19.637260425762, 66.425016638363], [19.637260425762, 65.010189130409], [22.903744488345, 65.010189130409], [22.903744488345, 66.425016638363],
                ],
            ],
        ];
    }
}
