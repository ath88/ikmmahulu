<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Bulgaria - west of 24°E.
 * @internal
 */
class Extent4428
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24, 44.224716186524], [22.36527633667, 44.224716186524], [22.36527633667, 41.320549011231], [24, 41.320549011231], [24, 44.224716186524],
                ],
            ],
        ];
    }
}
