<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Pakistan - north of 35°35'N.
 * @internal
 */
class Extent1668
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [77.006002299126, 37.060787200928], [71.187789916992, 37.060787200928], [71.187789916992, 35.58333333], [77.006002299126, 35.58333333], [77.006002299126, 37.060787200928],
                ],
            ],
        ];
    }
}
