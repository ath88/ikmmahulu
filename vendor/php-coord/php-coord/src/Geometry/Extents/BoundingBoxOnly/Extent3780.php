<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - North Island - Poverty Bay mc.
 * @internal
 */
class Extent3780
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [178.62698173523, -37.496059417725], [176.73796062, -37.496059417725], [176.73796062, -39.03923043], [178.62698173523, -39.03923043], [178.62698173523, -37.496059417725],
                ],
            ],
        ];
    }
}
