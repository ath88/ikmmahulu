<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Dunedin vcrs.
 * @internal
 */
class Extent3803
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [171.27071836003, -43.829829605014], [167.73462037609, -43.829829605014], [167.73462037609, -46.3900060911], [171.27071836003, -46.3900060911], [171.27071836003, -43.829829605014],
                ],
            ],
        ];
    }
}
