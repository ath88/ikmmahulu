<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Kuwait - onshore.
 * @internal
 */
class Extent3267
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [48.470911889888, 30.084159851074], [46.546943664551, 30.084159851074], [46.546943664551, 28.538883209229], [48.470911889888, 28.538883209229], [48.470911889888, 30.084159851074],
                ],
            ],
        ];
    }
}
