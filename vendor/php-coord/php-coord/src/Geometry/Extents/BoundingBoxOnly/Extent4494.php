<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Kansas - Hays.
 * @internal
 */
class Extent4494
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-99.0375, 40.002352], [-100.180456, 40.002352], [-100.180456, 38.696264], [-99.0375, 38.696264], [-99.0375, 40.002352],
                ],
            ],
        ];
    }
}
