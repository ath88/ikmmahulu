<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - onshore 10.5°E to 13.5°E.
 * @internal
 */
class Extent3996
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13.5, 54.732431145027], [10.5, 54.732431145027], [10.5, 47.39527130127], [13.5, 47.39527130127], [13.5, 54.732431145027],
                ],
            ],
        ];
    }
}
