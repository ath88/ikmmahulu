<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - GoM OCS - west of 96°W.
 * @internal
 */
class Extent2171
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-95.873023593134, 28.423334904526], [-97.211482716314, 28.423334904526], [-97.211482716314, 25.975158230373], [-95.873023593134, 25.975158230373], [-95.873023593134, 28.423334904526],
                ],
            ],
        ];
    }
}
