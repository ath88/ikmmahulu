<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Asia - Middle East - Israel, Palestine Territory, Turkey - offshore.
 * @internal
 */
class Extent2896
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [35.044151500916, 33.524921147001], [32.990981530991, 33.524921147001], [32.990981530991, 31.357949771819], [35.044151500916, 31.357949771819], [35.044151500916, 33.524921147001],
                ],
            ],
            [
                [
                    [41.468211672563, 43.446944156001], [28.03304033245, 43.446944156001], [28.03304033245, 40.962603609285], [41.468211672563, 40.962603609285], [41.468211672563, 43.446944156001],
                ],
            ],
        ];
    }
}
