<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Manipur.
 * @internal
 */
class Extent4409
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [94.751007080078, 25.697650909424], [92.973602294922, 25.697650909424], [92.973602294922, 23.842729568482], [94.751007080078, 23.842729568482], [94.751007080078, 25.697650909424],
                ],
            ],
        ];
    }
}
