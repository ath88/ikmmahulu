<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Sweden.
 * @internal
 */
class Extent1225
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [24.167007446289, 69.060302734375], [10.030027530083, 69.060302734375], [10.030027530083, 54.962442169034], [24.167007446289, 54.962442169034], [24.167007446289, 69.060302734375],
                ],
            ],
        ];
    }
}
