<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Portugal - Madeira island onshore.
 * @internal
 */
class Extent3679
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-16.66555650127, 32.920278001904], [-17.304517198681, 32.920278001904], [-17.304517198681, 32.587496351124], [-16.66555650127, 32.587496351124], [-16.66555650127, 32.920278001904],
                ],
            ],
        ];
    }
}
