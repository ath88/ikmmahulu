<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Netherlands.
 * @internal
 */
class Extent1172
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [7.2108882264223, 55.765000167947], [2.5393598961474, 55.765000167947], [2.5393598961474, 50.753883361816], [7.2108882264223, 50.753883361816], [7.2108882264223, 55.765000167947],
                ],
            ],
        ];
    }
}
