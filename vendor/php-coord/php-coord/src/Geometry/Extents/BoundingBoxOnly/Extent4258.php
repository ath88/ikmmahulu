<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Indiana - Carroll.
 * @internal
 */
class Extent4258
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-86.373436000002, 40.737540000017], [-86.774468999997, 40.737540000017], [-86.774468999997, 40.431004999978], [-86.373436000002, 40.431004999978], [-86.373436000002, 40.737540000017],
                ],
            ],
        ];
    }
}
