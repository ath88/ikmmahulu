<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Iran - South Pars block 11.
 * @internal
 */
class Extent3140
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [52.407698, 26.632873], [52.227496, 26.632873], [52.227496, 26.464281], [52.407698, 26.464281], [52.407698, 26.632873],
                ],
            ],
        ];
    }
}
