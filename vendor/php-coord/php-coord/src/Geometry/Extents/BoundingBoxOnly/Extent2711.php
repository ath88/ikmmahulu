<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/China - west of 76.5°E.
 * @internal
 */
class Extent2711
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [76.5, 40.644523620606], [73.62004852295, 40.644523620606], [73.62004852295, 35.81332397461], [76.5, 35.81332397461], [76.5, 40.644523620606],
                ],
            ],
        ];
    }
}
