<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Norway - onshore - 12ºE to 13ºE.
 * @internal
 */
class Extent3653
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [13, 68.147304434487], [12.655568808914, 68.147304434487], [12.655568808914, 67.757108572954], [13, 67.757108572954], [13, 68.147304434487],
                ],
            ],
            [
                [
                    [13, 66.57620542231], [12.852483455058, 66.57620542231], [12.852483455058, 66.447127119857], [13, 66.447127119857], [13, 66.57620542231],
                ],
            ],
            [
                [
                    [13, 66.408344851762], [12.884021572438, 66.408344851762], [12.884021572438, 66.269523096837], [13, 66.269523096837], [13, 66.408344851762],
                ],
            ],
            [
                [
                    [13, 66.267602854045], [12, 66.267602854045], [12, 63.324785095533], [13, 63.324785095533], [13, 66.267602854045],
                ],
            ],
            [
                [
                    [12.856109619141, 63.209537070996], [12, 63.209537070996], [12, 59.88582611084], [12.856109619141, 59.88582611084], [12.856109619141, 63.209537070996],
                ],
            ],
            [
                [
                    [12.126141805668, 65.747458753001], [12, 65.747458753001], [12, 65.55552330428], [12.126141805668, 65.55552330428], [12.126141805668, 65.747458753001],
                ],
            ],
        ];
    }
}
