<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - Ouvea.
 * @internal
 */
class Extent2813
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [166.70464402066, -20.341421740303], [166.44588552732, -20.341421740303], [166.44588552732, -20.765943637774], [166.70464402066, -20.765943637774], [166.70464402066, -20.341421740303],
                ],
            ],
        ];
    }
}
