<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Africa - AEF east of 21°E.
 * @internal
 */
class Extent2554
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [27.459720611572, 21.0519795357], [21, 21.0519795357], [21, 4.1274995803834], [27.459720611572, 4.1274995803834], [27.459720611572, 21.0519795357],
                ],
            ],
        ];
    }
}
