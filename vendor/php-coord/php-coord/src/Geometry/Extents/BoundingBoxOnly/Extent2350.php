<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Mozambique - A.
 * @internal
 */
class Extent2350
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [34.494008878744, -23.910328668221], [31.919441223145, -23.910328668221], [31.919441223145, -26.860279083252], [34.494008878744, -26.860279083252], [34.494008878744, -23.910328668221],
                ],
            ],
        ];
    }
}
