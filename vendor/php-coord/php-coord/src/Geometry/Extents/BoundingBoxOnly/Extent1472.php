<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Libya - 12°E to 14°E onshore.
 * @internal
 */
class Extent1472
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14, 33.058822440805], [12, 33.058822440805], [12, 22.808295025017], [14, 22.808295025017], [14, 33.058822440805],
                ],
            ],
        ];
    }
}
