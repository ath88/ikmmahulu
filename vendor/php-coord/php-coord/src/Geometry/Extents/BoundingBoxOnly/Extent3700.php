<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Brazil - Santos and Pelotas.
 * @internal
 */
class Extent3700
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-40.201427459717, -22.668334960938], [-53.374298095703, -22.668334960938], [-53.374298095703, -35.708560943604], [-40.201427459717, -35.708560943604], [-40.201427459717, -22.668334960938],
                ],
            ],
        ];
    }
}
