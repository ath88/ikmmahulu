<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - 114°W to 108°W.
 * @internal
 */
class Extent3527
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-108, 84], [-114, 84], [-114, 48.999435424805], [-108, 48.999435424805], [-108, 84],
                ],
            ],
        ];
    }
}
