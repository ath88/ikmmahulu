<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/South America - 78°W to 72°W, N hemisphere and SAD69 by country.
 * @internal
 */
class Extent3832
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-72, 12.307291030884], [-78, 12.307291030884], [-78, 0], [-72, 0], [-72, 12.307291030884],
                ],
            ],
        ];
    }
}
