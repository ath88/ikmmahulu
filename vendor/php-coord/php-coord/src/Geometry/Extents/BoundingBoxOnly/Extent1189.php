<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * South America/Peru.
 * @internal
 */
class Extent1189
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-68.673904418945, -0.036874771118164], [-84.672222137451, -0.036874771118164], [-84.672222137451, -21.04967880249], [-68.673904418945, -21.04967880249], [-68.673904418945, -0.036874771118164],
                ],
            ],
        ];
    }
}
