<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/India - Sikkim.
 * @internal
 */
class Extent4416
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [88.919731140137, 28.130968093872], [88.016967773437, 28.130968093872], [88.016967773437, 27.081609725952], [88.919731140137, 27.081609725952], [88.919731140137, 28.130968093872],
                ],
            ],
        ];
    }
}
