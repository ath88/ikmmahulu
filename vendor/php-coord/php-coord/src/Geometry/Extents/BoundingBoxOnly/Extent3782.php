<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Zealand - South Island - Collingwood mc.
 * @internal
 */
class Extent3782
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [173.12157965655, -40.448360443115], [172.16996124048, -40.448360443115], [172.16996124048, -41.21059149], [173.12157965655, -41.21059149], [173.12157965655, -40.448360443115],
                ],
            ],
        ];
    }
}
