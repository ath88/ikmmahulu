<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - Ile des Pins.
 * @internal
 */
class Extent2820
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [167.60866468114, -22.49250497064], [167.36513765954, -22.49250497064], [167.36513765954, -22.721918642456], [167.60866468114, -22.721918642456], [167.60866468114, -22.49250497064],
                ],
            ],
        ];
    }
}
