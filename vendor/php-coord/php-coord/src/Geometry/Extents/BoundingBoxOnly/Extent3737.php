<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Bhutan - Chhukha district.
 * @internal
 */
class Extent3737
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [89.828890298225, 27.314164394005], [89.267621758733, 27.314164394005], [89.267621758733, 26.717662811279], [89.828890298225, 26.717662811279], [89.828890298225, 27.314164394005],
                ],
            ],
        ];
    }
}
