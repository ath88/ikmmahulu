<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Indonesia - 117°E to 120°E onshore.
 * @internal
 */
class Extent3517
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [120, -9.239884976109], [118.88324745921, -9.239884976109], [118.88324745921, -10.143074729065], [120, -10.143074729065], [120, -9.239884976109],
                ],
            ],
            [
                [
                    [120, -8.3667021706086], [119.32474623722, -8.3667021706086], [119.32474623722, -8.9000573739283], [120, -8.9000573739283], [120, -8.3667021706086],
                ],
            ],
            [
                [
                    [119.23432129957, -8.0330084009716], [117, -8.0330084009716], [117, -9.1603338713099], [119.23432129957, -9.1603338713099], [119.23432129957, -8.0330084009716],
                ],
            ],
            [
                [
                    [120, 0.75038147251428], [118.71043320006, 0.75038147251428], [118.71043320006, -5.7582376853084], [120, -5.7582376853084], [120, 0.75038147251428],
                ],
            ],
            [
                [
                    [119.05724835258, 4.3587489128117], [117, 4.3587489128117], [117, -1.2762276586943], [119.05724835258, -1.2762276586943], [119.05724835258, 4.3587489128117],
                ],
            ],
        ];
    }
}
