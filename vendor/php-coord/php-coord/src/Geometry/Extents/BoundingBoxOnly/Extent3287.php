<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/Nigeria - onshore.
 * @internal
 */
class Extent3287
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [14.649651527405, 13.891498565674], [2.692499637604, 13.891498565674], [2.692499637604, 4.2227754918812], [14.649651527405, 4.2227754918812], [14.649651527405, 13.891498565674],
                ],
            ],
        ];
    }
}
