<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/Australia - mainland.
 * @internal
 */
class Extent1281
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [153.68103981018, -10.656530380249], [112.85186576843, -10.656530380249], [112.85186576843, -39.197008132935], [153.68103981018, -39.197008132935], [153.68103981018, -10.656530380249],
                ],
            ],
        ];
    }
}
