<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/Canada - Manitoba and Ontario.
 * @internal
 */
class Extent2415
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-74.358075474967, 60.008697112007], [-102, 60.008697112007], [-102, 41.675552368164], [-74.358075474967, 41.675552368164], [-74.358075474967, 60.008697112007],
                ],
            ],
        ];
    }
}
