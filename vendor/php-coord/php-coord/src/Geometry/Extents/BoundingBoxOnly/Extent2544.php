<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Germany - Thuringen.
 * @internal
 */
class Extent2544
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [12.555150032043, 51.637107849121], [9.9222221374512, 51.637107849121], [9.9222221374512, 50.204719543457], [12.555150032043, 50.204719543457], [12.555150032043, 51.637107849121],
                ],
            ],
        ];
    }
}
