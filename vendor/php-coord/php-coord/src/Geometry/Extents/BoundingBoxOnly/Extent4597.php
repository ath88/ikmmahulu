<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Spain - Canary Islands - El Hierro.
 * @internal
 */
class Extent4597
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-17.833477020264, 27.898611068726], [-18.219863891602, 27.898611068726], [-18.219863891602, 27.587495803833], [-17.833477020264, 27.587495803833], [-17.833477020264, 27.898611068726],
                ],
            ],
        ];
    }
}
