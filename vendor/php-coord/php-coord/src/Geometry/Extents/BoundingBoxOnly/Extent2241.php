<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oklahoma - SPCS - N.
 * @internal
 */
class Extent2241
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.42855201209, 37.002312], [-102.99770944261, 37.002312], [-102.99770944261, 35.271905705029], [-94.42855201209, 35.271905705029], [-94.42855201209, 37.002312],
                ],
            ],
        ];
    }
}
