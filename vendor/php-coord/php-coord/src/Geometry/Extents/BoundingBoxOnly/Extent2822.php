<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/New Caledonia - Grande Terre.
 * @internal
 */
class Extent2822
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [167.08186325579, -20.039441831035], [163.92972359971, -20.039441831035], [163.92972359971, -22.447005074937], [167.08186325579, -22.447005074937], [167.08186325579, -20.039441831035],
                ],
            ],
        ];
    }
}
