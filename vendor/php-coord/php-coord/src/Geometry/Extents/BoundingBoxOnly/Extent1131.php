<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Europe-FSU/Kazakhstan.
 * @internal
 */
class Extent1131
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [87.348205566406, 55.442626953125], [46.499160766602, 55.442626953125], [46.499160766602, 40.594436645508], [87.348205566406, 40.594436645508], [87.348205566406, 55.442626953125],
                ],
            ],
        ];
    }
}
