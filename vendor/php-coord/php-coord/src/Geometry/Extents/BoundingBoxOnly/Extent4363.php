<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Wisconsin - Crawford.
 * @internal
 */
class Extent4363
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-90.665859001811, 43.424632398408], [-91.215108952482, 43.424632398408], [-91.215108952482, 42.988174887188], [-90.665859001811, 42.988174887188], [-90.665859001811, 43.424632398408],
                ],
            ],
        ];
    }
}
