<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Australasia and Oceania/French Polynesia - 144°W to 138°W.
 * @internal
 */
class Extent3122
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-138, -4.5264778879997], [-144, -4.5264778879997], [-144, -31.236190913], [-138, -31.236190913], [-138, -4.5264778879997],
                ],
            ],
        ];
    }
}
