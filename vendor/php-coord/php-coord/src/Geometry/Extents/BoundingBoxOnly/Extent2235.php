<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - New York - SPCS - Long island.
 * @internal
 */
class Extent2235
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-71.800647735596, 41.213653711423], [-74.259082885707, 41.213653711423], [-74.259082885707, 40.477563134844], [-71.800647735596, 40.477563134844], [-71.800647735596, 41.213653711423],
                ],
            ],
            [
                [
                    [-71.890064239502, 41.297868728638], [-72.071993359666, 41.297868728638], [-72.071993359666, 41.23282897931], [-71.890064239502, 41.23282897931], [-71.890064239502, 41.297868728638],
                ],
            ],
        ];
    }
}
