<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Africa/South Africa - 18°E to 20°E.
 * @internal
 */
class Extent1456
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [19.999999984849, -28.381557172711], [17.999999103492, -28.381557172711], [17.999999103492, -34.872002359751], [19.999999984849, -34.872002359751], [19.999999984849, -28.381557172711],
                ],
            ],
        ];
    }
}
