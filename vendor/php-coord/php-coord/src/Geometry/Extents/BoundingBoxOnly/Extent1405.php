<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * North America/USA - Oklahoma.
 * @internal
 */
class Extent1405
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [-94.42855201209, 37.002312], [-102.99770944261, 37.002312], [-102.99770944261, 33.621136635399], [-94.42855201209, 33.621136635399], [-94.42855201209, 37.002312],
                ],
            ],
        ];
    }
}
