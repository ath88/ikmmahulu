<?php
/**
 * PHPCoord.
 *
 * @author Doug Wright
 */
declare(strict_types=1);

namespace PHPCoord\Geometry\Extents\BoundingBoxOnly;

/**
 * Asia-ExFSU/Afghanistan.
 * @internal
 */
class Extent1024
{
    public function __invoke(): array
    {
        return
        [
            [
                [
                    [74.915740966797, 38.471977233887], [60.504165649414, 38.471977233887], [60.504165649414, 29.406105041504], [74.915740966797, 29.406105041504], [74.915740966797, 38.471977233887],
                ],
            ],
        ];
    }
}
