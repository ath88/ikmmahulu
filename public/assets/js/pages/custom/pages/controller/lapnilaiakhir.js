"use strict";

// Class Definition
var FormCustom = function() {

    var handleSubmit = function(form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop( "disabled", true );
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function(data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch(err)
                {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop( "disabled", false );
                button.removeClass('disabled');
                button.text(button_text);  

                getIKM(form);
                init_datatable();

                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function() {
        $("#form_show").validate({
            rules: {
                tahun: {
                    required: true
                }
            },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var handleClick = function () {
        $("#export").click(function (e) {
            e.preventDefault();
            var val_name = $(this).attr('val_name');
            creteTableExcel("#table_export", val_name);
        });
    }

    var creteTableExcel = function (id, val_name) {
        $(id).tableExport({
            fileName: "kkn_excel_export_" + val_name,
            type: 'xlsx'
            // headers: true, // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
            // footers: false, // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
            // formats: ["xlsx", "csv", "txt"], // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
            // filename: "kkn_excel_export", // (id, String), filename for the downloaded file, (default: 'id')
            // bootstrap: false, // (Boolean), style buttons using bootstrap, (default: true)
            // exportButtons: true, // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
            // position: "top", // (top, bottom), position of the caption element relative to table, (default: 'bottom')
            // ignoreRows: null, // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
            // ignoreCols: null, // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
            // trimWhitespace: true, // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
            // RTL: false, // (Boolean), set direction of the worksheet to right-to-left (default: false)
            // sheetname: new Date().getFullYear(), // (id, String), sheet name for the exported spreadsheet, (default: 'id')
        });
    }

    var chart = function (datas) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartikm", am4charts.XYChart);
        chart.data = datas;
        chart.colors.step = 2;

        chart.legend = new am4charts.Legend()
        chart.legend.position = 'top'
        chart.legend.paddingBottom = 20
        chart.legend.labels.template.maxWidth = 95

        chart.exporting.menu = new am4core.ExportMenu();

        // Create axes
        var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'Ids'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 50;
        xAxis.fontSize = 11;

        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        var label = xAxis.renderer.labels.template;
        label.truncate = true;
        label.wrap = true;
        label.maxWidth = 120;

        xAxis.events.on("sizechanged", function (ev) {
            var axis = ev.target;
            var cellWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
            if (cellWidth < axis.renderer.labels.template.maxWidth) {
                axis.renderer.labels.template.rotation = -45;
                axis.renderer.labels.template.horizontalCenter = "right";
                axis.renderer.labels.template.verticalCenter = "middle";
            } else {
                axis.renderer.labels.template.rotation = 0;
                axis.renderer.labels.template.horizontalCenter = "middle";
                axis.renderer.labels.template.verticalCenter = "top";
            }
        });

        // Create series
        function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = field
            series.dataFields.categoryX = 'Ids'
            series.name = name

            series.events.on("hidden", arrangeColumns);
            series.events.on("shown", arrangeColumns);

            series.columns.template.tooltipText = "{categoryX} [bold]{name}[/]: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;

            // var bullet = series.bullets.push(new am4charts.LabelBullet())
            // bullet.interactionsEnabled = false
            // bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            // bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        function arrangeColumns() {

            var series = chart.series.getIndex(0);

            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                var delta = ((x1 - x0) / chart.series.length) * w;
                if (am4core.isNumber(delta)) {
                    var middle = chart.series.length / 2;

                    var newIndex = 0;
                    chart.series.each(function (series) {
                        if (!series.isHidden && !series.isHiding) {
                            series.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series.dummyData = chart.series.indexOf(series);
                        }
                    })
                    var visibleCount = newIndex;
                    var newMiddle = visibleCount / 2;

                    chart.series.each(function (series) {
                        var trueIndex = chart.series.indexOf(series);
                        var newIndex = series.dummyData;

                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                        series.animate({
                            property: "dx",
                            to: dx
                        }, series.interpolationDuration, series.interpolationEasing);
                        series.bulletsContainer.animate({
                            property: "dx",
                            to: dx
                        }, series.interpolationDuration, series.interpolationEasing);
                    })
                }
            }
        }

        createSeries("avRI", "Internal");
        createSeries("avRE", "Eksternal");
        createSeries("avNA", "Nilai Akhir");
    }

    var getIKM = function (form) {
        $.ajax({
            url: window.location.href + "/chartikm",
            type: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json',
            context: document.body,
            global: false,
            async: true,
            success: function (data) {
                //console.log(data);
                chart(data);
            }
        });
    }

    var init_datatable = function () {

        var table_eksternal = $('#kt_table_ikm');

        // begin first table
        table_eksternal.DataTable({
            responsive: true,
            paging: false,
            searching: false,
            info: false,

            // Order settings
            order: [
                [0, 'asc']
            ]
        });

    };

    return {
        // public functions
        init: function() {
            handleSubmitFormShow();
            handleClick();
        }
    };
}();

jQuery(document).ready(function() {
    FormCustom.init()
});