"use strict";

// Class Definition
var FormGeneral = function() {

    var handleClickDelete = function() {
        $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            
            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({title: "Deleted!", text: res.message, type: res.status}).then(
                                function(){ 
                                    location.reload();
                                }
                            ) ;                   
                        }
                    });
            })    
        });
    }

    var handleSubmitCustom = function(form) {
        var forms = $("#form_action")[0];
        var form_data = new FormData(forms);

        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop( "disabled", true );
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $("#form_action").attr('method'),
            url: $("#form_action").attr('action'),
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch(err)
                {
                    $('#response').fadeIn('slow').html(data);
                }
                
                
                button.prop( "disabled", false );
                button.removeClass('disabled');
                button.text(button_text);  
            }
        })
    }

    var handleSubmitFormCustom = function() {
        $("#form_action").validate({
            rules: {
                fileJdId: {
                    required: !0
                },
                fileName: {
                    required: !0
                },
            },
            submitHandler: function(e) {
                handleSubmitCustom(e);
                return false
            }
        });
    }

    return {
        // public functions
        init: function() {
            handleSubmitFormCustom();
            handleClickDelete();
        }
    };
}();

jQuery(document).ready(function() {
    FormGeneral.init()
});