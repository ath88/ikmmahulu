"use strict";

// Class Definition
var FormCustom = function () {

    var handleSubmit = function (form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop("disabled", true);
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch (err) {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop("disabled", false);
                button.removeClass('disabled');
                button.text(button_text);

                getPopulasiEksternal(form);
                getPopulasiInternal(form);
                init_datatable();

                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function () {
        $("#form_show").validate({
            rules: {
                tahun: {
                    required: true
                }
            },
            submitHandler: function (e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var handleClick = function () {
        $("#export").click(function (e) {
            e.preventDefault();
            var val_name = $(this).attr('val_name');
            creteTableExcel("#table_export", val_name);
        });
    }

    var creteTableExcel = function (id, val_name) {
        $(id).tableExport({
            fileName: "ikm_excel_export_" + val_name,
            type: 'xlsx'
            // headers: true, // (Boolean), display table headers (th or td elements) in the <thead>, (default: true)
            // footers: false, // (Boolean), display table footers (th or td elements) in the <tfoot>, (default: false)
            // formats: ["xlsx", "csv", "txt"], // (String[]), filetype(s) for the export, (default: ['xlsx', 'csv', 'txt'])
            // filename: "kkn_excel_export", // (id, String), filename for the downloaded file, (default: 'id')
            // bootstrap: false, // (Boolean), style buttons using bootstrap, (default: true)
            // exportButtons: true, // (Boolean), automatically generate the built-in export buttons for each of the specified formats (default: true)
            // position: "top", // (top, bottom), position of the caption element relative to table, (default: 'bottom')
            // ignoreRows: null, // (Number, Number[]), row indices to exclude from the exported file(s) (default: null)
            // ignoreCols: null, // (Number, Number[]), column indices to exclude from the exported file(s) (default: null)
            // trimWhitespace: true, // (Boolean), remove all leading/trailing newlines, spaces, and tabs from cell text in the exported file(s) (default: false)
            // RTL: false, // (Boolean), set direction of the worksheet to right-to-left (default: false)
            // sheetname: new Date().getFullYear(), // (id, String), sheet name for the exported spreadsheet, (default: 'id')
        });
    }

    var chartEksternal = function (datas) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chartEksternal = am4core.create("charteksternal", am4charts.XYChart);
        chartEksternal.data = datas;
        chartEksternal.colors.step = 2;

        chartEksternal.legend = new am4charts.Legend()
        chartEksternal.legend.position = 'top'
        chartEksternal.legend.paddingBottom = 20
        chartEksternal.legend.labels.template.maxWidth = 95

        chartEksternal.exporting.menu = new am4core.ExportMenu();

        // Create axes
        var xAxis = chartEksternal.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'eksJSekolah'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 50;
        xAxis.fontSize = 11;

        var yAxis = chartEksternal.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        var label = xAxis.renderer.labels.template;
        label.truncate = true;
        label.wrap = true;
        label.maxWidth = 120;

        xAxis.events.on("sizechanged", function (ev) {
            var axis = ev.target;
            var cellWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
            if (cellWidth < axis.renderer.labels.template.maxWidth) {
                axis.renderer.labels.template.rotation = -45;
                axis.renderer.labels.template.horizontalCenter = "right";
                axis.renderer.labels.template.verticalCenter = "middle";
            } else {
                axis.renderer.labels.template.rotation = 0;
                axis.renderer.labels.template.horizontalCenter = "middle";
                axis.renderer.labels.template.verticalCenter = "top";
            }
        });

        // Create series
        function createSeries(field, name) {
            var series = chartEksternal.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = field
            series.dataFields.categoryX = 'eksJSekolah'
            series.name = name

            series.events.on("hidden", arrangeColumns);
            series.events.on("shown", arrangeColumns);

            series.columns.template.tooltipText = "{categoryX} [bold]{name}[/]: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;

            // var bullet = series.bullets.push(new am4charts.LabelBullet())
            // bullet.interactionsEnabled = false
            // bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            // bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        function arrangeColumns() {

            var series = chartEksternal.series.getIndex(0);

            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                var delta = ((x1 - x0) / chartEksternal.series.length) * w;
                if (am4core.isNumber(delta)) {
                    var middle = chartEksternal.series.length / 2;

                    var newIndex = 0;
                    chartEksternal.series.each(function (series) {
                        if (!series.isHidden && !series.isHiding) {
                            series.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series.dummyData = chartEksternal.series.indexOf(series);
                        }
                    })
                    var visibleCount = newIndex;
                    var newMiddle = visibleCount / 2;

                    chartEksternal.series.each(function (series) {
                        var trueIndex = chartEksternal.series.indexOf(series);
                        var newIndex = series.dummyData;

                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                        series.animate({
                            property: "dx",
                            to: dx
                        }, series.interpolationDuration, series.interpolationEasing);
                        series.bulletsContainer.animate({
                            property: "dx",
                            to: dx
                        }, series.interpolationDuration, series.interpolationEasing);
                    })
                }
            }
        }

        createSeries("JumL", "LAKI - LAKI");
        createSeries("JumP", "PEREMPUAN");
        createSeries("total", "TOTAL");
    }

    var getPopulasiEksternal = function (form) {
        $.ajax({
            url: "/lappopulasi/chartpopeksternal",
            type: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json',
            context: document.body,
            global: false,
            async: true,
            success: function (data) {
                //console.log(data);
                chartEksternal(data);
            }
        });
    }

    var chartInternal = function (datas) {
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chartInternal = am4core.create("chartinternal", am4charts.XYChart);
        chartInternal.data = datas;
        chartInternal.colors.step = 2;

        chartInternal.legend = new am4charts.Legend()
        chartInternal.legend.position = 'top'
        chartInternal.legend.paddingBottom = 20
        chartInternal.legend.labels.template.maxWidth = 95
        
        chartInternal.exporting.menu = new am4core.ExportMenu();

        // Create axes
        var xAxis = chartInternal.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'intJabatan'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 50;
        xAxis.fontSize = 11;

        var yAxis = chartInternal.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        var label = xAxis.renderer.labels.template;
        label.truncate = true;
        label.wrap = true;
        label.maxWidth = 120;

        xAxis.events.on("sizechanged", function (ev) {
            var axis = ev.target;
            var cellWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
            if (cellWidth < axis.renderer.labels.template.maxWidth) {
                axis.renderer.labels.template.rotation = -45;
                axis.renderer.labels.template.horizontalCenter = "right";
                axis.renderer.labels.template.verticalCenter = "middle";
            } else {
                axis.renderer.labels.template.rotation = 0;
                axis.renderer.labels.template.horizontalCenter = "middle";
                axis.renderer.labels.template.verticalCenter = "top";
            }
        });

        // Create series
        function createSeries(field, name) {
            var series = chartInternal.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueY = field
            series.dataFields.categoryX = 'intJabatan'
            series.name = name

            series.events.on("hidden", arrangeColumns);
            series.events.on("shown", arrangeColumns);

            series.columns.template.tooltipText = "{categoryX} [bold]{name}[/]: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;

            // var bullet = series.bullets.push(new am4charts.LabelBullet())
            // bullet.interactionsEnabled = false
            // bullet.dy = 30;
            // bullet.label.text = '{valueY}'
            // bullet.label.fill = am4core.color('#ffffff')

            return series;
        }

        function arrangeColumns() {

            var series = chartInternal.series.getIndex(0);

            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
            if (series.dataItems.length > 1) {
                var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                var delta = ((x1 - x0) / chartInternal.series.length) * w;
                if (am4core.isNumber(delta)) {
                    var middle = chartInternal.series.length / 2;

                    var newIndex = 0;
                    chartInternal.series.each(function (series) {
                        if (!series.isHidden && !series.isHiding) {
                            series.dummyData = newIndex;
                            newIndex++;
                        } else {
                            series.dummyData = chartInternal.series.indexOf(series);
                        }
                    })
                    var visibleCount = newIndex;
                    var newMiddle = visibleCount / 2;

                    chartInternal.series.each(function (series) {
                        var trueIndex = chartInternal.series.indexOf(series);
                        var newIndex = series.dummyData;

                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                        series.animate({
                            property: "dx",
                            to: dx
                        }, series.interpolationDuration, series.interpolationEasing);
                        series.bulletsContainer.animate({
                            property: "dx",
                            to: dx
                        }, series.interpolationDuration, series.interpolationEasing);
                    })
                }
            }
        }

        createSeries("JumL", "LAKI-LAKI");
        createSeries("JumP", "PEREMPUAN");
        createSeries("total", "TOTAL");
    }

    var getPopulasiInternal = function (form) {
        $.ajax({
            url: "/lappopulasi/chartpopinternal",
            type: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json',
            context: document.body,
            global: false,
            async: true,
            success: function (data) {
                //console.log(data);
                chartInternal(data);
            }
        });
    }

    var init_datatable = function () {

        var table_eksternal = $('#kt_table_eksternal');
        var table_internal = $('#kt_table_internal');

        // begin first table
        table_eksternal.DataTable({
            responsive: true,
            paging: false,
            searching: false,
            info: false,

            // Order settings
            order: [
                [0, 'asc']
            ],
            footerCallback: function (row, data, start, end, display) {

                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };

                var juml = api.column(2).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                var jump = api.column(3).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                var total = api.column(4).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

                // Update footer
                $(api.column(2).footer()).html(KTUtil.numberString(juml.toFixed(0)));
                $(api.column(3).footer()).html(KTUtil.numberString(jump.toFixed(0)));
                $(api.column(4).footer()).html(KTUtil.numberString(total.toFixed(0)));
            },
        });

        // begin first table
        table_internal.DataTable({
            responsive: true,
            paging: false,
            searching: false,
            info: false,

            // Order settings
            order: [
                [0, 'asc']
            ],
            footerCallback: function (row, data, start, end, display) {

                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };

                var juml = api.column(2).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                var jump = api.column(3).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);
                var total = api.column(4).data().reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

                // Update footer
                $(api.column(2).footer()).html(KTUtil.numberString(juml.toFixed(0)));
                $(api.column(3).footer()).html(KTUtil.numberString(jump.toFixed(0)));
                $(api.column(4).footer()).html(KTUtil.numberString(total.toFixed(0)));
            },
        });

    };

    return {
        // public functions
        init: function () {
            handleSubmitFormShow();            
            handleClick();
        }
    };
}();

jQuery(document).ready(function () {
    FormCustom.init()
});