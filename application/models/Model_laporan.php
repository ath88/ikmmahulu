<?php
class Model_laporan extends Model_Master
{
  public function __construct()
  {
    parent::__construct();
  }

  private function internal($tahun)
  {
    $this->db->select("'99' intIds,intTahun,intJabatan,
        SUM(intF1) p1,
        SUM(intF2) p2,
        SUM(intF3) p3a,
        SUM(intF4) p3b,
        SUM(intF5) p4,
        SUM(intF6) p5,
        SUM(intF7) p6,
        SUM(intF8) p7,
        SUM(intF9) p8,
        SUM(intF10) p9,
        COUNT(intF1) r1,
        COUNT(intF2) r2,
        COUNT(intF3) r3a,
        COUNT(intF4) r3b,
        COUNT(intF5) r4,
        COUNT(intF6) r5,
        COUNT(intF7) r6,
        COUNT(intF8) r7,
        COUNT(intF9) r8,
        COUNT(intF10) r9,
        AVG(intF1) v1,
        AVG(intF2) v2,
        AVG(intF3) v3a,
        AVG(intF4) v3b,
        AVG(intF5) v4,
        AVG(intF6) v5,
        AVG(intF7) v6,
        AVG(intF8) v7,
        AVG(intF9) v8,
        AVG(intF10) v9,
        AVG(intF1)*(100/9)/4 k1,
        AVG(intF2)*(100/9)/4 k2,
        AVG(intF3)*(100/9/2)/4 k3a,
        AVG(intF4)*(100/9/2)/4 k3b,
        AVG(intF5)*(100/9)/4 k4,
        AVG(intF6)*(100/9)/4 k5,
        AVG(intF7)*(100/9)/4 k6,
        AVG(intF8)*(100/9)/4 k7,
        AVG(intF9)*(100/9)/4 k8,
        AVG(intF10)*(100/9)/4 k9,
        (
          COALESCE(AVG(intF1)*(100/9)/4,0) +
          COALESCE(AVG(intF2)*(100/9)/4,0) +
          COALESCE(AVG(intF3)*(100/9/2)/4,0) +
          COALESCE(AVG(intF4)*(100/9/2)/4,0) +
          COALESCE(AVG(intF5)*(100/9)/4,0) +
          COALESCE(AVG(intF6)*(100/9)/4,0) +
          COALESCE(AVG(intF7)*(100/9)/4,0) +
          COALESCE(AVG(intF8)*(100/9)/4,0) +
          COALESCE(AVG(intF9)*(100/9)/4,0) +
          COALESCE(AVG(intF10)*(100/9)/4,0)
        ) avRI", false);
    $this->db->from('d_internal');
    $this->db->where('intTahun', $tahun);
    $this->db->group_by('intTahun,intJabatan');
    $s1 = $this->db->get_compiled_select();

    $this->db->select("'00' intIds,intTahun,'DINAS PENDIDIKAN' intJabatan,
    SUM(p1) p1,
    SUM(p2) p2,
    SUM(p3a) p3a,
    SUM(p3b) p3b,
    SUM(p4) p4,
    SUM(p5) p5,
    SUM(p6) p6,
    SUM(p7) p7,
    SUM(p8) p8,
    SUM(p9) p9,
    SUM(r1) r1,
    SUM(r2) r2,
    SUM(r3a) r3a,
    SUM(r3b) r3b,
    SUM(r4) r4,
    SUM(r5) r5,
    SUM(r6) r6,
    SUM(r7) r7,
    SUM(r8) r8,
    SUM(r9) r9,
    AVG(v1) v1,
    AVG(v2) v2,
    AVG(v3a) v3a,
    AVG(v3b) v3b,
    AVG(v4) v4,
    AVG(v5) v5,
    AVG(v6) v6,
    AVG(v7) v7,
    AVG(v8) v8,
    AVG(v9) v9,
    AVG(k1) k1,
    AVG(k2) k2,
    AVG(k3a) k3a,
    AVG(k3b) k3b,
    AVG(k4) k4,
    AVG(k5) k5,
    AVG(k6) k6,
    AVG(k7) k7,
    AVG(k8) k8,
    AVG(k9) k9,
    AVG(avRI) avRI", false);
    $this->db->from('(' . $s1 .  ') univ');
    $s2 = $this->db->get_compiled_select();

    return $s1 . ' UNION ' . $s2 . ' ORDER BY intIds,intJabatan';
  }

  private function eksternal($tahun)
  {
    $this->db->select("'99' eksIds,eksTahun,eksJSekolah, 
    SUM(eksQ1) pv1,
    SUM(eksQ2) pv2,
    SUM(eksQ3) pv3a,
    SUM(eksQ4) pv3b,
    SUM(eksQ5) pv4,
    SUM(eksQ6) pv5a,
    SUM(eksQ7) pv5b,
    SUM(eksQ8) pv6,
    SUM(eksQ9) pv7,
    SUM(eksQ10) pv8,
    SUM(eksQ11) pv9,
    COUNT(eksQ1) cv1,
    COUNT(eksQ2) cv2,
    COUNT(eksQ3) cv3a,
    COUNT(eksQ4) cv3b,
    COUNT(eksQ5) cv4,
    COUNT(eksQ6) cv5a,
    COUNT(eksQ7) cv5b,
    COUNT(eksQ8) cv6,
    COUNT(eksQ9) cv7,
    COUNT(eksQ10) cv8,
    COUNT(eksQ11) cv9,
    AVG(eksQ1) av1,
    AVG(eksQ2) av2,
    AVG(eksQ3) av3a,
    AVG(eksQ4) av3b,
    AVG(eksQ5) av4,
    AVG(eksQ6) av5a,
    AVG(eksQ7) av5b,
    AVG(eksQ8) av6,
    AVG(eksQ9) av7,
    AVG(eksQ10) av8,
    AVG(eksQ11) av9,
    AVG(eksQ1)*(100/9)/4 kv1,
    AVG(eksQ2)*(100/9)/4 kv2,
    AVG(eksQ3)*(100/9/2)/4 kv3a,
    AVG(eksQ4)*(100/9/2)/4 kv3b,
    AVG(eksQ5)*(100/9)/4 kv4,
    AVG(eksQ6)*(100/9/2)/4 kv5a,
    AVG(eksQ7)*(100/9/2)/4 kv5b,
    AVG(eksQ8)*(100/9)/4 kv6,
    AVG(eksQ9)*(100/9)/4 kv7,
    AVG(eksQ10)*(100/9)/4 kv8,
    AVG(eksQ11)*(100/9)/4 kv9,
    (
      COALESCE(AVG(eksQ1)*(100/9)/4,0)+
      COALESCE(AVG(eksQ2)*(100/9)/4,0)+
      COALESCE(AVG(eksQ3)*(100/9/2)/4,0)+
      COALESCE(AVG(eksQ4)*(100/9/2)/4,0)+
      COALESCE(AVG(eksQ5)*(100/9)/4,0)+
      COALESCE(AVG(eksQ6)*(100/9/2)/4,0)+
      COALESCE(AVG(eksQ7)*(100/9/2)/4,0)+
      COALESCE(AVG(eksQ8)*(100/9)/4,0)+
      COALESCE(AVG(eksQ9)*(100/9)/4,0)+
      COALESCE(AVG(eksQ10)*(100/9)/4,0)+
      COALESCE(AVG(eksQ11)*(100/9)/4,0)
    ) avRE", false);
    $this->db->from('d_eksternal');
    $this->db->where('eksTahun', $tahun);
    $this->db->group_by('eksTahun,eksJSekolah');
    $s1 = $this->db->get_compiled_select();

    $this->db->select("'00' eksIds,eksTahun,'DINAS PENDIDIKAN' eksJSekolah, 
    SUM(pv1) pv1,
    SUM(pv2) pv2,
    SUM(pv3a) pv3a,
    SUM(pv3b) pv3b,
    SUM(pv4) pv4,
    SUM(pv5a) pv5a,
    SUM(pv5b) pv5b,
    SUM(pv6) pv6,
    SUM(pv7) pv7,
    SUM(pv8) pv8,
    SUM(pv9) pv9,
    SUM(cv1) cv1,
    SUM(cv2) cv2,
    SUM(cv3a) cv3a,
    SUM(cv3b) cv3b,
    SUM(cv4) cv4,
    SUM(cv5a) cv5a,
    SUM(cv5b) cv5b,
    SUM(cv6) cv6,
    SUM(cv7) cv7,
    SUM(cv8) cv8,
    SUM(cv9) cv9,
    AVG(av1) av1,
    AVG(av2) av2,
    AVG(av3a) av3a,
    AVG(av3b) av3b,
    AVG(av4) av4,
    AVG(av5a) av5a,
    AVG(av5b) av5b,
    AVG(av6) av6,
    AVG(av7) av7,
    AVG(av8) av8,
    AVG(av9) av9,
    AVG(kv1) kv1,
    AVG(kv2) kv2,
    AVG(kv3a) kv3a,
    AVG(kv3b) kv3b,
    AVG(kv4) kv4,
    AVG(kv5a) kv5a,
    AVG(kv5b) kv5b,
    AVG(kv6) kv6,
    AVG(kv7) kv7,
    AVG(kv8) kv8,
    AVG(kv9) kv9,
    AVG(avRE) avRE", false);
    $this->db->from('(' . $s1 . ') kp');
    $s2 = $this->db->get_compiled_select();

    return $s1 . ' UNION ' . $s2 . ' ORDER BY eksIds,eksJSekolah';
  }

  private function pop_eksternal($tahun)
  {
    $this->db->select("eksTahun,eksJSekolah,eksJK,COUNT(*) jumPop", false);
    $this->db->from('d_eksternal');
    $this->db->where('eksTahun', $tahun);
    $this->db->group_by('eksTahun,eksJSekolah,eksJK');

    $squery = $this->db->get_compiled_select();

    $this->db->select("eksTahun,eksJSekolah,
    SUM(CASE WHEN eksJK = 'Laki - Laki' THEN jumPop END) 'JumL',
    SUM(CASE WHEN eksJK = 'Perempuan' THEN jumPop END) 'JumP'", false);
    $this->db->from('(' . $squery . ') squery');
    $this->db->group_by('eksTahun,eksJSekolah');

    return $this->db->get_compiled_select();
  }

  private function pop_internal($tahun)
  {
    $this->db->select("intTahun,intJabatan,intJK,COUNT(*) jumPop", false);
    $this->db->from('d_internal');
    $this->db->where('intTahun', $tahun);
    $this->db->group_by('intTahun,intJabatan,intJK');

    $squery = $this->db->get_compiled_select();

    $this->db->select("intTahun,intJabatan,
    SUM(CASE WHEN intJK = 'Laki - Laki' THEN jumPop END) 'JumL',
    SUM(CASE WHEN intJK = 'Perempuan' THEN jumPop END) 'JumP'", false);
    $this->db->from('(' . $squery . ') squery');
    $this->db->group_by('intTahun,intJabatan');

    return $this->db->get_compiled_select();
  }

  function get_pop_eksternal($tahun)
  {
    $squery = $this->pop_eksternal($tahun);
    $this->db->select("datas.*,(COALESCE(JumL,0)+COALESCE(JumP,0)) total", false);
    $this->db->from('(' . $squery . ') datas');
    $this->db->group_by('eksTahun,eksJSekolah');

    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }

  function get_pop_internal($tahun)
  {
    $squery = $this->pop_internal($tahun);
    $this->db->select("datas.*,(COALESCE(JumL,0)+COALESCE(JumP,0)) total", false);
    $this->db->from('(' . $squery . ') datas');
    $this->db->group_by('intTahun,intJabatan');

    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }

  function get_eksternal($tahun)
  {
    $squery = $this->eksternal($tahun);
    $this->db->select("datas.*,
                      CASE 
                        WHEN avRE > 81.26 THEN 'A'
                        WHEN avRE > 62.51 THEN 'B'
                        WHEN avRE > 43.76 THEN 'C'
                        ELSE 'D' END mutu", false);
    $this->db->from('(' . $squery . ') datas', false);
    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }

  function get_internal($tahun)
  {
    $squery = $this->internal($tahun);
    $this->db->select("datas.*,
                      CASE 
                        WHEN avRI > 81.26 THEN 'A'
                        WHEN avRI > 62.51 THEN 'B'
                        WHEN avRI > 43.76 THEN 'C'
                        ELSE 'D' END mutu", false);
    $this->db->from('(' . $squery . ') datas', false);

    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }

  function get_nilaiakhir($tahun)
  {
    $in = $this->internal($tahun);
    $ek = $this->eksternal($tahun);

    $this->db->select("eksternal.eksIds,avRE", false);
    $this->db->from("(" . $ek . ") eksternal", false);
    $this->db->where("eksternal.eksIds = '00'");
    $s0 = $this->db->get_compiled_select();

    $this->db->select("internal.intIds,avRI", false);
    $this->db->from("(" . $in . ") internal", false);
    $this->db->where("internal.intIds = '00'");
    $s1 = $this->db->get_compiled_select();

    $this->db->select("eksIds Ids,avRE,avRI", false);
    $this->db->from("(" . $s0 . ") s0", false);
    $this->db->join("(" . $s1 . ") s1", "eksIds = intIds");
    $s2 = $this->db->get_compiled_select();

    $this->db->select("Ids,avRE,avRI,((COALESCE(avRE,0)+COALESCE(avRI,0))/2) avNA", false);
    $this->db->from("(" . $s2 . ") nilaiakhir", false);
    $s3 = $this->db->get_compiled_select();

    $this->db->select("datas.*,
                      CASE 
                        WHEN avNA > 81.26 THEN 'A'
                        WHEN avNA > 62.51 THEN 'B'
                        WHEN avNA > 43.76 THEN 'C'
                        ELSE 'D' END mutu", false);
    $this->db->from('(' . $s3 . ') datas', false);

    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }

  function get_responden_eksternal($field, $tahun, $unit)
  {
    $this->db->select("COUNT(" . $field . ") tt");
    $this->db->from("d_eksternal");
    $this->db->where('eksTahun', $tahun);
    if (!in_array($unit, ['DINAS PENDIDIKAN']))
      $this->db->where('eksJSekolah', $unit);
    $tt = $this->db->get_compiled_select();

    $this->db->select($field . ",COUNT(" . $field . ") freq,COUNT(" . $field . ")/tt*100 percent");
    $this->db->from("d_eksternal");
    $this->db->from("(" . $tt . ") t");
    $this->db->where('eksTahun', $tahun);
    $this->db->where($field . ' > 0');
    if (!in_array($unit, ['DINAS PENDIDIKAN']))
      $this->db->where('eksJSekolah', $unit);
    $this->db->group_by($field);
    $this->db->order_by($field);
    $dt = $this->db->get_compiled_select();

    $this->db->select("*");
    $this->db->from("r_skala");
    $this->db->join('(' . $dt . ') dt', 'skId = ' . $field, 'LEFT');

    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }

  function get_responden_internal($field, $tahun, $unit)
  {
    $this->db->select("COUNT(" . $field . ") tt");
    $this->db->from("d_internal");
    $this->db->where('intTahun', $tahun);
    if (!in_array($unit, ['DINAS PENDIDIKAN']))
      $this->db->where('intJabatan', $unit);
    $tt = $this->db->get_compiled_select();

    $this->db->select($field . ",COUNT(" . $field . ") freq,COUNT(" . $field . ")/tt*100 percent");
    $this->db->from("d_internal");
    $this->db->from("(" . $tt . ") t");
    $this->db->where('intTahun', $tahun);
    $this->db->where($field . ' > 0');
    if (!in_array($unit, ['DINAS PENDIDIKAN']))
      $this->db->where('intJabatan', $unit);
    $this->db->group_by($field);
    $this->db->order_by($field);
    $dt = $this->db->get_compiled_select();

    $this->db->select("*");
    $this->db->from("r_skala");
    $this->db->join('(' . $dt . ') dt', 'skId = ' . $field, 'LEFT');

    $qr = $this->db->get();
    if ($qr->num_rows() > 0)
      return $qr->result();
    else
      return false;
  }
}
