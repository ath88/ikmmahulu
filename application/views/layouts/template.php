<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">
<!-- begin::Head -->

<head>
	<meta charset="utf-8" />
	<title>
		<?= base_url(); ?>
	</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--begin::Web font -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Web font -->
	<!--begin::Base Styles -->
	<!--begin::Page Vendors -->
	<link href="<?= base_url(); ?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendors -->
	<link href="<?= base_url(); ?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Base Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="<?= base_url(); ?>assets/css/skins/header/base/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/skins/header/menu/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url(); ?>assets/css/skins/aside/light.css" rel="stylesheet" type="text/css" />
	<!-- <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon_bak2.ico" /> -->

	<link href="https://unpkg.com/tableexport/dist/css/tableexport.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.1.1/css/ol.css" type="text/css">
	<style>
		.map {
			height: 400px;
			width: 100%;
		}
	</style>
</head>
<!-- end::Head -->
<!-- end::Body -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
	<!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="index.html">
				<div style="position:absolute;top:30%;left:14px;font-size:16px;font-weight:bold;color:aliceblue">
					Dinas Pendidikan dan Kebudayaan - Kabupaten Mahakam Ulu
				</div>
				<!-- <img alt="Logo" src="assets/media/logos/logo-light.png" /> -->
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
			<!-- <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button> -->
			<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->


	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<?php $this->load->view('layouts/sidebar'); ?>

			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<?php $this->load->view('layouts/header'); ?>

				<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">



					<!-- begin:: Content -->
					<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

						<!--Begin::Dashboard 1-->

						<?php
						if (!empty($page))
							$this->load->view($page);
						else
							$this->load->view('layouts/error_page');
						?>

						<!--End::Dashboard 1-->
					</div>

					<!-- end:: Content -->
				</div>

				<?php $this->load->view('layouts/footer'); ?>

			</div>
		</div>
	</div>

	<!-- end:: Page -->

	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>

	<!-- end::Scrolltop -->

	<!-- begin::Global Config(global config for global JS sciprts) -->
	<script>
		var KTAppOptions = {
			"colors": {
				"state": {
					"brand": "#5d78ff",
					"dark": "#282a3c",
					"light": "#ffffff",
					"primary": "#5867dd",
					"success": "#34bfa3",
					"info": "#36a3f7",
					"warning": "#ffb822",
					"danger": "#fd3995"
				},
				"base": {
					"label": [
						"#c5cbe3",
						"#a1a8c3",
						"#3d4465",
						"#3e4466"
					],
					"shape": [
						"#f0f3ff",
						"#d9dffa",
						"#afb4d4",
						"#646c9a"
					]
				}
			}
		};
	</script>

	<!-- end::Global Config -->

	<!--begin::Base Scripts -->
	<script src="<?= base_url(); ?>assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/js/scripts.bundle.js" type="text/javascript"></script>

	<!--begin::Page Vendors -->
	<script src="<?= base_url(); ?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<!--end::Page Vendors -->
	<!--begin::Page Snippets -->
	<script src="<?= base_url(); ?>assets/plugins/custom/flot/flot.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/js/pages/custom/pages/dashboard.js" type="text/javascript"></script>
	<script src="<?= base_url(); ?>assets/js/pages/custom/pages/form-submit-general.js" type="text/javascript"></script>
	<!--end::Page Snippets -->

	<!--begin::Page Amcharts -->
	<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
	<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
	<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
	<!--end::Page Amcharts -->

	<!--begin::Page TableExport -->
	<script src="https://rawcdn.githack.com/hhurz/tableExport.jquery.plugin/ac867b593f515e2e920aed075e895757f51ee0e4/libs/js-xlsx/xlsx.core.min.js"></script>
	<script src="https://rawcdn.githack.com/hhurz/tableExport.jquery.plugin/ac867b593f515e2e920aed075e895757f51ee0e4/libs/FileSaver/FileSaver.min.js"></script>
	<script src="https://rawcdn.githack.com/hhurz/tableExport.jquery.plugin/ac867b593f515e2e920aed075e895757f51ee0e4/libs/html2canvas/html2canvas.min.js"></script>
	<script src="https://rawcdn.githack.com/hhurz/tableExport.jquery.plugin/ac867b593f515e2e920aed075e895757f51ee0e4/tableExport.min.js"></script>
	<!--end::Page TableExport -->

	<!--begin:: OpenStreetMaps -->
	<script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.1.1/build/ol.js"></script>
	<script type="text/javascript">
		var map = new ol.Map({
			target: 'map',
			layers: [
				new ol.layer.Tile({
					source: new ol.source.OSM()
				})
			],
			view: new ol.View({
				center: ol.proj.fromLonLat([115.27190480, 0.50111530]),
				zoom: 10.5
			})
		});

		function createMarker(lng, lat, description, type) {
			var marker = new ol.Feature({
				geometry: new ol.geom.Point(
					ol.proj.fromLonLat([lng, lat])
				),
			});
			var colorMarker = '#5867dd';

			if (type == 1)
				colorMarker = '#34bfa3';
			else if (type == 2)
				colorMarker = '#36a3f7';
			else if (type == 3)
				colorMarker = '#ffb822';
			else if (type == 4)
				colorMarker = '#fa1313';
			marker.setStyle(new ol.style.Style({
				image: new ol.style.Icon( /** @type {olx.style.IconOptions} */ ({
					color: colorMarker,
					crossOrigin: 'anonymous',
					src: 'https://openlayers.org/en/v4.6.5/examples/data/dot.png'
				}))
			}));
			marker.setProperties({
				'name': lng + ';' + lat,
				'description': description
			});
			return marker;
		}
		<?php
		$i = 1;
		$vmarker = $fmarker = '';
		if (isset($KoorLatLng)) {
			if ($KoorLatLng != false) {
				foreach ($KoorLatLng as $row) {
					$description = str_replace("'", "", $row->Lokasi);
					$vmarker .= "var marker$i = createMarker(" . $row->Lng . "," . $row->Lat . ",'" . $description . "'," . $row->Type . ");\n";
					$fmarker .= "marker$i, ";
					$i++;
				}
				$fmarker = substr($fmarker, 0, strlen($fmarker) - 2);
			}
		}
		?>
		<?= $vmarker ?>
		var vectorSource = new ol.source.Vector({
			features: [<?= $fmarker ?>]
		});
		var markerVectorLayer = new ol.layer.Vector({
			source: vectorSource,
		});

		map.addLayer(markerVectorLayer);

		// display popup on click
		var container = document.getElementById('popup');
		var content = document.getElementById('popup-content');
		var closer = document.getElementById('popup-closer');

		map.on('click', function(evt) {
			var feature = map.forEachFeatureAtPixel(evt.pixel,
				function(feature, layer) {
					return feature;
				});
			if (feature) {
				console.log(feature);
				var overlay = new ol.Overlay({
					element: container,
					autoPan: true,
					autoPanAnimation: {
						duration: 250
					}
				});
				map.addOverlay(overlay);

				var geometry = feature.getGeometry();
				var coord = geometry.getCoordinates();

				//coordTransform = ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
				var contentHTML = 'Location: ' + feature.get('name');
				contentHTML += '<br/> Description: ' + feature.get('description');
				content.innerHTML = contentHTML;

				overlay.setPosition(coord);
			}
		});
	</script>
	<!--end:: OpenStreetMaps -->

	<!--begin::Custom Page -->
	<?php if (isset($scripts)) : ?>
		<?php foreach ($scripts as $script) : ?>
			<script type="text/javascript" src="<?= base_url(); ?>assets/js/pages/custom/pages/<?= $script ?>.js"></script>
		<?php endforeach; ?>
	<?php endif; ?>
	<!--end::Custom Page -->
</body>
<!-- end::Body -->

</html>