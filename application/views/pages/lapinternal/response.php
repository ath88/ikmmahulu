<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> IKM
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-checkable" id="kt_table_internal">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Jenis Pegawai</th>
                                            <th>P1</th>
                                            <th>P2</th>
                                            <th>P3a</th>
                                            <th>P3b</th>
                                            <th>P4</th>
                                            <th>P5</th>
                                            <th>P6</th>
                                            <th>P7</th>
                                            <th>P8</th>
                                            <th>P9</th>
                                            <th>IKM</th>
                                            <th>MUTU</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $r1 = $r2 = $r3a = $r3b = $r4 = $r5 = $r6 = $r7 = $r8 = $r9 = $rr = 0;
                                            foreach ($datas as $row) {
                                                $key = json_encode(['unit' => $row->intJabatan, 'tahun' => $tahun]);
                                                $key = $this->encryptions->encode($key, $this->config->item('encryption_key'));
                                                //$key = $this->encryptions->encode($row->kknsId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->intJabatan ?></td>
                                                    <td class="text-right"><?= number_format($row->k1,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k2,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k3a,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k3b,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k4,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k5,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k6,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k7,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k8,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->k9,2) ?></td>
                                                    <td class="text-right"><a href="<?= $responden_url . '/' . $key ?>" id="responden<?= $i ?>" class="responden"><?= number_format($row->avRI, 3) ?></a></td>
                                                    <td class="text-center"><?= $row->mutu ?></td>                                                     
                                                </tr>
                                        <?php
                                            $r1 += $row->k1;
                                            $r2 += $row->k2;
                                            $r3a += $row->k3a;
                                            $r3b += $row->k3b;
                                            $r4 += $row->k4;
                                            $r5 += $row->k5;
                                            $r6 += $row->k6;
                                            $r7 += $row->k7;
                                            $r8 += $row->k8;
                                            $r9 += $row->k9;
                                            $rr += $row->avRI;
                                            }
                                        ?>
                                        <?php /**?>
                                            <tr>
                                                <th scope="row" colspan="2">Rata-Rata</th>
                                                <td class="text-right"><?= number_format($r1,3) ?></td>
                                                <td class="text-right"><?= number_format($r2,3) ?></td>
                                                <td class="text-right"><?= number_format($r3a,3) ?></td>
                                                <td class="text-right"><?= number_format($r3b,3) ?></td>
                                                <td class="text-right"><?= number_format($r4,3) ?></td>
                                                <td class="text-right"><?= number_format($r5,3) ?></td>
                                                <td class="text-right"><?= number_format($r6,3) ?></td>
                                                <td class="text-right"><?= number_format($r7,3) ?></td>
                                                <td class="text-right"><?= number_format($r8,3) ?></td>
                                                <td class="text-right"><?= number_format($r9,3) ?></td>
                                                <td class="text-right"><?= number_format($rr,3) ?></td>                                                    
                                            </tr>
                                        <?php**/ ?>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div id="chartikminternal" style="width: 100%!important;height: 500px;"></div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> PARTSIPASI RESPONDEN
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">Jenis Pegawai</th>
                                            <th>P1</th>
                                            <th>P2</th>
                                            <th>P3a</th>
                                            <th>P3b</th>
                                            <th>P4</th>
                                            <th>P5</th>
                                            <th>P6</th>
                                            <th>P7</th>
                                            <th>P8</th>
                                            <th>P9</th>
                                            <th>Responden</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $r1 = $r2 = $r3a = $r3b = $r4 = $r5 = $r6 = $r7 = $r8 = $r9 = $rr = 0;
                                            foreach ($datas as $row) {
                                                //$key = $this->encryptions->encode($row->kknsId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->intJabatan ?></td>
                                                    <td class="text-right"><?= number_format($row->p1,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p2,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p3a,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p3b,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p4,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p5,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p6,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p7,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p8,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->p9,2) ?></td>
                                                    <td class="text-right"><?= number_format($row->r1,3) ?></td>                                             
                                                </tr>
                                        <?php
                                            $r1 += $row->p1;
                                            $r2 += $row->p2;
                                            $r3a += $row->p3a;
                                            $r3b += $row->p3b;
                                            $r4 += $row->p4;
                                            $r5 += $row->p5;
                                            $r6 += $row->p6;
                                            $r7 += $row->p7;
                                            $r8 += $row->p8;
                                            $r9 += $row->p9;
                                            $rr += $row->r1;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->