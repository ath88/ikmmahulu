<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> IKM <?= $unit != false ? strtoupper($unit->unitNama) : '' ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <?php
                    foreach ($datas as $row) :
                    ?>
                        <!--begin::Section-->
                        <div class="kt-section">
                            <span class="kt-section__info">
                                <?= $row->display ?>
                            </span>
                            <div class="kt-section__content">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>No</th>
                                                <th>Responden</th>
                                                <th>Frequency</th>
                                                <th>Percent</th>
                                                <th>Cumulative Percent</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($row->datas != false) {
                                                $i = 1;
                                                $qp = $rt = 0;
                                                foreach ($row->datas as $row2) {
                                            ?>
                                                    <tr>
                                                        <th scope="row"><?= $i++ ?></th>
                                                        <td class="text-right"><?= number_format($row2->skId, 0) ?></td>
                                                        <td class="text-right"><?= number_format($row2->freq, 2) ?></td>
                                                        <td class="text-right"><?= number_format($row2->percent, 2) ?></td>
                                                        <td class="text-right"><?= number_format(($qp += $row2->percent), 2) ?></td>
                                                    </tr>
                                                <?php
                                                    $rt += $row2->freq;
                                                }
                                                ?>
                                                <tr>
                                                    <th scope="row" colspan="2">Total</th>
                                                    <td class="text-right"><?= number_format($rt, 2) ?></td>
                                                    <td class="text-right">&nbsp;</td>
                                                    <td class="text-right">&nbsp;</td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!--end::Section-->
                    <?php endforeach; ?>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->