<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> EKSTERNAL
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-checkable" id="kt_table_eksternal">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Jenis Sekolah</th>
                                            <th>Laki - Laki</th>
                                            <th>Perempuan</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($popeks != false) {
                                            $i = 1;
                                            foreach ($popeks as $row) {
                                                //$key = $this->encryptions->encode($row->kknsId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->eksJSekolah ?></td>
                                                    <td class="text-right"><?= number_format($row->JumL, 0) ?></td>
                                                    <td class="text-right"><?= number_format($row->JumP, 0) ?></td>
                                                    <td class="text-right"><?= number_format($row->total, 0) ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>TOTAL</th>
                                            <th class="text-right">JUML</th>
                                            <th class="text-right">JUMP</th>
                                            <th class="text-right">Total</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div id="charteksternal" style="width: 100%!important;height: 500px;"></div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> INTERNAL
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-checkable" id="kt_table_internal">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Jenis Pegawai</th>
                                            <th>Laki - Laki</th>
                                            <th>Perempuan</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($popeks != false) {
                                            $i = 1;
                                            foreach ($popint as $row) {
                                                //$key = $this->encryptions->encode($row->kknsId, $this->config->item('encryption_key'));
                                                $total
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->intJabatan ?></td>
                                                    <td class="text-right"><?= number_format($row->JumL, 0) ?></td>
                                                    <td class="text-right"><?= number_format($row->JumP, 0) ?></td>
                                                    <td class="text-right"><?= number_format($row->total, 0) ?></td>
                                                </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>TOTAL</th>
                                            <th class="text-right">JUML</th>
                                            <th class="text-right">JUMP</th>
                                            <th class="text-right">Total</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div id="chartinternal" style="width: 100%!important;height: 500px;"></div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->