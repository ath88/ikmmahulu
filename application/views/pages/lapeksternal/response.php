<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> IKM
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-checkable" id="kt_table_eksternal">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Jenis Sekolah</th>
                                            <th>P1</th>
                                            <th>P2</th>
                                            <th>P3a</th>
                                            <th>P3b</th>
                                            <th>P4</th>
                                            <th>P5a</th>
                                            <th>P5b</th>
                                            <th>P6</th>
                                            <th>P7</th>
                                            <th>P8</th>
                                            <th>P9</th>
                                            <th>IKM</th>
                                            <th>MUTU</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $r1 = $r2 = $r3a = $r3b = $r4 = $r5a = $r5b = $r6 = $r7 = $r8 = $r9 = $rr = 0;
                                            foreach ($datas as $row) {
                                                $key = json_encode(['unit' => $row->eksJSekolah, 'tahun' => $tahun]);
                                                $key = $this->encryptions->encode($key, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->eksJSekolah ?></td>
                                                    <td class="text-right"><?= number_format($row->kv1, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv2, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv3a, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv3b, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv4, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv5a, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv5b, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv6, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv7, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv8, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->kv9, 2) ?></td>
                                                    <td class="text-right"><a href="<?= $responden_url . '/' . $key ?>" id="responden<?= $i ?>" class="responden"><?= number_format($row->avRE, 3) ?></a></td>
                                                    <td class="text-center"><?= $row->mutu ?></td>
                                                </tr>
                                            <?php
                                                $r1 += $row->kv1;
                                                $r2 += $row->kv2;
                                                $r3a += $row->kv3a;
                                                $r3b += $row->kv3b;
                                                $r4 += $row->kv4;
                                                $r5a += $row->kv5a;
                                                $r5b += $row->kv5b;
                                                $r6 += $row->kv6;
                                                $r7 += $row->kv7;
                                                $r8 += $row->kv8;
                                                $r9 += $row->kv9;
                                                $rr += $row->avRE;
                                            }
                                            ?>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div id="chartikmeksternal" style="width: 100%!important;height: 500px;"></div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?> PARTSIPASI RESPONDEN
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th rowspan="2">No</th>
                                            <th rowspan="2">Jenis Sekolah</th>
                                            <th>P1</th>
                                            <th>P2</th>
                                            <th>P3a</th>
                                            <th>P3b</th>
                                            <th>P4</th>
                                            <th>P5a</th>
                                            <th>P5b</th>
                                            <th>P6</th>
                                            <th>P7</th>
                                            <th>P8</th>
                                            <th>P9</th>
                                            <th>Responden</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $r1 = $r2 = $r3a = $r3b = $r4 = $r5a = $r5b = $r6 = $r7 = $r8 = $r9 = $rr = 0;
                                            foreach ($datas as $row) {
                                                //$key = $this->encryptions->encode($row->kknsId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td><?= $row->eksJSekolah ?></td>
                                                    <td class="text-right"><?= number_format($row->pv1, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv2, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv3a, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv3b, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv4, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv5a, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv5b, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv6, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv7, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv8, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->pv9, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->cv1, 3) ?></td>
                                                </tr>
                                        <?php
                                                $r1 += $row->pv1;
                                                $r2 += $row->pv2;
                                                $r3a += $row->pv3a;
                                                $r3b += $row->pv3b;
                                                $r4 += $row->pv4;
                                                $r5a += $row->pv5a;
                                                $r5b += $row->pv5b;
                                                $r6 += $row->pv6;
                                                $r7 += $row->pv7;
                                                $r8 += $row->pv8;
                                                $r9 += $row->pv9;
                                                $rr += $row->cv1;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->