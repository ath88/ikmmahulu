<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                </div>

                <div class="kt-portlet__body">
                    <div class="kt-section__info">
                        <strong>Data <?= $datas['nama_sekolah'] ?></strong>
                    </div>
                    <!-- <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                        <div class="kt-widget__body">
                            <div class="kt-widget__content">
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Email:</span>
                                    <a href="#" class="kt-widget__data">matt@fifestudios.com</a>
                                </div>
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Phone:</span>
                                    <a href="#" class="kt-widget__data">44(76)34254578</a>
                                </div>
                                <div class="kt-widget__info">
                                    <span class="kt-widget__label">Location:</span>
                                    <span class="kt-widget__data">Melbourne</span>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="kt-separator kt-separator--space-lg kt-separator--border-solid"></div>

                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-primary" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#kt_tabs_lahan" role="tab">Lahan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_tanda_bangunan" role="tab">Bangunan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#kt_tabs_tanda_prasarana" role="tab">Prasarana</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_tabs_lahan" role="tabpanel">
                            <div class="input-group">
                                <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                            </div>
                                            <?php
                                            $j = 1;
                                            $label = 'A';
                                            for ($i = 13; $i <= 16; $i++) :
                                                if (isset($def[($label . $i)])) :
                                            ?>
                                                    <div class="kt-widget__info">
                                                        <span class="kt-widget__label <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                        <span class="kt-widget__data <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $datas[($label . $i)] == 1 ? 'YA' : 'TIDAK' ?></span>
                                                    </div>
                                            <?php endif;
                                            endfor; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                            </div>
                                            <?php
                                            $label = 'A';
                                            for ($i = 17; $i <= 21; $i++) :
                                                if (isset($def[($label . $i)])) :
                                            ?>
                                                    <div class="kt-widget__info">
                                                        <span class="kt-widget__label <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                        <span class="kt-widget__data <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $datas[($label . $i)] == 1 ? 'YA' : 'TIDAK' ?></span>
                                                    </div>
                                            <?php endif;
                                            endfor; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="kt_tabs_tanda_bangunan" role="tabpanel">
                            <div class="input-group">
                                <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                            </div>
                                            <?php
                                            $j = 1;
                                            $label = 'B';
                                            for ($i = 4; $i <= 20; $i++) :
                                                if (isset($def[($label . $i)])) :
                                            ?>
                                                    <div class="kt-widget__info">
                                                        <span class="kt-widget__label <?= in_array(($label . $i), ['B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11']) ? '' : ($datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success') ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                        <span class="kt-widget__data <?= in_array(($label . $i), ['B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11']) ? '' : ($datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success') ?>"><?= in_array(($label . $i), ['B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11']) ? $datas[($label . $i)] : ($datas[($label . $i)] == 1 ? 'YA' : 'TIDAK') ?></span>
                                                    </div>
                                            <?php endif;
                                            endfor; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                            </div>
                                            <?php
                                            $label = 'B';
                                            for ($i = 21; $i <= 31; $i++) :
                                                if (isset($def[($label . $i)])) :
                                            ?>
                                                    <div class="kt-widget__info">
                                                        <span class="kt-widget__label <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                        <span class="kt-widget__data <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $datas[($label . $i)] == 1 ? 'YA' : 'TIDAK' ?></span>
                                                    </div>
                                            <?php endif;
                                            endfor; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="kt_tabs_tanda_prasarana" role="tabpanel">
                            <div class="input-group">
                                <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                            </div>
                                            <?php
                                            $j = 1;
                                            $label = 'C';
                                            for ($i = 4; $i <= 54; $i++) :
                                                if (isset($def[($label . $i)])) :
                                            ?>
                                                    <div class="kt-widget__info">
                                                        <span class="kt-widget__label <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                        <span class="kt-widget__data <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $datas[($label . $i)] == 1 ? 'YA' : 'TIDAK' ?></span>
                                                    </div>
                                            <?php endif;
                                            endfor; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                            </div>
                                            <?php
                                            $label = 'C';
                                            for ($i = 55; $i <= 106; $i++) :
                                                if (isset($def[($label . $i)])) :
                                            ?>
                                                    <div class="kt-widget__info">
                                                        <span class="kt-widget__label <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                        <span class="kt-widget__data <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $datas[($label . $i)] == 1 ? 'YA' : 'TIDAK' ?></span>
                                                    </div>
                                            <?php endif;
                                            endfor; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if (isset($def[('C' . 166)])) : ?>
                                <div class="input-group">
                                    <div class="kt-widget kt-widget--user-profile-1 col-lg-6">
                                        <div class="kt-widget__body">
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__info">
                                                    <span class="kt-widget__label kt-font-boldest">Identitas</span>
                                                    <span class="kt-widget__label kt-font-boldest">Ketersediaan</span>
                                                </div>
                                                <?php
                                                $label = 'C';
                                                for ($i = 107; $i <= 166; $i++) :
                                                    if (isset($def[($label . $i)])) :
                                                ?>
                                                        <div class="kt-widget__info">
                                                            <span class="kt-widget__label <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $j++ . '. ' ?><?= $def[($label . $i)] ?>:</span>
                                                            <span class="kt-widget__data <?= $datas[($label . $i)] == 0 ? 'kt-font-danger' : 'kt-font-success' ?>"><?= $datas[($label . $i)] == 1 ? 'YA' : 'TIDAK' ?></span>
                                                        </div>
                                                <?php endif;
                                                endfor; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->