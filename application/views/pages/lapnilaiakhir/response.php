<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        &nbsp;
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-checkable" id="kt_table_ikm">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Internal</th>
                                            <th>Eksternal</th>
                                            <th>Nilai Akhir</th>
                                            <th>Mutu</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($datas != false) {
                                            $i = 1;
                                            $internal = $eksternal = $nilaiakhir = 0;
                                            foreach ($datas as $row) {
                                                //$key = $this->encryptions->encode($row->kknsId, $this->config->item('encryption_key'));
                                        ?>
                                                <tr>
                                                    <th scope="row"><?= $i++ ?></th>
                                                    <td class="text-right"><?= number_format($row->avRI, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->avRE, 2) ?></td>
                                                    <td class="text-right"><?= number_format($row->avNA, 2) ?></td>
                                                    <td><?= $row->mutu ?></td>
                                                </tr>
                                        <?php
                                                $internal += $row->avRI;
                                                $eksternal += $row->avRE;
                                                $nilaiakhir += $row->avNA;
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <!--end::Section-->
                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div id="chartikm" style="width: 100%!important;height: 500px;"></div>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->