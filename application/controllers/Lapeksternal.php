<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class lapeksternal extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/lapeksternal/';
        $this->_path_js = 'controller/';
        $this->_judul = 'Eksternal';
        $this->_controller_name = 'lapeksternal';
        $this->_model_name = 'model_laporan';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . $this->_controller_name];
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);

                $tahun = $data['tahun'] = $this->input->post('tahun');

                $data['datas'] = $this->{$this->_model_name}->get_eksternal($tahun);
                $data['responden_url'] = site_url($this->_controller_name . '/responden') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function responden()
    {
        if (IS_AJAX) {
            $data = $this->get_master($this->_path_page . $this->_page_index);

            $key = json_decode($this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key')));

            if (in_array($key->unit, ['DINAS PENDIDIKAN'])) :
                $data['unit'] = (object)['unitNama' => 'DINAS PENDIDIKAN'];
            else :
                $data['unit'] = (object)['unitNama' => $key->unit];
            endif;
            $data['datas'] = [
                (object)[
                    'name' => 'eksQ1',
                    'display' => 'P1: PERSYARATAN TEKNIS DAN ADMINISTRASI',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ1', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ2',
                    'display' => 'P2: TATA CARA PELAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ2', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ3',
                    'display' => 'P3A: KECEPATAN WAKTU PELAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ3', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ4',
                    'display' => 'P3B: KETEPATAN WAKTU PELAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ4', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ5',
                    'display' => 'P4: KESESUAIAN BIAYA DENGAN MUTU LAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ5', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ6',
                    'display' => 'P5A: KESESUAIAN HASIL LAYANAN AKADEMIK DENGAN JANJI LAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ6', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ7',
                    'display' => 'P5B: KESESUAIAN HASIL LAYANAN KEMAHASISWAAN DENGAN JANJI LAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ7', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ8',
                    'display' => 'P6: KEMAMPUAN DAN KOMPETENSI SDM PEMBERI LAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ8', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ9',
                    'display' => 'P7: SIKAP DAN PERILAKU PEMBERI LAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ9', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ10',
                    'display' => 'P8: TANGGUNGJAWAB PEMBERI LAYANAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ10', $key->tahun, $key->unit)
                ],
                (object)[
                    'name' => 'eksQ11',
                    'display' => 'P9: PENANGANAN PENGADUAN, SARAN DAN MASUKKAN',
                    'datas' => $this->{$this->_model_name}->get_responden_eksternal('eksQ11', $key->tahun, $key->unit)
                ],
            ];

            $pages = $this->_path_page . 'responden';
            $this->load->view($pages, $data);
        }
    }

    function chartikmeksternal()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);

                $tahun = $this->input->post('tahun');
                $result = $this->{$this->_model_name}->get_eksternal($tahun);
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }
}
