<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class lappopulasi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/lappopulasi/';
        $this->_path_js = 'controller/';
        $this->_judul = 'Populasi';
        $this->_controller_name = 'lappopulasi';
        $this->_model_name = 'model_laporan';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . $this->_controller_name];
        $data['show_url'] = site_url($this->_controller_name . '/response') . '/';
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);

                $tahun = $this->input->post('tahun');

                $data['popeks'] = $this->{$this->_model_name}->get_pop_eksternal($tahun);
                $data['popint'] = $this->{$this->_model_name}->get_pop_internal($tahun);
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    function chartpopeksternal()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);

                $tahun = $this->input->post('tahun');
                $result = $this->{$this->_model_name}->get_pop_eksternal($tahun);
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    function chartpopinternal()
    {
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);

                $tahun = $this->input->post('tahun');
                $result = $this->{$this->_model_name}->get_pop_internal($tahun);
                header('Content-Type: application/json');
                echo json_encode($result);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }
}
