<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class infrastruktur extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/infrastruktur/';
        $this->_path_js = 'controller/';
        $this->_judul = 'Infrastruktur';
        $this->_path_file = '../upload_files/';
        $this->_controller_name = 'infrastruktur';
        $this->_model_name = 'model_infrastruktur';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    private function datassd($files)
    {
        if (file_exists($files)) {
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($files);
            $cek = true;
            for ($j = 1; $j < $spreadsheet->getSheetCount(); $j++) {
                $i = 0;
                $worksheet = $spreadsheet->getSheet($j)->toArray(null, true, false, true);
                foreach ($worksheet as $row) {
                    if ($i >= 5 and !empty($row['C'])) {
                        if ($j == 1) {
                            $newrow[$i] = [
                                'Id' => $i,
                                'nama_sekolah' => $row['C']
                            ];
                            $a = 1;
                            foreach ($row as $srow) {
                                if ($a >= 13 and $a <= 21) {
                                    $newrow[$i]['A' . $a] = $srow;
                                    $cek = (($srow == 0 or $cek == false) ? false : true);
                                }
                                $a++;
                            }
                        } elseif ($j == 2) {
                            $b = 1;
                            foreach ($row as $srow) {
                                if ($b >= 4 and $b <= 31) {
                                    $newrow[$i]['B' . $b] = $srow;
                                    $cek = (($srow == 0 or $cek == false) ? false : true);
                                }
                                $b++;
                            }
                        } elseif ($j == 3) {
                            $c = 1;
                            foreach ($row as $srow) {
                                if ($c >= 4 and $c <= 107) {
                                    $newrow[$i]['C' . $c] = $srow;
                                    $cek = (($srow == 0 or $cek == false) ? false : true);
                                }
                                $c++;
                            }
                        }
                    }
                    if (isset($newrow[$i]['Id'])) {
                        $newrow[$i]['status'] = $cek;
                        $KoorLatLng[$i] = (object)['Lng' => 115.24167760 + mt_rand(-0.1 * 1000000, 0.1 * 1000000) / 1000000, 'Lat' => 0.52094150 + mt_rand(-0.05 * 1000000, 0.05 * 1000000) / 1000000, 'Lokasi' => $row['C'], 'Type' => $cek ? 1 : 4];
                    }
                    $i++;
                }
            }

            return ['row' => $newrow, 'koor' => $KoorLatLng];
        } else
            return false;
    }

    private function datassmp($files)
    {
        if (file_exists($files)) {
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($files);
            $cek = true;
            for ($j = 1; $j < $spreadsheet->getSheetCount(); $j++) {
                $i = 0;
                $worksheet = $spreadsheet->getSheet($j)->toArray(null, true, false, true);
                foreach ($worksheet as $row) {
                    if ($i >= 5 and !empty($row['C'])) {
                        if ($j == 1) {
                            $newrow[$i] = [
                                'Id' => $i,
                                'nama_sekolah' => $row['C']
                            ];
                            $a = 1;
                            foreach ($row as $srow) {
                                if ($a >= 13 and $a <= 21) {
                                    $newrow[$i]['A' . $a] = $srow;
                                    $cek = (($srow == 0 or $cek == false) ? false : true);
                                }
                                $a++;
                            }
                        } elseif ($j == 2) {
                            $b = 1;
                            foreach ($row as $srow) {
                                if ($b >= 4 and $b <= 31) {
                                    $newrow[$i]['B' . $b] = $srow;
                                    $cek = (($srow == 0 or $cek == false) ? false : true);
                                }
                                $b++;
                            }
                        } elseif ($j == 3) {
                            $c = 1;
                            foreach ($row as $srow) {
                                if ($c >= 4 and $c <= 169) {
                                    $newrow[$i]['C' . $c] = $srow;
                                    $cek = (($srow == 0 or $cek == false) ? false : true);
                                }
                                $c++;
                            }
                        }
                    }
                    if (isset($newrow[$i]['Id'])) {
                        $newrow[$i]['status'] = $cek;
                        $KoorLatLng[$i] = (object)['Lng' => 115.24167760 + mt_rand(-0.1 * 1000000, 0.1 * 1000000) / 1000000, 'Lat' => 0.52094150 + mt_rand(-0.05 * 1000000, 0.05 * 1000000) / 1000000, 'Lokasi' => $row['C'], 'Type' => $cek ? 1 : 4];
                    }
                    $i++;
                }
            }
            return ['row' => $newrow, 'koor' => $KoorLatLng];
        } else
            return false;
    }

    private function datasmarker($files)
    {
        $data = false;
        if ($files != false) {
            $newrow = $KoorLatLng = false;
            foreach ($files as $r) {
                if (file_exists($r->path)) {
                    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($r->path);
                    for ($j = 0; $j < $spreadsheet->getSheetCount(); $j++) {
                        $i = 0;
                        $worksheet = $spreadsheet->getSheet($j)->toArray(null, true, false, true);
                        foreach ($worksheet as $row) {
                            if ($i != 0 and !empty($row['A'])) {
                                if (isset($row['G'])) {
                                    //Convert UTMRef to LatLng
                                    $UTMRef = new PHPCoord\UTMRef($row['G'], $row['H'], 0.0, 'N', 50); //Easting, Northing
                                    $LatLng = $UTMRef->toLatLng();
                                    $KoorLatLng[] = (object)['Lat' => $LatLng->getLat(), 'Lng' => $LatLng->getLng(), 'Lokasi' => $row['C'], 'Type' => $r->type];

                                    $newrow[] = (object)[
                                        'Id' => $row['B'],
                                        'lokasi' => $row['C'],
                                        'alamat' => $row['D'],
                                        'kel' => $row['E'],
                                        'kec' => $row['F'],
                                        'koorX' => $row['G'],
                                        'koorY' => $row['H'],
                                        'koorLat' => $LatLng->getLat(),
                                        'koorLng' => $LatLng->getLng(),
                                    ];
                                }
                            }
                            $i++;
                        }
                    }
                    //print_r($KoorLatLng);
                }
                $data['datas'] = $newrow;
                $data['KoorLatLng'] = $KoorLatLng;
            }
        }

        return $data;
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [$this->_path_js . $this->_controller_name];
        $data['datas'] = ['infrastruktur_sd_2021.xlsx', 'infrastruktur_smp_2021.xlsx'];
        $data['sekolah_url'] = site_url($this->_controller_name . '/sekolah') . '/';
        $data['download_url'] = site_url($this->_controller_name . '/download') . '/';
        $this->load->view($this->_template, $data);
    }

    public function sekolah()
    {
        $data = $this->get_master($this->_path_page . 'sekolah');
        $key = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['files'] = $key;
        $data['scripts'] = [$this->_path_js . $this->_controller_name];
        $data['status_page'] = 'Sekolah';
        $data['detil_url'] = site_url($this->_controller_name . '/detil') . '/';
        $files = $this->_path_file . $key;
        $key = ['fileId' => $key];
        if (strpos($files, 'sd') !== false)
            $datas = $this->datassd($files);
        else
            $datas = $this->datassmp($files);

        $data['datas'] = $datas['row'];
        $data['KoorLatLng'] = $datas['koor'];

        $this->load->view($this->_template, $data);
    }

    public function detil()
    {
        $data = $this->get_master($this->_path_page . 'detil');
        $keys = json_decode($this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key')));
        $data['scripts'] = [$this->_path_js . $this->_controller_name];
        $data['status_page'] = 'Detil Sekolah';
        $files = $this->_path_file . $keys->files;
        $key = ['fileId' => $keys->files];
        if (strpos($files, 'sd') !== false) {
            $data['def'] = $this->definition_key('SD');
            $datas = $this->datassd($files);
        } else {
            $data['def'] = $this->definition_key('SMP');
            $datas = $this->datassmp($files);
        }

        $data['datas'] = $datas['row'][$keys->id];

        $this->load->view($this->_template, $data);
    }

    public function download()
    {
        $this->load->helper('download');
        $key = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        force_download($this->_path_file . $key, NULL);
    }

    private function definition_key($jenis)
    {
        $def['SMP'] = [
            "A13" => "Bebas potensi bahaya",
            "A14" => "Terdapat akses penyelamatan darurat",
            "A15" => "Kemiringan lahan dibawah 15 derajat",
            "A16" => "Tidak dibangun pada sempadan sungai/jalur kereta",
            "A17" => "Tidak terdapat pencemaran air",
            "A18" => "Tidak terdapat pencemaran kebisingan",
            "A19" => "Tidak terdapat pencemaran udara",
            "A20" => "RTRW",
            "A21" => "Kepemilikan lahan",
            "B4" => "Jumlah lantai (1-3)", "B5" => "Jumlah rombel (6-24)",
            "B6" => "Luas lahan (m2)", "B7" => "Luar minimum lahan (m2)",
            "B8" => "Ket. Luas Lahan", "B9" => "Luas bangunan (m2)",
            "B10" => "Luas minimum lantai bangunan (m2)", "B11" => "Ket. luas Bangunan",
            "B13" => "Ketentuan tata bangunan - Koefisien dasar bangunan maks 30%",
            "B14" => "Ketentuan tata bangunan - Koefisien lantan dan ketinggian bangunan sesuai",
            "B15" => "Ketentuan tata bangunan - Jarak bebas bangunan, sempadan, dll",
            "B16" => "Persyaratan keselamatan - Konstruksi yang stabil dan kukuh sampai pembebanan maksimum",
            "B17" => "Persyaratan keselamatan - Terdapat sistem proteksi kebakaran dan petir",
            "B18" => "Persyaratan Kesehatan - Terdapat ventilasi udara dan pencahayaan",
            "B19" => "Persyaratan Kesehatan - Terdapat sanitasi di dalam dan luar bangunan",
            "B20" => "Persyaratan Kesehatan - Bahan bangunan yang digunakan aman bagi pengguna dan lingkungan",
            "B21" => "Terdapat fasilitas dan aksesabilitas yang mudah, aman dan nyaman termasuk penyandang cacat",
            "B22" => "Persyaratan Kenyamanan - Bangunan mampu meredam getaran dan kebisingan yang menggangung kegiatan",
            "B23" => "Persyaratan Kenyamanan - Memiliki pengaturan penghawaan yang baik",
            "B24" => "Persyaratan Kenyamanan - Terdapat lampu penerangan tiap ruangan",
            "B25" => "Persyaratan bangunan bertingkat - Maksimum 3 lantai",
            "B26" => "Persyaratan bangunan bertingkat - Dilengkapi tangga yang mempertimbangkan kemudahan, keamanan, keselamatan dan kesehatan pengguna",
            "B27" => "Sistem Keamanan - Tersedia Peringatan bahaya, pintu keluar dan jalur evakuasi bila terjadi kebakaran dan/atau bencana lain",
            "B28" => "Sistem Keamanan - Terdapat Akses evakuasi yang dapat dicapai dengan mudah dan dilengkapi penunjuk arah yang jelas",
            "B29" => "Instalasi listrik dengan daya minimum 900 watt",
            "B30" => "Kualitas bangunan minimum permanen kelas B,seduai PP 19 2005 pasal 45 dan mengacu pada standar PU",
            "B31" => "Terdapat IMB dan izin penggunaan sesuai ketentuan perundangan yang berlaku",
            "C4" => "Ruang Kelas - Perabot - Kursi peserta didik", "C5" => "Ruang Kelas - Perabot - Meja Peserta didik",
            "C6" => "Ruang Kelas - Perabot - Kursi guru", "C7" => "Ruang Kelas - Perabot - Meja guru",
            "C8" => "Ruang Kelas - Perabot - Lemari", "C9" => "Ruang Kelas - Perabot - Papan panjang",
            "C10" => "Ruang Kelas - Media Pendidikan - Papan Tulis", "C11" => "Ruang Kelas - Perlengkapan Lain - Tempat sampah",
            "C12" => "Ruang Kelas - Perlengkapan Lain - Tempat cuci tangan", "C13" => "Ruang Kelas - Perlengkapan Lain - Jam dinding",
            "C14" => "Ruang Kelas - Perlengkapan Lain - Kotak kontak", "C15" => "Ruang perpustakaan - Buku teks pelajaran",
            "C16" => "Ruang perpustakaan - Buku panduan pendidikan", "C17" => "Ruang perpustakaan - Buku pengayaan",
            "C18" => "Ruang perpustakaan - Buku referensi", "C19" => "Ruang perpustakaan - Sumber belajar lain",
            "C20" => "Ruang perpustakaan - Perabot - Rak buku", "C21" => "Ruang perpustakaan - Perabot - Rak majalah",
            "C22" => "Ruang perpustakaan - Perabot - Rak surat kabar", "C23" => "Ruang perpustakaan - Perabot - Meja baca",
            "C24" => "Ruang perpustakaan - Perabot - Kursi baca", "C25" => "Ruang perpustakaan - Perabot - Kursi kerja",
            "C26" => "Ruang perpustakaan - Perabot - Meja kerja/sirkulasi", "C27" => "Ruang perpustakaan - Perabot - Lemari katalog",
            "C28" => "Ruang perpustakaan - Perabot - Lemari", "C29" => "Ruang perpustakaan - Perabot - Papan pengumuman",
            "C30" => "Ruang perpustakaan - Perabot - Meja multimedia", "C31" => "Ruang perpustakaan - Media Pendidikan - Peralatan multimedia",
            "C32" => "Ruang perpustakaan - Perlengkapan lain - Buku inventaris", "C33" => "Ruang perpustakaan - Perlengkapan lain - Tempat sampah",
            "C34" => "Ruang perpustakaan - Perlengkapan lain - Kotak kontak", "C35" => "Ruang perpustakaan - Perlengkapan lain - Jam dinding",
            "C36" => "Ruang Laboratorium IPA - Perabot - Kursi", "C37" => "Ruang Laboratorium IPA - Perabot - Meja peserta didik",
            "C38" => "Ruang Laboratorium IPA - Perabot - Meja demonstrasi", "C39" => "Ruang Laboratorium IPA - Perabot - Meja Persiapan",
            "C40" => "Ruang Laboratorium IPA - Perabot - Lemari alat", "C41" => "Ruang Laboratorium IPA - Perabot - Lemari bahan",
            "C42" => "Ruang Laboratorium IPA - Perabot - Bak cuci", "C43" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Mistar",
            "C44" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Jangka sorong", "C45" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Timbangan",
            "C46" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Stopwatch", "C47" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Rol Meter",
            "C48" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Termometer 100 C", "C49" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gelas Ukur",
            "C50" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Massa logam", "C51" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Multimeter AC/DC, 10 kilo ohm/volt",
            "C52" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Batang magnet", "C53" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Globe",
            "C54" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Model tata surya", "C55" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Garpu tala",
            "C56" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Bidang miring", "C57" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Dinamometer",
            "C58" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Katrol tetap", "C59" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Katrol bergerak",
            "C60" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Balok kayu", "C61" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Percobaan muai panjang",
            "C62" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Percobaan optik", "C63" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Percobaan rangkaian listrik",
            "C64" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gelas kimia", "C65" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Model molekul sederhana",
            "C66" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Pembakar spiritus", "C67" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Cawan penguapan",
            "C68" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Kaki tiga", "C69" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Plat tetes",
            "C70" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Pipet tetes + karet", "C71" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Mikroskop monokuler",
            "C72" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Kaca pembesar", "C73" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Poster genetika",
            "C74" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Model kerangka manusia", "C75" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Model tubuh manusia",
            "C76" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gambar/model pencernaan manusia", "C77" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gambar/model sistem peredaran darah manusia",
            "C78" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gambar/model sistem pernafasan manusia", "C79" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gambar/model jantung manusia",
            "C80" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gambar/model mata manusia", "C81" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Gambar/model telinga manusia",
            "C82" => "Ruang Laboratorium IPA - Peralatan Pendidikan - Petunjuk percobaan", "C83" => "Ruang Laboratorium IPA - Media Pendidikan - Papan tulis",
            "C84" => "Ruang Laboratorium IPA - Perlengkapan lain - Kotak kontak", "C85" => "Ruang Laboratorium IPA - Perlengkapan lain - Alat pemadam kebakaran",
            "C86" => "Ruang Laboratorium IPA - Perlengkapan lain - Peralatan P3K", "C87" => "Ruang Laboratorium IPA - Perlengkapan lain - Tempat sampah",
            "C88" => "Ruang Laboratorium IPA - Perlengkapan lain - Jam dinding", "C88" => "Ruang Pimpinan - Perabot - Kursi pimpinan", "C53" => "Ruang Pimpinan - Perabot - Meja pimpinan",
            "C89" => "Ruang Pimpinan - Perabot - Kursi dan meja tamu", "C90" => "Ruang Pimpinan - Perabot - Lemari",
            "C91" => "Ruang Pimpinan - Perabot - Papan statistik", "C92" => "Ruang Pimpinan - Perlengkapan lain - Simbol kenegaraan",
            "C93" => "Ruang Pimpinan - Perlengkapan lain - Tempat sampah", "C94" => "Ruang Pimpinan - Perlengkapan lain - Mesin tik/komputer",
            "C95" => "Ruang Pimpinan - Perlengkapan lain - Filling cabinet", "C96" => "Ruang Pimpinan - Perlengkapan lain - Brankas",
            "C97" => "Ruang Pimpinan - Perlengkapan lain - Jam dinding", "C98" => "Ruang Guru - Perabot - Kursi kerja",
            "C99" => "Ruang Guru - Perabot - Meja kerja", "C100" => "Ruang Guru - Perabot - Lemari",
            "C101" => "Ruang Guru - Perabot - Papan statistik", "C102" => "Ruang Guru - Perabot - Papan pengumuman",
            "C103" => "Ruang Guru - Perlengkapan lain - Tempat sampah", "C104" => "Ruang Guru - Perlengkapan lain - Tempat cuci tangan",
            "C105" => "Ruang Guru - Perlengkapan lain - Jam dinding", "C106" => "Ruang Guru - Perlengkapan lain - Penanda waktu",
            "C106" => "Ruang tata usaha - Perabot - Kursi kerja",
            "C107" => "Ruang tata usaha - Perabot - Meja kerja",
            "C108" => "Ruang tata usaha - Perabot - Lemari",
            "C109" => "Ruang tata usaha - Perabot - Papan statistik",
            "C110" => "Ruang tata usaha - Perlengkapan lain - Mesin ketik/ komputer",
            "C111" => "Ruang tata usaha - Perlengkapan lain - Filing cabinet",
            "C112" => "Ruang tata usaha - Perlengkapan lain - Brankas",
            "C113" => "Ruang tata usaha - Perlengkapan lain - Telepon",
            "C114" => "Ruang tata usaha - Perlengkapan lain - Jam dinding",
            "C115" => "Ruang tata usaha - Perlengkapan lain - Kotak kontak",
            "C116" => "Ruang tata usaha - Perlengkapan lain - Penanda waktu",
            "C117" => "Ruang tata usaha - Perlengkapan lain - Tempat sampah",
            "C118" => "Tempat beribadah - Perabot - Lemari/rak",
            "C118" => "Tempat beribadah - Perlengkapan lain - Perlengkapan ibadah",
            "C119" => "Tempat beribadah - Perlengkapan lain - Jam dinding",
            "C120" => "Ruang Konseling - Perabot - Meja kerja",
            "C121" => "Ruang Konseling - Perabot - Kursi kerja",
            "C122" => "Ruang Konseling - Perabot - Kursi tamu",
            "C123" => "Ruang Konseling - Perabot - Lemari",
            "C124" => "Ruang Konseling - Perabot - Papan kegiatan",
            "C125" => "Ruang Konseling - Peralatan konseling - Instrumen konseling",
            "C126" => "Ruang Konseling - Peralatan konseling - Buku sumber",
            "C127" => "Ruang Konseling - Peralatan konseling - Media pengembangan kepribadian",
            "C128" => "Ruang Konseling - Perlengkapan lain - Jam dinding",
            "C129" => "Ruang UKS - Perabot - Lemari", "C130" => "Ruang UKS - Perabot - Meja",
            "C131" => "Ruang UKS - Perabot - Kursi", "C132" => "Ruang UKS - Perlengkapan lain - Catatan kesehatan peserta didik",
            "C133" => "Ruang UKS - Perlengkapan lain - Perlengkapan P3K", "C134" => "Ruang UKS - Perlengkapan lain - Tandu",
            "C135" => "Ruang UKS - Perlengkapan lain - Selimut", "C136" => "Ruang UKS - Perlengkapan lain - Tensimeter",
            "C137" => "Ruang UKS - Perlengkapan lain - Termometer badan", "C138" => "Ruang UKS - Perlengkapan lain - Timbangan badan",
            "C139" => "Ruang UKS - Perlengkapan lain - Pengukur tinggi badan", "C140" => "Ruang UKS - Perlengkapan lain - Tempat sampah",
            "C141" => "Ruang UKS - Perlengkapan lain - Tempat cuci tangan", "C142" => "Ruang UKS - Perlengkapan lain - Jam dinding",
            "C143" => "Ruang organisasi kesiswaan - Perabot - Meja",
            "C144" => "Ruang organisasi kesiswaan - Perabot - Kursi",
            "C145" => "Ruang organisasi kesiswaan - Perabot - Papan tulis",
            "C146" => "Ruang organisasi kesiswaan - Perabot - Lemari",
            "C147" => "Ruang organisasi kesiswaan - Perlengkapan lain - Jam dinding",
            "C148" => "Jamban - Perlengkapan lain - Kloset jongkok", "C149" => "Jamban - Perlengkapan lain - Tempat air",
            "C150" => "Jamban - Perlengkapan lain - Gayung", "C151" => "Jamban - Perlengkapan lain - Gantungan pakaian",
            "C153" => "Jamban - Perlengkapan lain - Tempat sampah", "C154" => "Gudang - Perabot - Lemari",
            "C155" => "Gudang - Perabot - Rak", "C156" => "Ruang sirkulasi",
            "C157" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Tiang bendera", "C158" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Bendera",
            "C159" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan bola voli", "C160" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan sepakbola",
            "C161" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan senam", "C162" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan atletik",
            "C163" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan seni budaya", "C164" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan keterampilan",
            "C165" => "Tempat Bermain/Berolahraga - Perlengkapan lain - Pengeras suara", "C166" => "Tempat Bermain/Berolahraga - Perlengkapan lain - Tape recorder"
            
        ];

        $def['SD'] = [
            "A13" => "Bebas potensi bahaya",
            "A14" => "Terdapat akses penyelamatan darurat",
            "A15" => "Kemiringan lahan dibawah 15 derajat",
            "A16" => "Tidak dibangun pada sempadan sungai/jalur kereta",
            "A17" => "Tidak terdapat pencemaran air",
            "A18" => "Tidak terdapat pencemaran kebisingan",
            "A19" => "Tidak terdapat pencemaran udara",
            "A20" => "RTRW",
            "A21" => "Kepemilikan lahan",
            "B4" => "Jumlah lantai (1-3)", "B5" => "Jumlah rombel (6-24)",
            "B6" => "Luas lahan (m2)", "B7" => "Luar minimum lahan (m2)",
            "B8" => "Ket. Luas Lahan", "B9" => "Luas bangunan (m2)",
            "B10" => "Luas minimum lantai bangunan (m2)", "B11" => "Ket. luas Bangunan",
            "B13" => "Ketentuan tata bangunan - Koefisien dasar bangunan maks 30%",
            "B14" => "Ketentuan tata bangunan - Koefisien lantan dan ketinggian bangunan sesuai",
            "B15" => "Ketentuan tata bangunan - Jarak bebas bangunan, sempadan, dll",
            "B16" => "Persyaratan keselamatan - Konstruksi yang stabil dan kukuh sampai pembebanan maksimum",
            "B17" => "Persyaratan keselamatan - Terdapat sistem proteksi kebakaran dan petir",
            "B18" => "Persyaratan Kesehatan - Terdapat ventilasi udara dan pencahayaan",
            "B19" => "Persyaratan Kesehatan - Terdapat sanitasi di dalam dan luar bangunan",
            "B20" => "Persyaratan Kesehatan - Bahan bangunan yang digunakan aman bagi pengguna dan lingkungan",
            "B21" => "Terdapat fasilitas dan aksesabilitas yang mudah, aman dan nyaman termasuk penyandang cacat",
            "B22" => "Persyaratan Kenyamanan - Bangunan mampu meredam getaran dan kebisingan yang menggangung kegiatan",
            "B23" => "Persyaratan Kenyamanan - Memiliki pengaturan penghawaan yang baik",
            "B24" => "Persyaratan Kenyamanan - Terdapat lampu penerangan tiap ruangan",
            "B25" => "Persyaratan bangunan bertingkat - Maksimum 3 lantai",
            "B26" => "Persyaratan bangunan bertingkat - Dilengkapi tangga yang mempertimbangkan kemudahan, keamanan, keselamatan dan kesehatan pengguna",
            "B27" => "Sistem Keamanan - Tersedia Peringatan bahaya, pintu keluar dan jalur evakuasi bila terjadi kebakaran dan/atau bencana lain",
            "B28" => "Sistem Keamanan - Terdapat Akses evakuasi yang dapat dicapai dengan mudah dan dilengkapi penunjuk arah yang jelas",
            "B29" => "Instalasi listrik dengan daya minimum 900 watt",
            "B30" => "Kualitas bangunan minimum permanen kelas B,seduai PP 19 2005 pasal 45 dan mengacu pada standar PU",
            "B31" => "Terdapat IMB dan izin penggunaan sesuai ketentuan perundangan yang berlaku",
            "C4" => "Ruang Kelas - Perabot - Kursi peserta didik", "C5" => "Ruang Kelas - Perabot - Meja Peserta didik",
            "C6" => "Ruang Kelas - Perabot - Kursi guru", "C7" => "Ruang Kelas - Perabot - Meja guru",
            "C8" => "Ruang Kelas - Perabot - Lemari", "C9" => "Ruang Kelas - Perabot - Rak hasil karya peserta didik",
            "C10" => "Ruang Kelas - Perabot - Papan panjang", "C11" => "Ruang Kelas - Peralatan Pendidikan - Alat peraga",
            "C12" => "Ruang Kelas - Media Pendidikan - Papan tulis", "C13" => "Ruang Kelas - Perlengkapan Lain - Tempat sampah",
            "C14" => "Ruang Kelas - Perlengkapan Lain - Tempat cuci tangan", "C15" => "Ruang Kelas - Perlengkapan Lain - Jam dinding",
            "C16" => "Ruang Kelas - Perlengkapan Lain - Kotak kontak", "C17" => "Ruang perpustakaan - Buku teks pelajaran",
            "C18" => "Ruang perpustakaan - Buku panduan pendidikan", "C19" => "Ruang perpustakaan - Buku pengayaan",
            "C20" => "Ruang perpustakaan - Buku referensi", "C21" => "Ruang perpustakaan - Sumber belajar lain",
            "C22" => "Ruang perpustakaan - Perabot - Rak buku", "C23" => "Ruang perpustakaan - Perabot - Rak majalah",
            "C24" => "Ruang perpustakaan - Perabot - Rak surat kabar", "C25" => "Ruang perpustakaan - Perabot - Meja baca",
            "C26" => "Ruang perpustakaan - Perabot - Kursi baca", "C27" => "Ruang perpustakaan - Perabot - Kursi kerja",
            "C28" => "Ruang perpustakaan - Perabot - Meja kerja/sirkulasi", "C29" => "Ruang perpustakaan - Perabot - Lemari katalog",
            "C30" => "Ruang perpustakaan - Perabot - Lemari", "C31" => "Ruang perpustakaan - Perabot - Papan pengumuman",
            "C32" => "Ruang perpustakaan - Perabot - Meja multimedia", "C33" => "Ruang perpustakaan - Media Pendidikan - Peralatan multimedia",
            "C34" => "Ruang perpustakaan - Perlengkapan lain - Buku inventaris", "C35" => "Ruang perpustakaan - Perlengkapan lain - Tempat sampah",
            "C36" => "Ruang perpustakaan - Perlengkapan lain - Kotak kontak", "C37" => "Ruang perpustakaan - Perlengkapan lain - Jam dinding",
            "C38" => "Laboratorium IPA - Perabot - Lemari", "C39" => "Laboratorium IPA - Peralatan Pendidikan - Model kerangka manusia",
            "C40" => "Laboratorium IPA - Peralatan Pendidikan - Model tubuh manusia", "C41" => "Laboratorium IPA - Peralatan Pendidikan - Globe",
            "C42" => "Laboratorium IPA - Peralatan Pendidikan - Model tata surya", "C43" => "Laboratorium IPA - Peralatan Pendidikan - Kaca pembesar",
            "C44" => "Laboratorium IPA - Peralatan Pendidikan - Cermin datar", "C45" => "Laboratorium IPA - Peralatan Pendidikan - Cermin cekung",
            "C46" => "Laboratorium IPA - Peralatan Pendidikan - Cermin cembung", "C47" => "Laboratorium IPA - Peralatan Pendidikan - Lensa datar",
            "C48" => "Laboratorium IPA - Peralatan Pendidikan - Lensa cekung", "C49" => "Laboratorium IPA - Peralatan Pendidikan - Lensa cembung",
            "C50" => "Laboratorium IPA - Peralatan Pendidikan - Magnet batang", "C51" => "Laboratorium IPA - Peralatan Pendidikan - Poster IPA,terdiri dari: a)metamorfosis, b)hewan langka, c)hewan dilindungi, d)tanaman khas; e)contoh ekosistem, f). Sistem-sistem pernafasan hewan",
            "C52" => "Ruang Pimpinan - Perabot - Kursi pimpinan", "C53" => "Ruang Pimpinan - Perabot - Meja pimpinan",
            "C54" => "Ruang Pimpinan - Perabot - Kursi dan meja tamu", "C55" => "Ruang Pimpinan - Perabot - Lemari",
            "C56" => "Ruang Pimpinan - Perabot - Papan statistik", "C57" => "Ruang Pimpinan - Perlengkapan lain - Simbol kenegaraan",
            "C58" => "Ruang Pimpinan - Perlengkapan lain - Tempat sampah", "C59" => "Ruang Pimpinan - Perlengkapan lain - Mesin tik/komputer",
            "C60" => "Ruang Pimpinan - Perlengkapan lain - Filling cabinet", "C61" => "Ruang Pimpinan - Perlengkapan lain - Brankas",
            "C62" => "Ruang Pimpinan - Perlengkapan lain - Jam dinding", "C63" => "Ruang Guru - Perabot - Kursi kerja",
            "C64" => "Ruang Guru - Perabot - Meja kerja", "C65" => "Ruang Guru - Perabot - Lemari",
            "C66" => "Ruang Guru - Perabot - Papan statistik", "C67" => "Ruang Guru - Perabot - Papan pengumuman",
            "C68" => "Ruang Guru - Perlengkapan lain - Tempat sampah", "C69" => "Ruang Guru - Perlengkapan lain - Tempat cuci tangan",
            "C70" => "Ruang Guru - Perlengkapan lain - Jam dinding", "C71" => "Ruang Guru - Perlengkapan lain - Penanda waktu",
            "C72" => "Tempat beribadah - Perabot - Lemari/Rak", "C73" => "Tempat beribadah - Perlengkapan lain - Perlengkapan ibadah",
            "C74" => "Tempat beribadah - Perlengkapan lain - Jam dinding", "C75" => "Tempat tidur",
            "C76" => "Ruang UKS - Perabot - Lemari", "C77" => "Ruang UKS - Perabot - Meja",
            "C78" => "Ruang UKS - Perabot - Kursi", "C79" => "Ruang UKS - Perlengkapan lain - Catatan kesehatan peserta didik",
            "C80" => "Ruang UKS - Perlengkapan lain - Perlengkapan P3K", "C81" => "Ruang UKS - Perlengkapan lain - Tandu",
            "C82" => "Ruang UKS - Perlengkapan lain - Selimut", "C83" => "Ruang UKS - Perlengkapan lain - Tensimeter",
            "C84" => "Ruang UKS - Perlengkapan lain - Termometer badan", "C85" => "Ruang UKS - Perlengkapan lain - Timbangan badan",
            "C86" => "Ruang UKS - Perlengkapan lain - Pengukur tinggi badan", "C87" => "Ruang UKS - Perlengkapan lain - Tempat sampah",
            "C88" => "Ruang UKS - Perlengkapan lain - Tempat cuci tangan", "C89" => "Ruang UKS - Perlengkapan lain - Jam dinding",
            "C90" => "Jamban - Perlengkapan lain - Kloset jongkok", "C91" => "Jamban - Perlengkapan lain - Tempat air",
            "C92" => "Jamban - Perlengkapan lain - Gayung", "C93" => "Jamban - Perlengkapan lain - Gantungan pakaian",
            "C94" => "Jamban - Perlengkapan lain - Tempat sampah", "C95" => "Gudang - Perabot - Lemari",
            "C96" => "Gudang - Perabot - Rak", "C97" => "Ruang sirkulasi",
            "C98" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Tiang bendera", "C99" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Bendera",
            "C100" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan bola voli", "C101" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan sepakbola",
            "C102" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan senam", "C103" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan atletik",
            "C104" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan seni budaya", "C105" => "Tempat Bermain/Berolahraga - Peralatan Pendidikan - Peralatan keterampilan",
            "C106" => "Tempat Bermain/Berolahraga - Perlengkapan lain - Pengeras suara", "C107" => "Tempat Bermain/Berolahraga - Perlengkapan lain - Tape recorder"
        ];
        return $def[$jenis];
    }
}
